<?php
if (isset($_COOKIE['pseudo'])) // vérification pièces
{

include('../../../../fonctions/connexionbdd.php');
$liste = $bdd->prepare('SELECT coins FROM test_upppj WHERE pseudo= ?') or die(print_r($bdd->errorInfo())); // On récupère les pièces.
$liste->execute(array(htmlspecialchars($_COOKIE['pseudo'])));
$entrees = $liste->fetch();
$liste->closeCursor();
if ($_COOKIE['coins'] != $entrees['coins']) // si les pièces ne sont pas les mêmes (stockées dans le cookie).
{
setcookie("coins", $entrees['coins'], (time() + 666666666));
}

}


if (isset($_GET['img'])) // Création de l'image récapitulative.
{

include('../../../../fonctions/connexionbdd.php');
$liste = $bdd->prepare('SELECT coins FROM test_upppj WHERE pseudo= ?') or die(print_r($bdd->errorInfo())); // On cherche le pseudo.
$liste->execute(array(htmlspecialchars($_GET['img'])));
if ($entrees = $liste->fetch()) // Si le compte existe...
{

header ("Content-type: image/png"); 
$image = imagecreatefrompng("upppj.png");
$couleurpseudo = imagecolorallocate($image, 111, 129, 159);
$couleurscore = imagecolorallocate($image, 247, 200, 89);
imagestring($image, 5, 30, 40, $_GET['img'], $couleurpseudo);
imagestring($image, 5, 242, 40, $entrees['coins'], $couleurscore);
imagepng($image); 

}
else // Sinon on affiche un message d'erreur.
{
header('Location: ?pseudo'); 
}

}

if (isset($_GET['give'])) // Si on donne une pièce.
{

if (isset($_COOKIE['pseudo'])) // si notre compte existe.
{

if ($_GET['give'] == $_COOKIE['pseudo']) // si on essaye de se faire un autodon.
{

header('Location: ?autodon');

}

else // sinon, si le pseudo est différent du cookie du pseudo
{

include('../../../../fonctions/connexionbdd.php');
$liste = $bdd->prepare('SELECT * FROM test_upppj WHERE pseudo= ?') or die(print_r($bdd->errorInfo())); // On cherche le pseudo.
$liste->execute(array(htmlspecialchars($_GET['give'])));
if ($entrees = $liste->fetch()) // Si le compte existe...
{
$coins = $entrees['coins']+1;

$liste = $bdd->prepare('SELECT datebonus FROM test_upppj WHERE pseudo= ?') or die(print_r($bdd->errorInfo())); // On cherche à savoir si la personne a reçu sa pièce journalière.
$liste->execute(array(htmlspecialchars($_GET['give'])));

if ($entrees['datebonus'] == date(Y).'-'.date(m).'-'.date(d)) // Si la date de reçu est la même...
{

header('Location: ?doncompte2');

}
else // Sinon, si la date est différente, alors on vérifie que la personne peut donner sa pièce journalière.
{

$liste = $bdd->prepare('SELECT coinbonus FROM test_upppj WHERE pseudo= ?') or die(print_r($bdd->errorInfo())); // On cherche à savoir si la personne a donné sa pièce journalière.
$liste->execute(array($_COOKIE['pseudo']));
$entrees = $liste->fetch();
	if ($entrees['coinbonus'] == date(Y).'-'.date(m).'-'.date(d)) // Si la date de don est la même...
	{
	
	header('Location: ?dejadon');
	
	}
	
	else // Sinon, si la personne n'a pas déjà donné sa pièce journalière, on donne la pièce et on actualise les dates.
	{


		$req = $bdd->prepare('UPDATE test_upppj SET coins = :coins, datebonus = :datebonus WHERE pseudo = :pseudo'); // on augmente les coins de 1 du receveur.
		$req->execute(array(
		'coins' => $coins,
		'datebonus' => date(Y).'-'.date(m).'-'.date(d),
		'pseudo' => htmlspecialchars($_GET['give']),
		));
		$req = $bdd->prepare('UPDATE test_upppj SET coinbonus = :coinbonus WHERE pseudo = :pseudo'); // on augmente les coins de 1 du receveur.
		$req->execute(array(
		'coinbonus' => date(Y).'-'.date(m).'-'.date(d),
		'pseudo' => htmlspecialchars($_COOKIE['pseudo']),
		));
		$req->closeCursor();
		$liste->closeCursor();
		setcookie("bonus", date(Y).'-'.date(m).'-'.date(d), (time() + 666666666));
		header('Location: ?donegive'); 

	}

}

}
else // Si le compte n'existe pas (don pièce).
{

$liste->closeCursor();
header('Location: ?doncompte');

}

}

}

else // Si nous ne somme pas connecté.
{

header('Location: ?connect'); 

}

}

if (isset($_GET['deco'])) // Si on veut se deconnecter.
{
setcookie("date", "", -1); // On re-définit les cookies.
setcookie("pseudo", "", -1);
setcookie("coin", "", -1);
setcookie("coins", "", -1);
setcookie("bonus", "", -1);
header('Location: ?donedeco'); 
}
if (isset($_GET['recuperer'])) // Si on veut récupérer sa pièce.
{
if ($_POST['verification1'] == $_POST['verification2']){
	include('../../../../fonctions/connexionbdd.php');
	$liste = $bdd->prepare('SELECT * FROM test_upppj WHERE pseudo= ?') or die(print_r($bdd->errorInfo())); // On cherche le pseudo.
	$liste->execute(array($_POST['pseudo']));
	
	if ($entrees = $liste->fetch()) // Si le compte existe...
	{
	
		$mdp = crypt($_POST['mdp'], '$8a$9$gz3r68y198g15fzmfne13$');
		$liste = $bdd->prepare('SELECT * FROM test_upppj WHERE pseudo= :pseudo AND mdp= :mdp') or die(print_r($bdd->errorInfo()));
		$liste->execute(array(
		'pseudo' => htmlspecialchars($_POST['pseudo']),
		'mdp' => $mdp,));
		
		if ($entrees = $liste->fetch()) // Si le mot de passe correspond...
		{
		
			if($entrees['date'] == date(Y).'-'.date(m).'-'.date(d)) // Si la date est la même qu'aujourd'hui...
			{
			
			setcookie("date", date(Y).'-'.date(m).'-'.date(d), (time() + 666666666)); // On re-définit les cookies.
			setcookie("pseudo", htmlspecialchars($entrees['pseudo']), (time() + 666666666));
			setcookie("coins", $entrees['coins'], (time() + 666666666));
			setcookie("bonus", $entrees['coinbonus'], (time() + 666666666));
			header('Location: http://l3m.in/p/projets/tests/upppj/upppj.php'); 
			
			}
			else // Si la date est différente (càd que le joueur n'a pas validé sa pièce journalière).
			{
			
			$coins = $entrees['coins']+1; // on récupère la valeur des pièces qu'il a, et on ajoute 1.
			include('../../../../fonctions/connexionbdd.php');
			$req = $bdd->prepare('UPDATE test_upppj SET coins = :coins, date = :date WHERE pseudo = :pseudo'); // on augmente les coins de 1.
			$req->execute(array(
			'coins' => $coins,
			'date' => date(Y).'-'.date(m).'-'.date(d),
			'pseudo' => htmlspecialchars($_POST['pseudo']),
			));
			setcookie("date", date(Y).'-'.date(m).'-'.date(d), (time() + 666666666)); // On re-définit les cookies.
			setcookie("pseudo", $_POST['pseudo'], (time() + 666666666));
			setcookie("coins", $coins, (time() + 666666666));
			setcookie("bonus", $entrees['coinbonus'], (time() + 666666666));
			$req->closeCursor();
			header('Location: ?done'); 
			}
		
		}
		else // si le mdp ne correspond pas.
		{
		$liste->closeCursor();
		header('Location: ?mdp'); 
		}
		$liste->closeCursor();
	}
	else // si le compte (pseudo) n'existe pas.
	{
	
	include('../../../../fonctions/connexionbdd.php');
$mdp = crypt($_POST['mdp'], '$8a$09$1egv8dso9jmwh3y3cdoac$');
$req = $bdd->prepare('INSERT INTO test_upppj(pseudo, mdp, date, coins) VALUES(:pseudo, :mdp, NOW(), :coins)'); // On ajoute le nouveau joueur.
$req->execute(array(
    'pseudo' => htmlspecialchars($_POST['pseudo']),
    'mdp' => $mdp,
    'coins' => 1,
    ));
	$req->closeCursor();
	setcookie("date", date(Y).'-'.date(m).'-'.date(d), (time() + 666666666)); // On re-définit les cookies.
	setcookie("pseudo", $_POST['pseudo'], (time() + 666666666));
	setcookie("coins", 1, (time() + 666666666));
	header('Location: ?doneaccount'); 
	
	}

}
else // Si le code de vérification est faux.
{
header('Location: ?code'); 
}
}
?>
<!DOCTYPE html>
<html>
   <head>
        <title>Test #8 - Une petite pièce par jour ?</title>
        <meta charset="utf-8">
		<link href='http://fonts.googleapis.com/css?family=Lato|Coming+Soon' rel='stylesheet' type='text/css'>
        <style type="text/css">
	body{max-width: 850px; min-width: 450px; margin: auto; font-family: 'Lato', sans-serif; background-image: url('bg_u.png'); color: #364245;}
	section{padding: 45px 15px 10px 15px; margin: -40px auto 15px auto; background-image: url('bg_u2.png'); border-radius: 0 0 15px 15px; box-shadow: 0 0 3px 2px #D6CDE7 inset;}
	h1{font-family: 'Coming Soon', cursive; margin-left: 42px; text-shadow: 0 5px 3px #7C97A0; padding: 0 0 5px 0; transition: padding 0.4s, text-shadow 0.4s;}
	h1:hover{padding: 5px 0 0 0; text-shadow: 0 2px 1px #7C97A0; transition: padding 0.4s, text-shadow 0.4s;}
	h1 a {color: #3F5367;}
	p{background-image: url('bg_u.png'); box-shadow: 0 0 3px 2px #E9E6ED; padding: 7px; border-radius: 8px;}
	p span{color: #566F87; text-shadow: 0 1px 1px #8399AF;}
	.coin{width: 17px; height: 17px; background-image: url('coin.gif'); display: inline-block;}
	.coins{width: 17px; height: 17px; background-image: url('coin.png'); display: inline-block;}
	.deco{width: 17px; height: 17px; background-image: url('deco.png'); display: inline-block; opacity: 0.3; transition: opacity 0.3s;}
	.deco:hover{opacity: 1; transition: opacity 0.3s;}
	a{text-decoration: none; color: #62819F; transition: color 0.3s;}
	a:hover{color: #364245; transition: color 0.3s;}
		</style>
    </head>
<body>
<section>
<h1><a href="http://l3m.in/p/projets/tests/upppj/upppj.php">Une petite pièce par jour ?</a></h1>
<?php if(isset($_COOKIE['pseudo'])) { // si_cookie_connexion. ?>
<p style="text-align: right;"><span>
<a href="?img=<?php echo $_COOKIE['pseudo']; ?>" title="Accès à votre image récapitulative."><img src="img.png" alt="image récapitulative" /></a> 
<?php echo $_COOKIE['pseudo'] .'</span> &bull; <span>'. $_COOKIE['coins'] .'</span> '; ?> <span class="coin"> </span>
<?php if ($_COOKIE['date'] == date(Y).'-'.date(m).'-'.date(d)) { ?> &bull; 
Vous avez récupéré votre pièce journalière.<?php } else { ?>
Vous n'avez pas encore récupéré votre pièce jounalière. <a href="http://l3m.in/p/projets/tests/upppj/upppj.php?piece">La récupérer</a> !<?php } ?> &bull; 
<?php if (isset($_COOKIE['bonus']) && $_COOKIE['bonus'] == date(Y).'-'.date(m).'-'.date(d)) { ?>
<img src="bonus.png" title="vous avez donné votre pièce bonus aujourd'hui" alt="bonus donné" /><?php } else { 
?> <img src="boonus.png" title="vous n'avez pas donné votre pièce bonus aujourd'hui" alt="bonus non donné" /> <?php }
try{$bdd = new PDO('mysql:host=;dbname=', '', '');} // on se connecte à la bdd.
catch(Exception $e){die('Erreur : '.$e->getMessage());}
$liste = $bdd->prepare('SELECT datebonus FROM test_upppj WHERE pseudo= ?') or die(print_r($bdd->errorInfo())); // On cherche à savoir si la personne a reçu sa pièce journalière.
$liste->execute(array(htmlspecialchars($_COOKIE['pseudo'])));
$entrees = $liste->fetch();
$liste->closeCursor();
if ($entrees['datebonus'] == date(Y).'-'.date(m).'-'.date(d)) { 
?>&bull; <img src="bonusrecu.png" alt="bonus reçu aujourd'hui" title="vous avez reçu votre pièce bonus journalière" /><?php }
else
{ ?>&bull; <img src="bonusrecuu.png" alt="bonus non reçu aujourd'hui" title="vous n'avez pas encore reçu votre pièce bonus journalière" /><?php } ?> &bull;
<a href="?deco"><span title="Cliquez ici pour vous déconnecter." class="deco"> </span></a></p>
<?php
} // Fin si_cookie_connexion.

if($_COOKIE["post"] == date(Y).'-'.date(m).'-'.date(d)) { ?>
<p>Vous avez déjà gagné votre pièce aujourd'hui. Que voulez-vous faire de plus ?</p>
<?php } elseif (isset($_GET['connect'])) { ?>
<p>Vous n'êtes pas connecté sur Upppj. Pour pouvoir donner une pièce il faut que vous possédiez un compte (et que vous soyez connecté dessus).<br />
<a href="http://l3m.in/p/projets/tests/upppj/upppj.php">Retour à l'accueil</a>.</p>
<?php } elseif (isset($_GET['autodon'])) { ?>
<p>Vous essayez de vous donner une pièce à vous-même ? Ce n'est pas bien (et ça ne marche pas) :c<br />
<a href="http://l3m.in/p/projets/tests/upppj/upppj.php">Retour à l'accueil</a>.</p>
<?php } elseif (isset($_GET['donegive'])) { ?>
<p>La pièce a été donnée à cette personne :)<br />
<a href="http://l3m.in/p/projets/tests/upppj/upppj.php">Retour à l'accueil</a>.</p>
<?php } elseif (isset($_GET['doncompte'])) { ?>
<p>Ce compte n'existe pas. Impossible d'y donner une pièce.<br />
<a href="http://l3m.in/p/projets/tests/upppj/upppj.php">Retour à l'accueil</a>.</p>
<?php } elseif (isset($_GET['doncompte2'])) { ?>
<p>Ce compte a déjà reçu une pièce aujourd'hui, réessayez demain.<br />
<a href="http://l3m.in/p/projets/tests/upppj/upppj.php">Retour à l'accueil</a>.</p>
<?php } elseif (isset($_GET['dejadon'])) { ?>
<p>Vous avez déjà donné votre pièce journalière. Réessayez demain :)<br />
<a href="http://l3m.in/p/projets/tests/upppj/upppj.php">Retour à l'accueil</a>.</p>
<?php } elseif (isset($_GET['code'])) { ?>
<p>Le code que vous avez entré est faux.<br />
<a href="http://l3m.in/p/projets/tests/upppj/upppj.php">Retour à l'accueil</a>.</p>
<?php } elseif (isset($_GET['pseudo'])) { ?>
<p>Ce compte n'existe pas. Faites bien attention aux majuscules et aux minuscules.<br />
<a href="http://l3m.in/p/projets/tests/upppj/upppj.php">Retour à l'accueil</a>.</p>
<?php } elseif (isset($_GET['done'])) { ?>
<p>Vous avez gagné une pièce ! Vous avez <?php echo $_COOKIE['coins']; ?> pièces, <?php echo $_COOKIE['pseudo']; ?>.<br />
<a href="http://l3m.in/p/projets/tests/upppj/upppj.php">Retour à l'accueil</a>.</p>
<?php } elseif (isset($_GET['doneaccount'])) { ?>
<p>Votre inscription est terminée, <?php echo $_COOKIE['pseudo']; ?>. Vous venez également de gagner votre première pièce !<br />
<a href="http://l3m.in/p/projets/tests/upppj/upppj.php">Retour à l'accueil</a>.</p>
<?php } elseif (isset($_GET['piece'])) { ?>
<p>Afin de récupérer votre pièce, merci de rentrer votre pseudo, votre mot de passe et la vérification anti-bot.
(si c'est la première fois que vous vous connectez, votre compte sera créé automatiquement).</p>
<form method="post" action="?recuperer"><p>
<input type="text" name="pseudo" maxlength="20" required /> &bull; <input type="password" name="mdp" maxlength="666" required />
<?php $verif1 = rand(1,10); $verif2 = rand(1,10); ?><input type="hidden" name="verification1" value=<?php echo $verif1 + $verif2; ?> />
 &bull; <?php echo $verif1 ." + ". $verif2; ?> = <input class="input" type="text" name="verification2" maxlength="2" required /> &bull; 
<input type="submit" value="Valider." />
</p></form>
<?php } elseif (isset($_GET['donedeco'])) { ?>
<p>Vous avec été déconnecté avec succès.<br />
<a href="http://l3m.in/p/projets/tests/upppj/upppj.php">Retour à l'accueil</a>.</p>
<?php } elseif (isset($_GET['rank'])) { ?>
<p>Vous vous trouvez sur le classement de ce jeu.<br />
<a href="http://l3m.in/p/projets/tests/upppj/upppj.php">Retour à l'accueil</a>.</p>
<p style="font-size: 0.85em;">
<?php try
{$bdd = new PDO('mysql:host=;dbname=', '', '');} // on se connecte à la bdd.
catch(Exception $e)
{die('Erreur : '.$e->getMessage());}
$req = $bdd->prepare('SELECT * FROM test_upppj ORDER BY coins DESC'); // On sélectionne les gens selon leurs pièces.
$req->execute(); // On lance la requête.
while ($donnees = $req->fetch())
{
$i++; ?>
<span><?php echo $i; ?></span> &bull; <?php echo $donnees['pseudo']; ?> - <?php echo $donnees['coins']; ?> <span class="coins"> </span> 
&bull; <a href="?img=<?php echo $donnees['pseudo']; ?>"><img src="img.png" alt="image récapitulative" /></a>
<?php if (isset($_COOKIE['bonus']) && $_COOKIE['bonus'] == date(Y).'-'.date(m).'-'.date(d)) { echo "."; } 
else { ?> - <a style="opacity: 0.5;" href="?give=<?php echo $donnees['pseudo']; ?>">lui offrir une pièce</a>.<?php } ?><br />
<?php }
$req->closeCursor();
echo "</p>"; } elseif (isset($_GET['mdp'])) { ?>
<p>Le mot de passe ne correspond pas à celui qui est entré dans la bdd pour ce pseudonyme.<br />
<a href="http://l3m.in/p/projets/tests/upppj/upppj.php?piece">Réessayer</a>.</p>
<?php } else {
if(isset($_COOKIE['pseudo'])) { ?>
<p><span>Rendez-vous demain :)</span></p>
<?php } else { ?>
<p><span>Upppj?</span> (à prononcer <span>upji</span>, diminutif de <span>UnePetitePièceParJour?</span>) est un test de sodimel.<br />
Vous pouvez gagner une pièce par jour. Et pour l'instant c'est déjà pas mal.<br /><br />
<a href="?piece">Récupérer ma pièce</a>.</p>
<?php } } ?>

<p style="opacity: 0.9; text-align: right; font-size: 0.8em;">
Un <span><a href="http://l3m.in/p/projets/tests/">test</a></span> de <span><a href="http://l3m.in/">sodimel</a></span>. En version 0.11.<br />
Date du serveur : <?php echo date(d).'/'.date(m).'/'.date(Y); ?>, heure du serveur : <?php echo date(H).':'.date(i).':'.date(s); ?>.
</p>

<p style="opacity: 0.7;">
<?php
include('../../../../fonctions/connexionbdd.php');
$req = $bdd->prepare('SELECT id FROM test_upppj ORDER BY id DESC LIMIT 1'); // On sélectionne l'id du dernier insctit.
$req->execute(); // On lance la requête.
if ($donnees = $req->fetch())
{
$rep = $bdd->prepare('SELECT SUM(coins) FROM test_upppj;'); // On compte le total des coins distribués.
$rep->execute(); // On lance la requête.
if ($coins = $rep->fetch())
{

echo "Il y a ". $donnees['id'] ." personnes qui ont commencé à ramasser des pièces, et ". $coins['SUM(coins)'] ." <span class=\"coins\"> </span> pièces ont été ramassées au total.<br />";
$random = rand(1, $donnees['id']); // On choisit un membre aléatoirement.
$req = $bdd->prepare('SELECT * FROM test_upppj WHERE id = ?');
$req->execute(array($random));
if ($donnees = $req->fetch())
{
echo "<a href=\"http://l3m.in/p/projets/tests/upppj/upppj.php?rank\">Classement</a> 
&bull; Un membre au hasard : <span><a href=\"?img=". $donnees['pseudo'] ."\" title=\"Cliquez sur ce pseudonyme pour accéder à son image récapitulative.\">". $donnees['pseudo']
 ."</a></span> (possède ". $donnees['coins'] ." <span class=\"coins\"> </span>, ". $donnees['id'];
 if ($donnees['id'] == 1) { echo "<sup>er</sup> inscrit) \o/ "; } else { echo "<sup>ème</sup> inscrit) "; }
if (isset($_COOKIE['bonus']) && $_COOKIE['bonus'] == date(Y).'-'.date(m).'-'.date(d)) { echo "."; } 
else {
echo "- <a style=\"opacity: 0.5;\" href=\"?give=".$donnees['pseudo']."\">lui offrir une pièce</a>.";
}
}
}
$rep->closeCursor();
}
$req->closeCursor();
?>
</p>
</section>
</body>
</html>