<?php
session_start();
include('../../../../config.php');
include('fonctions.php');
verif_bdd();
verif_grille();
verif_classement($_SESSION['id']);

if(isset($_GET['inscription']) && isset($_POST['pseudo'])){
	inscrire($_POST['pseudo']);
}

if(isset($_SESSION['id']) && verif_joueur($_SESSION['id'])){
	maj_temps($_SESSION['id']);
	$connecte = true;
}

if(isset($_GET['d']) && isset($connecte) && $connecte == true){
	deplacer_joueur($_GET['d']);
}

?>


<!DOCTYPE html>
<html>
   <head>
        <title>Deplacement</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="design.css" />
        <meta name="viewport" content="width=device-width" />
        <script src="../../../../analytics.js" type="text/javascript"></script>
    </head>
<body>
 <h1>Déplacement</h1>
<?php if(isset($_GET['erreur'])){ ?>

	<p>Il y a eu une erreur.</p>

<?php } if(isset($_GET['aide'])){ ?>

	<h3>Aide du jeu :</h3>
	<p>&bull; Vous vous choisissez un pseudonyme, puis vous commencez le jeu.<br />
	&bull; Vous êtes le <span class="joueur">O</span>, les autres joueurs sont les <span class="autrejoueur">o</span>.<br />
	&bull; Il y a (pour l'instant) deux types de bonus : les <span class="bonuspoints">&starf;</span>, qui vous rapportent des points et de l'xp, et les <span class="bonus">&there4;</span>, qui peuvent augmenter (ou réduire) votre attaque max, 
	votre défense max, votre vie, ou bien vous donner des déplacements longs (5 cases), des déplacements très longs (10 cases) ou bien vous rendre invisible aux yeux des autres joueurs pendant 20 tours.<br />
	&bull; A chaque fois que vous tombez sur la même case qu'un bonus, vous le "validez", prenez ses effets et il réapparait autre part sur la carte.<br />
	&bull; Il y a 150 <span class="bonuspoints">&starf;</span> entre [-50;-50] et [50;50], 100 <span class="bonus">&there4;</span><sup>depl5</sup> entre [-50;-50] et [50;50], 75 
	<span class="bonus">&there4;</span><sup>depl10</sup> entre [-75;-75] et [75;75].<br />
	&bull; Passer la souris sur les pseudos de la liste des membres en bas de page vous permet de voir dans combien de temps ils seront déconnectés.</p>

	<p><a href="http://l3m.in/p/projets/tests/deplacement/">Retour en jeu</a></p>

<?php } elseif(isset($_GET['classement'])){ ?>

	<h3>Classement</h3>

<?php afficher_classement(); ?>

	<p><a href="http://l3m.in/p/projets/tests/deplacement/">Retour en jeu</a></p>

<?php } elseif(isset($connecte) && $connecte == true){ ?>

	<p>
	<?php afficher_stats_joueur(); ?>
	</p>
	<p>
	<?php afficher_carte($_SESSION['id']); ?>
	</p>

	<p>
	<?php afficher_liste_joueurs(); ?>
	</p>

	<p>
	<a href="?classement">Classement</a>
	</p>

<?php } else{ ?>

	
	<p>Les données ne sont pas sauvegardées plus d'une heure après votre dernière actualisation. Les données enregistrées (temporairement) : adresse ip, pseudonyme, score, position x, position y.</p>

	<form method="post" action="?inscription">
	<p>
		Pseudonyme : <input type="text" name="pseudo" maxlength="42" required /> <br />
		<input type="submit" value="Jouer" />
	</p>
	</form>

	<p class="petit">Un projet de <a href="http://twinoid.com/user/12661">sodimel</a> (<a href="http://l3m.in/">l3m website</a>) - merci à <a href="http://www.css3create.com/Infobulle-avec-effet-de-transparence-en-CSS">css3create.com</a>.</p>

	<?php } ?>	
</body>
</html>