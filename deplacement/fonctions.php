<?php

function verif_bdd(){
    include('../../../../fonctions/connexionbdd.php');
    $desinscrire = $bdd->prepare('DELETE FROM deplacement_joueurs_test WHERE temps < :temps') or die(print_r($bdd->errorInfo()));
    $desinscrire->execute(array('temps' => time()));
}

function verif_grille(){
    include('../../../../fonctions/connexionbdd.php');
    verif_grille_chercher_bonus(1, 150, 50);
    verif_grille_chercher_bonus(2, 100, 50);
    verif_grille_chercher_bonus(3, 50, 75);
}

function verif_grille_chercher_bonus($type, $nombre_mini, $coord){
    include('../../../../fonctions/connexionbdd.php');
    $chercher_nombre = $bdd->query('SELECT COUNT(type) FROM deplacement_test WHERE type = '. $type .'');
    $nombre_bonus = $chercher_nombre->fetchColumn();

    //echo $nombre_bonus ."/". $nombre_mini ." ";

    while($nombre_bonus < $nombre_mini){
        if($type == 1){
            $score_bonus = mt_rand(1,15);
        }
        else{
            $score_bonus = 0;
        }
        $coordmoins = 0 - $coord;
        $x = mt_rand($coordmoins,$coord);
        $y = mt_rand($coordmoins,$coord);
        $inscription_bonus = $bdd->prepare('INSERT INTO deplacement_test(x, y, score, type) VALUES(:x, :y, :score, :type)');
        $inscription_bonus->execute(array('x' => $x, 'y' => $y, 'score' => $score_bonus, 'type' => $type));
        $nombre_bonus++;
    }

    $chercher_bonus = $bdd->prepare('SELECT * FROM deplacement_test WHERE x = :x AND y = :y AND type = :type') or die(print_r($bdd->errorInfo()));
    $chercher_bonus->execute(array('x' => $_SESSION['x'], 'y' => $_SESSION['y'], 'type' => $type));

    if ($donnees = $chercher_bonus->fetch()){
        if($type == 1){

                $_SESSION['score'] += $donnees['score'];
                $donner_bonus_star = $bdd->prepare('UPDATE deplacement_joueurs_test SET score = :score WHERE id = :id');
                $donner_bonus_star->execute(array('score' => $_SESSION['score'], 'id' => $_SESSION['id']));

                $virer_bonus_star = $bdd->prepare('DELETE FROM deplacement_test WHERE x = :x AND y = :y AND type = :type') or die(print_r($bdd->errorInfo()));
                $virer_bonus_star->execute(array('x' => $_SESSION['x'], 'y' => $_SESSION['y'], 'type' => $type));
                $score = mt_rand(1,15);
                $x = mt_rand(-$coord,$coord);
                $y = mt_rand(-$coord,$coord);
                $inscription_star = $bdd->prepare('INSERT INTO  deplacement_test(x, y, score, type) VALUES(:x, :y, :score, 2)');
                $inscription_star->execute(array('x' => $x, 'y' => $y, 'score' => $score));
                header('Location:http://l3m.in/p/projets/tests/deplacement/');
        }
        if($type == 2){

                $_SESSION['dep5'] ++;
                $donner_bonus_dep5 = $bdd->prepare('UPDATE deplacement_joueurs_test SET dep5 = :dep5 WHERE id = :id');
                $donner_bonus_dep5->execute(array('dep5' => $_SESSION['dep5'], 'id' => $_SESSION['id']));

                $virer_bonus_dep5 = $bdd->prepare('DELETE FROM deplacement_test WHERE x = :x AND y = :y AND type = 2') or die(print_r($bdd->errorInfo()));
                $virer_bonus_dep5->execute(array('x' => $_SESSION['x'], 'y' => $_SESSION['y']));
                $x = mt_rand(-$coord,$coord);
                $y = mt_rand(-$coord,$coord);
                $inscription_bonus_dep5 = $bdd->prepare('INSERT INTO  deplacement_test(x, y, score, type) VALUES(:x, :y, 0, 2)');
                $inscription_bonus_dep5->execute(array('x' => $x, 'y' => $y));
                header('Location:http://l3m.in/p/projets/tests/deplacement/');
        }
        if($type == 3){

                $_SESSION['dep10'] ++;
                $donner_bonus_dep10 = $bdd->prepare('UPDATE deplacement_joueurs_test SET dep10 = :dep10 WHERE id = :id');
                $donner_bonus_dep10->execute(array('dep10' => $_SESSION['dep10'], 'id' => $_SESSION['id']));

                $virer_bonus_dep10 = $bdd->prepare('DELETE FROM deplacement_test WHERE x = :x AND y = :y AND type = 3') or die(print_r($bdd->errorInfo()));
                $virer_bonus_dep10->execute(array('x' => $_SESSION['x'], 'y' => $_SESSION['y']));
                $x = mt_rand(-$coord,$coord);
                $y = mt_rand(-$coord,$coord);
                $inscription_bonus_dep10 = $bdd->prepare('INSERT INTO  deplacement_test(x, y, score, type) VALUES(:x, :y, 0, 3)');
                $inscription_bonus_dep10->execute(array('x' => $x, 'y' => $y));
                header('Location:http://l3m.in/p/projets/tests/deplacement/');
        }
    }
}

function verif_joueur($id){
	include('../../../../fonctions/connexionbdd.php');
	$chercherid = $bdd->prepare('SELECT * FROM deplacement_joueurs_test WHERE id = :id') or die(print_r($bdd->errorInfo()));
    $chercherid->execute(array('id' => $id));
    if ($donnees = $chercherid->fetch()){
    	if($_SESSION['id'] == $donnees['id'] && $_SESSION['ip'] == $donnees['ip'] && $_SESSION['points'] == $donnees['points'] && $_SESSION['x'] == $donnees['x'] && $_SESSION['y'] == $donnees['y']){
    		return true;
    	}
    	return false;
    }
    return false;
}

function verif_classement($id){
    if(isset($_SESSION['id'])){
        include('../../../../fonctions/connexionbdd.php');
        $chercherscores = $bdd->prepare('SELECT * FROM deplacement_classement_test ORDER BY score') or die(print_r($bdd->errorInfo()));
        $chercherscores->execute();
        if($score = $chercherscores->fetch()){
            $pireid_profil = $score['id_profil'];
            $pireid = $score['id'];
            $pirescore = $score['score'];
        }
        $chercherscoresid = $bdd->prepare('SELECT score, id FROM deplacement_classement_test WHERE id_profil = :id') or die(print_r($bdd->errorInfo()));
        $chercherscoresid->execute(array('id' => $_SESSION['id']));
        if($donnees = $chercherscoresid -> fetch()){
            if($_SESSION['score'] > $donnes['score']){
                $inscription_score = $bdd->prepare('INSERT INTO  deplacement_classement_test(id_profil, pseudo, score, date) VALUES(:id_profil, :pseudo, :score, :temps)');
                $inscription_score->execute(array('id_profil' => $_SESSION['id'], 'pseudo' => $_SESSION['pseudo'], 'score' => $_SESSION['score'], 'temps' => time()));

                $virer_score = $bdd->prepare('DELETE FROM deplacement_classement_test WHERE score = :pirescore AND id = :pireid') or die(print_r($bdd->errorInfo()));
                $virer_score->execute(array('pirescore' => $donnees['score'], 'pireid' => $donnees['id']));
            }
        }
        else{
            if($_SESSION['score'] > $pirescore){
                $inscription_score = $bdd->prepare('INSERT INTO  deplacement_classement_test(id_profil, pseudo, score, date) VALUES(:id_profil, :pseudo, :score, :temps)');
                $inscription_score->execute(array('id_profil' => $_SESSION['id'], 'pseudo' => $_SESSION['pseudo'], 'score' => $_SESSION['score'], 'temps' => time()));

                $virer_score = $bdd->prepare('DELETE FROM deplacement_classement_test WHERE score = :pirescore AND id = :pireid') or die(print_r($bdd->errorInfo()));
                $virer_score->execute(array('pirescore' => $pirescore, 'pireid' => $pireid));
            }
        }
    }
}

// ----

function inscrire($pseudo){
	include('../../../../fonctions/connexionbdd.php');
	$id = mt_rand(10000000,99999999);
	$id .= substr(time(), 6);
	$pseudo = htmlspecialchars(substr($pseudo, 0, 42));
	$x = mt_rand(-15,15);
	$y = mt_rand(-15,15);
	$temps = time()+3600;
	$score = 0;
	$ip = $_SERVER["REMOTE_ADDR"];

	$inscriptionbdd = $bdd->prepare('INSERT INTO  deplacement_joueurs_test(id, x, y, pseudo, temps, score, ip) VALUES(:id, :x, :y, :pseudo, :temps, :score, :ip)');
    $inscriptionbdd->execute(array('id' => $id,
	    					 	   'x' => $x,
	    						   'y' => $y,
	   							   'pseudo' => $pseudo,
	 							   'temps' => $temps,
	 							   'score' => $score,
	 							   'ip' => $ip));
    $inscriptionbdd->closeCursor();

    $_SESSION['id'] = $id;
    $_SESSION['pseudo'] = $pseudo;
    $_SESSION['x'] = $x;
    $_SESSION['y'] = $y;
    $_SESSION['temps'] = $temps;
    $_SESSION['score'] = $score;
    $_SESSION['ip'] = $ip;
    $_SESSION['dep5'] = 0;
    $_SESSION['dep10'] = 0;
    $_SESSION['invi'] = 0;
    $_SESSION['xp'] = 0;
    $_SESSION['vie'] = 10;
    $_SESSION['min-atk'] = 1;
    $_SESSION['max-atk'] = 3;
    $_SESSION['min-def'] = 0;
    $_SESSION['max-def'] = 3;

    header('Location:http://l3m.in/p/projets/tests/deplacement/');

}

function maj_temps($id){
	include('../../../../fonctions/connexionbdd.php');
	$_SESSION['temps'] = time()+3600;
	$modiftemps = $bdd->prepare('UPDATE deplacement_joueurs_test SET temps = :temps where id = :id') or die(print_r($bdd->errorInfo()));
    $modiftemps->execute(array('temps' => $_SESSION['temps'],
    						  'id' => $id));
    $modiftemps->closeCursor();
}

function deplacer_joueur($direction){
    include('../../../../fonctions/connexionbdd.php');

    $chercherjoueur = $bdd->prepare('SELECT * FROM deplacement_joueurs_test WHERE x = :x AND y = :y') or die(print_r($bdd->errorInfo()));
    $chercherjoueur->execute(array('x' => $_SESSION['x'], 'y' => $_SESSION['y']));
    if($donnees = $chercherjoueur->fetch()){
        switch ($direction) {
        case "bas":
            $_SESSION['x']++;
            $req = $bdd->prepare('UPDATE deplacement_joueurs_test SET x = :x WHERE pseudo = :pseudo AND id = :id');
            $req->execute(array('x' => $_SESSION['x'], 'pseudo' => $_SESSION['pseudo'], 'id' => $_SESSION['id']));
            header('Location:http://l3m.in/p/projets/tests/deplacement/');
            break;
        case "haut":
            $_SESSION['x']--;
            $req = $bdd->prepare('UPDATE deplacement_joueurs_test SET x = :x WHERE pseudo = :pseudo AND id = :id');
            $req->execute(array('x' => $_SESSION['x'], 'pseudo' => $_SESSION['pseudo'], 'id' => $_SESSION['id']));
            header('Location:http://l3m.in/p/projets/tests/deplacement/');
            break;
        case "gauche":
            $_SESSION['y']--;
            $req = $bdd->prepare('UPDATE deplacement_joueurs_test SET y = :y WHERE pseudo = :pseudo AND id = :id');
            $req->execute(array('y' => $_SESSION['y'], 'pseudo' => $_SESSION['pseudo'], 'id' => $_SESSION['id']));
            header('Location:http://l3m.in/p/projets/tests/deplacement/');
            break;
        case "droite":
            $_SESSION['y']++;
            $req = $bdd->prepare('UPDATE deplacement_joueurs_test SET y = :y WHERE pseudo = :pseudo AND id = :id');
            $req->execute(array('y' => $_SESSION['y'], 'pseudo' => $_SESSION['pseudo'], 'id' => $_SESSION['id']));
            header('Location:http://l3m.in/p/projets/tests/deplacement/');
            break;
        default:
            header('Location:http://l3m.in/p/projets/tests/deplacement/?erreur');
            break;
    }
}
}

// ----

function afficher_carte($id){
	include('../../../../fonctions/connexionbdd.php');
	$x1 = $_SESSION['x'] - 5;
	$x2 = $_SESSION['x'] + 5;
	$y1 = $_SESSION['y'] - 5;
	$y2 = $_SESSION['y'] + 5;
	$compteur = 0;

	$chercherjoueurs = $bdd->prepare('SELECT * FROM deplacement_joueurs_test WHERE x >= :x1 AND x <= :x2 AND y >= :y1 AND y <= :y2') or die(print_r($bdd->errorInfo()));
    $chercherjoueurs->execute(array('x1' => $x1, 'x2' => $x2, 'y1' => $y1, 'y2' => $y2));
    while ($donnees = $chercherjoueurs->fetch()){
    	$compteur++;

    	$pseudo[$compteur] = $donnees['pseudo'];
    	$x[$compteur] = $donnees['x'];
    	$y[$compteur] = $donnees['y'];
        $id[$compteur] = $donnees['id'];
    	$score[$compteur] = $donnees['score'];
    }

    $chercherbonus = $bdd->prepare('SELECT * FROM deplacement_test WHERE x >= :x1 AND x <= :x2 AND y >= :y1 AND y <= :y2') or die(print_r($bdd->errorInfo()));
    $chercherbonus->execute(array('x1' => $x1, 'x2' => $x2, 'y1' => $y1, 'y2' => $y2));
    while ($donnees = $chercherbonus->fetch()){
        $compteur++;

        $bonus_type[$compteur] = $donnees['type'];
        $bonus_x[$compteur] = $donnees['x'];
        $bonus_y[$compteur] = $donnees['y'];
        $bonus_score[$compteur] = $donnees['score'];
    }

    for($i = $x1+1; $i < $x2; $i++){
    	for($j = $y1+1; $j < $y2; $j++){

            echo "<div>";

            ecrire_position($i, $j);
            ecrire_fleches($i, $j);

            for($k = 1; $k <= $compteur; $k++){ // affichage des joueurs et des objets
                afficher_joueurs_objets($x[$k], $y[$k], $i, $j, $bonus_x[$k], $bonus_y[$k], $bonus_type[$k], $pseudo[$k], $score[$k], $bonus_score[$k]);
            }
    		
            echo "<span class=\"contenu\">o</span>";

    		echo "</div>\n";
    	}
    	echo "<br />";
    }
}

function ecrire_position($i, $j){
    if($i == 0 && $j == 0){
        echo "<span class=\"autrejoueur\">[". $i .";". $j ."]<span class=\"infobulle\">Bonjour ". $_SESSION['pseudo'] .", vous avez ". $_SESSION['score'] ." points :)<br />
        Lisez <a href=\"http://l3m.in/p/projets/tests/deplacement/?aide\">l'aide</a>, c'est utile.</span></span><br />";
    }
    else{
        echo "[". $i .";". $j ."]<br />";
    }
}

function ecrire_fleches($i, $j){
    if($_SESSION['x'] == $i - 1 && $_SESSION['y'] == $j){
        echo "<a href=\"?d=bas\">&darr;</a>";
    }
     if($_SESSION['x'] == $i + 1 && $_SESSION['y'] == $j){
        echo "<a href=\"?d=haut\">&uarr;</a>";
    }
    if($_SESSION['x'] == $i && $_SESSION['y'] == $j + 1){
        echo "<a href=\"?d=gauche\">&larr;</a>";
    }
    if($_SESSION['x'] == $i && $_SESSION['y'] == $j - 1){
        echo "<a href=\"?d=droite\">&rarr;</a>";
    }
}

function afficher_joueurs_objets($x, $y, $i, $j, $bonus_x, $bonus_y, $bonus_type, $pseudo, $score, $bonus_score){
        if($_SESSION['x'] == $x && $x == $i && $_SESSION['y'] == $y && $y == $j && $pseudo == $_SESSION['pseudo']){
                echo "<span class=\"joueur\">O<span class=\"infobulle\">". $_SESSION['pseudo'] ." - score : ". $_SESSION['score'] ."</span></span>";
        }
        elseif($x == $i && $y == $j){
            echo "<span class=\"autrejoueur\">o<span class=\"infobulle\">". $pseudo ." - score : ". $score ."</span></span>";
        }
        elseif($bonus_x == $i && $bonus_y == $j && $bonus_type == 1  && $bonus_x != ""){
            echo "<span class=\"bonuspoints\">&starf;<span class=\"infobulle\">Bonus de points rapportant ". $bonus_score ." points !</span></span>";
        }
        elseif($bonus_x == $i && $bonus_y == $j && $bonus_type != 1 && $bonus_x != ""){
            echo "<span class=\"bonus\">&there4;<span class=\"infobulle\">Il y a un bonus sur cette case ! Ou bien est-ce un malus ?</span></span>";
        }
}

function afficher_stats_joueur(){
    ?>
    Vous êtes connecté, <?php echo $_SESSION['pseudo']; ?> &bull; position : [<?php echo $_SESSION['x'] .";". $_SESSION['y']; ?>] &bull; score : <?php echo $_SESSION['score']; ?> - <a href="http://l3m.in/p/projets/tests/deplacement/">actualiser</a><br />
    Dep5 : <?php echo $_SESSION['dep5']; ?>, Dep10 : <?php echo $_SESSION['dep10']; ?>, Invi : <?php echo $_SESSION['invi']; ?>, XP : <?php echo $_SESSION['xp']; ?>, 
    Atk : [<?php echo $_SESSION['min-atk']; ?>;<?php echo $_SESSION['max-atk']; ?>], Def : [<?php echo $_SESSION['min-def']; ?>;<?php echo $_SESSION['max-def']; ?>], Vie : <?php echo $_SESSION['vie']; ?>.
    <?php
}

// ----

function afficher_liste_joueurs(){
    include('../../../../fonctions/connexionbdd.php');
    echo "Liste des joueurs :<br />";
    $chercherlistejoueurs = $bdd->query('SELECT * FROM deplacement_joueurs_test ORDER BY score DESC') or die(print_r($bdd->errorInfo()));
    while ($donnees = $chercherlistejoueurs->fetch()){
        $temps = $donnees['temps'] - time();
        echo "&bull; <span>". $donnees['pseudo'] ."<span class=\"infobulle\">id : ". $donnees['id'] ." - ". ecrire_temps($temps, $donnees['id']) ."</span></span> - ". $donnees['score'] ." points - [". $donnees['x'] ."][". $donnees['y'] ."]<br />";
    }
}

function afficher_classement(){
    include('../../../../fonctions/connexionbdd.php');
    $chercherscores = $bdd->prepare('SELECT * FROM deplacement_classement_test ORDER BY score DESC') or die(print_r($bdd->errorInfo()));
    $chercherscores->execute();
    $i = 1;
    while($score = $chercherscores->fetch()){
        echo "<p>";
        if($i == 1){
            echo $i ."er";
        }
        elseif($i == 2){
            echo $i ."nd";
        }
        else{
            echo $i ."ème";
        }
        echo " - <span>". $score['pseudo'] ."<span class=\"infobulle\">id ". $score['id_profil'] ."</span></span> a atteint <span class=\"joueur\">". $score['score'] ."</span> points le ". date('d/m/Y à H:i:s', $score['date']) ." </p><hr />";
        $i++;
    }
}

function ecrire_temps($temps, $id){
    if(date("s", $temps) == 0){
        if(date("i", $temps) == 0){
            if($id == $_SESSION['id']){
                return "vous vous déconnecterez automatiquement après une heure d'inactivité";
            }
            return "déconnexion dans une heure";
        }
        else{
            return "déconnexion dans ". date("i", $temps) ." minutes";
        }
    }
    else{
        return "déconnexion dans ". date("i", $temps) ." minutes et ". date("s", $temps) ." secondes";
    }
    
}

?>