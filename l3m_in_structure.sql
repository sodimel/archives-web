-- phpMyAdmin SQL Dump
-- version 
-- https://www.phpmyadmin.net/
--
-- Hôte : 
-- Généré le :  
-- Version du serveur :  
-- Version de PHP :  

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `l3m_in`
--
CREATE DATABASE IF NOT EXISTS `l3m_in` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `l3m_in`;

-- --------------------------------------------------------

--
-- Structure de la table `argent_marche`
--

CREATE TABLE `argent_marche` (
  `id` int(11) NOT NULL,
  `id_l3m` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `valeur` double NOT NULL,
  `commentaire` varchar(255) NOT NULL,
  `en_vente` int(11) NOT NULL,
  `temps_obtention` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `argent_objets`
--

CREATE TABLE `argent_objets` (
  `id` int(11) NOT NULL,
  `id_l3m` int(11) NOT NULL,
  `temps` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `autorpg_endgames`
--

CREATE TABLE `autorpg_endgames` (
  `id` int(11) NOT NULL,
  `temps` int(11) NOT NULL,
  `aventures` int(11) NOT NULL,
  `objetstrouves` int(11) NOT NULL,
  `objetsachetes` int(11) NOT NULL,
  `quetes` int(11) NOT NULL,
  `contenu` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `cpinay_comments`
--

CREATE TABLE `cpinay_comments` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `commentaire` text NOT NULL,
  `datecomment` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `cpinay_paragraphes`
--

CREATE TABLE `cpinay_paragraphes` (
  `id` int(11) NOT NULL,
  `page` int(11) NOT NULL,
  `contenu` text NOT NULL,
  `datemaj` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `deplacement_classement_test`
--

CREATE TABLE `deplacement_classement_test` (
  `id` int(11) NOT NULL,
  `id_profil` bigint(50) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `score` int(11) NOT NULL,
  `date` int(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `deplacement_joueurs_test`
--

CREATE TABLE `deplacement_joueurs_test` (
  `id` bigint(35) NOT NULL,
  `x` int(5) NOT NULL,
  `y` int(5) NOT NULL,
  `pseudo` varchar(42) NOT NULL,
  `temps` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `depl5` int(11) NOT NULL DEFAULT 0,
  `depl10` int(11) NOT NULL DEFAULT 0,
  `invi` int(11) NOT NULL DEFAULT 0,
  `xp` int(11) NOT NULL DEFAULT 0,
  `vie` int(11) NOT NULL DEFAULT 10,
  `min-atk` int(11) NOT NULL DEFAULT 1,
  `max-atk` int(11) NOT NULL DEFAULT 3,
  `min-def` int(11) NOT NULL DEFAULT 0,
  `max-def` int(11) NOT NULL DEFAULT 3
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `deplacement_test`
--

CREATE TABLE `deplacement_test` (
  `id` int(11) NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `gems_tests`
--

CREATE TABLE `gems_tests` (
  `id` int(11) NOT NULL,
  `id_joueur` varchar(15) NOT NULL,
  `camp` int(11) NOT NULL,
  `gemmes` int(11) NOT NULL,
  `mktime_minuit` int(11) NOT NULL,
  `temps_entree` int(11) NOT NULL,
  `ip` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `gems_tests_logs`
--

CREATE TABLE `gems_tests_logs` (
  `id` int(11) NOT NULL,
  `id_joueur` varchar(15) NOT NULL,
  `pseudo_joueur` varchar(20) NOT NULL,
  `time_joueur` int(11) NOT NULL,
  `camp` int(11) NOT NULL,
  `gemmes` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `jeu_exilemap_joueurs`
--

CREATE TABLE `jeu_exilemap_joueurs` (
  `id_joueur` int(11) NOT NULL,
  `positionX` int(11) NOT NULL,
  `positionY` int(11) NOT NULL,
  `energie` int(2) NOT NULL DEFAULT 15,
  `argent` int(8) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `jeu_exilemap_map`
--

CREATE TABLE `jeu_exilemap_map` (
  `id_tuile` int(11) NOT NULL,
  `evenement` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `l3m_blog`
--

CREATE TABLE `l3m_blog` (
  `id` int(11) NOT NULL,
  `date` varchar(255) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `category` varchar(255) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `l3m_comments`
--

CREATE TABLE `l3m_comments` (
  `id` int(11) NOT NULL,
  `idmessage` int(11) NOT NULL,
  `pseudo` varchar(42) NOT NULL,
  `email` varchar(255) NOT NULL,
  `lien` varchar(255) NOT NULL,
  `commentaire` text NOT NULL,
  `date` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL DEFAULT 'inconnu'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `l3m_liste_projets`
--

CREATE TABLE `l3m_liste_projets` (
  `id` int(11) NOT NULL,
  `nom` varchar(250) CHARACTER SET utf8 NOT NULL,
  `lien` varchar(250) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `l3m_membres`
--

CREATE TABLE `l3m_membres` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `mdp` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `grade` int(11) NOT NULL DEFAULT 0,
  `avatar` text NOT NULL,
  `ip_connexion` varchar(255) NOT NULL DEFAULT '0',
  `date_inscription` varchar(10) NOT NULL,
  `genre` int(1) NOT NULL,
  `bio` text NOT NULL,
  `site` varchar(255) NOT NULL,
  `pieces` int(42) NOT NULL DEFAULT 0,
  `piecestotales` varchar(66) NOT NULL DEFAULT '0',
  `piece_404` int(1) NOT NULL DEFAULT 0,
  `piece_good` int(1) NOT NULL DEFAULT 0,
  `pieces_upppj` int(42) NOT NULL DEFAULT 0,
  `argent` float NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `mrendus_votes`
--

CREATE TABLE `mrendus_votes` (
  `id` int(255) NOT NULL,
  `time` int(255) NOT NULL,
  `ip` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `personnalisation_ip`
--

CREATE TABLE `personnalisation_ip` (
  `id` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `id_page` int(11) NOT NULL,
  `temps` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `personnalisation_membres`
--

CREATE TABLE `personnalisation_membres` (
  `id` int(11) NOT NULL,
  `id_l3m` int(11) NOT NULL,
  `pub` tinyint(4) NOT NULL DEFAULT 1,
  `nom_page` varchar(255) NOT NULL,
  `points` int(11) NOT NULL DEFAULT 0,
  `points_total` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `politique`
--

CREATE TABLE `politique` (
  `id` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `vote` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `pythonwars`
--

CREATE TABLE `pythonwars` (
  `id_l3m` int(11) NOT NULL,
  `pseudo_l3m` varchar(255) NOT NULL,
  `mdp` int(11) NOT NULL,
  `xp` int(11) NOT NULL DEFAULT 0,
  `attaque` int(11) NOT NULL DEFAULT 3,
  `defense` int(11) NOT NULL DEFAULT 1,
  `vie` int(11) NOT NULL DEFAULT 10,
  `ip_connexion` varchar(20) NOT NULL,
  `temps_connect` int(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `quizz_users`
--

CREATE TABLE `quizz_users` (
  `id` int(11) NOT NULL,
  `rank` int(1) NOT NULL DEFAULT 0,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `connectdate` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `sensor`
--

CREATE TABLE `sensor` (
  `id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `dateReceived` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `test_upppj`
--

CREATE TABLE `test_upppj` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(255) CHARACTER SET latin1 NOT NULL,
  `mdp` varchar(255) CHARACTER SET latin1 NOT NULL,
  `date` date NOT NULL,
  `coins` int(11) NOT NULL DEFAULT 0,
  `datebonus` date NOT NULL,
  `coinbonus` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `twinoid_ast_liste`
--

CREATE TABLE `twinoid_ast_liste` (
  `id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `date` int(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `twinoid_ast_propositions`
--

CREATE TABLE `twinoid_ast_propositions` (
  `id` int(11) NOT NULL,
  `temps` int(15) NOT NULL,
  `id_twinoid` int(11) NOT NULL,
  `pseudo_twinoid` varchar(255) NOT NULL,
  `lien` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `twinoid_bdd`
--

CREATE TABLE `twinoid_bdd` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `other` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `twinoid_jumeauid_liens`
--

CREATE TABLE `twinoid_jumeauid_liens` (
  `id` int(11) NOT NULL,
  `lien` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT 'Pas de description.',
  `votes` int(11) NOT NULL DEFAULT 0,
  `id_auteur` int(11) NOT NULL,
  `ip_auteur` varchar(255) NOT NULL,
  `pseudo_auteur` varchar(50) NOT NULL,
  `date_soumission` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `twinoid_jumeauid_votes`
--

CREATE TABLE `twinoid_jumeauid_votes` (
  `id` int(11) NOT NULL,
  `id_lien` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `id_twino` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `twinoid_twinocoin`
--

CREATE TABLE `twinoid_twinocoin` (
  `id` int(11) NOT NULL,
  `id_twino` int(11) NOT NULL,
  `pseudo_twino` varchar(255) NOT NULL,
  `coins` int(11) NOT NULL DEFAULT 25,
  `key_operation` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `twinoid_twinocoin_applis`
--

CREATE TABLE `twinoid_twinocoin_applis` (
  `id` int(11) NOT NULL,
  `id_appli` int(11) NOT NULL,
  `nom_appli` varchar(255) NOT NULL,
  `id_twino_proprio` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `retraits_effectues` int(11) NOT NULL DEFAULT 0,
  `depots_effectues` int(11) NOT NULL DEFAULT 0,
  `clef` text NOT NULL,
  `confirm` int(1) NOT NULL DEFAULT 0,
  `rand_number` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `twinoid_twinocoin_log`
--

CREATE TABLE `twinoid_twinocoin_log` (
  `id` int(11) NOT NULL,
  `action` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `argent_marche`
--
ALTER TABLE `argent_marche`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `argent_objets`
--
ALTER TABLE `argent_objets`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `autorpg_endgames`
--
ALTER TABLE `autorpg_endgames`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `cpinay_comments`
--
ALTER TABLE `cpinay_comments`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `cpinay_paragraphes`
--
ALTER TABLE `cpinay_paragraphes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `deplacement_classement_test`
--
ALTER TABLE `deplacement_classement_test`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `deplacement_test`
--
ALTER TABLE `deplacement_test`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `gems_tests`
--
ALTER TABLE `gems_tests`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `gems_tests_logs`
--
ALTER TABLE `gems_tests_logs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `l3m_blog`
--
ALTER TABLE `l3m_blog`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `l3m_comments`
--
ALTER TABLE `l3m_comments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `l3m_liste_projets`
--
ALTER TABLE `l3m_liste_projets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `l3m_membres`
--
ALTER TABLE `l3m_membres`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `mrendus_votes`
--
ALTER TABLE `mrendus_votes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `personnalisation_ip`
--
ALTER TABLE `personnalisation_ip`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `personnalisation_membres`
--
ALTER TABLE `personnalisation_membres`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `politique`
--
ALTER TABLE `politique`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `quizz_users`
--
ALTER TABLE `quizz_users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sensor`
--
ALTER TABLE `sensor`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `test_upppj`
--
ALTER TABLE `test_upppj`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `twinoid_ast_liste`
--
ALTER TABLE `twinoid_ast_liste`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `twinoid_ast_propositions`
--
ALTER TABLE `twinoid_ast_propositions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `twinoid_bdd`
--
ALTER TABLE `twinoid_bdd`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `twinoid_jumeauid_liens`
--
ALTER TABLE `twinoid_jumeauid_liens`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `twinoid_jumeauid_votes`
--
ALTER TABLE `twinoid_jumeauid_votes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `twinoid_twinocoin`
--
ALTER TABLE `twinoid_twinocoin`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `twinoid_twinocoin_applis`
--
ALTER TABLE `twinoid_twinocoin_applis`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `twinoid_twinocoin_log`
--
ALTER TABLE `twinoid_twinocoin_log`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `argent_marche`
--
ALTER TABLE `argent_marche`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `argent_objets`
--
ALTER TABLE `argent_objets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `autorpg_endgames`
--
ALTER TABLE `autorpg_endgames`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `cpinay_comments`
--
ALTER TABLE `cpinay_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `cpinay_paragraphes`
--
ALTER TABLE `cpinay_paragraphes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `deplacement_classement_test`
--
ALTER TABLE `deplacement_classement_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `deplacement_test`
--
ALTER TABLE `deplacement_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `gems_tests`
--
ALTER TABLE `gems_tests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `gems_tests_logs`
--
ALTER TABLE `gems_tests_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `l3m_blog`
--
ALTER TABLE `l3m_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `l3m_comments`
--
ALTER TABLE `l3m_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `l3m_liste_projets`
--
ALTER TABLE `l3m_liste_projets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `l3m_membres`
--
ALTER TABLE `l3m_membres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `mrendus_votes`
--
ALTER TABLE `mrendus_votes`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `personnalisation_ip`
--
ALTER TABLE `personnalisation_ip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `personnalisation_membres`
--
ALTER TABLE `personnalisation_membres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `politique`
--
ALTER TABLE `politique`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `quizz_users`
--
ALTER TABLE `quizz_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `sensor`
--
ALTER TABLE `sensor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `test_upppj`
--
ALTER TABLE `test_upppj`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `twinoid_ast_liste`
--
ALTER TABLE `twinoid_ast_liste`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `twinoid_ast_propositions`
--
ALTER TABLE `twinoid_ast_propositions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `twinoid_bdd`
--
ALTER TABLE `twinoid_bdd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `twinoid_jumeauid_liens`
--
ALTER TABLE `twinoid_jumeauid_liens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `twinoid_jumeauid_votes`
--
ALTER TABLE `twinoid_jumeauid_votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `twinoid_twinocoin`
--
ALTER TABLE `twinoid_twinocoin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `twinoid_twinocoin_applis`
--
ALTER TABLE `twinoid_twinocoin_applis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `twinoid_twinocoin_log`
--
ALTER TABLE `twinoid_twinocoin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
