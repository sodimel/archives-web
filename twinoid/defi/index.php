<?php
session_start();
    include('fonctions.php');
    if(!isset($_SESSION['defi-etape'])){
        $_SESSION['defi-etape'] = "1";
        header('Location: http://l3m.in/p/twinoid/defi/');
    }

    if(isset($_GET['verif']) && isset($_POST['texte'])){
        $texte = htmlspecialchars($_POST['texte']);
        $verif = verif($texte);
    }
?>
<!DOCTYPE html>
<html>
   <head>
        <title>Défi G55 !</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="design.css" />
        <meta name="viewport" content="width=device-width" />
    </head>
<body>

    <h1>Enigme bistrotG55</h1>
    <h3>Etape <?php echo $_SESSION['defi-etape']; ?></h3>

    <?php if(isset($verif) && $verif != ""){
        echo "<p class=\"center\">". $verif ."</p>";
    } ?>
	
    <form method="post" action="?verif">
        <input type="text" name="texte" maxlength="10" required /><br />      
        <input class="bouton" type="submit" value="Vérifier le texte" />
    </form>

    <?php if($_SESSION['defi-etape'] == "10"){
        echo fin();
    } ?>

    <p class="small center">Attention, ne vous tuez pas en jouant, ce n'est qu'un jeu. Jouez.</p>
	
</body>
</html>