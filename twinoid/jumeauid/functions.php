<?php
/*
	Fonction de connnexion à twinoid pour récupérer les infos
*/
	function connect()
	{
		$gettoken = token(); // fonction token qui récupère... le token depuis twinoid.
		$_SESSION['token'] = $gettoken->access_token;

		$infos = post($_SESSION['token']); // fonction post qui poste le token pour récupérer des infos de la personne qui se connecte

		$_SESSION['jum-id-twinoid'] = $infos->id;
		$_SESSION['jum-pseudo-twinoid'] = $infos->name;
	}
/*
	Fonction de récupération de token pour chercher les infos
*/
	function token()
	{
		$url = "https://twinoid.com/oauth/token";
		$id = 255;
		$secret = "";
		$redirect = "http%3A%2F%2Fl3m.in%2Fp%2Ftwinoid%2Fjumeauid%2F%3Fconnect";

		$contextOptions = array( // on demande un token pour récup les infos
		   'http' => array(
		   'method' => 'POST',
		   'header' => 'Content-type: application/x-www-form-urlencoded',
		   'content' => "client_id=".$id."&client_secret=".$secret."&redirect_uri=".$redirect."&code=".$_GET['code']."&grant_type=authorization_code"
		   )
		);
		$context = stream_context_create($contextOptions);
		$resultat = file_get_contents($url, false, $context);
		$resultat = json_decode($resultat);
		return $resultat;
	}
/*
	Récupération d'informations depuis le site de la MT.
*/
	function post($token) // on récupère l'id et le pseudo depuis le site de la MT
	{
		$url = "https://twinoid.com/graph/me?fields=id,name&access_token=".$token."";
		$json = file_get_contents($url);
		$json = json_decode($json);
		return $json;
	}

/*
	Verifications des pages.
*/

function verif_page(){
	if(!isset($_SESSION['jum-pseudo-twinoid']) && !isset($_SESSION['jum-id-twinoid'])){
		if(isset($_GET['suggest']) OR isset($_GET['suggestlink'])){
			header('Location:http://l3m.in/p/twinoid/jumeauid/?bad');
		}
	}

}

function connecte(){
	if(isset($_SESSION['jum-pseudo-twinoid']) && isset($_SESSION['jum-id-twinoid']))
		return true;
	else
		return false;
}

/*
	Gestion du contenu.
*/

function afficher_boutons(){
?>
	<span class="boutonmenu">Journal
		<span class="deroulant">
			<a class="sousmenuhaut" href="http://l3m.in/p/twinoid/jumeauid/">Liens du jour</a>
			<a href="?menudujour">Top du jour</a>
			<a href="?maxibestof">Top global</a>
			<a class="sousmenubas" href="?all">Tous les liens</a></span>
		</span>
	</span>
	<a href="?about">A propos</a>
<?php

	if(isset($_SESSION['jum-pseudo-twinoid']) && isset($_SESSION['jum-id-twinoid'])){
	?>
		<a href="?suggest">Suggérer</a>
		<a class="petit" href="?profile"><?php echo $_SESSION['jum-pseudo-twinoid']; ?></a>
		<a class="petit" href="?deco">Déconnexion</a>
	<?php
	}
	else{
	?>
		<a href="?connect">Connexion</a>
	<?php
	}
}

function texte_page($page, $id, $nombre){
	$page_afficher = "<p class=\"centre navigation\">";
	$pagesuivante = $page+1;
	$pageprecedente = $page-1;

	if($page > 1) {
		if(isset($_GET['maxibestof']))
			$page_afficher .= "<a href=\"?maxibestof&page=". $pageprecedente ."\">Page précédente</a>";
		elseif(isset($_GET['all'])){
			$page_afficher .= "<a href=\"?all&page=". $pageprecedente ."\">Page précédente</a>";
		}
		elseif(isset($_GET['menudujour'])){
			$page_afficher .= "<a href=\"?menudujour&page=". $pageprecedente ."\">Page précédente</a>";
		}
		elseif(isset($_GET['profilesubmit'])){
			$page_afficher .= "<a href=\"?profilesubmit&page=". $pageprecedente ."\">Page précédente</a>";
		}
		else{
			$page_afficher .= "<a href=\"?page=". $pageprecedente ."\">Page précédente</a>";
		}
	}
	else{
		$page_afficher .= "<i>Page précédente</i>";
	}

	$page_afficher .= " &bull; Page #". $page ." &bull; ";

	if($id > 1 && $nombre == 10) {
		if(isset($_GET['maxibestof']))
			$page_afficher .= "<a href=\"?maxibestof&page=". $pagesuivante ."\">Page suivante</a>";
		elseif(isset($_GET['all'])){
			$page_afficher .= "<a href=\"?all&page=". $pagesuivante ."\">Page suivante</a>";
		}
		elseif(isset($_GET['profilesubmit'])){
			$page_afficher .= "<a href=\"?profilesubmit&page=". $pagesuivante ."\">Page suivante</a>";
		}
		elseif(isset($_GET['menudujour'])){
			$page_afficher .= "<a href=\"?menudujour&page=". $pagesuivante ."\">Page suivante</a>";
		}
		else{
			$page_afficher .= "<a href=\"?page=". $pagesuivante ."\">Page suivante</a>";
		}
	}
	else{
		$page_afficher .= "<i>Page suivante</i>";
	}

	return $page_afficher;
}

/*
	Verif et envoi du lien.
*/

function verifier_lien($lien){
	$lien_verif = substr($lien, 0, 20);
	if($lien == htmlspecialchars($lien)){
		$a = preg_match("/https:\/\/twinoid.com\//", $lien_verif);
		$b = preg_match("/http:\/\/twinoid.com\//", $lien_verif);
		if($a == 1 OR $b == 1)
			return true;
	}
	return false;
}

function verif_bdd($lien){
	include("../../../fonctions/connexionbdd.php");
	$chercherlien = $bdd->prepare('SELECT * FROM twinoid_jumeauid_liens where lien = :lien') or die(print_r($bdd->errorInfo()));
	$chercherlien->execute(array('lien' => htmlspecialchars($lien)));
	if ($trouver = $chercherlien->fetch()){
		return false;
	}
	return true;
}

function verif_id_bdd($id){
	include("../../../fonctions/connexionbdd.php");
	$chercherlien = $bdd->prepare('SELECT * FROM twinoid_jumeauid_liens where id = :id') or die(print_r($bdd->errorInfo()));
	$chercherlien->execute(array('id' => htmlspecialchars($id)));
	if ($trouver = $chercherlien->fetch()){
		return true;
	}
	return false;
}

function verif_id_twin($id){
	include("../../../fonctions/connexionbdd.php");

	if(isset($_SESSION['jum-id-twinoid'])){
		$chercherip = $bdd->prepare('SELECT * FROM twinoid_jumeauid_votes where id_twino = :id and id_lien = :id_lien') or die(print_r($bdd->errorInfo()));
		$chercherip->execute(array('id' => $_SESSION['jum-id-twinoid'], 'id_lien' => htmlspecialchars($id)));
		if ($trouver = $chercherip->fetch())
			return true;
		else
			return false;
	}
	else{
		return false;
	}
}

function verif_idtwin($id){
	include("../../../fonctions/connexionbdd.php");

	if(isset($_SESSION['jum-id-twinoid'])){
		$chercherip = $bdd->prepare('SELECT * FROM twinoid_jumeauid_votes where id_twino = :id and id_lien = :id_lien') or die(print_r($bdd->errorInfo()));
		$chercherip->execute(array('id' => $_SESSION['jum-id-twinoid'], 'id_lien' => htmlspecialchars($id)));
		if ($trouver = $chercherip->fetch())
			return true;
		else
			return false;
	}
	else
		return false;

}

function envoyer_lien($lien, $description){
	if($description == "")
		$description = "Pas de description.";
	
	include("../../../fonctions/connexionbdd.php");

	$inscrire_lien = $bdd->prepare('INSERT INTO twinoid_jumeauid_liens(lien, description, id_auteur, ip_auteur, pseudo_auteur, date_soumission) VALUES(:lien, :description, :id_auteur, :ip_auteur, :pseudo_auteur, NOW())');
	$inscrire_lien->execute(array('lien' => htmlspecialchars($lien), 'description' => htmlspecialchars($description), 'id_auteur' => $_SESSION['jum-id-twinoid'], 'ip_auteur' => $_SERVER["REMOTE_ADDR"], 'pseudo_auteur' => $_SESSION['jum-pseudo-twinoid']));

	$id = $bdd->query('SELECT id FROM twinoid_jumeauid_liens ORDER BY id DESC LIMIT 0, 1');
	$a = $id->fetch();

	$link = "Location:http://l3m.in/p/twinoid/jumeauid/?done=". $a['id'];
	header($link);
}


/*
	Affichage du contenu ou d'une partie du contenu.
*/

function afficher_journal($i,$page){
	include('../../../fonctions/connexionbdd.php');

	$nombre = 0;

	$page = ($page-1)*10;
	if($page <= 0)
		$page = 0;

	if($i == 1){
		$liens = $bdd->query('SELECT * FROM twinoid_jumeauid_liens ORDER BY votes DESC limit '. $page .', 10');
	}
	if($i == 2){
		$liens = $bdd->query('SELECT * FROM twinoid_jumeauid_liens ORDER BY date_soumission DESC limit '. $page .', 10');
	}
	if($i == 3){
		$liens = $bdd->query('SELECT * FROM twinoid_jumeauid_liens WHERE date_soumission >= DATE_SUB(DATE(NOW()), INTERVAL 1 DAY) ORDER BY id DESC limit '. $page .', 10');
		$date = 1;
	}
	if($i == 4){
		$liens = $bdd->query('SELECT * FROM twinoid_jumeauid_liens WHERE date_soumission >= DATE_SUB(DATE(NOW()), INTERVAL 1 DAY) ORDER BY votes DESC limit '. $page .', 10');
		$date = 1;
	}
	if($i == 5){
		$liens = $bdd->prepare('SELECT * FROM twinoid_jumeauid_liens WHERE id_auteur = :auteur ORDER BY id DESC limit '. $page .', 10');
		$liens ->execute(array('auteur' => $_SESSION['jum-id-twinoid']));
		$date = 1;
	}
	while($a = $liens->fetch()){

		$id = $a['id'];
		$nombre++;
		?><div class="link">
			&bull; Lien soumis par <a class="pseudo" href="http://twinoid.com/user/<?php echo $a['id_auteur']; ?>"><?php echo $a['pseudo_auteur']; ?></a> 
			<span class="separator">|</span> <?php echo $a['votes']; ?> <?php
			if(isset($_SESSION['jum-id-twinoid'])){
				if(verif_idtwin($id)) { ?>
					<a title="Dékuber le post." href="?dekuber=<?php echo $id; ?>"><img class="kube" src="kubeon.png" alt="déjà-voté" /></a>
				<?php } else{ ?>
					<a title="Kuber le post." href="?vote=<?php echo $a['id']; ?>"><img class="kube" src="kubeoff.png" alt="pas-voté" /></a>
				<?php }
			} else { ?>
				<span><img class="kube" src="kubeoff.png" alt="connectez-vous pour voter" /><span class="infobulle">Vous devez être connecté pour pouvoir voter.</span></span>
			<?php } ?> 
			<br />
			<span class="petit">
				le <?php echo date('d/m/Y \à H:i:s', strtotime($a['date_soumission'])); ?> (<a href="?link=<?php echo $id; ?>">lien direct</a>).<?php
				if(isset($_SESSION['jum-id-twinoid']) && $_SESSION['jum-id-twinoid'] == 12661){
					?> <a href="?supprimer=<?php echo $id; ?>">supprimer</a>/<a href="?editer=<?php echo $id; ?>">éditer</a><?php
					}
				elseif(isset($_SESSION['jum-id-twinoid']) && $_SESSION['jum-id-twinoid'] == $a['id_auteur'] && isset($_GET['profilesumbit'])){
					?> <a href="?supprimer=<?php echo $id; ?>">supprimer ce lien</a><?php
				} ?>
			</span><br />
			<span class="decalage">
				<a class="lien" target="_blank" href="<?php echo $a['lien']; ?>">
					<?php echo $a['lien']; ?>
				</a><br />
				<div class="description-lien"><?php echo formatage($a['description']); ?></div>
			</span>
		</div><hr />

		<?php

	}

	echo texte_page(($page/10)+1, $id, $nombre);
}

function afficher_lien($id){
	$id = htmlspecialchars($id);
	include("../../../fonctions/connexionbdd.php");
	$chercherlien = $bdd->prepare('SELECT * FROM twinoid_jumeauid_liens WHERE id = :id') or die(print_r($bdd->errorInfo()));
	$chercherlien->execute(array('id' => $id));
	if($a = $chercherlien->fetch()){
		?><p>Visualisation du lien #<?php echo $id; ?> : </p>
		<div class="link">
			Lien soumis par <a class="pseudo" href="http://twinoid.com/user/<?php echo $a['id_auteur']; ?>"><?php echo $a['pseudo_auteur']; ?></a> 
			<span class="separator">|</span> <?php echo $a['votes']; ?> <?php
			if(isset($_SESSION['jum-id-twinoid'])){
				if(verif_idtwin($id)) { ?>
					<a title="Dékuber le post." href="?dekuber=<?php echo $id; ?>"><img class="kube" src="kubeon.png" alt="déjà-voté" /></a>
				<?php } else{ ?>
					<a title="Kuber le post." href="?vote=<?php echo $a['id']; ?>"><img class="kube" src="kubeoff.png" alt="pas-voté" /></a>
				<?php }
			} else { ?>
				<span><img class="kube" src="kubeoff.png" alt="connectez-vous pour voter" /><span class="infobulle">Vous devez être connecté pour pouvoir voter.</span></span>
			<?php } ?> 
			<br />
			<span class="petit">
				le <?php echo date('d/m/Y \à H:i:s', strtotime($a['date_soumission'])); ?>.<?php if(isset($_SESSION['jum-id-twinoid']) && $_SESSION['jum-id-twinoid'] == 12661) { ?> <a href="?supprimer=<?php echo $id; ?>">supprimer</a>/<a href="?editer=<?php echo $id; ?>">éditer</a><?php } ?>
			</span><br />
			<span class="decalage">
				<a class="lien" target="_blank" href="<?php echo $a['lien']; ?>">
					<?php echo $a['lien']; ?>
				</a><br />
				<div class="description-lien"><?php echo formatage($a['description']); ?></div>
			</span>
		</div>
		<hr />

		<p>
			Lien de partage :<br />
			<span class="lien-partage">http://l3m.in/p/twinoid/jumeauid/?link=<?php echo $id; ?></span>
			<br /><br />
			Vous pouvez aussi copier/coller ce petit morceau de texte pour indiquer sur le post en lui-même qu'il a été soumis sur jumeauïd :<br />
			<span class="lien-partage petit">
				<?php afficher_contenu_partage($id); ?>
			</span>
		</p>
		<?php
	}
}

function afficher_contenu_partage($id){
	$id = htmlspecialchars($id);
	include("../../../fonctions/connexionbdd.php");
	
	$chercherlien = $bdd->prepare('SELECT id FROM twinoid_jumeauid_liens WHERE id = :id') or die(print_r($bdd->errorInfo()));
	$chercherlien->execute(array('id' => $id));
	if($a = $chercherlien->fetch()){
		?>
			[aparte]Soumis sur [link=http://l3m.in/p/twinoid/jumeauid/?link=<?php echo $id; ?>]Jumeauïd[/link] ![/aparte]
		<?php
	}
}

/*
	Voter/dévoter pour un lien, supprimer/éditer un lien.
*/

function envoyer_vote($id){
	if(verif_id_bdd($id)){

		if(isset($_SESSION['jum-id-twinoid'])){

			if(!verif_id_twin($id)){
				include('../../../fonctions/connexionbdd.php');
				$id = htmlspecialchars($id);

				if(isset($_SESSION['jum-id-twinoid'])){
					$ajouter_vote = $bdd->prepare('INSERT INTO twinoid_jumeauid_votes(id_lien, ip, id_twino) VALUES(:id_lien, :ip, :id_twino)');
					$ajouter_vote->execute(array('id_lien' => $id, 'ip' => $_SERVER["REMOTE_ADDR"], 'id_twino' => $_SESSION['jum-id-twinoid']));
				}
				else{
					$ajouter_vote = $bdd->prepare('INSERT INTO twinoid_jumeauid_votes(id_lien, ip) VALUES(:id_lien, :ip)');
					$ajouter_vote->execute(array('id_lien' => $id, 'ip' => $_SERVER["REMOTE_ADDR"]));
				}
				

				$votes = $bdd->query('SELECT votes FROM twinoid_jumeauid_liens WHERE id = '. $id .'');
				if($a = $votes->fetch()){

					$ajouter_vote_lien = $bdd->prepare('UPDATE twinoid_jumeauid_liens SET votes = :votes WHERE id = :id') or die(print_r($bdd->errorInfo()));
					$ajouter_vote_lien->execute(array('id' => $id, 'votes' => $a['votes']+1));

				}

				if(isset($_SESSION['page']) && strlen($_SESSION['page']) > 1)
					$lien = "Location:http://l3m.in/p/twinoid/jumeauid/?". $_SESSION['page'] ."&okay";
				else
					$lien = "Location:http://l3m.in/p/twinoid/jumeauid/?okay";
				
				header($lien);
			}
			else
				if(isset($_SESSION['page']) && strlen($_SESSION['page']) > 1)
					$lien = "Location:http://l3m.in/p/twinoid/jumeauid/?voted&". $_SESSION['page'];
				else
					$lien = "Location:http://l3m.in/p/twinoid/jumeauid/?voted";
				
				header($lien);
		}
		else
			header('Location:http://l3m.in/p/twinoid/jumeauid/?twino_required');
	}
	else
		header('Location:http://l3m.in/p/twinoid/jumeauid/?lostinlamancha');
}

function dekuber($id){
	if(verif_id_bdd($id)){
		if(verif_id_twin($id)){
			include('../../../fonctions/connexionbdd.php');
			$id = htmlspecialchars($id);

			$supprimer_vote = $bdd->prepare('DELETE FROM twinoid_jumeauid_votes WHERE id_lien = :id_lien AND ip = :ip');
			$supprimer_vote->execute(array('id_lien' => $id, 'ip' => $_SERVER["REMOTE_ADDR"]));

			if(isset($_SESSION['jum-id-twinoid'])){
				$supprimer_vote = $bdd->prepare('DELETE FROM twinoid_jumeauid_votes WHERE id_lien = :id_lien AND id_twino = :id');
			$supprimer_vote->execute(array('id_lien' => $id, 'id' => $_SESSION['jum-id-twinoid']));
			}

			$votes = $bdd->query('SELECT votes FROM twinoid_jumeauid_liens WHERE id = '. $id .'');
			if($a = $votes->fetch()){
				$enlever_vote_lien = $bdd->prepare('UPDATE twinoid_jumeauid_liens SET votes = :votes WHERE id = :id') or die(print_r($bdd->errorInfo()));
				$enlever_vote_lien->execute(array('id' => $id, 'votes' => $a['votes']-1));

			if(isset($_SESSION['page']) && strlen($_SESSION['page']) > 1)
				$lien = "Location:http://l3m.in/p/twinoid/jumeauid/?". $_SESSION['page'] ."&dommage";
			else
				$lien = "Location:http://l3m.in/p/twinoid/jumeauid/?dommage";
			
			header($lien);
			}
		}
	}
}

function supprimer_lien($id){
	if($_SESSION['jum-id-twinoid'] == "12661" OR $_SESSION['jum-id-twinoid'] == id_auteur($id)){
		include('../../../fonctions/connexionbdd.php');
		$supprimer_vote = $bdd->prepare('DELETE FROM twinoid_jumeauid_liens WHERE id = :id');
		$supprimer_vote->execute(array('id' => $id));
		$lien = "Location:http://l3m.in/p/twinoid/jumeauid/?supp=". $id;
		header($lien);
	}
	else{
		$lien = "Location:http://l3m.in/p/twinoid/jumeauid/?noooope";
		header($lien);
	}
}

function lien($id){
	$id = htmlspecialchars($id);
	include("../../../fonctions/connexionbdd.php");
	$chercherlien = $bdd->prepare('SELECT * FROM twinoid_jumeauid_liens WHERE id = :id') or die(print_r($bdd->errorInfo()));
	$chercherlien->execute(array('id' => $id));
	if($trouver = $chercherlien->fetch()){
		return $trouver['lien'];
	}
	return false;
}

function id_auteur($id){
	$id = htmlspecialchars($id);
	include("../../../fonctions/connexionbdd.php");
	$chercherlien = $bdd->prepare('SELECT * FROM twinoid_jumeauid_liens WHERE id = :id') or die(print_r($bdd->errorInfo()));
	$chercherlien->execute(array('id' => $id));
	if($trouver = $chercherlien->fetch()){
		return $trouver['id_auteur'];
	}
	return false;
}

function contenu($id){
	$id = htmlspecialchars($id);
	include("../../../fonctions/connexionbdd.php");
	$cherchercontenu = $bdd->prepare('SELECT * FROM twinoid_jumeauid_liens WHERE id = :id') or die(print_r($bdd->errorInfo()));
	$cherchercontenu->execute(array('id' => $id));
	if($trouver = $cherchercontenu->fetch()){
		return $trouver['description'];
	}
	return false;
}

function editer_lien($id, $lien, $description){
	include('../../../fonctions/connexionbdd.php');
	$modifier_lien = $bdd->prepare('UPDATE twinoid_jumeauid_liens SET lien = :lien, description = :description WHERE id = :id') or die(print_r($bdd->errorInfo()));
	$modifier_lien->execute(array('id' => $id, 'lien' => htmlspecialchars($lien), 'description' => htmlspecialchars($description)));

	$lien = "Location:http://l3m.in/p/twinoid/jumeauid/?edit=". $id;
	header($lien);
}

/*
	Vrac
*/

function formatage($texte){
	$texte = preg_replace('#:piouz:#', '<img src="piouz.png" class="smiley" alt=":piouz:" />', $texte);
	$texte = preg_replace('#:calim:#', '<img src="calim.png" class="smiley" alt=":calim:" />', $texte);
	$texte = preg_replace('#:cute:#', '<img src="cute.gif" class="smiley" alt=":cute:" style="width: 35px; height: 35px; vertical-align: middle;" />', $texte);
	return $texte;
}

function ecrire_smileys(){
	?>
	<b>Smileys twinoid:</b> :calim: = <img src="calim.png" class="smiley" alt=":calim:" />, :piouz: = <img src="piouz.png" class="smiley" alt=":piouz:" />, :cute: = <img src="cute.gif" class="smiley" alt=":cute:" style="width: 35px; height: 35px; vertical-align: middle;" />
	<?php
}

function afficher_profil(){
	?>
		<p class="description">
			<b>Bienvenue sur votre récapitulatif, <?php echo $_SESSION['jum-pseudo-twinoid']; ?>.</b>
		</p>
		<p>
			<b>Statistiques : </b><br />
			&bull; Vous avez kubé <?php echo nb_kubes_profil($_SESSION['jum-id-twinoid']); ?> liens.<br />
			&bull; Vous avez soumis <?php echo nb_soumissions_profil($_SESSION['jum-id-twinoid']); ?> liens (<a href="?profilesubmit">les voir</a>).<br />
		</p>
	<?php
}

function nb_kubes_profil($id){
	$id = htmlspecialchars($id);
	include("../../../fonctions/connexionbdd.php");
	$chercher_kubes = $bdd->prepare('SELECT COUNT(id) FROM twinoid_jumeauid_votes WHERE id_twino = :id_twino');
	$chercher_kubes->execute(array('id_twino' => $id));
	$nombre_kubes = $chercher_kubes->fetchColumn();
	return $nombre_kubes;
}

function nb_soumissions_profil($id){
	$id = htmlspecialchars($id);
	include("../../../fonctions/connexionbdd.php");
	$chercher_soumissions = $bdd->prepare('SELECT COUNT(id) FROM twinoid_jumeauid_liens WHERE id_auteur = :id_twino');
	$chercher_soumissions->execute(array('id_twino' => $id));
	$nombre_soumissions = $chercher_soumissions->fetchColumn();
	return $nombre_soumissions;
}

function afficher_profil_liens(){
	if(!isset($_GET['page']) OR !is_numeric($_GET['page']))
		$page = 0;
	else
		$page = $_GET['page'];
	?>
		<p class="description">
			Voici les liens que vous avez soumis, <?php echo $_SESSION['jum-pseudo-twinoid']; ?>.
		</p>
	<?php

	afficher_journal(5,$page);
}

?>