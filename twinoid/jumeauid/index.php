<?php
session_start();
include('functions.php');

verif_page();

if(isset($_GET['maxibestof'])){
	$_SESSION['page'] = "maxibestof";
}
if(isset($_GET['menudujour'])){
	$_SESSION['page'] = "menudujour";
}
if(isset($_GET['all'])){
	$_SESSION['page'] = "all";
}
if(isset($_GET['profilesubmit'])){
	$_SESSION['page'] = "profilesubmit";
}
if(isset($_GET['link'])){
	$_SESSION['page'] = "link=". $_GET['link'];
}
if(empty($_GET)){
	$_SESSION['page'] = "";
}

if(isset($_GET['page'])){
	$_SESSION['page'] .= "&page=". $_GET['page'];
}

if(isset($_GET['connect']) && !isset($_GET['state'])){
	header('Location:https://twinoid.com/oauth/auth?response_type=code&client_id=255&redirect_uri=http%3A%2F%2Fl3m.in%2Fp%2Ftwinoid%2Fjumeauid%2F%3Fconnect&scope=contacts&state=connexion');
}

if(isset($_GET['connect']) && $_GET['state'] == "connexion"){
	connect();
	$lien = "Location:http://l3m.in/p/twinoid/jumeauid/?". $_SESSION['page'];
	header($lien);
}

if(isset($_GET['suggestlink'])){
	if(verifier_lien($_POST['lien'])) // vérifie que le lien est un simple lien
		if(verif_bdd($_POST['lien'])) // vérifie si le lien est déjà présent dans la bdd
			if(strlen($_POST['description']) < 256)
				envoyer_lien($_POST['lien'], $_POST['description']);
			else{
				$lien = "Location:http://l3m.in/p/twinoid/jumeauid/?suggest&lenght&link=". $_POST['lien'] ."&desc=". $_POST['description'];
				header($lien);
			}
		else
			header('Location:http://l3m.in/p/twinoid/jumeauid/?already');
	else
			header('Location:http://l3m.in/p/twinoid/jumeauid/?synthaxe');
}

if(isset($_GET['vote']) and is_numeric($_GET['vote'])){
	envoyer_vote($_GET['vote']);
}

if(isset($_GET['dekuber']) && is_numeric($_GET['dekuber'])){
	dekuber($_GET['dekuber']);
}

if(isset($_GET['supprimer']) && isset($_SESSION['jum-id-twinoid'])){
	supprimer_lien($_GET['supprimer']);
}

if(isset($_GET['envoi_edition']) && $_SESSION['jum-id-twinoid'] == 12661){
	editer_lien($_GET['envoi_edition'], $_POST['lien'], $_POST['description']);
}

if(isset($_GET['deco'])){
	session_destroy();
	header('Location:http://l3m.in/p/twinoid/jumeauid/');
}

if(!isset($_GET['page']))
	$_GET['page'] = 1;

if(!is_numeric($_GET['page']))
	header('Location:http://l3m.in/p/twinoid/jumeauid/');

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Jumeauïd</title>
	<meta name="description" content="Jumeauid, le journal de Twinoid autogéré par les membres." />
	<link rel="stylesheet" href="design.css" />
	<link href='https://fonts.googleapis.com/css?family=Raleway|Poiret+One' rel='stylesheet' type='text/css'>
	<link rel="icon" href="favicon.ico" />
	<meta name="viewport" content="width=device-width" />
</head>
<body>
	<div id="title">
	<a href="http://l3m.in/p/twinoid/jumeauid/"><h1> </h1></a>
	</div>

	<nav>
		<?php
		afficher_boutons();
		?>
	</nav>

	<?php
// Segments précis de page :

	if(isset($_GET['done'])){
	?>
	<p class="good">
		Votre lien a bien été soumis <?php echo $_SESSION['jum-pseudo-twinoid']; ?> <img src="cute.gif" style="width: 35px; height: 35px; vertical-align: middle;" alt=":cute" />
	</p>
	<p class="lien-partage">
		Vous pouvez copier/coller ce court texte pour indiquer aux gens sur le post que le lien a bien été soumis :<br />
		<span class="lien-partage petit">
			<?php afficher_contenu_partage($_GET['done']); ?>
		</span>
	</p>
	<?php
	}
	if(isset($_GET['lenght'])){
	?>
	<p class="bad">
		La description que vous avez entré est (beaucoup) trop longue. Essayez de ne pas dépasser les 255 caractères.
	</p>
	<?php
	}
	if(isset($_GET['noooope'])){
	?>
	<p class="bad">
		Tu n'es pas l'auteur de ce post, tu n'as pas le droit de faire ça.
	</p>
	<?php
	}
	if(isset($_GET['dommage'])){
	?>
	<p class="bad">
		Vous venez bien de dékuber un post.
	</p>
	<?php
	}
	if(isset($_GET['supp']) && isset($_SESSION['jum-id-twinoid']) && $_SESSION['jum-id-twinoid'] == 12661){
	?>
	<p class="good">
		POST <?php echo $_GET['supp']; ?> SUPPRIMÉ MOUAHAHAHAH
	</p>
	<?php
	}
	if(isset($_GET['edit']) && isset($_SESSION['jum-id-twinoid']) && $_SESSION['jum-id-twinoid'] == 12661){
	?>
	<p class="good">
		POST <?php echo $_GET['supp']; ?> ÉDITÉ MOUAHAHAHAH
	</p>
	<?php
	}
	if(isset($_GET['voted'])){
	?>
	<p class="bad">
		Vous avez déjà voté pour ce lien !
	</p>
	<?php
	}
	if(isset($_GET['twino_required'])){
	?>
	<p class="baad">
		Vous devez vous connecter via twinoid pour pouvoir voter.
	</p>
	<?php
	}
	if(isset($_GET['synthaxe'])){
	?>
	<p class="baad">
		Ce lien n'a pas la bonne syntaxe. Vérifiez bien qu'il commence par "http://twinoid.com" ou par "https://twinoid.com".<br />
		Si votre lien est un lien de forum d'un jeu de la motion twin possédant son propre nom de domaine (kadokado, hordes, kube, kingdom...), passez par <a href="https://twinoid.com/fr/tid/forum#!">cette</a> adresse pour le retrouver.
	</p>
	<?php
		}
	if(isset($_GET['okay'])){
	?>
	<p class="good">
		Vote enregistré !
	</p>
	<?php
		}
	if(isset($_GET['lostinlamancha'])){
	?>
	<p class="bad">
		Ce lien n'est pas enregistré dans la base de données. Donc pour pouvoir voter pour lui disons simplement que c'est mal barré.
	</p>
	<?php
	}
	if(isset($_GET['already'])){
	?>
	<p class="bad">
		Ce lien a déjà été soumis sur Jumeauid.
	</p>
	<?php
	}
	if(isset($_GET['bad'])){
	?>
	<p class="bad">
		La page que vous essayez d'atteindre nécessite une authentification préalable.<br />
		Pour ce faire, merci de cliquer sur le lien "connexion" dans le menu.
	</p>
	<?php
	}

// Pages affichées :

	if(isset($_GET['suggest']) and connecte()){
	?>
	<p>
		Vous voici sur la page de suggestion de lien. Faites bien attention à ne soumettre qu'un lien qui commence par "http://twinoid.com/" ou par "https://twinoid.com/", sinon vous perdrez le droit de poster des liens.<br />
		La description doit faire moins de 255 caractères, les sauts de ligne ne seront pas pris en compte.
	</p>
	<p>
		Évitez aussi de soumettre un lien qui est réservé aux amis de son auteur (icone <img src="amis.png" alt="icon" />), sinon votre lien soumis sera supprimé.
	</p>

	<p>
		<?php ecrire_smileys(); ?>
	</p>
	<form method="post" action="?suggestlink">
		<div style="width: 250px; margin: auto;">
			<input type="lien" name="lien" maxlength="100" class="input grand" required <?php if(isset($_GET['link'])) echo "value=\"". $_GET['link'] ."\""; ?> /><br />
			<textarea class="input" name="description" cols="31"><?php if(isset($_GET['desc'])) echo "value=\"". $_GET['desc'] ."\""; else echo "Pas de description."; ?></textarea>
			<?php if(isset($_GET['desc'])) { ?><br />
				<span class="petit">Votre description fait <?php echo strlen($_GET['desc']);?> caractères.</span>
			<?php } ?>
			<input class="bouton-sumbit" type="submit" value="Soumettre le lien" />
		</div>
	</form>
	<hr />
	<?php
	}
	elseif(isset($_GET['editer']) && isset($_SESSION['jum-id-twinoid']) && $_SESSION['jum-id-twinoid'] == 12661){
		$contenu = contenu($_GET['editer']);
		$lien = lien($_GET['editer']);
		?>
		<form method="post" action="?envoi_edition=<?php echo $_GET['editer']; ?>">
			<input type="text" name="lien" value="<?php echo $lien; ?>" required /> <br />
			<textarea name="description" required ><?php echo $contenu; ?></textarea><br />
			<input type="submit" value="Editer" /><br />
			<input type="reset" value="Réinitialiser" />
		</form>
		<?php
	}
	elseif(isset($_GET['link']) && is_numeric($_GET['link'])){
		afficher_lien($_GET['link']);
	}
	elseif(isset($_GET['profile']) and connecte()){
		afficher_profil();
	}
	elseif(isset($_GET['profilesubmit']) and connecte()){
		afficher_profil_liens();
	}
	elseif(isset($_GET['about'])){
		?>
		<p>
			<b>Jumeauïd, qu'est-ce que c'est ?</b><br />
			Jumeauïd est une application qui vous permet de soumettre des liens relatifs à Twinoid que vous jugez intéressants.<br />
			Une fois le lien soumis, tous les twinoidiens peuvent le "kuber". Vous pouvez de fait visualiser des classements de liens, vous permettant de découvrir des pépites parfois insoupçonnées de Twinoid :)<br />
			Vous pouvez aussi dékuber un lien soumis en cliquant sur le kube orange.
		</p>
		<hr />
		<p>
			<b>Comment ça fonctionne les classements ?</b><br />
			C'est plutôt simple.<br />
			&bull; Le "journal" c'est les liens soumis dans les dernières 24 heures, le plus récent en premier.<br />
			&bull; Les liens les "mieux notés du jour" sont les liens les... mieux notés du jour.<br />
			&bull; Les liens les mieux notés "de tous les temps" c'est la même chose mais en plus global.<br />
			&bull; La rubrique "tous les liens" quand à elle recence tous les liens soumis depuis que l'application a été lancée, le plus récent en premier.
		</p>
		<hr />
		<p>
			<b>Et mes données personnelles ?</b> <img src="calim.png" alt=":calim:" / ><br />
			Jumeauid ne stocke que votre couple id/pseudo twinoid (pour le cas où vous soumettez un lien) et votre adresse ip/id twinoid (pour le cas où vous votez pour un lien).
		</p>
		<hr />
		<p>
			<b>Je peux en savoir plus sur les votes ?</b><br />
			Au départ tout le monde pouvait voter sans être connecté, et en étant connecté. Mais ce système comportait de trop nombreuses failles et les classement n'étaient plus fiables.<br />
			Aujourd'hui vous devez être connecté à twinoid pour pouvoir voter. La bonne nouvelle c'est que les votes sont liés à votre compte et plus à votre adresse ip, donc vous gardez les kubes que vous avez soumis même si vous changez de réseau !
		</p>
		<hr />
		<p style="opacity: 0.7; text-shadow: 1px 1px 0 white;">jumeauïd, version 1.3</p>
		<?php
	}
	else{
		echo "<p class=\"description\">";
		if(isset($_GET['maxibestof'])){
		?>Voici le fleuron de l'élite du gratin de ce qui s'est fait de mieux sur twinoid.</p>
		<?php
			afficher_journal(1,$_GET['page']);
		}
		elseif(isset($_GET['all'])){
		?>Voici l'ensemble de tous les posts soumis à ce jour (les plus récents en premier).</p>
		<?php
			afficher_journal(2,$_GET['page']);
		}
		elseif(isset($_GET['menudujour'])){
		?>Voici le gratin de l'élite du fleuron de ce qui a été soumis aujourd'hui.</p>
		<?php
			afficher_journal(4,$_GET['page']);
		}
		else{
		?>Voici les posts soumis aujourd'hui, le véritable journal Jumeauïd.</p>
		<?php
			afficher_journal(3,$_GET['page']);
		}
	}

	?>

	<footer class="petit centre">
		Jumeauïd est un projet de <a target="_blank" href="https://twinoid.com/user/12661">sodimel</a> et est hébergé sur le <a href="http://l3m.in/">l3m website</a>. Une partie des éléments de ce site sont issus de <a target="_blank" href="https://twinoid.com/">Twinoid</a>.<br />
		Des cookies (notamment des cookies de session) sont enregistrés sur votre navigateur.<br />
	</footer>
</body>
</html>