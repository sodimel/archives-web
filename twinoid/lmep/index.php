<?php if ($_GET['p'] == "balancelasauce") {

} ?>
<!DOCTYPE html>
<html>
   <head>
        <title>LMEP - Liste Mise En Page</title>
        <meta charset="utf-8">
        <meta name="description" content="Page de listing de code de mise en page de messages Twinoid." />
        <meta name="Keywords" content="twinoid, mise, en, page, sodimel, coucou :D" />
        <link rel="stylesheet" href="design.css" />
        <link href="favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
        <style>
<?php $i = rand(1,2);
if ($i == 1) {
$angle = rand(-7,-3);
$angle2 = rand(7,3); }
else { 
$angle = rand(3,7);
$angle2 = rand(-3,-7); } ?>
.logo:hover{
transform: rotate(<?php echo $angle; ?>deg);
-webkit-transform: rotate(<?php echo $angle; ?>deg);
transition: transform 0.5s;}
.logo2:hover{
transform: rotate(<?php echo $angle2; ?>deg);
-webkit-transform: rotate(<?php echo $angle2; ?>deg);
transition: transform 0.5s;}
        </style>
    </head>
<body>
<div id="page">
<div id="bann"><div id="banner"><div class="logo"><a href="http://l3m.in/p/twinoid/lmep/"><img src="banner.png" alt="bannière" /></a></div> 
<div class="logo2"><a href="http://l3m.in/p/twinoid/lmep/?p=nexus"><img src="lien1.png" alt="bannière" /></a></div> 
<div class="logo2"><a href="http://l3m.in/p/twinoid/lmep/?p=forum"><img src="lien2.png" alt="bannière" /></a></div> </div>
</div>
<?php if ($_GET['p'] == "nexus") { ?>
<p>Liste des scripts pour générer du code sur les nexus.</p>
<p>&bull; <a href="http://l3m.in/p/twinoid/qe/">Question existentielle</a>. Ce petit formulaire vous permettra de poster des questions existentielles, à la sauce sodimel.<br />
&bull; <a href="http://l3m.in/p/twinoid/qe/1.php">Instant culture</a>. Ce formulaire est adapté aux instants culture de Chrys. </p>
<?php } elseif ($_GET['p'] == "forum") { ?>
<p>Liste des scripts pour générer du code sur les forums.</p>
<?php } elseif ($_GET['p'] == "propose") { ?>
<form method="POST" action="?p=balancelasauce">
<p>Proposez votre propre script via cet input :<br />
<textarea name="lien" rows="6" cols="42" placeholder="Entrez le contenu du script ici." ></textarea><br />
<input type="submit" value="Poster" /> &bull; <input type="reset" value="Réinitialiser" /><br /><br />
<i><b>Aide à la mise en page :</b><br />
&bull; La balise "gras" reste telle qu'elle est (**montexte**).<br />
&bull; Idem pour la balise "italique" (//montexte//).<br />
&bull; Comming son.</i>
</p></form>
<?php } else { ?>
<p>Bienvenue sur le site de contenu de mise en page des messages sur Twinoid.<br />
Ce site est encore en développement.<br />
<a href="?p=propose">Vous pouvez proposer des scripts en cliquant ici</a>.</p>
<?php } ?>
</div>
</body>
</html>