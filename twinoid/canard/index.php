<?php

if(isset($_GET['dl'])) {
	if(is_numeric($_GET['dl'])) {
		$nom = "Canard_dechaine-". $_GET['dl'] .".png";
		if(file_exists($nom)) {
		  header("Content-type: application/force-download"); 
		  header('Content-Disposition: attachment; filename="'. $nom .'"'); //Nom du fichier
		  header('Content-Length: '. filesize($nom)); //Taille du fichier
		  readfile($nom);
		} else { $error=1; }
	} else { $error=1; }
}

?>
<!DOCTYPE html>
<html>
   <head>
        <title>La légion de Balmung.</title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="design.css" />
        <link href="fav.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
        <meta name="viewport" content="width=device-width" />
		<link href='http://fonts.googleapis.com/css?family=Coming+Soon' rel='stylesheet' type='text/css'>
    </head>

<body>

	<header>
	
		<a href="http://l3m.in/p/twinoid/canard/"><div id="header"> </div></a>
		
	</header>
	
<?php if($error == 1) { ?>

	<section class="section">
		<p>Le fichier <b><?php echo $_GET['dl']; ?></b> n'existe pas, nous sommes désolés.<br />
		<a href="http://l3m.in/p/twinoid/canard/">Retour</a>.</p>
	</section>

<?php } else { ?>

	<section class="section">
	
		<p>Bienvenue sur la page dédiée au téléchargement des différentes éditions du journal de la <b>Légion de Balmung</b>, j'ai nommé <b>Le Canard Déchaîné</b>.</p>
		<ul>
		<?php
		$fichier = fopen('t2l42cs3z.a77c', 'r');
		$fechier = fgets($fichier);
		echo $fechier;
		fclose($fichier);
		?>
		</ul>
		
	</section>

<?php } ?>
	
	<section>
	
	<p class="small">Cette page est hébergée par le <a href="http://l3m.in/">l3m website</a>, et est directement liée avec le groupe twinoid de 
	<a href="http://twinoid.com/g/la-legion-de-balmung">La Légion de Balmung</a>.</p>
	
	</section>
	
</body>
</html>