<?php

function fonctions_ville(){
	if($_GET['ville'] == "" && $_POST['ville'] != ""){

		$ville = $_POST['ville'];
		$ville = str_replace(' ', '-', $ville);
		header('Location:http://l3m.in/p/twinoid/ministats/?ville='. $ville .'');
	}

	$lien = 'http://'. $_GET['ville'] .'.miniville.fr/xml';

	$contenu = file_get_contents($lien);
	$ligne = file($lien);

	if(preg_match('#<title>MiniVille</title>#', $contenu)){
		header('Location:http://l3m.in/p/twinoid/ministats/?erreur');
	}

	if($_GET['ville'] != "" && !isset($_POST['ville'])){
		trouver_variables($contenu, $ligne[12]);
	}
}


function trouver_variables($contenu, $ligne){

	global $adresse, $nom, $classement, $population, $revenus, $chomage, $transport, $crime, $pollution, $antiville, $comm, $envi, $indu, $secu, $trans, $lien;

	preg_match('#<host>.*</host>#', $contenu, $adresse);
	$adresse[0] = str_replace('<host>', '', $adresse[0]);
	$adresse[0] = str_replace('</host>', '', $adresse[0]);
	$adresse[0] = "http://". $adresse[0];

	preg_match('#<name>.*</name>#i', $contenu, $nom);
	$nom[0] = str_replace('<name>', '', $nom[0]);
	$nom[0] = str_replace('</name>', '', $nom[0]);

	preg_match('#<ranking>.*</ranking>#i', $contenu, $classement);
	$classement[0] = str_replace('<ranking>', '', $classement[0]);
	$classement[0] = str_replace('</ranking>', '', $classement[0]);

	preg_match('#<population>.*</population>#i', $contenu, $population);
	$population[0] = str_replace('<population>', '', $population[0]);
	$population[0] = str_replace('</population>', '', $population[0]);

	preg_match('#<incomes>.*</incomes>#i', $contenu, $revenus);
	$revenus[0] = str_replace('<incomes>', '', $revenus[0]);
	$revenus[0] = str_replace('</incomes>', '', $revenus[0]);
	$revenus[0] = strrev(wordwrap(strrev($revenus[0]),3,".",1));

	preg_match('#<unemployment>.*</unemployment>#i', $contenu, $chomage);
	$chomage[0] = str_replace('<unemployment>', '', $chomage[0]);
	$chomage[0] = str_replace('</unemployment>', '', $chomage[0]);

	preg_match('#<transport>.*</transport>#i', $contenu, $transport);
	$transport[0] = str_replace('<transport>', '', $transport[0]);
	$transport[0] = str_replace('</transport>', '', $transport[0]);

	preg_match('#<criminality>.*</criminality>#i', $contenu, $crime);
	$crime[0] = str_replace('<criminality>', '', $crime[0]);
	$crime[0] = str_replace('</criminality>', '', $crime[0]);

	preg_match('#<pollution>.*</pollution>#i', $contenu, $pollution);
	$pollution[0] = str_replace('<pollution>', '', $pollution[0]);
	$pollution[0] = str_replace('</pollution>', '', $pollution[0]);

	preg_match('#<nextnuke>.*</nextnuke>#i', $contenu, $antiville);
	$antiville[0] = str_replace('<nextnuke>', '', $antiville[0]);
	$antiville[0] = str_replace('</nextnuke>', '', $antiville[0]);

	$ligne = explode(" ", $ligne);

	while(! is_numeric($ligne[1][0])){
		$ligne[1] = substr($ligne[1],1);
	}
	while(! is_numeric($ligne[2][0])){
		$ligne[2] = substr($ligne[2],1);
	}
	while(! is_numeric($ligne[3][0])){
		$ligne[3] = substr($ligne[3],1);
	}
	while(! is_numeric($ligne[4][0])){
		$ligne[4] = substr($ligne[4],1);
	}
	while(! is_numeric($ligne[5][0])){
		$ligne[5] = substr($ligne[5],1);
	}

	$comm[0] = floatval($ligne[1]);

	$envi[0] = floatval($ligne[2]);

	$indu[0] = floatval($ligne[3]);

	$secu[0] = floatval($ligne[4]);

	$trans[0] = floatval($ligne[5]);

	$lien[0] = array(
				"ind" => 0.0,
				"tra" => 0.0,
				"sec" => 0.0,
				"env" => 0.0,
			   );
/*
	if($population[0] > 50 && $indu[0] > 0) $lien[0]["ind"] = ($population[0]/3)/$indu[0];
	if($population[0] > 100 && $trans[0] > 0) $lien[0]["tra"] = ($population[0]/5)/$trans[0];
	if($population[0] > 300 && $secu[0] > 0) $lien[0]["sec"] = ($population[0]/4)/$secu[0];
	if($population[0] > 500 && $envi[0] > 0) $lien[0]["env"] = ($population[0]/4)/$envi[0];
*/
}


// todo : remplacer cte bouse par les pourcentages, c'est quand même plus pratique
function lien_ideal($lien){
	arsort($lien);
	if(current($lien) < 1){
		return "";
	}
	else{
		$key = key($lien);
		return $key;
	}
}

?>