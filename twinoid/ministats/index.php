<?php 

include('fonctions.php');

if(isset($_GET['ville'])){
	fonctions_ville();
	$title = " - ". $_GET['ville'];
}
elseif(isset($_GET['erreur'])){
	$title = " - erreur";
}
else{
	$title = " - accueil";
}

if(isset($_GET['lien-ideal'])){
	if(mt_rand(0,100) == 100){
		header('Location:http://'. strtolower($nom[0]) .'.antiville.fr/');
	}
	else{
		$link = lien_ideal($lien[0]);
		header('Location:http://'. strtolower($nom[0]) .'.miniville.fr/'. $link);
	}
}

?>
<!DOCTYPE html>
<html>
   <head>
        <meta charset="utf-8">
        <title>MiniStats<?php echo $title; ?></title>
        <link rel="stylesheet" href="design.css" />
        <meta name="viewport" content="width=device-width" />
        <link href="favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
        <script src="http://l3m.in/analytics.js" type="text/javascript"></script>
        <link href='http://fonts.googleapis.com/css?family=Inconsolata|Ubuntu' rel='stylesheet' type='text/css'>
    </head>
<body>

<div id="page">

<header>
	<h1><span><?php if(isset($_GET['ville'])) { ?><a href="<?php echo $adresse[0]; ?>"><?php echo $nom[0]; ?></a><?php } else { echo "MiniStats"; } ?></span></h1>
</header>


<?php

if(isset($_GET['ville'])){
?>

<p>
<a href="?ville=<?php echo strtolower($nom[0]); ?>&lien-ideal">Visiter via le meilleur lien possible</a> <i class="petit">(expérimental - 1 chance sur 100 de renvoyer sur antiville)</i>
</p>

<p>
	<?php echo $population[0]; ?> habitants, <?php echo $classement[0]; ?> ème au classement.
</p>

<p class="petit">
	Revenus : <?php echo $revenus[0]; ?> €<br />
	Taux de chômage : <?php echo $chomage[0]; ?>% <span class="spoil">([pop-50-(ind*3)]/pop)</span><br />
	Efficacité des transports : <?php echo $transport[0]; ?>% <span class="spoil">(tra*5/100+(pop-100)/pop)</span><br />
	Puissance du crime : <?php echo $crime[0]; ?>% <span class="spoil">([(pop-300)-(4*sec)]/pop)</span><br />
	Taux de pollution : <?php echo $pollution[0]; ?>%<br />
</p>

<p class="petit">
	Commerce : <?php echo $comm[0]; ?><br />
	Environnement : <?php echo $envi[0]; ?><br />
	Industrie : <?php echo $indu[0]; ?><br />
	Securité : <?php echo $secu[0]; ?><br />
	Transports : <?php echo $trans[0]; ?><br />
</p>

<p>
	<a href="http://l3m.in/p/twinoid/ministats/">Voir les stats d'une autre ville</a>.
</p>

</div>

<?php
}
elseif(isset($_GET['erreur'])){
?>


<p>
	Erreur, la ville n'existe pas. Assurez-vous de bien avoir entré le nom contenu dans l'url de la ville.
</p>

<p>
	Exemple, pour la ville <span class="erreur">&é"'(-è_çà)=aaa</span>, le lien est <span class="erreur">http://e-ecaaaa.miniville.fr/</span>, il vous faut donc entrer <span class="erreur">e-ecaaaa</span>.
</p>

<p>
	<a href="http://l3m.in/p/twinoid/ministats/">Retour à l'accueil</a>.
</p>

</div>

<?
}
else{
?>

<form method="post" action="?ville">
	<p>
		Entrez le nom de la ville dont vous voulez avoir des statistiques.
	</p>

	<input class="testicule" type="text" name="ville" maxlength="255" required /> 

	<input type="submit" value="Voir les stats." />

</form>

<p class="petit">Sources : dossier miniville sur <a href="http://www.kookyoo.net/dossier/Jeux-Video/miniville-astuces-guide-tutoriel-conseils-antiville-gagner-mega-proxy-list-page1-00000030">kookyoo.net</a>, et 
<a href="http://www.twinpedia.com/miniville">twinpedia</a>.<br />
Version 0.2 - Création par <a href="http://twinoid.com/user/12661">sodimel</a> - <a href="http://l3m.in/">l3m website</a></p>
</div>


<?php
}
?>