<?php

// Cette fonction affiche le header en fonction de la page sur laquelle on se trouve.

	function afficherheader($title){ ?>
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="utf-8">
				<title><?php echo $title; ?></title>
				<link rel="stylesheet" href="design.css" />
				<link href='http://fonts.googleapis.com/css?family=Dosis|Hind|PT+Sans+Narrowr' rel='stylesheet'>
				<link rel="icon" href="favicon.ico" />
				<meta name="viewport" content="width=device-width" />
			</head>
		<body>

		<h1>Gestion de votre compte Twino<span class="koin">Coin</span></h1>
		<hr />
<?php
	}

	function merci(){ ?>
		<hr />
		<p style="font-size: 0.7em;">Merci à <a href="http://twinoid.com/user/2491648">TheLion</a> pour l'idée, merci au <a href="http://twd.io/e/OAwW0w/0">framework</a> de 
		<a href="http://twinoid.com/user/1711366">Ralof</a> qui m'a bien aidé, merci à <a href="http://twinoid.com/user/113">PifyZ</a> pour son morceau de code et à 
		<a href="http://twinoid.com/user/3138322">BSimo</a> pour le méga brainstorming sur le fonctionnement de l'appli + pour tous les conseils et astuces au niveau du code.<br />
		<?php 
	}

	function paragraphe($number) // cette fonction affiche du contenu en fonction du nombre entré sur index.php, c'est la page en gros :P
	{
		switch ($number) {
			case 0: ?>
				<p>Vous avez refusé l'accès à l'application ?<br />
				Très bien. A une prochaine fois !</p>
				<p><a href="http://l3m.in/p/twinoid/twinocoin/">Retour à l'accueil</a>.</p>
			<?php

				break;
			case 1: ?>

				<p><span class="textegris">Connexion réussie</span>.</p>
				<p>Vous avez <?php echo $_SESSION['coins']; ?> <span class="tc">TC</span> sur votre compte, <span class="pseudo"><?php echo $_SESSION['pseudo']; ?></span>.</p>
				<p><a href="http://l3m.in/p/twinoid/twinocoin/">Retour à l'accueil</a>.</p>
			<?php

				break;
			case 2: ?>

				<p>Vous êtes désormais inscrit, <span class="pseudo"><?php echo $_SESSION['pseudo']; ?></span>.<br />
				Pour vous souhaiter la bienvenue sur le réseau TwinoCoin, vous avez été crédité de <?php echo $_SESSION['coins']; ?> <span class="tc">TC</span>.</p>
				<p><a href="http://l3m.in/p/twinoid/twinocoin/">Retour à l'accueil</a>.</p>
			<?php

				break;
			case 3: ?>

				<p>
					Bienvenue sur cette page d'informations<?php if(checkinscrit(0)) { ?>, <span class="pseudo"><?php echo $_SESSION['pseudo']; ?></span><?php } ?>.<br />
					Vous pouvez récupérer une clef pour faire payer les joueurs sur votre appli via cette page.<br /><br />

					&bull; Lors de l'inscription, une clef sera générée en fonction de l'id de votre appli, de son nom (une vérification est effectuée via l'api twinoid pour vérifier que vous 
					possédez bien l'application) et d'un peu d'aléatoire.<br />
					&bull; Vous pourrez faire appel à la page <span class="textegris">debiter.php?debiter</span> avec votre id d'appli, votre clef, l'id twinoid du joueur que vous voulez 
					faire payer et le montant à débiter, et la page vous retournera un code (relatif au timestamp et aux infos données (vous aurez donc un code actif seulement 5 minutes)).<br />
					<span class="textegris" style="font-size: 0.8em;">Un code de démonstration est disponible <a href="http://pastebin.com/EC6Gq7P8">ici</a>.</span><br />
					&bull; Ce code, vous pourrez l'utiliser pour diriger l'utilisateur vers cette page (en post (un bouton "payer", par exemple), avec comme paramètres (en hidden ?) le code et 
					l'adresse de redirection).<br />
					<span class="textegris" style="font-size: 0.8em;">Un code de démonstration est disponible <a href="http://pastebin.com/1NX2F3ZW">ici</a>.</span><br />
					&bull; L'application demandera alors une confirmation au joueur avant de débiter le montant, puis elle le redirigera vers votre page avec un code de confirmation (<span class="textegris">good</span>).
				</p>
				<?php if(checkinscrit(0)) { ?><p><a href="http://l3m.in/p/twinoid/twinocoin/dev.php?submit" class="button">Se rendre sur la page d'ajout</a><?php } ?>
					<?php if(!checkinscrit(0)) { ?><p><?php } ?><a href="http://l3m.in/p/twinoid/twinocoin/" class="button" style="font-size: 0.8em; width: 160px;">Retour à l'accueil</a></p>
					<?php

				break;
			case 4:

				if(checkinscrit(0)) { ?>

					<form method="post" action="?submitappli">
						<p><span class="pseudo">Ajoutez votre application en indiquant son id :</span><br />
						<span class="textegris" style="font-size: 0.8em;">Si votre application a changé de nom, il suffit de la re-proposer en utilisant l'input ci dessous.</span><br />
							<div style="style=display: inline-block; width: 70px; margin: auto;"><input type="number" name="id" id="id" maxlength="50" class="input" style="width: 70px; height: 40px;" required /></div><br />
						<br />		
							<input class="button" type="submit" value="Soumettre votre application" />
							<a href="http://l3m.in/p/twinoid/twinocoin/" class="button" style="font-size: 0.8em; width: 160px;">Retour à l'accueil</a>
						</p>
					</form>

			<?php } else { ?>

				<p>Vous devez vous connecter afin de proposer votre application.</p>
				<p><a href="https://twinoid.com/oauth/auth?response_type=code&client_id=196&redirect_uri=http%3A%2F%2Fl3m.in%2Fp%2Ftwinoid%2Ftwinocoin%2Findex.php%3Fvalid&scope=contacts&state=" class="button">Se connecter à Twinoid</a></p>
			
			<?php
				}

				break;
			case 5: ?>

				<p>
					Votre application est inscrite dans la base de données, vous pourrez faire des retraits de <span class="tc">TC</span> aux membres.<br />
					Voici votre code; notez-le quelque part tout de suite (<a href="http://twinoid.com/user/notes/">bloc notes twinoid</a>), il vous permettra d'accéder à TwinoCoin depuis votre appli.<br />
					<span class="pseudo"><?php echo $_GET['applidone']; ?></span><br />
					<a href="http://l3m.in/p/twinoid/twinocoin/" class="button" style="font-size: 0.8em; width: 160px;">Retour à l'accueil</a>
				</p>
			<?php

				break;
			case 6: ?>

				<p>
					Il semblerait que vous ne soyez pas le créateur de l'application que vous souhaitez ajouter.<br />
					<a href="http://l3m.in/p/twinoid/twinocoin/" class="button" style="font-size: 0.8em; width: 160px;">Retour à l'accueil</a>
				</p>
			<?php

				break;
			case 7: ?>

				<p>
					Votre application est déjà inscrite dans la base de données, <?php echo $_SESSION['pseudo']; ?>.<br />
					Néanmoins, voici votre code, il vous permettra d'accéder à TwinoCoin depuis votre appli :<br />
					<span class="pseudo"><?php echo $_GET['applideja']; ?></span><br />
					<a href="http://l3m.in/p/twinoid/twinocoin/dev.php?changercode=<?php echo $_GET['applideja']; ?>" class="button" style="font-size: 0.8em; width: 160px;">Changer de code</a>
					<a href="http://l3m.in/p/twinoid/twinocoin/" class="button" >Retour à l'accueil</a>
				</p>
			<?php

				break;
			case 8: ?>

				<p>
					Le changement de nom de votre application a été pris en compte, <?php echo $_SESSION['pseudo']; ?>.<br />
					<a href="http://l3m.in/p/twinoid/twinocoin/" class="button" style="font-size: 0.8em; width: 160px;">Retour à l'accueil</a>
				</p>
			<?php

				break;
				case 9: ?>

				<p>
					Le code de votre appli a été re-généré, <?php echo $_SESSION['pseudo']; ?>. Le voici :<br />
					<span class="textegris"><?php echo $_GET['changementcode']; ?></span>
					<a href="http://l3m.in/p/twinoid/twinocoin/" class="button" style="font-size: 0.8em; width: 160px;">Retour à l'accueil</a>
				</p>
			<?php

				break;
			default:

				if(checkinscrit(0)) { ?>
					
					<p><span class="textegris">Vous êtes connecté - <a href="?deco" style="font-size: 0.8em;">se déconnecter</a> - <a href="dev.php?infos" style="font-size: 0.8em;">développeur</a>.</span><br />
					Vous avez <?php echo $_SESSION['coins']; ?> <span class="tc">TC</span> sur votre compte TwinoCoin, <span class="pseudo"><?php echo $_SESSION['pseudo']; ?></span>.</p>
					
				<?php } ?>

					<p><img src="coin.png" alt="icon twinocoin"  class="icon" /> TwinoCoin est une petite monnaie relative à la plateforme <img src="twino.png" alt="icon twinoid" class="icon" /> <a href="http://twinoid.com/">Twinoid</a>, permettant des interactions entre d'autres applications twinoid et les joueurs.</p>
		
					<p style="font-size: 0.8em;"><b>Attention :</b><br />
					&bull; <i>Cette monnaie n'a aucune valeur réelle, elle n'existe que pour le «fun» (il est interdit d'en échanger contre de l'argent réel, vous pouvez juste proposer des services via une application tierce contre rémunération en <span class="tc">TC</span>).</i><br />
					&bull; <i>Elle n'est liée à Twinoid que par la nécessité de se connecter à l'application TwinoCoin afin de synchroniser son compte Twinoid avec son "compte" <img src="coin.png" alt="icon twinocoin" class="icon" style="width: 19px; height: 19px;" /> TwinoCoin.</i><br />
					&bull; <i>Des applications twinoid peuvent vous faire "payer" certains services contre des <span class="tc">TC</span>. Vous serez toujours redirigé vers l'appli TwinoCoin lors d'un achat.</i><br />
					&bull; <i>Seules quelques applications sont habilitées à vous fournir des TC (et ce afin d'éviter que n'importe qui crée son application et se donne de grandes quantités de <span class="tc">TC</span>).</i></p>

					<?php if(! checkinscrit(0))
					{ ?>
						<p><a href="https://twinoid.com/oauth/auth?response_type=code&client_id=196&redirect_uri=http%3A%2F%2Fl3m.in%2Fp%2Ftwinoid%2Ftwinocoin%2Findex.php%3Fvalid&scope=contacts&state=" class="button">Se connecter à Twinoid</a></p>
			<?php   }
		}
	}

?>