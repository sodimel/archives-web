<?php
session_start();

include 'lib.php'; // les fonctions sont dans le fichier lib.php
include 'contenu.php'; // le code html, pour être vraiment modulable, est contenu dans contenu.php :D

if(isset($_SESSION['pseudo']) && $_SESSION['pseudo'] == "sodimel"){

	afficherheader("TwinoCoin - log");

	try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
	catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
	$cherchernombreinscrits = $bdd->query('SELECT COUNT(*) AS id FROM twinoid_twinocoin ORDER BY ID');
	$nombreinscrits = $cherchernombreinscrits->fetch();
	$chercherapplisinscrits = $bdd->query('SELECT COUNT(*) AS id FROM twinoid_twinocoin_applis ORDER BY ID');
	$nombreapplis = $chercherapplisinscrits->fetch();
	$cherchernombrelog = $bdd->query('SELECT COUNT(*) AS id FROM twinoid_twinocoin_log ORDER BY ID');
	$nombrelog = $cherchernombrelog->fetch();

	echo "<p>Page des logs de TwinoCoin. Attention, ça peut mettre du temps à charger :calim:</p>
	<p class=\"textegris\" style=\"font-size: 0.8em;\">Il y a ". $nombreinscrits[0] ." inscrits sur twinocoin, et ". $nombreapplis[0] ." applications inscrites.<br />
	Il y a ". $nombrelog[0] ." entrées dans la bdd pour les logs.</p>

	<p>";


	$afficherlogs = $bdd->query('SELECT * FROM twinoid_twinocoin_log ORDER BY id DESC');
	while ($log = $afficherlogs->fetch()) { echo "<span class=\"textegris\">[". $log['id'] ."]</span> ". $log['action'] ."<br />"; }
	$afficherlogs->closeCursor();
	$fintemps = tempschargement(); ?></p>
	<hr />
    <i>La page a été générée en <?php echo round($fintemps-$debuttemps, 3); ?> seconde(s).</i></p>
<?php } ?>