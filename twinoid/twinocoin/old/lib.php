<?php
/*
    Fonction globale, sur toutes les pages
*/
    function tempschargement()
    {
        list($usec, $sec) = explode(" ",microtime());
        return ((float)$usec + (float)$sec);
    }
    $debuttemps = tempschargement();
/*
    Fonction de connnexion à twinoid pour récupérer les infos
*/
    function connect($redirect){
        $gettoken = token(); // fonction token qui récupère... le token depuis twinoid.
        $_SESSION['token'] = $gettoken->access_token;
        $_SESSION['expire'] = $gettoken->expires_in + time();

        $infos = post($_SESSION['token']); // fonction post qui poste le token pour récupérer des infos de la personne qui se connecte

        $id = $infos->id;
        $pseudo = $infos->name;

        if(checkinscrit($id)) {
            modifpseudo($id, $pseudo);
            $coins = coins($id);
            if($redirect == 1) {
                header("Location:http://l3m.in/p/twinoid/twinocoin/?connect");
                exit();
            }
            elseif($redirect == 2) {
                header("Location:http://l3m.in/p/twinoid/twinocoin/debiter.php?payerdebit");
                exit();
            }
        }
        else{
            inscrire($id, $pseudo);
            if($redirect == 1) {
                header("Location:http://l3m.in/p/twinoid/twinocoin/?inscrit");
                exit();
            }
            elseif($redirect == 2) {
                header("Location:http://l3m.in/p/twinoid/twinocoin/debiter.php?payerdebit&inscrit");
                exit();
            }
        }
    }
/*
    Fonction de récupération de token pour chercher les infos
*/
    function token()
    {
        $url = "https://twinoid.com/oauth/token";
        $id = 196;
        $secret = "";
        $redirect = "http%3A%2F%2Fl3m.in%2Fp%2Ftwinoid%2Ftwinocoin%2Findex.php%3Fvalid";

        $contextOptions = array( // on demande un token pour récup les infos
           'http' => array(
           'method' => 'POST',
           'header' => 'Content-type: application/x-www-form-urlencoded',
           'content' => "client_id=".$id."&client_secret=".$secret."&redirect_uri=".$redirect."&code=".$_GET['code']."&grant_type=authorization_code"
           )
        );
        $context = stream_context_create($contextOptions);
        $resultat = file_get_contents($url, false, $context);
        $resultat = json_decode($resultat);
        return $resultat;
    }
/*
    Fonction qui vérifie si une personne est déjà inscrite dans la bdd
*/
    function checkinscrit($id) // on vérifie si la personne est déjà inscrite
    {
        if(! isset($_SESSION['pseudo']) && ! isset($_SESSION['id']) && ! isset($_SESSION['coins']) && $id != 0){
            try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
            catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
            $chercherinscrit = $bdd->prepare('SELECT * FROM twinoid_twinocoin where id_twino = :id_twino') or die(print_r($bdd->errorInfo()));
            $chercherinscrit->execute(array('id_twino' => $id));
            if ($trouver = $chercherinscrit->fetch()){
                $_SESSION['pseudo'] = $trouver['pseudo_twino'];
                $_SESSION['id'] = $trouver['id_twino'];
                $_SESSION['coins'] = $trouver['coins'];
                    savebdd(5, $_SESSION['pseudo'], $_SESSION['id'], 0, ""); // fonction de log, type d'action 1 = regist, 2 = regist app, 3 = -TC, 4 = +TC, 5 = connect, 6 = deco - type.pseudo.id.montant.appli
                return true;
                exit();
            }
            else{ 
                return false;
                exit();
            }
            $chercherinscrit->closecursor();
        }
        elseif(! isset($_SESSION['pseudo']) && ! isset($_SESSION['id']) && ! isset($_SESSION['coins']) && $id == 0){
            return false;
        }
        else{
            return true;
        }
    }
/*
    Fonction qui vérifie si la personne est connectée
*/
    function verifconnect() // vérification de la connexion et de la validité du token d'accès (si c'est plus valide on reconnecte)
    {
        if(isset($_SESSION['pseudo']) && isset($_SESSION['id']) && isset($_SESSION['coins']))
            {
            if($_SESSION['expire'] < time()){
                header("Location:https://twinoid.com/oauth/auth?response_type=code&client_id=196&redirect_uri=http%3A%2F%2Fl3m.in%2Fp%2Ftwinoid%2Ftwinocoin%2Findex.php%3Fvalid&scope=contacts&state=");
                return true;
            }
        return true;
        }
        else
            return false;
    }
/*
    Récupération d'informations depuis le site de la MT.
*/
    function post($token) // on récupère l'id et le pseudo depuis le site de la MT
    {
        $url = "https://twinoid.com/graph/me?fields=id,name&access_token=".$token."";
        $json = file_get_contents($url);
        $json = json_decode($json);
        return $json;
    }
/*
    Fonction qui vérifie si le pseudo du joueur qui se connecte a changé
*/
    function modifpseudo($id, $pseudo) // on vérifie si la personne (déjà inscrite) a changé de pseudo (si c'est le cas on update son pseudo)
    {
        if($id != "" && $pseudo != ""){
            try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
            catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
            $chercherpseudo = $bdd->prepare('SELECT * FROM twinoid_twinocoin where id_twino = :id_twino and pseudo_twino = :pseudo_twino') or die(print_r($bdd->errorInfo()));
            $chercherpseudo->execute(array('id_twino' => $id, 'pseudo_twino' => $pseudo));
            if ($trouver = $chercherpseudo->fetch()) {}
            else{
                $modifpseudo = $bdd->prepare('UPDATE twinoid_twinocoin SET pseudo = :pseudo_twino where id_twino = :id') or die(print_r($bdd->errorInfo()));
                $modifpseudo->execute(array('pseudo_twino' => $pseudo, 'id_twino' => $id));
            }
        }
    }
/*
    Fonction d'inscription de la personne dans la bdd
*/
    function inscrire($id, $pseudo) // si la personne n'est pas inscrite on l'inscrit dans la bdd :)
    {
        try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
        catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
        $inscrit = $bdd->prepare('INSERT INTO twinoid_twinocoin(id_twino, pseudo_twino) VALUES(:id_twino, :pseudo_twino)');
        $inscrit->execute(array('id_twino' => $id,'pseudo_twino' => $pseudo));
        $inscrit->closeCursor();
        $_SESSION['pseudo'] = $pseudo;
        $_SESSION['id'] = $id;
        $_SESSION['coins'] = 25;
            savebdd(1, $pseudo, $id, 0, ""); // fonction de log, type d'action 1 = regist, 2 = regist app, 3 = -TC, 4 = +TC, 5 = connect - type.pseudo.id.montant.appli
            savebdd(4, $pseudo, $id, 25, "<span class=\"depot\">twinocoin</span>"); // fonction de log, type d'action 1 = regist, 2 = regist app, 3 = -TC, 4 = +TC, 5 = connect - type.pseudo.id.montant.appli
    }
/*
    Fonction qui enregistre l'application d'un joueur dans la bdd
*/
    function postappli($id)
    {
        if($id >= 0){
            if(isset($_SESSION['token']) && $_SESSION['expire'] > time()) { // si le token est encore valide

                $url = "https://twinoid.com/graph/application/". $id ."?fields=id,name,url,owner&access_token=".$_SESSION['token'].""; // on demande des infos à twinoid sur l'application à inscrire
                $json = file_get_contents($url);
                $json = json_decode($json);
                $id_app = $json->id;
                $name_app = $json->name;
                $adresse = $json->url;
                $id_proprio = $json->owner->id;

            }
            else { // sinon on récupère un nouveau token et on fait la demande d'infos sur l'appli

                $gettoken = token(); // fonction token qui récupère... le token depuis twinoid.
                $_SESSION['token'] = $gettoken->access_token;
                $_SESSION['expire'] = $gettoken->expires_in + time();
                $url = "https://twinoid.com/graph/application/". $id ."?fields=id,name,url,owner&access_token=".$_SESSION['token'].""; // on demande des infos à twinoid sur l'application à inscrire
                $json = file_get_contents($url);
                $json = json_decode($json);
                $id_app = $json->id;
                $name_app = $json->name;
                $adresse = $json->url;
                $id_proprio = $json->owner->id;
            }
        }

        if($id_proprio == $_SESSION['id']){

            $rand_number = rand(5,99);
            $clef = sha1("APP-". $id_app ."-TwinoCoin-". $rand_number ."-". $_SESSION['id']);

            try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
            catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
                
            $chercherappli = $bdd->prepare('SELECT * FROM twinoid_twinocoin_applis where id_appli = :id_appli') or die(print_r($bdd->errorInfo()));
            $chercherappli->execute(array('id_appli' => $id_app));

                if ($trouver = $chercherappli->fetch()){ // si l'appli existe déjà
                    if($trouver['nom_appli'] != $name_app) { // si le nom de l'appli a changé
                        $modifnomapp = $bdd->prepare('UPDATE twinoid_twinocoin_applis SET nom_appli = :nom_appli where id_appli = :id_appli') or die(print_r($bdd->errorInfo()));
                        $modifnomapp->execute(array('nom_appli' => $name_app, 'id_appli' => $id_app));
                        $modifnomapp->closecursor;
                        header("Location:http://l3m.in/p/twinoid/twinocoin/dev.php?appliname");
                    }
                    else{
                        header("Location:http://l3m.in/p/twinoid/twinocoin/dev.php?applideja=". $trouver['clef'] ."");
                        exit();
                    }
                }
                else {
                    $addappli = $bdd->prepare('INSERT INTO twinoid_twinocoin_applis(id_appli, nom_appli, id_twino_proprio, url, clef, rand_number) VALUES(:id_appli, :nom_appli, :id_twino_proprio, :url, :clef, :rand_number)');
                    $addappli->execute(array('id_appli' => $id_app, 'nom_appli' => $name_app, 'id_twino_proprio' => $_SESSION['id'], 'url' => $adresse, 'clef' => $clef, 'rand_number' => $rand_number));
                    $addappli->closeCursor();
                    savebdd(2, $_SESSION['pseudo'], $_SESSION['id'], 0, $name_app); // fonction de log, type d'action 1 = regist, 2 = regist app, 3 = -TC, 4 = +TC, 5 = connect, 6 = deco - type.pseudo.id.montant.appli
                    header("Location:http://l3m.in/p/twinoid/twinocoin/dev.php?applidone=". $clef ."");
                    exit();
                }

            $chercherappli->closecursor();
        }
        else{
            header("Location:http://l3m.in/p/twinoid/twinocoin/dev.php?erreurdeproprietaire");
        }
    }
/*
    Fonction du changement de code d'une application
*/
    function changecode($code){ // changement du code d'une appli
        try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
        catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
                
        $chercherappli = $bdd->prepare('SELECT * FROM twinoid_twinocoin_applis where clef = :code') or die(print_r($bdd->errorInfo()));
        $chercherappli->execute(array('code' => $code)); // on cherche l'appli
        if ($trouver = $chercherappli->fetch()){ // si elle existe
            $rand_number = rand(5,99); // on change le code
            $clef = sha1("APP-". $trouver['id_appli'] ."-TwinoCoin-". $rand_number ."-". $_SESSION['id']);
            $modifcodeapp = $bdd->prepare('UPDATE twinoid_twinocoin_applis SET clef = :clef where id_appli = :id_appli') or die(print_r($bdd->errorInfo()));
            $modifcodeapp->execute(array('clef' => $clef, 'id_appli' => $trouver['id_appli']));
            $modifcodeapp->closecursor();
            savebdd(7, $_SESSION['pseudo'], $_SESSION['id'], 0, $trouver['nom_appli']); // fonction de log
            header("Location:http://l3m.in/p/twinoid/twinocoin/dev.php?changementcode=". $clef ."");
        }
    }
/*
    Fonction qui permet de connaitre son nombre de TC
*/
    function coins($id) // fonction générique permettant de connaitre son nombre de TC
    {
        try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
        catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
        $chercherinscrit = $bdd->prepare('SELECT * FROM twinoid_twinocoin where id_twino = :id_twino') or die(print_r($bdd->errorInfo()));
        $chercherinscrit->execute(array('id_twino' => $id));
        if ($trouver = $chercherinscrit->fetch()){

            $chercherpieces = $bdd->prepare('SELECT coins FROM twinoid_twinocoin where id_twino = :id_twino') or die(print_r($bdd->errorInfo()));
            $chercherpieces->execute(array('id_twino' => $id));
            if ($nombrepieces = $chercherpieces->fetch()) return $nombrepieces['coins'];
            else return "erreur - twinocoins indisponibles (contactez sodimel sur twinoid svp)";
        }
    }
/*
Fonction de log :
 Types d'action :
  1 = inscription
  2 = incription d'application
  3 = retrait
  4 = dépôt
  5 = connexion
  6 = déconnexion volontaire depuis le bouton "déconnexion" (requiert d'être connecté pour que le log fonctionne)
  7 = mise à jour du code d'une application
*/
    function savebdd($typeaction, $pseudo, $id, $montant, $application)
    {
        switch ($typeaction) {
            case '1':
                $action = "<span class=\"tc\">[". date("d-m-Y") ." à ". date("H:i") ."]</span> <span class=\"pseudo\">[inscription]</span> - ". $pseudo ." (id ". $id .") s'est inscrit sur twinocoin.";
            break;
            case '2':
                $action = "<span class=\"tc\">[". date("d-m-Y") ." à ". date("H:i") ."]</span> <span class=\"appli\">[inscription application]</span> - ". $pseudo ." (id ". $id .") a inscrit son application (". $application .") sur twinocoin.";
            break;
            case '3':
                $action = "<span class=\"tc\">[". date("d-m-Y") ." à ". date("H:i") ."]</span> <span class=\"retrait\">[retrait]</span> - ". $pseudo ." (id ". $id .") s'est vu retirer ". $montant ." TC de son compte via ". $application .".";
            break;
            case '4':
                $action = "<span class=\"tc\">[". date("d-m-Y") ." à ". date("H:i") ."]</span> <span class=\"depot\">[dépôt]</span> - ". $pseudo ." (id ". $id .") s'est vu crédité de ". $montant ." TC sur son compte via ". $application .".";
            break;
            case '5':
                $action = "<span class=\"tc\">[". date("d-m-Y") ." à ". date("H:i") ."]</span> <span class=\"connect\">[connexion]</span> - ". $pseudo ." (id ". $id .") s'est connecté sur twinocoin.";
            break;
            case '6':
                $action = "<span class=\"tc\">[". date("d-m-Y") ." à ". date("H:i") ."]</span> <span class=\"deco\">[déconnexion]</span> - ". $pseudo ." (id ". $id .") s'est déconnecté de twinocoin (manuellement).";
            break;
            case '7':
                $action = "<span class=\"tc\">[". date("d-m-Y") ." à ". date("H:i") ."]</span> <span class=\"code\">[changement code appli]</span> - ". $pseudo ." (id ". $id .") a changé le code de son application (". $application .").";
            break;
            default:
                $action = "/!\ <span class=\"tc\">[". date("d-m-Y") ." à ". date("H:i") ."]</span> <span class=\"pseudo\">[action inconnue]</span> - pseudo : ". $pseudo ." - id : ". $id ." - montant = ". $montant ." - application :". $application .".";
            break;
        }
 
        try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
        catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
        $log = $bdd->prepare('INSERT INTO twinoid_twinocoin_log(action) VALUES(:action)');
        $log->execute(array('action' => $action));
        $log->closeCursor();
    }


    function debiter($id, $code, $id_twino, $montant)
    {
        try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
        catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

        $chercherappli = $bdd->prepare('SELECT clef FROM twinoid_twinocoin_applis where id_appli = :id') or die(print_r($bdd->errorInfo()));
        $chercherappli->execute(array('id' => $id));

        if ($trouver = $chercherappli->fetch()){
            if($trouver['clef'] == $code){
                $chercherjoueur = $bdd->prepare('SELECT * FROM twinoid_twinocoin where id_twino = :id_twino') or die(print_r($bdd->errorInfo()));
                $chercherjoueur->execute(array('id_twino' => $id_twino));

                if ($trouver = $chercherjoueur->fetch()){ // si le joueur existe
                    if($trouver['coins'] >= $coins){ // si le joueur a assez de pièces pour payer
                        $temps = time()+300; // timestamp de 5 minutes
                        $code = $id ."|". $code ."|". $id_twino ."|". $temps;
                        $codereturn = sha1($code);
                        $code .= "|". $montant; // le code contient l'id de l'appli, son code, l'id twino du joueur, le timestamp de dans 5 minutes et le montant.

                        $modifcodedebit = $bdd->prepare('UPDATE twinoid_twinocoin SET key_operation = :clef where id_twino = :id_twino') or die(print_r($bdd->errorInfo()));
                        $modifcodedebit->execute(array('clef' => $code, 'id_twino' => $id_twino));
                        return $codereturn;
                    }
                    else return "error4"; // Le joueur n'a pas assez de TC.
                }
                else return "error3"; // Joueur non inscrit sur twinocoin.
            }
            else return "error2"; // Clef incorrecte.
        }
        else return "error1"; // Application non trouvée.
    }

    function verifdebiter($id_twino, $codepost, $redirect){
        try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
        catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

        $chercherpaiement = $bdd->prepare('SELECT * FROM twinoid_twinocoin where id_twino = :id_twino') or die(print_r($bdd->errorInfo()));
        $chercherpaiement->execute(array('id_twino' => $id_twino));
        if ($trouver = $chercherpaiement->fetch()){

            list($id, $code, $id_twino, $temps, $montant) = explode("|", $trouver['key_operation']);
            $codereturncompare = sha1($id ."|". $code ."|". $id_twino ."|". $temps);
            if(time() <= $temps){ // si la requete est faite moins de 5 minutes après la génération du code

                $_SESSION['codepost'] = $codepost;
                if($codepost == $codereturncompare){ // si le code de confirmation est le bon

                    $chercherappli = $bdd->prepare('SELECT nom_appli FROM twinoid_twinocoin_applis where id_appli = :id_appli') or die(print_r($bdd->errorInfo()));
                    $chercherappli->execute(array('id_appli' => $id));
                    if ($trouver = $chercherappli->fetch()){

                        $_SESSION['montant'] = $montant;
                        $_SESSION['nomappli'] = $trouver['nom_appli'];

                            if(isset($_SESSION['id']) && $_SESSION['id'] == $id_twino)
                            {
                                return true;
                            }
                            else{
                                connect(2);
                            }
                        return false;
                    }
                return false;
                }
            return false;
            }
        return false;
        }
    return false;
    }

/*
    list($id, $code, $id_twino, $temps, $montant) = explode("|", $code);
    $codereturncompare = sha1($id ."|". $code ."|". $id_twino ."|". $temps);
*/

?>