<?php
session_start();

include('../../../config.php'); // le timezone
include 'lib.php'; // les fonctions sont dans le fichier lib.php
include 'contenu.php'; // le code html, pour être vraiment modulable, est contenu dans contenu.php :D

	if(isset($_GET['verif']) && isset($_GET['clef'])){ // vérification de paiement par l'application après que le joueur ait payé

		$id = htmlspecialchars($_GET['verif']);
		$clef = htmlspecialchars($_GET['clef']);

		echo verifier_operation($id, $clef);

	}

	if(isset($_GET['valid'])){
		include('../../../fonctions/connexionbdd.php'); // connexion à la bdd

		$coins = $_SESSION['coins'] - $_SESSION['montant']; // le nouveau montant est défini pour l'utilisateur
		if($coins >= 0){

			$chercherappli = $bdd->prepare('SELECT * FROM twinoid_twinocoin_applis where nom_appli = :nom_appli') or die(print_r($bdd->errorInfo()));
	        $chercherappli->execute(array('nom_appli' => $_SESSION['nomappli']));
	        if ($trouver = $chercherappli->fetch()){
	        	$retraits_effectues = $trouver['retraits_effectues'] + 1;
	        	$debitplus = $bdd->prepare('UPDATE twinoid_twinocoin_applis SET retraits_effectues  = :retraits where nom_appli = :nom_appli') or die(print_r($bdd->errorInfo()));
	        	$debitplus->execute(array('retraits' => $retraits_effectues, 'nom_appli' => $_SESSION['nomappli']));
	        	$key = $trouver['clef'];
	        }

			$debit = $bdd->prepare('UPDATE twinoid_twinocoin SET coins = :coins, key_operation = :key where id_twino = :id_twino') or die(print_r($bdd->errorInfo()));
	        $debit->execute(array('coins' => $coins, 'key' => $key, 'id_twino' => $_SESSION['id']));

	        savebdd(3, $_SESSION['pseudo'], $_SESSION['id'], $_SESSION['montant'], $_SESSION['nomappli'], $retraits_effectues); // fonction de log, type d'action 1 = regist, 2 = regist app, 3 = -TC, 4 = +TC, 5 = connect, 6 = deco - type.pseudo.id.montant.appli
	        header('Location:'.$_GET['valid']);
		}
		else{

			$erreur = $bdd->prepare('UPDATE twinoid_twinocoin SET key_operation = :key where id_twino = :id_twino') or die(print_r($bdd->errorInfo()));
	        $erreur->execute(array('key' => "poor", 'id_twino' => $_SESSION['id']));

			header('Location:'.$_GET['valid']);
		}
	}

    if(isset($_GET['debiter']) && isset($_GET['id']) && isset($_GET['code']) && isset($_GET['id_twino']) && isset($_GET['montant'])){ // si on paye on execute la fonction
        echo debiter($_GET['id'], $_GET['code'], $_GET['id_twino'], $_GET['montant']);
    }

    if(isset($_GET['payerdebit'])){
    	if(isset($_POST['id']) && isset($_POST['code']) && isset($_POST['redirect'])){
    		$_SESSION['id_appli_payer'] = $_POST['id'];
    		$_SESSION['code_appli_payer'] = $_POST['code'];
    		$_SESSION['redirect_appli_payer'] = $_POST['redirect'];
    	}
    	if(verifdebiter($_SESSION['id_appli_payer'], $_SESSION['code_appli_payer'], $_SESSION['redirect_appli_payer']) == true){
    		?>

				<!DOCTYPE html>
				<html>
					<head>
						<meta charset="utf-8">
						<title>Twinocoin - paiement</title>
						<link rel="stylesheet" href="design.css" />
						<link href='http://fonts.googleapis.com/css?family=Dosis|Hind|PT+Sans+Narrowr' rel='stylesheet'>
						<link rel="icon" href="favicon.ico" />
						<meta name="viewport" content="width=device-width" />
					</head>
				<body>
				<h1>Gestion de votre compte Twino<span class="koin">Coin</span></h1>
				<hr />
				<?php /* if(isset($_GET['inscrit'])) { ?>
					<p>Vous êtes maintenant enregistré sur Twinocoin, <span class="pseudo"><?php echo $_SESSION['pseudo']; ?></span>, vous avez reçu 25 <span class="tc">TC</span> et vous pouvez maintenant 
					autoriser des applications à vous débiter des <span class="tc">TC</span>.</p>
				<?php } */ ?>
				<p><a href="debiter.php?valid=<?php echo $_SESSION['redirect_appli_payer']; ?>" class="button">
				Confirmer le paiement de <?php echo $_SESSION['montant']; ?> TC à l'application <span class="deco"><?php echo $_SESSION['nomappli']; ?></span></a></p>
    		<?php
    	}

    }

?>