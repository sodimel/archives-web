<?php
session_start();

include 'lib.php'; // les fonctions sont dans le fichier lib.php
include 'contenu.php'; // le code html, pour être vraiment modulable, est contenu dans contenu.php :D

verifconnect();

if(isset($_GET['submitappli'])){

    postappli($_POST['id']);

}
if(isset($_GET['changercode'])){

    changecode($_GET['changercode']);

}

// début du code à proprement parler

    	afficherheader("TwinoCoin - Développeurs"); // On charge le header avec le title personnalisé.

?>

    <?php
    	switch (key($_GET)) { // la fonction d'affichage du texte selon la page sur laquelle on est

            case 'infos': // infos aux développeurs
            	paragraphe(3);
            break;

            case 'submit': // formulaire soumission d'une application
            	paragraphe(4);
            break;

            case 'applidone': // traitement de la soumission d'une application
                paragraphe(5);
            break;

            case 'erreurdeproprietaire': // si l'id du proprio de l'appli n'est pas l'id de la personne qui demande à lier l'appli
                paragraphe(6);
            break;

            case 'applideja': // si l'appli a déjà été soumise
                paragraphe(7);
            break;

            case 'appliname': // si l'appli a changé de nom
                paragraphe(8);
            break;

            case 'changementcode': // si on demande à changer le code de son appli
                paragraphe(9);
            break;
            default: // infos aux développeurs aussi
            	paragraphe(3);
            break;
        }

    merci();

    $fintemps = tempschargement(); ?>
    <i>La page a été générée en <?php echo round($fintemps-$debuttemps, 3); ?> seconde(s).</i></p>

</body>
</html>