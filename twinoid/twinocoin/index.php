<?php
session_start();

include('../../../config.php'); // le timezone
include('lib.php'); // les fonctions sont dans le fichier lib.php
include('contenu.php'); // le code html, pour être vraiment modulable, est contenu dans contenu.php :D

// début du code à proprement parler

verifconnect(); // vérification de la connexion et de la validité du token d'accès (si c'est plus valide on reconnecte)

    if(isset($_GET['valid']) && isset($_GET['code'])){ // si on se connecte
    	connect(1);
    }

    if(isset($_GET['deco'])) { // si on se déconnecte
    	if(isset($_SESSION['pseudo'])){ // si on est connecté... :P
    		savebdd(6, $_SESSION['pseudo'], $_SESSION['id'], 0, "", -1); // fonction de log, type d'action 1 = regist, 2 = regist app, 3 = -TC, 4 = +TC, 5 = connect, 6 = deco - type.pseudo.id.montant.appli
    	}
    	session_destroy();
    	header("Location:http://l3m.in/p/twinoid/twinocoin/");
    }

    if(isset($_GET['inscrit'])) {
    	afficherheader("TwinoCoin - Inscription terminée !"); // On charge le header avec le title personnalisé.
    }
    else{
    	afficherheader("TwinoCoin"); // On charge le header avec le title personnalisé.
    }

?>


<?php
    switch (key($_GET)) { // la fonction d'affichage du texte selon la page sur laquelle on est
        case 'error':
            if ($_GET['error'] == 'access_denied'){ // si on est sur la page d'erreur
        		paragraphe(0);
        	}
        break;

        case 'connect': // si on vient de se connecter
        	paragraphe(1);
        break;

        case 'inscrit': // si on vient de s'inscrire
        	paragraphe(2);
        break;

        default: // la base, le texte explicatif :)
        	paragraphe(42);
        break;
    }

    merci();
    
    $fintemps = tempschargement(); ?>
    <i>La page a été générée en <?php echo round($fintemps-$debuttemps, 3); ?> seconde(s).</i></p>

</body>
</html>