<?php
session_start();
include('../../../config.php');
include('functions.php');

if(isset($_GET['suggestion']) && isset($_SESSION['pseudo-twinoid']) && isset($_SESSION['id-twinoid'])){
	header('Location:http://l3m.in/p/twinoid/ast/?suggerer');
}

if(isset($_GET['connect']) && isset($_GET['code'])){
	connect();
	if($_GET['state'] == "mod"){
	header('Location:http://l3m.in/p/twinoid/ast/mod.php');
	}
	else{
	header('Location:http://l3m.in/p/twinoid/ast/?suggerer');
	}
}

if(isset($_GET['soumettrelien']) && isset($_POST['lien'])){
	poster_lien(htmlspecialchars($_POST['lien']));
	header('Location:http://l3m.in/p/twinoid/ast/?cimer');
}


?>
<!DOCTYPE html>
<html>
   <head>
		<title>Aujourd'hui sur Twinoid</title>
		<meta charset="utf-8">
		<meta name="description" content="Liste des numéros d'Aujourd'hui sur Twinoid." />
		<link rel="stylesheet" href="design.css" />
		<link href='https://fonts.googleapis.com/css?family=Poiret+One|Cabin' rel='stylesheet' type='text/css'>
		<link rel="icon" href="favicon.ico" />
		<meta name="viewport" content="width=device-width" />
	</head>
<body>
	<h1>Aujourd'hui sur Twinoid</h1>
	<p class="small soustitre">Pour ne rien manquer des posts "intéressants" de votre Nexus et être notifié à chaque nouveau post, abonnez-vous au <a href="http://twinoid.com/ev/38406448">post dédié</a> !</p>

<?php if(isset($_GET['suggestion'])){ ?>

	<p>
		<b>Suggérer un lien pour AsT :</b><br />
		Vous allez être redirigé vers Twinoid (vérification de votre identité oblige). Si c'est la première fois que vous suggérez un lien, autorisez l'application.
	</p>

	<p>
		<a class="bouton" href="https://twinoid.com/oauth/auth?response_type=code&client_id=224&redirect_uri=http%3A%2F%2Fl3m.in%2Fp%2Ftwinoid%2Fast%2F%3Fconnect&scope=contacts&state=">Suggérer un lien</a>
	</p>

<?php } elseif(isset($_GET['suggerer']) && isset($_SESSION['pseudo-twinoid']) && isset($_SESSION['id-twinoid'])) { ?>

	<p>
		Tu es connecté, <span title="id <?php echo $_SESSION['id-twinoid']; ?>"><?php echo $_SESSION['pseudo-twinoid']; ?></span>.<br />
		Tu n'as plus qu'à entrer le lien que tu aimerais voir paraitre dans AsT :)
	</p>

	<form method="post" action="?soumettrelien">
		<div style="style=display: inline-block; width: 250px; margin: auto;">
		<input type="lien" name="lien" maxlength="50" class="input" style="width: 250px; height: 40px;" required />
		</div><br />
		<br />		
		<input class="bouton" type="submit" value="Soumettre le lien" />
		<a href="http://l3m.in/p/twinoid/ast/" class="bouton" style="font-size: 0.8em; width: 160px;">Retour à l'accueil</a>
	</form>


<?php } else {


if(isset($_GET['cimer']) && isset($_SESSION['pseudo-twinoid'])){
?>
	<p>
		Merci pour ta soumission, <?php echo $_SESSION['pseudo-twinoid']; ?> ! Jvais regarder ça bientôt et ça apparaitra peut-être dans le prochain AsT <img src="http://data.twinoid.com/img/smile/square/smile.png" alt=":)" />
	</p>

<?php } ?>

	<p><b>Liste des posts d'AsT :</b><br /><br />
		<?php
			ecrire_liste_ast();
		?>
	</p>

	<p class="small" style="opacity: 0.7">
	<a href="?suggestion">Suggérer un post pour AsT</a> (<i>déjà <?php ecrire_nb_liens(); ?> liens soumis !</i>) &bull; AsT est un projet de <a href="http://twinoid.com/user/12661">sodimel</a> <img src="http://data.twinoid.com/img/smile/square/calim.png" alt="calim" />
	</p>

<?php } ?>
	
</body>
</html>