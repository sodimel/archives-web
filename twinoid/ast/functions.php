<?php
/*
	Fonction de connnexion à twinoid pour récupérer les infos
*/
	function connect()
	{
		$gettoken = token(); // fonction token qui récupère... le token depuis twinoid.
		$_SESSION['token'] = $gettoken->access_token;

		$infos = post($_SESSION['token']); // fonction post qui poste le token pour récupérer des infos de la personne qui se connecte

		$_SESSION['id-twinoid'] = $infos->id;
		$_SESSION['pseudo-twinoid'] = $infos->name;
	}
/*
	Fonction de récupération de token pour chercher les infos
*/
	function token()
	{
		$url = "https://twinoid.com/oauth/token";
		$id = 224;
		$secret = "";
		$redirect = "http%3A%2F%2Fl3m.in%2Fp%2Ftwinoid%2Fast%2F%3Fconnect";

		$contextOptions = array( // on demande un token pour récup les infos
		   'http' => array(
		   'method' => 'POST',
		   'header' => 'Content-type: application/x-www-form-urlencoded',
		   'content' => "client_id=".$id."&client_secret=".$secret."&redirect_uri=".$redirect."&code=".$_GET['code']."&grant_type=authorization_code"
		   )
		);
		$context = stream_context_create($contextOptions);
		$resultat = file_get_contents($url, false, $context);
		$resultat = json_decode($resultat);
		return $resultat;
	}
/*
	Récupération d'informations depuis le site de la MT.
*/
	function post($token) // on récupère l'id et le pseudo depuis le site de la MT
	{
		$url = "https://twinoid.com/graph/me?fields=id,name&access_token=".$token."";
		$json = file_get_contents($url);
		$json = json_decode($json);
		return $json;
	}

	function poster_lien($lien)
	{
		include('../../../fonctions/connexionbdd.php');
		$inscrire_lien = $bdd->prepare('INSERT INTO twinoid_ast_propositions(temps, id_twinoid, pseudo_twinoid, lien) VALUES(:temps, :id_twinoid, :pseudo_twinoid, :lien)');
		$inscrire_lien->execute(array('temps' => time(), 'id_twinoid' => $_SESSION['id-twinoid'], 'pseudo_twinoid' => $_SESSION['pseudo-twinoid'], 'lien' => $lien));
	}

	function afficher_liste(){
		include('../../../fonctions/connexionbdd.php');
		$liens = $bdd->query('SELECT * FROM twinoid_ast_propositions ORDER BY id DESC');
		$i = 0;
		while($a = $liens->fetch()){
			$i++;
			?>&bull; Lien de <a href="http://twinoid.com/user/<?php echo $a['id_twinoid']; ?>"><?php echo $a['pseudo_twinoid']; ?></a>, soumis le 
			<?php echo date('d/m/Y \à H:i:s', $a['temps']) ?> : <a href="<?php echo $a['lien']; ?>"><?php echo $a['lien']; ?></a>.<br /><?php
		}
		if($i == 0){
			?>
			&bull; Pas encore de lien suggéré pour aujourd'hui !
			<?php
		}
	}

	function ecrire_nb_liens(){
		include('../../../fonctions/connexionbdd.php');
		$liens = $bdd->query('SELECT id FROM twinoid_ast_propositions ORDER BY id DESC');
		if($a = $liens->fetch()){
			echo $a['id'];
		}
	}

	function vider_table(){
		include('../../../fonctions/connexionbdd.php');
		$vider = $bdd->query("DELETE FROM twinoid_ast_propositions");  
	}

	function ajouter_ast($lien){
		include('../../../fonctions/connexionbdd.php');
		$inscrire_lien = $bdd->prepare('INSERT INTO twinoid_ast_liste(link, date) VALUES(:link, :date)');
		$inscrire_lien->execute(array('link' => $lien, 'date' => time()));
	}

	function ecrire_liste_ast(){
		include('../../../fonctions/connexionbdd.php');
		$liens = $bdd->query('SELECT * FROM twinoid_ast_liste ORDER BY id');
		while($a = $liens->fetch()){
			$i++;
			?>
				&bull; <a href="<?php echo $a['link']; ?>" target="_blank">AsT #<?php echo $a['id']; ?></a> - <i><?php echo date('d/m/Y', $a['date']) ?></i>.<br />
			<?php
		}
	}

?>