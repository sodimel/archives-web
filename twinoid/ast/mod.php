<?php
session_start();
include('../../../config.php');
include('functions.php');

if(isset($_GET['vidertable']) && isset($_SESSION['id-twinoid']) && $_SESSION['id-twinoid'] == "12661"){
	vider_table();
}

if(isset($_GET['ajouterast']) && isset($_SESSION['id-twinoid']) && $_SESSION['id-twinoid'] == "12661"){
	ajouter_ast($_POST['lien']);
}

?>
<!DOCTYPE html>
<html>
   <head>
		<title>AsT - mod</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="design.css" />
		<link href='https://fonts.googleapis.com/css?family=Poiret+One|Cabin' rel='stylesheet' type='text/css'>
		<link rel="icon" href="favicon.ico" />
		<meta name="viewport" content="width=device-width" />
	</head>
<body>

	<h1>Aujourd'hui sur Twinoid</h1>
	<p class="small soustitre">Modération. Retour à l'accueil en cliquant sur <a href="http://l3m.in/p/twinoid/ast/">ce lien</a> !</p>


<?php if(!isset($_SESSION['pseudo-twinoid']) && !isset($_SESSION['id-twinoid'])) { ?>

	<p>
		<a class="bouton" href="https://twinoid.com/oauth/auth?response_type=code&client_id=224&redirect_uri=http%3A%2F%2Fl3m.in%2Fp%2Ftwinoid%2Fast%2F%3Fconnect&scope=contacts&state=mod">Se connecter à la modération AsT</a>
	</p>

<?php } elseif(isset($_SESSION['pseudo-twinoid']) && isset($_SESSION['id-twinoid']) && $_SESSION['id-twinoid'] == "12661") {
	if(isset($_GET['vidertable'])){
		?>
			<p>
				Table vidée !
			</p>
		<?php
	}
	if(isset($_GET['ajouterast'])){
		?>
			<p>
				AsT ajouté dans la BDD, bien joué sodimel :)
			</p>
		<?php
	} ?>

	<p>
		Tu es connecté, <?php echo $_SESSION['pseudo-twinoid']; ?>.<br />
	</p>

	<form method="post" action="?ajouterast">
		<div style="style=display: inline-block; width: 250px; margin: auto;">
		<input type="lien" name="lien" maxlength="50" class="input" style="width: 250px; height: 40px;" required />
		</div><br />
		<br />		
		<input class="bouton" type="submit" value="Envoyer l'AsT" />
	</form>

	<p><b>Liste des liens suggérés :</b><br /><br />
		<?php afficher_liste(); ?>
	</p>

	<p>
		Modèle de AsT :
	</p>

	<pre>**[link=http://l3m.in/p/twinoid/ast/]Aujourd'hui sur Twinoid[/link] :**&lt;code&gt;&lt;/code&gt;
TEXTE TEXTE
&lt;code&gt;&lt;/code&gt;&lt;question&gt;
Nombre de vues : 
&lt;/question&gt;[aparte]A plus tard pour un autre post récapitulatif ![/aparte]</pre>

<p class="small">
	<a href="?vidertable">Vider les liens</a>.
</p>

<?php } else { ?>

	<p>
		Tu es connecté, <?php echo $_SESSION['pseudo-twinoid']; ?>, mais tu n'as pas les droits pour voir le contenu de cette page. Toutefois et si tu en as envie, go envoyer un mp à sodimel pour savoir si tu peux être modérateur des 
		liens soumis par les twinoidiens pour AsT.
	</p>

<?php } ?>

</body>
</html>