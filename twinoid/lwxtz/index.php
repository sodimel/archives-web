<!DOCTYPE html>
<html>
   <head>
        <meta charset="utf-8">
        <title>Alphabounce - lwxtz2004</title>
        <link rel="stylesheet" href="design.css" />
        <meta name="viewport" content="width=device-width" />
        <link href="http://www.alphabounce.com/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
        <link href='http://fonts.googleapis.com/css?family=Walter+Turncoat' rel='stylesheet' type='text/css'>
    </head>
<body>
	<div id="page">
		<div id="h1"><a href="http://twinoid.com/user/17226" title="lien vers son profil"><img src="h1.png" alt="logo" /></a></div>
		<p>Lw' et plein d'autres gens ont fait des fichiers statistiques concernant alphabounce; en voici quelques uns.</p>

		<p>
			&bull; <a href="abconcours.xls" title="">ABConcours</a>, relatif à la quête PID de lw. <small>[<?php echo date("d/m/Y à h:i:s", filemtime("abconcours.xls")); ?>]</small><br />
			&bull; <a href="labcdab.xls" title="">l'ABC d'AB</a>, par <a href="https://twinoid.com/user/258137" title="lien vers sont profil Twinoid">Vifargent</a> et relatif au concours sur les bonus imaginaires. <small>[<?php echo date("d/m/Y à h:i:s", filemtime("labcdab.xls")); ?>]</small><br />
			&bull; <a href="abcazutil.xls" title="">ABCazutil</a>. <small>[<?php echo date("d/m/Y à h:i:s", filemtime("abcazutil.xls")); ?>]</small><br />
			&bull; <a href="abcimetiere.xls" title="">ABCimetière</a>. <small>[<?php echo date("d/m/Y à h:i:s", filemtime("abcimetiere.xls")); ?>]</small><br />
			&bull; <a href="abcommercants.xls" title="">ABCommerçants</a> (fichier créé par <a href="https://twinoid.com/user/1407470" title="lien vers son profil Twinoid">MacLambert</a> à la base). <small>[<?php echo date("d/m/Y à h:i:s", filemtime("abcommercants.xls")); ?>]</small><br />
			&bull; <a href="abcomparatif.xls" title="">ABComparatif</a>. <small>[<?php echo date("d/m/Y à h:i:s", filemtime("abcomparatif.xls")); ?>]</small><br />
			&bull; <a href="abcuriosites.xls" title="">ABCuriosités</a>. <small>[<?php echo date("d/m/Y à h:i:s", filemtime("abcuriosites.xls")); ?>]</small><br />
			&bull; <a href="destructrices.xls" title="">Molécules destructrices</a> (fichier créé par <a href="http://www.alphabounce.com/user/331814" title="lien vers son profil Alphabounce">Cappuccino</a> à la base). <small>[<?php echo date("d/m/Y à h:i:s", filemtime("destructrices.xls")); ?>]</small>
		</p>

		<footer>
			<p><a href="http://twinoid.com/user/17226" title="lien vers son profil">Lien vers le profil de Lwxtz2004</a> - 
			Les fichiers appartiennent à Lwxtz2004, MacLambert, Cappuccino et à Vifargent, leur contenu est relatif à 
			<a href="http://alphabounce.com" title="">Alphabounce</a>, un jeu de la Motion Twin. Page hébergée sur le 
			<a href="http://www.l3m.in/" title="Lien vers l'accueil du l3m website.">l3m website</a>.</p>
		</footer>
	</div>
	<div class="footer-space"></div>
</body>
</html>