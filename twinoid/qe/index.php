<?php if ($_GET['p'] == "generemoncodeespecedeprogramme") {
if ($_POST['lien'] == "") {
$code = '**Question existentielle #'.$_POST["id"].' :**<br /><br />
'.$_POST["question"].'<br /><br />
&lt;question&gt;<br />
'.nl2br($_POST["reponses"]).'
<br />&lt;/question&gt;<br /><br />

//Ne votez pas si vous avez juste envie de voir les votes ! En effet le sondage est ouvert et un bouton "voir les votes" est disponible.//<br />
[spoil]Bla bla likez/partagez si vous le souhaitez, bla bla pub pour ce sondage, bla bla merci.<br />
//Code généré de manière semi automatique par [link=http://l3m.in/p/twinoid/qe/]cette[/link] page.//[/spoil]';

$lien = "**Question existentielle #".$_POST["id"]." :**\n
".$_POST['question']."\n
<question>
".$_POST['reponses']."</question>

//Ne votez pas si vous avez juste envie de voir les votes ! En effet le sondage est ouvert et un bouton «voir les votes» est disponible.//
[spoil]Bla bla likez/partagez si vous le souhaitez, bla bla pub pour ce sondage, bla bla merci.
//Code généré de manière semi automatique par [link=http://l3m.in/p/twinoid/qe/]cette[/link] page.//[/spoil]";
}
else
{
$code = '**Question existentielle #'.$_POST["id"].' :**<br /><br />
'.$_POST["question"].'<br /><br />
&lt;question&gt;<br />
'.nl2br($_POST["reponses"]).'
<br />&lt;/question&gt;<br /><br />

//Ne votez pas si vous avez juste envie de voir les votes ! En effet le sondage est ouvert et un bouton "voir les votes" est disponible.//<br />
[spoil]Bla bla likez/partagez si vous le souhaitez, bla bla pub pour ce sondage, bla bla merci.<br />
//Code généré de manière semi automatique par [link=http://l3m.in/p/twinoid/qe/]cette[/link] page.//[/spoil]<br /><br />

[aparte]Vous pouvez toujours répondre à la [link='.$_POST["lien"].']question existentielle précédente[/link] ![/aparte]';

$lien = "**Question existentielle #".$_POST["id"]." :**\n
".$_POST['question']."\n
<question>
".$_POST['reponses']."</question>

//Ne votez pas si vous avez juste envie de voir les votes ! En effet le sondage est ouvert et un bouton «voir les votes» est disponible.//
[spoil]Bla bla likez/partagez si vous le souhaitez, bla bla pub pour ce sondage, bla bla merci.
//Code généré de manière semi automatique par [link=http://l3m.in/p/twinoid/qe/]cette[/link] page.//[/spoil]

[aparte]Vous pouvez toujours répondre à la [link=".$_POST['lien']."]question existentielle précédente[/link] ![/aparte]";
}
} ?>
<!DOCTYPE html>
<html>
   <head>
        <title>Gestion mise en page QE</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="design.css" />
        <link href='http://fonts.googleapis.com/css?family=Della+Respira|Share+Tech+Mono' rel='stylesheet' type='text/css'>
    </head>
<body>
<div id="page">
<div id="banner"><a href="http://l3m.in/p/twinoid/qe/"><img src="ban1.png" alt="generateur de mise en page de QE" /></a><a href="http://twinoid.com/user/12661"><img src="ban2.png" alt="par sodimel" /></a></div>
<p>Cette page sert à générer le code d'un sondage sur le thème des questions existentielles (que j'ai été le premier à utiliser). A la base seulement développé 
pour moi, je décide de partager ce... truc, histoire que tout le monde puisse se mettre un peu à ce type de sondage.</p>
<?php if ($_GET['p'] == "generemoncodeespecedeprogramme") { ?>

<p>Voici votre code à copier-coller :</p>
<p class="grey"><?php echo $code; ?></p>
<p>N'oubliez pas de cliquer sur ce bouton une fois votre sondage posté : <img src="publier.png" alt="bouton publier les votes" />, parce que sinon après les gens ils gueulent.<br />
<i>Vous voulez poster votre sondage directement sur votre nexus ? <a href="http://twinoid.com/tid/nexus?t=<?php echo urlencode($lien); ?>">Cliquez ici</a>.</i></p>

<?php } else { ?>
<form method="POST" action="?p=generemoncodeespecedeprogramme">
<p>Numéro (ou titre) de la question : <input required class="input" type="text" name="id" maxlength="42" /><br /><br />
<textarea name="question" rows="4" placeholder="Entrez votre question ici." required ></textarea> 
<textarea name="reponses" rows="4" placeholder="Entrez les réponses du sondage ici." required ></textarea>
<textarea name="lien" rows="4" placeholder="Lien de la précédente QE (laissez vide s'il n'y a pas de question)." ></textarea>
<br /><input type="submit" value="Poster" /> &bull; <input type="reset" value="Réinitialiser" />
</p></form>
<?php } ?>
</div>
</body>
</html>