<?php
	include('functions.php');
?>
<!DOCTYPE html>
<html>
   <head>
		<title>Map du serveur secret.</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="design.css" />
		<link rel="stylesheet" href="dots.css" />
		<link href='https://fonts.googleapis.com/css?family=Josefin+Sans' rel='stylesheet' type='text/css'>
		<link rel="icon" href="favicon.ico" />
		<meta name="viewport" content="width=device-width" />
	</head>
<body>
	<h1>Map interactive du serveur sercret.</h1>

<?php if(isset($_GET['view'])){ ?>
<p>«« <a href="http://l3m.in/p/mc/">Retour à la map</a>.</p>
	<?php
		afficher_description($_GET['view']);

	}
	else{ ?>

	<div id="map">

		<?php afficher_points_verts(); ?>

	</div>

	<p>
		&bull; Faut cliquer sur les petits ronds de couleur verte.<br />
		&bull; Dans le futur : yaura des galeries de photos/des liens vers des vidéos/des gros pavés explicatifs.
	</p>
	<p>
		&bull; <i><a href="http://goo.gl/forms/hMzHD97n2m" target="_blank">NOMMEZ LE SERVEUR</a></i>
	</p>

<?php } ?>

	<p class="small right">Un projet de <a href="http://l3m.in">sodimel</a>.<br />
	<a href="http://imgur.com/a/yrC4H">Lien</a> vers les screenshots utilisé dans les descriptions + des bonus.<br />
	<a href="http://i.imgur.com/QDA87DI.jpg" target="_blank">AHAH ON A TROUV2 UNE JUNGLE</a>.</p>

</body>
</html>