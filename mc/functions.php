<?php

function nb_lignes($chemin){
	$nom = fopen($chemin, 'r');
	$contenu = fread($nom, filesize($chemin));
	return substr_count($contenu, "\n");
}

function afficher_points_verts(){
	$contenu = file("pointsverts.txt");
	$nb_actions = nb_lignes("pointsverts.txt");

	$i = 0;
	$id = 0;
	while($i < $nb_actions){
		$contenu[$i] = str_replace("\n", "", $contenu[$i]);
		$id++;
		?>
<div class="dot_position <?php echo $contenu[$i]; ?>">
			<span>
				<input type="checkbox" id="<?php echo $id; ?>" class="checkbox" />
				<label for="<?php echo $id; ?>" class="label"></label>
				<span class="infobulle">
				<?php
				$nb_onglets = substr_count($contenu[$i+1], ".");
				$onglets = explode(".",$contenu[$i+1]);

				$j = 0;
				while($j < $nb_onglets){
					if($j > 1){ echo "<hr />"; }
					str_replace("\n", "", $onglets[$j]);
				?>
<a href="?view=<?php echo $onglets[$j]; ?>"><?php echo $onglets[$j+1]; ?></a>
				<?php 
				$j = $j+2;
				}
				?>
</span>
			</span>
		</div>
		<?php
	$i = $i+2;
	}
}

function afficher_description($chemin){
	$chemin = htmlspecialchars($chemin .".txt");
	$nom = fopen($chemin, 'r');
	$contenu = fread($nom, filesize($chemin));
	echo $contenu;
}




?>