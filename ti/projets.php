<?php include("header.php");
$id = intval(htmlspecialchars($_GET["projet"]));
?>
<h1>Ti<sup> Projet <?php echo $id; ?></sup></h1>
<?php
try
{
    $bdd = new PDO('mysql:host=;dbname=', '', '');
} catch (Exception $e)
{ die('Erreur : ' . $e->getMessage()); }
$liste = $bdd->query('SELECT * FROM `programmes` WHERE `id` = '. $id .'') or die(print_r($bdd->errorInfo()));
if ($entree = $liste->fetch()) { ?>
<p><b>Projet #<?php echo $entree['id']; ?></b>, c'est un programme de type <?php echo $entree['type']; ?>, mis en ligne le <?php echo $entree['date']; ?>.<br />
<b>Description globale :</b> <?php echo $entree['description']; ?></p>
<pre><?php echo $entree['code']; ?></pre>
<p><?php 
if ($entree['lienpreview'] != "") { ?><a href="<?php echo $entree['lienpreview']; ?>">Lien vers la vidéo de démo</a>.<?php ; }
else { echo "Aucune vidéo d'aperçu disponible."; }?></p>
<?php }
else {?><p>Ce projet n'existe pas (ou plus), son id (<i><?php echo $id; ?></i>) n'est associée à rien d'exploitable dans la base de données.</p>
<?php } ?>
<p>&bull; <a href="http://l3m.in/p/projets/ti/" title="retour">Retour à la liste des projets</a>.</p>