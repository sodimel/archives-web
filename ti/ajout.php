<?php if (isset($_POST['mdp']) && ($_POST['mdp'] == "unmotdepasse")) {
setcookie("connect", "connect", time()+3600);
header('Location: http://l3m.in/p/projets/ti/ajout.php');
}
if ($_GET['p'] == "logout") {

setcookie("connect", "connect", time()-3600);
header('Location: http://l3m.in/p/projets/ti/ajout.php');

}
if ($_GET['p'] == "ajout2") {

$date = date("d").'/'.date("m").'/'.date("Y");
$description = $_POST['description'];
$code = $_POST['programme'];
$code = ":".$code;
$code = preg_replace("#\n#", "\n:", $code);
try
{
    $bdd = new PDO('mysql:host=;dbname=', '', '');
} catch (Exception $e)
{ die('Erreur : ' . $e->getMessage()); }
$req = $bdd->prepare('INSERT INTO programmes(description, type, date, lienpreview, code) VALUES(:description, :type, :date, :lienpreview, :code)');
$req->execute(array(
    'description' => $description,
    'type' => $_POST['type'],
    'date' => $date,
    'lienpreview' => $_POST['preview'],
    'code' => $code
    ));
header('Location: http://l3m.in/p/projets/ti/');

}

if ($_GET['p'] == "modif2") {

try
{
    $bdd = new PDO('mysql:host=;dbname=', '', '');
} catch (Exception $e)
{ die('Erreur : ' . $e->getMessage()); }
$req = $bdd->prepare('SELECT * FROM programmes WHERE id = ?');
$req->execute(array($_POST['id']));
if ($donnees = $req->fetch()) {
}
$description = $donnees['description'];
$type = $donnees['type'];
$date = $donnees['date'];
$lienpreview = $donnees['lienpreview'];
$code = $donnees['code'];
}

if ($_GET['p'] == "modif3") {

try
{
    $bdd = new PDO('mysql:host=;dbname=', '', '');
} catch (Exception $e)
{ die('Erreur : ' . $e->getMessage()); }
$req = $bdd->prepare('UPDATE programmes SET description = :descr, type = :type, lienpreview = :lien, code = :code, date = :date WHERE id = :id');
$req->execute(array(
	'descr' => $_POST['description'],
	'type' => $_POST['type'],
	'lien' => $_POST['preview'],
	'code' => $_POST['programme'],
	'date' => $_POST['date'],
	'id' => $_POST['id'],
	));
header('Location: http://l3m.in/p/projets/ti/ajout.php');

}
include("header.php"); ?>

<body>
<div id="page">
<h1>Ti<sup> ajout</sup></h1>
<?php if (isset($_COOKIE['connect']) && ($_COOKIE['connect'] = "connect")) { 
if ($_GET['p'] == "ajout1") { // Ajout du programme : remplissage des entrées. ?>

<p>Ajout d'un programme.</p>
<form method="POST" action="ajout.php?p=ajout2">
<p style="text-align: center;">Type <i>(calcul/divertissement/jeu)</i> :<br />
<input class="input" type="text" name="type" maxlength="75" required /><br />
Lien Preview :<br />
<input class="input" type="text" name="preview" maxlength="75" /><br />
Description :<br /><textarea name="description" required></textarea><br />
Programme :<br /><textarea name="programme" required></textarea><br />
<input type="submit" value="Envoyer" />
</p></form>

<?php } elseif ($_GET['p'] == "modif1") { ?>
<p>Modification d'un programme.</p>

<form method="POST" action="ajout.php?p=modif2">
<p style="text-align: center;">Id de l'entrée à modifier :<input class="input" type="text" name="id" maxlength="2" required />
<input type="submit" value="Envoyer" />
</p></form>

<?php } elseif ($_GET['p'] == "modif2") { ?>
<p>Modification d'un programme.</p>

<form method="POST" action="ajout.php?p=modif3">
<p style="text-align: center;">Type <i>(calcul/divertissement/jeu)</i> :<br />
<input class="input" type="text" name="type" maxlength="75" value="<?php echo $type; ?>" required /><br />
Lien Preview :<br />
<input class="input" type="text" name="preview" maxlength="75" value="<?php echo $lienpreview; ?>" /><br />
Description :<br /><textarea name="description" required><?php echo $description; ?></textarea><br />
Programme :<br /><textarea name="programme" required><?php echo $code; ?></textarea><br />
Date <i>(ne pas modifier)</i> :<br />
<input class="input" type="text" name="date" maxlength="75" value="<?php echo $date; ?>" required /><br />
<input type="hidden" name="id" value="<?php echo $_POST['id'] ?>" />
<input type="submit" value="Envoyer" />
</p></form>

<?php } elseif ($_GET['p'] == "supp1") { ?>
<p>Suppression d'un programme.</p>
<?php } ?>
<p>&bull; <a href="ajout.php?p=ajout1" title="ajouter">Ajout d'un programme</a><br />
&bull; <a href="ajout.php?p=modif1" title="modifier">Modifier un programme</a><br />
&bull; <a href="ajout.php?p=supp1" title="supprimer">Supprimer un programme</a><br /><br />
&bull; <a href="ajout.php?p=logout" title="deconnexion">Se deconnecter</a><br />
&bull; <a href="http://l3m.in/p/projets/ti/" title="retour">Retour a l'index</a></p>
<?php } else { ?>
<p>Connexion :<br /></p>
<form method="POST" action="ajout.php">
<p>Mot de passe : <input class="input" type="password" name="mdp" id="mdp" maxlength="75" />
</p></form>
<?php } ?>
</div>
</body>
</html>