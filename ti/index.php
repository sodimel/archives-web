<?php include("header.php"); ?>

<?php if ($_GET['p'] == "astuces") { // Astuces de codage ?>

<h1>Ti<sup> astuces</sup></h1>
<p>&bull; En langage TiBASIC on n'est pas obligé de fermer les parenthèses des fonctions (genre <i>For(A,B,C</i> est une écriture valide qui sera interpretée correctement).<br />
&bull; Un système de défilement du texte est parfois mis en place dans le programmes exposés (un <i>Disp</i> suivi de plein de <i>""</i> vides), il n'est absolument pas nécessaire 
et peut ne pas être incorporé au programme (mais ce sera moins joli du coup).
</p>
<p>&bull; <a href="index.php">Retour accueil</a>.<br /></p>

<?php } elseif ($_GET['p'] == "infos") { // Informations sur la page ?>

<h1>Ti<sup> infos</sup></h1>
<p>Projet de page par Corentin Bettiol. Bientôt : mise en place d'une BDD, mise en place d'un aperçu (sans doute sous forme de .gif)  des programmes en exécution.<br />
Ce code est testé uniquement sous <i>Ti82 stat.fr</i>. 
Il est possible que le code ne fonctionne pas sous les autres versions des calculettes Ti (notamment à cause de la traduction partielle des fonctions).</p>
<div id="alexis"> </div><p>Puisqu'il a demandé ça je me suis amusé un peu :<br />
<?php if (isset($_COOKIE['canard']) && ($_COOKIE['canard'] = "canard")) { ?>
<a href="index.php?p=changenormal">Changer le fond de la page</a>
<?php } else { ?>
<a href="index.php?p=changecanard">Changer le fond de la page</a>
<?php } ?>.</p>
<p>&bull; <a href="index.php">Retour accueil</a>.<br /></p>

<?php } else { // Accueil de la page ?>

<h1>Ti<sup> programmes</sup></h1>
<p>Bienvenue sur cette page web. <br /><br />
<b>Liste des projets :</b><br />
<?php
try
{
    $bdd = new PDO('mysql:host=;dbname=', '', '');
} catch (Exception $e)
{ die('Erreur : ' . $e->getMessage()); }
$liste = $bdd->query('SELECT `id`, `date` FROM `programmes`') or die(print_r($bdd->errorInfo()));
while ($entree = $liste->fetch()) { ?>
&bull; <a href="projets.php?projet=<?php echo $entree[id]; ?>">Projet #<?php echo $entree[id]; ?></a> (upload le <?php echo $entree[date]; ?>).<br />
<?php } ?></p>

<p><b>News 17/04/14 :</b> MàJ en cours de la page, ça devrait bientôt être fonctionnel :)<br />
<b>*</b>actualisation : le site est fonctionnel.<br />
<b>News 14/10/13 :</b> L'ajout des programmes est désormais automatisé ! La visualisation l'est aussi ainsi que la liste des programmes créés.</p>
<p>&bull; <a href="?p=astuces">Astuces de codage sous TiBASIC</a>.<br />
&bull; <a href="?p=infos">Informations sur cette page</a>.<br /></p>
<?php } ?>
</div>
</body>
</html>