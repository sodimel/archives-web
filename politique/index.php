<?php
include('../../fonctions/connexionbdd.php');


if(isset($_GET['vote']) && is_numeric($_GET['vote'])){
	$cherchervote = $bdd->prepare('SELECT * FROM politique where ip = :ip') or die(print_r($bdd->errorInfo()));
	$cherchervote->execute(array('ip' => $_SERVER["REMOTE_ADDR"]));
	if ($trouver = $cherchervote->fetch()){
		?>
		<p class="good">Tu as déjà voté :c</p>
		<?php
	}
	else{
		$voter = $bdd->prepare('INSERT INTO politique(ip, vote) VALUES(:ip, :vote)');
		$voter->execute(array('ip' => $_SERVER["REMOTE_ADDR"], 'vote' => htmlspecialchars($_GET['vote'])));
		?>
		<p class="bad">Vote enregistré ! :)</p>
		<?php
	}
}



$cherchervote = $bdd->prepare('SELECT * FROM politique where ip = :ip') or die(print_r($bdd->errorInfo()));
$cherchervote->execute(array('ip' => $_SERVER["REMOTE_ADDR"]));
if ($trouver = $cherchervote->fetch()){
	$votes1 = $bdd->query('SELECT count(id) FROM politique where vote = 1');
	if($vote1 = $votes1->fetch()){$vot1 = true;}
	else{$vot1 = false;}

	$votes2 = $bdd->query('SELECT count(id) FROM politique where vote = 2');
	if($vote2 = $votes2->fetch()){$vot2 = true;}
	else{$vot2 = false;}

	$votes3 = $bdd->query('SELECT count(id) FROM politique where vote = 3');
	if($vote3 = $votes3->fetch()){$vot3 = true;}
	else{$vot3 = false;}

	$votes4 = $bdd->query('SELECT count(id) FROM politique where vote = 4');
	if($vote4 = $votes4->fetch()){$vot4 = true;}
	else{$vot4 = false;}

	$votes5 = $bdd->query('SELECT count(id) FROM politique where vote = 5');
	if($vote5 = $votes5->fetch()){$vot5 = true;}
	else{$vot5 = false;}

	$votes6 = $bdd->query('SELECT count(id) FROM politique where vote = 6');
	if($vote6 = $votes6->fetch()){$vot6 = true;}
	else{$vot6 = false;}

	$votes7 = $bdd->query('SELECT count(id) FROM politique where vote = 7');
	if($vote7 = $votes7->fetch()){$vot7 = true;}
	else{$vot7 = false;}

	$votes8 = $bdd->query('SELECT count(id) FROM politique where vote = 8');
	if($vote8 = $votes8->fetch()){$vot8 = true;}
	else{$vot8 = false;}

	$votes9 = $bdd->query('SELECT count(id) FROM politique where vote = 9');
	if($vote9 = $votes9->fetch()){$vot9 = true;}
	else{$vot9 = false;}

	$votes10 = $bdd->query('SELECT count(id) FROM politique where vote = 10');
	if($vote10 = $votes10->fetch()){$vot10 = true;}
	else{$vot10 = false;}

	$votes11 = $bdd->query('SELECT count(id) FROM politique where vote = 11');
	if($vote11 = $votes11->fetch()){$vot11 = true;}
	else{$vot11 = false;}

	$total = $bdd->query('SELECT count(id) FROM politique');
	$votestotaux = $total->fetch();
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Le sondage rigolo politique</title>
	<link rel="stylesheet" href="design.css" />
	<meta name="viewport" content="width=device-width" />
</head>
<body>
	<header>
		<h1>Le sondage <s>politi</s>rigolo</h1>
	</header>
	<nav>
		<p>Parce que les noms des gens sans le milieu c'est plutôt rigolo, vous pouvez choisir sur cette page lequel des noms vous préférez.</p>
		<p><b>Ce n'est pas un vote politique, c'est juste un vote pour choisir le nom le plus rigolo !</b></p>
		<p><i>Passez votre souris ou bien appuyez sur les noms des politiques pour voir leur nom complet.</i></p>
	</nav>

	<section>
		<!-- <img src="#" alt="image-x" /> -->
		<div class="gauche">
			<p class="decouvre1">Fr<span class="cache1">anç</span>ois Fi<span class="cache1">ll</span>on</p>
		</div>
		<div class="droite">
		<?php if(!$vot1){ ?>
			<form method="post" action="?vote=1">
				<input type="submit" value="Voter" />
			</form>
		<?php } else { ?>
			<p><?php echo $vote1[0]; ?> votes</p>
			<p>
				<progress value="<?php echo $vote1[0]; ?>" max="<?php echo $votestotaux[0]; ?>"></progress>
			</p>
		<?php } ?>
		</div>
	</section>

	<section>
		<!-- <img src="#" alt="image-x" /> -->
		<div class="gauche">
			<p class="decouvre2">M<span class="cache2">ar</span>ine L<span class="cache2">eP</span>en</p>
		</div>
		<div class="droite">
		<?php if(!$vot2){ ?>
			<form method="post" action="?vote=2">
				<input type="submit" value="Voter" />
			</form>
		<?php } else { ?>
			<p><?php echo $vote2[0]; ?> votes</p>
			<p>
				<progress value="<?php echo $vote2[0]; ?>" max="<?php echo $votestotaux[0]; ?>"></progress>
			</p>
		<?php } ?>
		</div>
	</section>

	<section>
		<!-- <img src="#" alt="image-x" /> -->
		<div class="gauche">
			<p class="decouvre3">Em<span class="cache3">manu</span>el Ma<span class="cache3">c</span>ron</p>
		</div>
		<div class="droite">
		<?php if(!$vot3){ ?>
			<form method="post" action="?vote=3">
				<input type="submit" value="Voter" />
			</form>
		<?php } else { ?>
			<p><?php echo $vote3[0]; ?> votes</p>
			<p>
				<progress value="<?php echo $vote3[0]; ?>" max="<?php echo $votestotaux[0]; ?>"></progress>
			</p>
		<?php } ?>
		</div>
	</section>

	<section>
		<!-- <img src="#" alt="image-x" /> -->
		<div class="gauche">
			<p class="decouvre4">Je<span class="cache4">a</span>n Lass<span class="cache4">all</span>e</p>
		</div>
		<div class="droite">
		<?php if(!$vot4){ ?>
			<form method="post" action="?vote=4">
				<input type="submit" value="Voter" />
			</form>
		<?php } else { ?>
			<p><?php echo $vote4[0]; ?> votes</p>
			<p>
				<progress value="<?php echo $vote4[0]; ?>" max="<?php echo $votestotaux[0]; ?>"></progress>
			</p>
		<?php } ?>
		</div>
	</section>

	<section>
		<!-- <img src="#" alt="image-x" /> -->
		<div class="gauche">
			<p class="decouvre5">Na<span class="cache5">tha</span>lie Ar<span class="cache5">th</span>aud</p>
		</div>
		<div class="droite">
		<?php if(!$vot5){ ?>
			<form method="post" action="?vote=5">
				<input type="submit" value="Voter" />
			</form>
		<?php } else { ?>
			<p><?php echo $vote5[0]; ?> votes</p>
			<p>
				<progress value="<?php echo $vote5[0]; ?>" max="<?php echo $votestotaux[0]; ?>"></progress>
			</p>
		<?php } ?>
		</div>
	</section>

	<section>
		<!-- <img src="#" alt="image-x" /> -->
		<div class="gauche">
			<p class="decouvre6">J<span class="cache6">ean-L</span>uc Mél<span class="cache6">ench</span>on</p>
		</div>
		<div class="droite">
		<?php if(!$vot6){ ?>
			<form method="post" action="?vote=6">
				<input type="submit" value="Voter" />
			</form>
		<?php } else { ?>
			<p><?php echo $vote6[0]; ?> votes</p>
			<p>
				<progress value="<?php echo $vote6[0]; ?>" max="<?php echo $votestotaux[0]; ?>"></progress>
			</p>
		<?php } ?>
		</div>
	</section>

	<section>
		<!-- <img src="#" alt="image-x" /> -->
		<div class="gauche">
			<p class="decouvre7">Fr<span class="cache7">anç</span>ois A<span class="cache7">sseli</span>neau</p>
		</div>
		<div class="droite">
		<?php if(!$vot7){ ?>
			<form method="post" action="?vote=7">
				<input type="submit" value="Voter" />
			</form>
		<?php } else { ?>
			<p><?php echo $vote7[0]; ?> votes</p>
			<p>
				<progress value="<?php echo $vote7[0]; ?>" max="<?php echo $votestotaux[0]; ?>"></progress>
			</p>
		<?php } ?>
		</div>
	</section>

	<section>
		<!-- <img src="#" alt="image-x" /> -->
		<div class="gauche">
			<p class="decouvre8">Ni<span class="cache8">co</span>las Du<span class="cache8">pont-Ai</span>gnan</p>
		</div>
		<div class="droite">
		<?php if(!$vot8){ ?>
			<form method="post" action="?vote=8">
				<input type="submit" value="Voter" />
			</form>
		<?php } else { ?>
			<p><?php echo $vote8[0]; ?> votes</p>
			<p>
				<progress value="<?php echo $vote8[0]; ?>" max="<?php echo $votestotaux[0]; ?>"></progress>
			</p>
		<?php } ?>
		</div>
	</section>

	<section>
		<!-- <img src="#" alt="image-x" /> -->
		<div class="gauche">
			<p class="decouvre9">Phi<span class="cache9">lip</span>pe P<span class="cache9">out</span>ou</p>
		</div>
		<div class="droite">
		<?php if(!$vot9){ ?>
			<form method="post" action="?vote=9">
				<input type="submit" value="Voter" />
			</form>
		<?php } else { ?>
			<p><?php echo $vote9[0]; ?> votes</p>
			<p>
				<progress value="<?php echo $vote9[0]; ?>" max="<?php echo $votestotaux[0]; ?>"></progress>
			</p>
		<?php } ?>
		</div>
	</section>

	<section>
		<!-- <img src="#" alt="image-x" /> -->
		<div class="gauche">
			<p class="decouvre10">B<span class="cache10">en</span>oit H<span class="cache10">am</span>on</p>
		</div>
		<div class="droite">
		<?php if(!$vot10){ ?>
			<form method="post" action="?vote=10">
				<input type="submit" value="Voter" />
			</form>
		<?php } else { ?>
			<p><?php echo $vote10[0]; ?> votes</p>
			<p>
				<progress value="<?php echo $vote10[0]; ?>" max="<?php echo $votestotaux[0]; ?>"></progress>
			</p>
		<?php } ?>
		</div>
	</section>

	<section>
		<!-- <img src="#" alt="image-x" /> -->
		<div class="gauche">
			<p class="decouvre11">J<span class="cache11">acqu</span>es Chem<span class="cache11">in</span>ade</p>
		</div>
		<div class="droite">
		<?php if(!$vot11){ ?>
			<form method="post" action="?vote=11">
				<input type="submit" value="Voter" />
			</form>
		<?php } else { ?>
			<p><?php echo $vote11[0]; ?> votes</p>
			<p>
				<progress value="<?php echo $vote11[0]; ?>" max="<?php echo $votestotaux[0]; ?>"></progress>
			</p>
		<?php } ?>
		</div>
	</section>

	<footer>
		<p>
			Tout ceci n'est qu'un truc un peu rigolo en lien avec les élections, c'est pas quelque chose à prendre au sérieux, ce n'est pas à visée insultante ni polémique, soyez heureux et rigolez svp.
		</p>
		<p>
			<i>Votre adresse ip est enregistrée sur une base de données si vous votez, ça ne durera pas plus d'un mois, et je ne vais pas vendre ces données. C'est vraiment juste pour rigoler.</i>
		</p>
		<p class="petit">
			Un truc sauvage et totalement improvisé par <a href="http://l3m.in">Corentin Bettiol</a>.
		</p>
	</footer>

</body>
</html>