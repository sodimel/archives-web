<?php

function commencer_aventure(){
switch (rand(0,14)) {
	case "0":
		$_SESSION['argent'] = rand(2,6);
		return "Vous avez décidé de partir à l'aventure comme ça d'un coup. Vous avez quelques pièces au fond de votre poche (". $_SESSION['argent'] ." exactement) et c'est tout.";
	break;
	case "1":
		$atk = rand(1,3);
		$_SESSION['force'] += $atk;
		$def = rand(1,5);
		$_SESSION['defense'] += $def;
		$argent = rand(2,15);
		$_SESSION['argent'] = $argent;
		return "Après des mois d'entrainement passés à vous battre contre un infirme vous servant de punching ball, vous êtes prêt. Vous partez remplir une noble quête armé de votre épée +". $atk .", de votre bouclier +". $def ." et de ". $argent ." pièces en poche. La vie est belle pour certains.";
	break;
	case "2":
		return "Vous décidez du jour au lendemain de quitter votre boulot et de partir explorer le monde. Vous êtes fauché et n'avez aucun équipement, mais qu'importe, votre taf vous fait trop chier.<br />
		Et on vous comprend.";
	break;
	case "3":
		$_SESSION['force'] = $_SESSION['force'] + 1;
		return "Alors que vous vous baladiez gentiment en ville un vieil homme s'approche de vous pour vous demander d'aller lui chercher un médaillon magique à la con dans un coin qui ferait passer le trou du cul du monde pour une station balnéaire hyper luxueuse.<br />
		Vous l'envoyez balader. Et vous partez sur la route en quête d'une maison hébergeant des filles de joie, armé de votre bite et de votre couteau +1.";
	break;
	case "4":
		$_SESSION['argent'] = rand(2,6);
		return "Il fait atrocement chaud en ce moment. Vous allez chercher de l'eau au puits mais êtes téléporté en pleine forêt. Ca on peut dire que c'est un coup de pute du destin, parce que vous n'avez malheureusement pas pris votre arc +". rand(1,3) .".<br />
		Vous avez toutefois quelques piecettes (". $_SESSION['argent'] ." exactement) dans votre poche de pyjama. de... pyjama ? Et merde.";
	break;
	case "5":
		$_SESSION['argent'] = rand(10,50);
		return "Alors que vous vous baladez dans votre palace, une soudaine envie d'aventure vous prend. Vous emportez ". $_SESSION['argent'] ." pièces avec vous, puis vous donnez votre maison à un gamin sans le sou.<br />Quel altruisme !";
	break;
	case "6":
		$_SESSION['argent'] = rand(2,6);
		if(rand(0,1) == 1){
			$def = rand(1,5);
			$_SESSION['defense'] += $def;
			$texte = "Vous y trouvez ". $_SESSION['argent'] ." pièces, et ". defense() ." +". $def .". C'est toujours ça.";
		}
		else{
			$atk = rand(1,3);
			$_SESSION['force'] += $atk;
			$texte = "Vous y trouvez ". $_SESSION['argent'] ." pièces, et ". arme() ." +". $atk .". C'est déjà pas mal.";
		}
		$retour = "Vous vous réveillez la tête dans des excréments humains. La soirée d'hier semble avoir été mouvementée. Tel Marv dans Sin City, vous prenez votre dose de médicaments et vous mettez en route pour tenter de savoir qui vous a fait finir comme ça.<br />
		Juste avant de partir, vous fouillez vos poches.<br />". $texte;
		return $retour;
		
	break;
	case "7":
		$def = rand(1,5);
		$_SESSION['defense'] += $def;
		return "Après des années à avoir joué à Dwarf Fortress seul dans votre appartement pourri, vous vous retrouvez (suite à un défi débile prononcé après une soirée très arrosée) en pleine nature, avec ". defense() ." +". $def .". Vous décidez de partir explorer un peu les environs.";
	break;
	case "8":
		$_SESSION['argent'] = rand(2,6);
		return "Après avoir volé un khajiit à Corinth, vous vous faites mettre dehors et marchez quelques temps sous un soleil de plomb. Après ce qui vous semble une éternité, vous découvrez l'orée d'une forêt. Vous souriez. Vous allez vous refaire.";
	break;
	case "9":
		$_SESSION['argent'] = rand(5,12);
		return "Un jour, alors que vous vous baladez à Valtordu, vous voyez ". arme() ." dans une échoppe tenue par un vieil homme à l'aspect peu engageant.<br />
		Vous y demandez ce qu'elle fait là, ce à quoi il vous répond que c'est une arme +". rand(3,8) ." réservée à des rangers de haut niveau. Vous y rétorquez que vous vous foutez de ces informations, et que vous voulez juste savoir ce qu'une arme fout sur un comptoir. Il vous regarde pendant quelques instants avec un regard un peu vide, 
		puis il retourne à ses affaires (essuyer le comptoir, principalement). Vous décidez de sortir dans la rue sans avoir eu de réponse, et continuez quelques temps votre chemin. Après avoir <s>volé</s> trouvé ". $_SESSION['argent'] ." pièces, vous décidez de quitter la ville et d'aller explorer un peu les environs. Vous entrez alors dans la forêt d'Ouien.";
	break;
	case "10":
		return "Après une discussion qui a mal tourné avec la personne qui partage votre vie, vous décidez de déménager dans un bled paumé.<br />
		Après une nuit calme emplie de rêves érotiques, vous vous réveillez dans une forêt mystérieuse. Vous ne comprenez rien. En plus de ça votre boussole ne semble pas fonctionner. Qu'importe, vous partez à l'aventure.";
	break;
	case "11":
		$atk = rand(1,3);
		$_SESSION['force'] += $atk;
		return "Votre perroquet parle pendant son sommeil, et ce depuis que vous l'avez récupéré à l'asile, il y a presque un an.<br />
		Une nuit, vous l'entendez parler de son ancien propriétaire qui aurait caché des tonnes d'or dans une forêt non loin de chez vous. Vous prenez une lampe torche et ". arme() ." +". $atk .", et vous partez sur les traces de cette fortune.";
	break;
	case "12":
		$_SESSION['argent'] = rand(2,6);
		return "Après une journée de travail harrassante, vous vous autorisez une petite pause au bar, et prenez un ticket à gratter. Vous l'échangez contre ". $_SESSION['argent'] ." pièces, et vous reprenez une bière, offerte par le patron.<br />
		On ne sais pas trop ce qu'il avait mis dedans, mais vous vous réveillez dans un bois clairsemé, orée d'une grande forêt. Vous étouffez un juron et vous levez.<br />
		Il vous reste vos ". $_SESSION['argent'] ." pièces, et c'est tout. Pas de trace de ". defense() ." +". rand(1,5) .".";
	break;
	case "13":
		return "En vous rendant au lieu de rendez-vous, vous vous sentez épié. Qui donc pourrait vous suivre dans cette forêt déserte dans laquelle vous vous rendez afin de récupérer une cargaison de champignons du pays de la princesse Peach ? Personne.<br />
		Pourtant, il y a bien quelqu'un. Qui vous tombe dessus. Et vous assome (avec ". arme() .").<br />
		Vous ne vous réveillez qu'au petit matin, sans plus aucun sens de l'orientation. Vous vous levez et cherchez à rentrer chez vous.";
	break;
	case "14":
		$vie = rand(1,8);
		$_SESSION['vie'] += $vie;
		return "Après avoir vu l'affiche d'un marabout vous permettant d'augmenter de manière non négligeable vos points de vies, vous vous rendez dans son antre.<br />
		Il vous maraboute, et vous vous enfoncez dans un profond sommeil. Après quelques heures, vous vous réveillez en pleine forêt, complètement dépouillé.<br />
		Bon, au moins vos points de vie ont augmenté de ". $vie .", c'est déjà ça. Vous vous mettez en recherche de la ville.";
	break;
	}
}

function continuer_aventure(){
	if(isset($_SESSION['combat-tour']) and $_SESSION['combat-tour'] >= 0){
		return combatmonstre();
	}
	else{
		$rand = mt_rand(1,100);
		if($rand >= 1 && $rand <= 9){
			return loupermonstre();
		}
		if($rand >= 10 && $rand <= 19){
			return nouvellequete();
		}
		if($rand >= 20 && $rand <= 39){
			return trouverobjet();
		}
		if($rand >= 40 && $rand <= 54){
			return marchand();
		}
		if($rand >= 55 && $rand <= 69){
			return ilnesepasserien();
		}
		if($rand >= 70 && $rand <= 99){
			return combatmonstre();
		}
		if($rand == 100){
			return aleatoire_coffre();
		}
	}
}

function loupermonstre(){
	if(isset($_SESSION['monstrequete']) && $_SESSION['monstrequete'] != ""){
		if(rand(0,5) >= 4){
			$nom_monstre = $_SESSION['monstrequete'];
			$texte = debut_action() .", vous entendez au loin ". $nom_monstre ." qui s'approche. Vous vous planquez. ". $nom_monstre ." passe à côté de vous sans vous remarquer.";
			return $texte;
		}
		else{
			$nom_monstre = nom_monstre();
			$texte = debut_action() .", vous entendez au loin ". $nom_monstre ." qui s'approche. Vous vous planquez. ". $nom_monstre ." passe à côté de vous sans vous remarquer.";
			return $texte;
		}
	}
	else{
		$nom_monstre = nom_monstre();
		$texte = debut_action() .", vous entendez au loin ". $nom_monstre ." qui s'approche. Vous vous planquez. ". $nom_monstre ." passe à côté de vous sans vous remarquer.";
		return $texte;
	}
}

function trouverobjet(){
	$rand = rand(0,2);
	$_SESSION['objetstrouves'] = $_SESSION['objetstrouves'] + 1;

	switch (rand(0,2)) {
		case "0":
			$atk = rand(1,3);
			$_SESSION['force'] += $atk;
			$texte = debut_action() .", et vous tombez sur ". arme() ." +". $atk ." !<br />". phrase_trouver_objet();
			return $texte;
		break;
		case "1":
				$def = rand(1,3);
				$_SESSION['defense'] += $def;
				$texte = debut_action() .", et vous tombez sur ". defense() ." +". $def ." !<br />". phrase_trouver_objet();
				return $texte;
		break;
		case "2":
				$vie = rand(1,5);
				$_SESSION['vie'] += $vie;
				$texte = debut_action() .", et vous tombez sur ". vie() ." +". $vie ." !<br />". phrase_trouver_objet();
				return $texte;
		break;
	}
}

function marchand(){
	$rand = rand(0,3);
	if($rand == 0){
		$def = rand(2,8);
		$prix = rand(1,15);
		if($_SESSION['argent'] >= $prix){
			$_SESSION['objetsachetes'] = $_SESSION['objetsachetes'] + 1;
			$_SESSION['argent'] = $_SESSION['argent'] - $prix;
			$_SESSION['defense'] += $def;
			$transaction = "Il a ". defense() ." +". $def .", pour ". $prix ." pièces. Vous décidez de l'acheter, il vous reste ". $_SESSION['argent'] ." pièces, et vous avez maintenant ". $_SESSION['defense'] ." points de défense.<br />
			Vous remerciez le marchand et vous remettez à marcher.";
		}
		elseif($_SESSION['argent'] < $prix && $_SESSION['argent'] > 0){
			$transaction = "Il a ". defense() ." +". $def .", pour ". $prix ." pièces. Vous auriez bien aimé l'acheter, mais vous n'avez que ". $_SESSION['argent'] ." pièces.<br />
			Vous prenez congé du marchand et vous remettez à marcher.";
		}
		else{
			$transaction = "Il a ". defense() ." +". $def .", pour ". $prix ." pièces. Vous auriez bien aimé l'acheter, mais vous n'avez pas un rond.<br />
			Vous prenez congé du marchand et vous remettez à marcher.";
		}
	}
	elseif($rand == 1){
		$atk = rand(1,5);
		$prix = rand(3,15);
		if($_SESSION['argent'] >= $prix){
			$_SESSION['objetsachetes'] = $_SESSION['objetsachetes'] + 1;
			$_SESSION['argent'] = $_SESSION['argent'] - $prix;
			$_SESSION['force'] += $atk;
			$transaction = "Il a ". arme() ." +". $atk .", pour ". $prix ." pièces. Vous décidez de l'acheter, il vous reste ". $_SESSION['argent'] ." pièces, et vous avez maintenant ". $_SESSION['force'] ." points de force.<br />
			Vous remerciez le marchand et vous remettez à marcher.";
		}
		elseif($_SESSION['argent'] < $prix && $_SESSION['argent'] > 0){
			$transaction = "Il a ". arme() ." +". $atk .", pour ". $prix ." pièces. Vous auriez bien aimé l'acheter, mais vous n'avez que ". $_SESSION['argent'] ." pièces.<br />
			Vous prenez congé du marchand et vous remettez à marcher.";
		}
		else{
			$transaction = "Il a ". arme() ." +". $atk .", pour ". $prix ." pièces. Vous auriez bien aimé l'acheter, mais vous n'avez pas un rond.<br />
			Vous prenez congé du marchand et vous remettez à marcher.";
		}
	}
	elseif($rand == 2){
		$intuition = rand(1,5);
		$prix = rand(3,12);
		if($_SESSION['argent'] >= $prix){
			$_SESSION['objetsachetes'] = $_SESSION['objetsachetes'] + 1;
			$_SESSION['argent'] = $_SESSION['argent'] - $prix;
			$_SESSION['intuition'] += $intuition;
			$transaction = "Il a un flacon d'intuition +". $intuition .", pour ". $prix ." pièces. Vous décidez de l'acheter, il vous reste ". $_SESSION['argent'] ." pièces, et vous avez maintenant ". $_SESSION['intuition'] ." d'intuition.<br />
			Vous remerciez le marchand et vous remettez à marcher.";
		}
		elseif($_SESSION['argent'] < $prix && $_SESSION['argent'] > 0){
			$transaction = "Il a un flacon d'intuition +". $intuition .", pour ". $prix ." pièces. Vous auriez bien aimé l'acheter, mais vous n'avez que ". $_SESSION['argent'] ." pièces.<br />
			Vous prenez congé du marchand et vous remettez à marcher.";
		}
		else{
			$transaction = "Il a un flacon d'intuition +". $intuition .", pour ". $prix ." pièces. Vous auriez bien aimé l'acheter, mais vous n'avez pas un rond.<br />
			Vous prenez congé du marchand et vous remettez à marcher.";
		}
	}
	else{
		$vie = rand(2,8);
		$prix = rand(5,25);
		if($_SESSION['argent'] >= $prix){
			$_SESSION['objetsachetes'] = $_SESSION['objetsachetes'] + 1;
			$_SESSION['argent'] = $_SESSION['argent'] - $prix;
			$_SESSION['vie'] += $vie;
			$transaction = "Il a <span class=\"vie\">une fiole de soin</span> un peu périmée (rajoutant +". $vie ." points de vie), pour ". $prix ." pièces. Vous décidez de l'acheter, il vous reste ". $_SESSION['argent'] ." pièces, et vous avez maintenant ". $_SESSION['vie'] ." points de vie.<br />
			Vous remerciez le marchand et vous remettez à marcher.";
		}
		elseif($_SESSION['argent'] < $prix && $_SESSION['argent'] > 0){
			$transaction = "Il a <span class=\"vie\">une fiole de soin</span> un peu périmée (rajoutant +". $vie ." points de vie), pour ". $prix ." pièces. Vous auriez bien aimé l'acheter, mais vous n'avez que ". $_SESSION['argent'] ." pièces.<br />
			Vous prenez congé du marchand et vous remettez à marcher.";
		}
		else{
			$transaction = "Il a <span class=\"vie\">une fiole de soin</span> un peu périmée (rajoutant +". $vie ." points de vie), pour ". $prix ." pièces. Vous auriez bien aimé l'acheter, mais vous n'avez pas un rond.<br />
			Vous prenez congé du marchand et vous remettez à marcher.";
		}
	}
	$texte = debut_action() .", et vous entendez du bruit non loin. Vous vous planquez derrière un arbre, mais ce n'est qu'un marchand. Vous vous approchez et voyez ce qu'il a à vendre.<br />". $transaction;
	return $texte;
}

function nouvellequete(){
	if(!isset($_SESSION['monstrequete']) or $_SESSION['monstrequete'] == ""){
		$rand = rand(0,2);
		switch (rand(0,2)) {
			case "0":
			$_SESSION['monstrequete'] = nom_monstre();
			$_SESSION['argentquete'] = rand(10,25);
			return debut_action() .", puis vous voyez une personne étrange s'approcher de vous à pas lents.<br />
			Elle n'a pas l'air méchante, mais elle vous fait un peu peur. Elle vous demande si vous pouvez tuer ". $_SESSION['monstrequete'] ." pour elle, qui lui a volé ". defense() ." +". rand(1,4) ." plus tôt dans la journée.<br />
			Elle vous promet que si vous lui rapportez son arme, elle vous donnera ". $_SESSION['argentquete'] ." pièces.<br />
			Aussitôt après avoir entendu le montant de la récompense, vous acquiescez avec frénésie, puis partez à la recherche de l'odieuse créature à combattre.";
		break;
			case "1":
			$_SESSION['monstrequete'] = nom_monstre();
			$_SESSION['argentquete'] = rand(15,35);
			return debut_action() .", puis vous voyez une personne étrange s'approcher de vous à pas lents.<br />
			Elle n'a pas l'air méchante, mais elle vous fait un peu peur. Elle vous demande si vous pouvez tuer ". $_SESSION['monstrequete'] ." pour elle, qui lui a volé ". arme() ." +". rand(3,15) ." plus tôt dans la journée.<br />
			Elle vous promet que si vous lui rapportez son arme, elle vous donnera ". $_SESSION['argentquete'] ." pièces.<br />
			Aussitôt après avoir entendu le montant de la récompense, vous acquiescez avec frénésie, puis partez à la recherche de l'odieuse créature à combattre.";
		break;
			case "2":
			$_SESSION['monstrequete'] = nom_monstre();
			$_SESSION['argentquete'] = rand(10,30);
			return debut_action() .", puis vous voyez une personne étrange s'approcher de vous à pas lents.<br />
			Elle n'a pas l'air méchante, mais elle vous fait un peu peur. Elle vous demande si vous pouvez tuer ". $_SESSION['monstrequete'] ." pour elle, qui lui a volé ". vie() ." +". rand(3,8) ." plus tôt dans la journée.<br />
			Elle vous promet que si vous lui rapportez son arme, elle vous donnera ". $_SESSION['argentquete'] ." pièces.<br />
			Aussitôt après avoir entendu le montant de la récompense, vous acquiescez avec frénésie, puis partez à la recherche de l'odieuse créature à combattre.";
		break;
		}
	}
	else{
		return ilnesepasserien();
	}
}

function ilnesepasserien(){
	return debut_action() .". ". fin_action();
}

function aleatoire_coffre(){
	$rand = rand(0,2);
	$_SESSION['objetstrouves'] = $_SESSION['objetstrouves'] + 1;

	switch (rand(0,2)) {
		case "0":
			if(rand(0,5) == 0){
				$atk = rand(8,20);
				$_SESSION['force'] += $atk;
				$texte = debut_action() .", et vous tombez sur un gros coffre metallique. Il est ouvert.<br />
				Dedans se trouve ". arme() ." +". $atk ." !<br />". phrase_trouver_objet();
				return $texte;
			}
			else{
				return debut_action() .", et vous tombez sur un gros coffre metallique. Fermé.<br />
				Damn it, pas moyen de l'ouvrir. Vous repartez.";
			}

		break;
		case "1":
			if(rand(0,1) == 1){
				$def = rand(8,20);
				$_SESSION['defense'] += $def;
				$texte = debut_action() .", et vous tombez sur un gros coffre metallique. Il est ouvert.<br />
				Dedans se trouve ". defense() ." +". $def ." !<br />". phrase_trouver_objet();
				return $texte;
			}
			else{
				return debut_action() .", et vous tombez sur un gros coffre metallique. Fermé.<br />
				Damn it, pas moyen de l'ouvrir. Vous repartez.";
			}

		break;
		case "2":
			if(rand(0,1) == 1){
				$vie = rand(10,35);
				$_SESSION['vie'] += $vie;
				$texte = debut_action() .", et vous tombez sur un gros coffre metallique. Il est ouvert.<br />
				Dedans se trouve ". vie() ." +". $vie ." !<br />". phrase_trouver_objet();
				return $texte;
			}
			else{
				return debut_action() .", et vous tombez sur un gros coffre metallique. Fermé.<br />
				Damn it, pas moyen de l'ouvrir. Vous repartez.";
			}

		break;
	}
}