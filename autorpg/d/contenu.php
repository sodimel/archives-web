<?php

/*

Regex pour deswitcher

";
break;
	case "[0-9]*":
	return "

*/

function random_pick($i){
	if(strlen($i) > 0){
		$chemin = "c/". $i .".txt";
		$nom = fopen($chemin, 'r');
		$contenu = fread($nom, filesize($chemin));
		$nb_lignes = substr_count($contenu, "\n");

		$random = mt_rand(0, $nb_lignes);
		$nom = file($chemin);

		$pick = substr($nom[$random],0,-1);
		preg_match('#\[*[^ ]*\]#', $pick, $var);

		if(strlen($var[0]) > 0){
			
			$new = preg_replace('#\[*[^ ]*\]#', '$1', $var[0]);
			$new = random_pick($new);
			$pick = preg_replace($var[0], $new, $pick);
			$pick = preg_replace('#\[*[^ ]*\]#', '$1', $pick);
			
			return $pick;
		}
		else{
			return $pick;
		}
	}
}

function dire_quelque_chose_nombre_debut($nombre, $max){
	if(($nombre / $max) <= 0.1){
		echo random_pick("commentaire-0-1");
	}
	if(($nombre / $max) > 0.1 && ($nombre / $max) <= 0.2){
		echo random_pick("commentaire-1-2");
	}
	if(($nombre / $max) > 0.2 && ($nombre / $max) <= 0.3){
		echo random_pick("commentaire-2-3");
	}
	if(($nombre / $max) > 0.3 && ($nombre / $max) <= 0.4){
		echo random_pick("commentaire-3-4");
	}
	if(($nombre / $max) > 0.4 && ($nombre / $max) <= 0.5){
		echo random_pick("commentaire-4-5");
	}
	if(($nombre / $max) > 0.5 && ($nombre / $max) <= 0.6){
		echo random_pick("commentaire-5-6");
	}
	if(($nombre / $max) > 0.6 && ($nombre / $max) <= 0.7){
		echo random_pick("commentaire-6-7");
	}
	if(($nombre / $max) > 0.7 && ($nombre / $max) <= 0.8){
		echo random_pick("commentaire-7-8");
	}
	if(($nombre / $max) > 0.8 && ($nombre / $max) <= 0.9){
		echo random_pick("commentaire-8-9");
	}
	if(($nombre / $max) > 0.9 && ($nombre / $max) <= 1){
		echo random_pick("commentaire-9-10");
	}
}

function nom_monstre(){
	if(mt_rand(0,1) == 1){
		return random_pick("nom-monstres-1-masculin") ." ". random_pick("nom-monstres-2-masculin");
	}
	else{
		return random_pick("nom-monstres-1-feminin") ." ". random_pick("nom-monstres-2-feminin");
	}
}

function choisir_nom_monstre(){
	if(isset($_SESSION['monstrequete']) && $_SESSION['monstrequete'] != ""){
		if(rand(0,5) >= 4){
			$nom_monstre = $_SESSION['monstrequete'];
			return $nom_monstre;
		}
		else{
			$nom_monstre = nom_monstre();
			return $nom_monstre;
		}
	}
	else{
		$nom_monstre = nom_monstre();
		return $nom_monstre;
	}
}

function debut_action(){
	return random_pick("debut-action");
}

function fin_action(){
	return random_pick("fin-action");
}

function rencontre_monstre($nom){
	return str_replace("(nom)", $nom, random_pick("rencontre-monstre"));
}

function gain_combat($nom_monstre){
switch (rand(0,7)) {
	case "0":
	$argent = rand(1,10);
	$_SESSION['argent'] = $_SESSION['argent'] + $argent;
	$atk = rand(1,3);
	$_SESSION['force'] = $_SESSION['force'] + $atk;
	return "<li>". $nom_monstre ." avait une arme +". $atk ." (". arme() ."), ainsi que ". $argent ." pièces.". random_pick("prendre-items") . "</li>";
break;
	case "1":
	$argent = rand(1,10);
	$_SESSION['argent'] = $_SESSION['argent'] + $argent;
	$def = rand(1,3);
	$_SESSION['defense'] = $_SESSION['defense'] + $def;
	return "<li>". $nom_monstre ." avait ". defense() ." +". $def ." sur lui, mais aussi ". $argent ." pièces.". random_pick("prendre-items") . "</li>";
break;
	case "2":
	$argent = rand(5,15);
	$_SESSION['argent'] = $_SESSION['argent'] + $argent;
	return "<li>Vous trouvez ". $argent ." pièces sur ". $nom_monstre .".". random_pick("prendre-items") . "</li>";
break;
	case "3":
	$vie = rand(1,6);
	$_SESSION['vie'] = $_SESSION['vie'] + $vie;
	return "<li>Quelle chance ! ". $nom_monstre ." a une <span class=\"vie\">fiole de vie</span> +". $vie ." !". random_pick("prendre-items") . "</li>";
break;
	case "4":
	return "<li>Après avoir bien fouillé son cadavre il s'avère que ce monstre n'avait rien sur lui.</li>";
break;
	case "5":
	return "<li>Vous regardez la dépouille en sang deux minutes, puis vous décidez de ne pas la fouiller.</li>";
break;
	case "6":
	return "<li>Après avoir entendu un hurlement tout proche, vous décidez de ne pas vous attarder et vous repartez dans les bois.</li>";
break;
	case "7":
	return "<li>Vous êtes pris d'un haut-le-coeur en voyant le tas de tripes fumantes. Vous êtes une petite nature, vous vomissez et partez en courant.</li>";
break;
}
}

function action_combat_monstre(){
	return random_pick("action-combat-monstre");
}

function action_combat_humain(){
	return random_pick("action-combat-humain");
}

function arme(){
	return "<span class=\"force\">". random_pick("armes") ."</span>";
}

function defense(){
	return "<span class=\"defense\">". random_pick("defenses") ."</span>";
}

function vie(){
	return "<span class=\"vie\">". random_pick("vies") ."</span>";
}

function action_tuer_humain(){
	return random_pick("action-tuer-humain");
}

function aliment(){
	return random_pick("aliment");
}

function forme(){
	return random_pick("forme");
}

function phrase_esquive_monstre(){
	return random_pick("esquive-monstre");
}

function phrase_esquive_joueur(){
	return random_pick("esquive-joueur");
}

function evt_aleatoire(){
	return random_pick("event-aleatoire");
}

function phrase_trouver_objet(){
	return random_pick("trouver-objet");
}

function Personnage(){
	return random_pick("personnage");
}

function phrase_fin_aventure($av, $ob, $oc, $qu){
	return "Cette partie a duré ". $av ." tours (". commentaire_nb_aventures($av) .").<br />
	Au total, ". $ob ." objets ont été trouvés, et ". $oc ." ont été achetés à des marchands.<br />". commentaire_nb_quetes($qu);
}

function commentaire_nb_aventures($av){
	include('../../../../fonctions/connexionbdd.php');
	$chercher_nb_parties = $bdd->query('SELECT COUNT(id) FROM autorpg_endgames');
	$nb_parties = $chercher_nb_parties->fetchColumn();
	$chercher_nb_aventures = $bdd->query('SELECT SUM(aventures) FROM autorpg_endgames');
	$nb_aventures = $chercher_nb_aventures->fetchColumn();

	$nba = floor($nb_aventures/$nb_parties);

	if($av > $nba){
		return random_pick("com-endgame-sup");
	}
	else{
		return random_pick("com-endgame-inf");
	}
}

function commentaire_nb_quetes($qu){
	include('../../../../fonctions/connexionbdd.php');
	$chercher_nb_parties = $bdd->query('SELECT COUNT(id) FROM autorpg_endgames');
	$nb_parties = $chercher_nb_parties->fetchColumn();
	$chercher_nb_quetes = $bdd->query('SELECT SUM(quetes) FROM autorpg_endgames');
	$nb_quetes = $chercher_nb_quetes->fetchColumn();

	$nbq = floor($nb_quetes/$nb_parties);

	if($qu > $nba){
		$qa = $qu+1;
		return str_replace("(quetesplusun)", $qa, str_replace("(quetes)", $qu, random_pick("com-nb-quetes-sup-moyenne")));
	}
	else{
		return str_replace("(quetes)", $qu, random_pick("com-nb-quetes-inf-moyenne"));
	}
}

function texte_combattre_monstre(){
	return random_pick("bouton-commencer-combat");
}


function texte_fuir_monstre(){
	return random_pick("bouton-fuir-combat");
}

function race(){
	return random_pick("race");
}

function phrase_choix_combat($nom_monstre){
	return str_replace("(nom)", $nom_monstre, random_pick("phrase-choix-combat"));
}

function choix_combat(){
	return random_pick("choix-combat");
}

function choix_fuite(){
	return str_replace("(nom)", nom_monstre(), random_pick("choix-fuite"));
}

function choix_fuite_loupee(){
	return random_pick("choix-fuite-loupee");
}


/*function evenement_aleatoire($i){
	if($i == 0){ // événement aléatoire mineur
		$rand = rand(0,)
	}
	else{ // événement aléatoire majeur (mort brutale pour éviter une boucle infinie)

	}
}
*/
?>