<?php

function combatmonstre(){
	if(isset($_GET['combatmonstre']) and $_SESSION['choix-monstre'] == 0){
		$_SESSION['choix-monstre'] = 1;
		$texte = choix_combat();
		$texte .= "<ul>";
		$texte .= combat($_SESSION['combat-monstre']);
		return $texte;
	}
	elseif(isset($_GET['fuirmonstre']) and $_SESSION['choix-monstre'] == 0){
		$_SESSION['choix-monstre'] = 1;
		if(rand(0,100) > 90-($_SESSION['intuition']*2)){
			$_SESSION['combat-tour'] = -1;
			$texte = choix_fuite();
			return $texte;
		}
		else{
			$texte = choix_fuite_loupee();
			$texte .= combat($_SESSION['combat-monstre']);
			return $texte;
		}
	}
	if(!isset($_SESSION['combat-tour']) or $_SESSION['combat-tour'] == -1){
		$nom_monstre = choisir_nom_monstre();
		$texte = debut_action() .", ". rencontre_monstre($nom_monstre) .". ". phrase_choix_combat($nom_monstre) .".<br /><ul>";

		$_SESSION['choix-monstre'] = 0;
		$_SESSION['combat-monstre'] = $nom_monstre;
		$_SESSION['combat-tour'] = 0;
		$_SESSION['combat-atk'] = rand(1+intval($_SESSION['defense']/5),intval($_SESSION['defense']+($_SESSION['defense']/2)));
		$_SESSION['combat-def'] = rand(0+intval($_SESSION['force']/7),intval($_SESSION['force']+($_SESSION['force']/3)));
		$_SESSION['combat-vie'] = rand(1,intval($_SESSION['vie']+$_SESSION['vie']/3));
		if($_SESSION['combat-vie'] == 0){
			$_SESSION['combat-vie'] = 1;
		}
		$_SESSION['aleatoire0'] = 0;
		$_SESSION['aleatoire1'] = rand(10,25);

		preg_match('#un murloc#', $nom_monstre, $var);
		if(strlen($var[0]) > 0){
			$texte .= "<audio autoplay><source src=\"http://l3m.in/p/projets/tests/autorpg/d/murloc.mp3\" type=\"audio/mpeg\"><source src=\"http://l3m.in/p/projets/tests/autorpg/d/murloc.ogg\" type=\"audio/ogg\"></audio>";
		}
		return $texte;
	}
	else{
		$texte = "<ul>";
		$texte .= combat($_SESSION['combat-monstre']);
		return $texte;
	}
}

function combat($nom_monstre){

	$_SESSION['combat-tour']++;
	$_SESSION['aleatoire0']++;

	$texte = "<li>Combat, tour ". $_SESSION['combat-tour'] .".</li>";
	$texte .= "<li class=\"gauche\"><i><span class=\"nom\">". $nom_monstre ."</span> - ". $_SESSION['combat-vie'] ." <img src=\"d/hearth.gif\" alt=\"points de vie\" /> - ". $_SESSION['combat-atk'] ." <img src=\"d/sword.png\" alt=\"points d'attaque\" /> - ". $_SESSION['combat-def'] ." <img src=\"d/shield.png\" alt=\"points de défense\" /></li>";
	$texte .= "<li class=\"droite\"><i>Vous - ". $_SESSION['vie'] ." <img src=\"d/hearth.gif\" alt=\"points de vie\" /> - ". $_SESSION['force'] ." <img src=\"d/sword.png\" alt=\"points d'attaque\" /> - ". $_SESSION['defense'] ." <img src=\"d/shield.png\" alt=\"points de défense\" /></li>";

	$atkmonstre = $_SESSION['combat-atk'] + rand(-3,3);
	$defmonstre = $_SESSION['combat-def'] + rand(-1,1);
	$atkhumain = $_SESSION['force'] + rand(-3,3);
	$defhumain = $_SESSION['defense'] + rand(-1,1);

	$texte .= phase_attaque($nom_monstre, $atkhumain, $defmonstre, $atkmonstre, $defhumain);

	if($_SESSION['aleatoire0'] == $_SESSION['aleatoire1']){
		//$texte .= evenement_aleatoire(rand(0,1));
		$_SESSION['combat-vie'] = -1;
		$texte .= "<li>Evenement aléatoire ! ". evt_aleatoire() ."</li>";
	}

	if($_SESSION['combat-vie'] <= 0){
		$texte .=  "<li><span class=\"nom\">". $nom_monstre ."</span> n'a plus de vie. Vous l'avez vaincu en ". $_SESSION['combat-tour'] ." tour(s).</li>";
		$texte .= gain_combat($nom_monstre);
		$texte .= "</ul>";
		$texte .= quete($nom_monstre);
		$_SESSION['combat-tour'] = -1;
	}

	if($_SESSION['vie'] <= 0){
		$texte .=  "<li>Vous êtes mort. ". $nom_monstre ." ". action_tuer_humain() .".</li></ul>";
		$texte .= finjeu($nom_monstre);
		$_SESSION['aventure'] = -2;
	}

	return $texte;
}

function phase_attaque($nom_monstre, $atkhumain, $defmonstre, $atkmonstre, $defhumain){
	if(intuition_attaque() == 1){
		$texte .= "<li>Votre intuition vous fait commencer ce tour en premier.</li>";
		$texte .= attaquer($nom_monstre, $atkhumain, $defmonstre);
		if($_SESSION['combat-vie'] > 0){
			$texte .= recevoir_attaque($nom_monstre, $atkmonstre, $defhumain);
		}
	}
	else{
		$texte .= "<li>Votre intuition fait commencer ". $nom_monstre ." en premier.</li>";
		$texte .= recevoir_attaque($nom_monstre, $atkmonstre, $defhumain);
		if($_SESSION['vie'] > 0){
			$texte .= attaquer($nom_monstre, $atkhumain, $defmonstre);
		}
	}
	return $texte;
}

function attaquer($nom_monstre, $atkhumain, $defmonstre){
	if($atkhumain - $defmonstre > 0 and $_SESSION['vie'] > 0){
		if(rand(0,5) >= 3){
			if(jet_intelligence() == 1){
				$_SESSION['combat-vie'] = $_SESSION['combat-vie'] - ($atkhumain+1*2 - $defmonstre);
				$degats = $atkhumain+1*2 - $defmonstre;
				$crit = "Coup critique ! ";
			}
			else{
				$_SESSION['combat-vie'] = $_SESSION['combat-vie'] - ($atkhumain - $defmonstre);
				$degats = $atkhumain - $defmonstre;
				$crit = "";
			}
		}
		else{
			$_SESSION['combat-vie'] = $_SESSION['combat-vie'] - ($atkhumain - $defmonstre);
			$degats = $atkhumain - $defmonstre;
			$crit = "";
		}

		if($_SESSION['combat-vie'] < 0){
			$_SESSION['combat-vie'] = 0;
		}
		$texte = "<li>". $crit ."". action_combat_humain() ." ". $nom_monstre. ", et vous y faites ". $degats ." <img src=\"d/sword.png\" alt=\"points d'attaque\" />. Il lui reste encore ". $_SESSION['combat-vie'] ." <img src=\"d/hearth.gif\" alt=\"points de vie\" />.</li>";
		return $texte;
	}

	elseif($atkhumain - $defmonstre < 0 and $_SESSION['vie'] > 0){
		$degats = $attaque - $defmonstre;
		$texte = "<li>Vous foncez sur ". $nom_monstre. ", mais il esquive ". phrase_esquive_monstre() .". Le monstre ne perd aucun point de vie.</li>";
		return $texte;
	}
}

function recevoir_attaque($nom_monstre, $atkmonstre, $defhumain){
	if($atkmonstre - $defhumain > 0 and $_SESSION['combat-vie'] > 0){
		if(rand(0,5) >= 3){
			if(jet_intelligence() == 1){
				$degats = $atkmonstre+1*2 - $defhumain;
				$_SESSION['vie'] = $_SESSION['vie'] - $degats;
				$crit = "Coup critique ! ";
			}
			else{
				$degats = $atkmonstre - $defhumain;
				$_SESSION['vie'] = $_SESSION['vie'] - $degats;
				$crit = "";
			}
		}
		else{
			$degats = $atkmonstre - $defhumain;
			$_SESSION['vie'] = $_SESSION['vie'] - $degats;
			$crit = "";
		}
		
		if($_SESSION['vie'] < 0){
			$_SESSION['vie'] = 0;
		}
		$texte = "<li>". $crit ."<span class=\"nom\">". $nom_monstre. "</span> ". action_combat_monstre() ." et vous fait ". $degats ." <img src=\"d/sword.png\" alt=\"points d'attaque\" />. Il vous reste encore ". $_SESSION['vie'] ." <img src=\"d/hearth.gif\" alt=\"points de vie\" />.</li>";
		return $texte;
	}

	else{
		$texte .= "<li><span class=\"nom\">". $nom_monstre. "</span> va pour vous attaquer, mais ". phrase_esquive_joueur() .". Vous ne perdez aucun point de vie.</li>";
		return $texte;
	}
}

function intuition_attaque(){
	$nombre = 90 - ($_SESSION['intuition'] * 6);

	if(rand(0,100) >= $nombre){
		return 1;
	}
	else return 0;
}

function jet_intelligence(){
	$nombre = 90 - ($_SESSION['intelligence'] * 8);

	if(rand(0,100) >= $nombre){
		return 1;
	}
	else return 0;
}

function quete($nom){
	if(isset($_SESSION['monstrequete']) && $nom == $_SESSION['monstrequete']){
		$_SESSION['quetesfinies'] = $_SESSION['quetesfinies'] + 1;
		$_SESSION['argent'] = $_SESSION['argent'] + $_SESSION['argentquete'];
		$argent = $_SESSION['argentquete'];
		$_SESSION['monstrequete'] = "";
		$_SESSION['argentquete'] = "";
		return "Vous n'avez même pas fait trois pas qu'une forme se jette sur vous et vous serre dans ses bras avec amour. C'est l'étrange personne qui vous a donné la quête tout à l'heure !<br />
		Elle vous remercie chaleureusement, puis vous donne les ". $argent ." pièces d'or qu'elle vous avait promis. Elle repart ensuite entre les arbres, vous en faites de même.";
	}
	else{
		return "";
	}
}

?>