<?php 
session_start();
include('../../../../config.php');
include('fonctions.php');
include('d/contenu.php');
include('d/aventure.php');
include('d/combat.php');

if(isset($_GET['start']) && !isset($_SESSION['vie']) or $_SESSION['vie'] == 0){
    $_SESSION['vie'] = rand(5,15);
    $_SESSION['force'] = rand(3,10);
    $_SESSION['defense'] = rand(0,10);
    $_SESSION['intelligence'] = rand(0,10);
    $_SESSION['charisme'] = rand(0,10);
    $_SESSION['intuition'] = rand(0,10);
    $_SESSION['argent'] = 0;
    $_SESSION['aventure'] = 0;
    $_SESSION['quetesfinies'] = 0;
    $_SESSION['objetsachetes'] = 0;
    $_SESSION['objetstrouves'] = 0;
}

if(isset($_GET['aventure']) && isset($_SESSION['vie'])){
    $texte = appliquer_aventure($_GET['aventure']);
    $_SESSION['aventure'] = $_SESSION['aventure'] + 1;
}

if(isset($_GET['reset'])){
    session_destroy();
    if(isset($_GET['history'])){
        header('Location:http://l3m.in/p/projets/tests/autorpg/?history='. $_GET['history']);
    }
    else{
        header('Location:http://l3m.in/p/projets/tests/autorpg/');
    }
}

if(isset($_GET['tweet_connectdone'])){
    // http://defidataplus.net/tutoriaux/hashtag-twitter-php/
    // https://apps.twitter.com/app/8892243/
    // https://twitter.com/search?f=tweets&vertical=default&q=%23AutoRPG&src=typd
}

?>

<!DOCTYPE html>
<html>
   <head>
        <title>AutoRPG</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="design.css" />
        <link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
        <link rel="icon" href="favicon.ico" />
        <script src="../../../../analytics.js" type="text/javascript"></script>
        <meta name="viewport" content="width=device-width" />
    </head>
<body>

<div id="page">
<div class="page">
<div class="texte">

<div class="bordure haut"><img src="d/bordurehaut.png" /></div>
<div class="bordure bas"><img src="d/bordurebas.png" /></div>
<h1>Auto RPG</h1>
<?php

if(isset($_GET['history']) && is_numeric($_GET['history'])){

afficher_recap_partie(htmlspecialchars($_GET['history']));

}
elseif(isset($_GET['history']) && $_GET['history'] == ""){

afficher_stats_aventures();

}
else{

if(isset($_GET['aventure']) && isset($_SESSION['vie'])){
        afficher_menu();
        if($_SESSION['aventure'] >= 0){
            if(!isset($_SESSION['choix-monstre']) or $_SESSION['choix-monstre'] == 1){
                ?>
                    <p class="center"><a class="button opacity" href="?aventure=<?php echo $_SESSION['aventure']; ?>">La suite !</a></p>
                <?php
            }
            else{
                ?>
                    <p class="center opacity">
                        <a href="?aventure=<?php echo $_SESSION['aventure']; ?>&combatmonstre" class="button"><?php echo texte_combattre_monstre(); ?></a> &bull; 
                        <a href="?aventure=<?php echo $_SESSION['aventure']; ?>&fuirmonstre" class="button"><?php echo texte_fuir_monstre(); ?></a>
                    </p>
                <?php
            }
        }
        echo $texte;
        if($_SESSION['aventure'] == -1){
        ?>
            <p>
                <br />
                <div class="center"><a class="button" href="http://adf.ly/2799396/http://l3m.in/p/projets/tests/autorpg/?reset">Retour à l'accueil</a> <a class="button" href="?reset">Retour à l'accueil aussi</a></div>
                <br />
                <span class="small">Le premier bouton vous fait passer par une pub puis vous redirige vers l'accueil, le second vous renvoie à l'accueil directement. Est-ce que vous allez être bon et me permettre de pas 
                trop mettre d'argent de ma poche pour payer l'hébergement du site ? A vous de voir :)</span>
            </p>
            <p class="small">Partager votre score sur <a target="_blank" href="https://twitter.com/intent/tweet/?url=http://l3m.in/p/projets/tests/autorpg/&text=<?php echo $_SESSION['textetweet']; ?>">Twitter</a> (et me faire de la pub involontairement).</p>
        <?php
        }
    }
    elseif(isset($_GET['start'])){
        afficher_texte_pages_basiques("debut");
    }
    elseif(isset($_GET['changelog'])){
        afficher_texte_pages_basiques("changelog");
    }
    else{
        afficher_texte_pages_basiques("accueil");
    }

}

?>
</div>
</div>
</div>
</body>
</html>