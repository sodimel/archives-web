<?php
function afficher_texte_pages_basiques($a){

	if($a=="accueil"){
		include('../../../../fonctions/connexionbdd.php');
		$chercher_nb_parties = $bdd->query('SELECT COUNT(id) FROM autorpg_endgames');
		$nb_parties = $chercher_nb_parties->fetchColumn();
	?>
		<p>Bienvenue sur AutoRPG !<br />
		Ici, vous pourrez vous ennuyer quelques instants et suivre les pérégrinations de votre personnage, qui évoluera sans vous concerter.<br />
		Le jeu comporte beaucoup d'aléatoire, ce qui lui donne comme attrait principal d'avoir une rejouabilité quasiment infinie.</p>
		<p>Attention ! Le "jeu" n'est pas encore terminé. Le gameplay et le contenu continuent à être mis à jour régulièrement (consulter le changelog pour toutes les infos).<br />
		Attention !&sup2; N'actualisez pas la page après avoir commencé à jouer, cela vous renverra sur cette page en détruisant votre progression.<br />
		Attention !&sup3; Ce jeu comporte du contenu pouvant être vachement vulgaire et sale.</p>

		<p><b>Dernière nouveauté notable</b> : <span style="font-size:1.2em;">COUCOU UNE MISE À JOUR SAUVAGE APPARAÎT</span>.</p>

		<p class="center"><a href="?start" class="button">Lancer la bête</a></p>

		<p class="center small"><a href="?changelog">Changelog</a> &bull; <a href="?history=<?php echo $nb_parties; ?>">Historique</a> &bull; <a href="?history">Stats</a></p>

		<br /><br />
		<p class="small">Vous avez une idée à me soumettre, des dialogues incongrus vous passent par la tête ? Vous avez trouvé un bug ? Contactez-moi sur <a target="_blank" href="http://twinoid.com/user/12661">twinoid</a> ou bien via 
		le <a target="_blank" href="http://l3m.in/#contact">formulaire de contact</a> de mon site  :)<br />

		Contenu textuel et images sous <a rel="license" target="_blank" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0; opacity: 0.7" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a>, le reste du code est trop sale.</p>
	<?php
	}
	if($a=="debut"){
	?>
		<p>Voici vos statistiques :</p>
		<ul>
			<li><span class="vie">Vie</span> : <?php echo $_SESSION['vie']; dire_quelque_chose(", ","",$_SESSION['vie'],15); ?></li>
			<li><span class="force">Force</span> : <?php echo $_SESSION['force']; dire_quelque_chose(", ","",$_SESSION['force'],10); ?></li>
			<li><span class="defense">Defense</span> : <?php echo $_SESSION['defense']; dire_quelque_chose(", ","",$_SESSION['defense'],10); ?></li>
			<li>Intelligence : <?php echo $_SESSION['intelligence']; dire_quelque_chose(", ","",$_SESSION['intelligence'],10); ?></li>
			<li>Charisme : <?php echo $_SESSION['charisme']; dire_quelque_chose(", ","",$_SESSION['charisme'],10); ?></li>
			<li>Intuition : <?php echo $_SESSION['intuition']; dire_quelque_chose(", ","",$_SESSION['intuition'],10); ?></li>
		</ul>
		<p class="center"><a class="button" href="?aventure=0">Partir à l'aventure !</a></p>
	<?php
	}
	if($a=="changelog"){
		$contenu = "Changelog\n";
		$changelog =  fopen('d/changelog', r);
		while ($ligne = fread($changelog, filesize('d/changelog'))){
			$contenu .= $ligne;
		}
		fclose($changelog);
	?>
		<pre>
			<?php echo $contenu; ?>
		</pre>
		<p class="small"><a href="http://l3m.in/p/projets/tests/autorpg/">Retour à l'accueil</p>
	<?php
	}
}

function dire_quelque_chose($string_av, $string_ap, $nombre1, $nombre2){

	echo $string_av;
	dire_quelque_chose_nombre_debut($nombre1, $nombre2);
	echo $string_ap;
}

function afficher_menu(){
	?>
		<div class="menu"><span class="vie">Vie : <?php echo $_SESSION['vie']; ?></span> &bull; <span class="force">Force : <?php echo $_SESSION['force']; ?></span> &bull; <span class="defense">Defense : <?php echo $_SESSION['defense']; ?>
		</span> &bull; Intelligence : <?php echo $_SESSION['intelligence']; ?> &bull; Charisme : <?php echo $_SESSION['charisme']; ?> &bull; Intuition : <?php echo $_SESSION['intuition']; ?> &bull; Argent : <?php echo $_SESSION['argent']; ?> 
		&bull; <i><a href="?reset">reset ?</a></i>
		<?php if(isset($_SESSION['monstrequete']) && $_SESSION['monstrequete'] != ""){ ?>
			<br /><span class="small">Quete active : trouvez et tuez <?php echo $_SESSION['monstrequete']; ?> pour recevoir <?php echo $_SESSION['argentquete']; ?> pièces.</span>
		<?php } ?></div>
	<?php
}


function appliquer_aventure($i){
	if($i != $_SESSION['aventure']){
		header('Location:http://l3m.in/p/projets/tests/autorpg/?reset');
	}
	if($_SESSION['aventure'] == '-1'){
		header('Location:http://l3m.in/p/projets/tests/autorpg/?reset');
	}
	if($i == 0){
		return commencer_aventure();
	}
	else{
		return continuer_aventure();
	}
}

function afficher_recap_partie($id){
	include('../../../../fonctions/connexionbdd.php');
	$chercher_nb_parties = $bdd->query('SELECT COUNT(id) FROM autorpg_endgames');
	$nb_parties = $chercher_nb_parties->fetchColumn();

	$chercherip = $bdd->prepare('SELECT * FROM autorpg_endgames where id = :id') or die(print_r($bdd->errorInfo()));
	$chercherip->execute(array('id' => $id));

	if($infos = $chercherip->fetch()){
		echo "<p><b>Partie #". $infos['id'] ." du ". date('d/m/Y\, \t\e\r\m\i\n\é\e \à H:i:s', $infos['temps']) ." :</b></p>". phrase_fin_aventure($infos['aventures'], $infos['objetstrouves'], $infos['objetsachetes'], $infos['quetes']);
		
		if(strlen($infos["contenu"]) > 0){ ?>
			<p>
				Commentaire additionnel :<br />
				<span class="small"><?php echo $infos['contenu']; ?></span> 
			</p>
		<?php
		}

		$navigation = "<p class=\"center\">";
		$idsuivant = $id+1;
		$idprecedant = $id-1;
		if($id > 2){
			$navigation .= "<a href=\"?history=1\"><<</a> ";
		}
		else{
			$navigation .= "<i><<</i> ";
		}

		if($id > 1){
			$navigation .= " <a href=\"?history=". $idprecedant ."\">Précédent</a> &bull; ";
		}
		else{
			$navigation .= "<i>Précédent</i> &bull; ";
		}
		if($id < $nb_parties){
			$navigation .= "<a href=\"?history=". $idsuivant ."\">Suivant</a> ";
		}
		else{
			$navigation .= "<i>Suivant</i> ";
		}

		if($id < ($nb_parties-1)){
			$navigation .= "<a href=\"?history=". $nb_parties ."\">>></a> ";
		}
		else{
			$navigation .= "<i>>></i> ";
		}

		$navigation .= "<br /><a href=\"http://l3m.in/p/projets/tests/autorpg/\">Retour à l'accueil</a><br /><a class=\"small\" href=\"?history\">Statistiques</a></p>";

		echo $navigation;
	}
	else{
		echo "<p>Désolé, mais cette partie n'est pas disponible.</p>";
	}
}

function finjeu($nom_monstre){
	$_SESSION['textetweet'] = urlencode("J'ai survécu ". $_SESSION['aventure'] ." tours sur #AutoRPG, puis je suis mort bêtement.");

	$textefin = "<p><b>Vous avez survécu ". $_SESSION['aventure'] ." tours !</b><br />
	Vous avez trouvé ". $_SESSION['objetstrouves'] ." objets !<br />
	Vous avez acheté ". $_SESSION['objetsachetes'] ." objets !<br />
	Vous avez terminé ". $_SESSION['quetesfinies'] ." quêtes !</p>";

	$id = ajouter_fin_bdd();

	$textefin .= "<p><a target=\"_blank\" href=\"?reset&history=". $id ."\">Voici le lien de votre aventure</a>.</p>";

	return $textefin;
}

function ajouter_fin_bdd(){
	$temps = time();
	include('../../../../fonctions/connexionbdd.php');
	$commentaire = ajouter_commentaire_bdd($temps);

	$inscriptionbdd = $bdd->prepare('INSERT INTO autorpg_endgames(temps, aventures, objetstrouves, objetsachetes, quetes, contenu) VALUES(:temps, :aventures, :objetstrouves, :objetsachetes, :quetes, :contenu)');
	$inscriptionbdd->execute(array('temps' => $temps, 'aventures' => $_SESSION['aventure'], 'objetstrouves' => $_SESSION['objetstrouves'], 'objetsachetes' => $_SESSION['objetsachetes'], 'quetes' => $_SESSION['quetesfinies'], 'contenu' => $commentaire));

	$chercherid = $bdd->prepare('SELECT id FROM autorpg_endgames where temps = :temps') or die(print_r($bdd->errorInfo()));
	$chercherid->execute(array('temps' => $temps));
	if($infos = $chercherid->fetch()){
		return $infos['id'];
	}
}

function ajouter_commentaire_bdd($temps){
	include('../../../../fonctions/connexionbdd.php');

	$chercher_aventure_max = $bdd->query('SELECT id, aventures FROM autorpg_endgames ORDER BY aventures DESC');
	$nb_aventure_max = $chercher_aventure_max->fetch();

	$chercher_quetes_max = $bdd->query('SELECT id, quetes FROM autorpg_endgames ORDER BY quetes DESC');
	$nb_quetes_max = $chercher_quetes_max->fetch();

	$chercher_objets_achetes_max = $bdd->query('SELECT id, objetsachetes FROM autorpg_endgames ORDER BY objetstrouves DESC');
	$nb_objets_achetes_max = $chercher_objets_achetes_max->fetch();

	$chercher_objets_trouves_max = $bdd->query('SELECT id, objetstrouves FROM autorpg_endgames ORDER BY objetsachetes DESC');
	$nb_objets_trouves_max = $chercher_objets_trouves_max->fetch();

	$contenu = "";

	if($_SESSION['aventure'] >= $nb_aventure_max['aventures']){
		$contenu = "Bat le record de la plus longue aventure !<br />";
	}
	if( $_SESSION['objetstrouves'] >= $nb_objets_trouves_max['objetstrouves']){
		$contenu .= "Bat le record du plus d'objets trouvés !<br />";
	}
	if( $_SESSION['objetsachetes'] >= $nb_objets_achetes_max['objetsachetes']){
		$contenu .= "Bat le record du plus d'objets achetés !<br />";
	}
	if( $_SESSION['quetesfinies'] >= $nb_quetes_max['quetes']){
		$contenu .= "Bat le record du plus de quêtes terminées !";
	}

	return $contenu;
}

function afficher_stats_aventures(){
	include('../../../../fonctions/connexionbdd.php');

	?>
		<p><b>Voici les statistiques des parties enregistrées sur AutoRPG :</b></p>
	<?php

	$chercher_nb_parties = $bdd->query('SELECT COUNT(id) FROM autorpg_endgames');
	$nb_parties = $chercher_nb_parties->fetchColumn();

	$chercher_nb_aventures = $bdd->query('SELECT SUM(aventures) FROM autorpg_endgames');
	$nb_aventures = $chercher_nb_aventures->fetchColumn();

	$chercher_nb_quetes = $bdd->query('SELECT SUM(quetes) FROM autorpg_endgames');
	$nb_quetes = $chercher_nb_quetes->fetchColumn();

	$chercher_nb_objets_achetes = $bdd->query('SELECT SUM(objetsachetes) FROM autorpg_endgames');
	$nb_objets_achetes = $chercher_nb_objets_achetes->fetchColumn();

	$chercher_nb_objets_trouves = $bdd->query('SELECT SUM(objetstrouves) FROM autorpg_endgames');
	$nb_objets_trouves = $chercher_nb_objets_trouves->fetchColumn();

	$nb_aventures_moyen = round($nb_aventures/$nb_parties,2);
	$nb_quetes_moyen = round($nb_quetes/$nb_parties,3);
	$nb_objets_trouves_moyen = round($nb_objets_trouves/$nb_parties,2);
	$nb_objets_achetes_moyen = round($nb_objets_achetes/$nb_parties,2);
	$nb_objets_total = $nb_objets_achetes + $nb_objets_trouves;

	$chercher_aventure_max_total = $bdd->query('SELECT id, aventures FROM autorpg_endgames ORDER BY aventures DESC');
	$nb_aventure_max_total = $chercher_aventure_max_total->fetch();

	$chercher_quetes_max_total = $bdd->query('SELECT id, quetes FROM autorpg_endgames ORDER BY quetes DESC');
	$nb_quetes_max_total = $chercher_quetes_max_total->fetch();

	$chercher_objets_achetes_max_total = $bdd->query('SELECT id, objetsachetes FROM autorpg_endgames ORDER BY objetsachetes DESC');
	$nb_objets_achetes_max_total = $chercher_objets_achetes_max_total->fetch();

	$chercher_objets_trouves_max_total = $bdd->query('SELECT id, objetstrouves FROM autorpg_endgames ORDER BY objetstrouves DESC');
	$nb_objets_trouves_max_total = $chercher_objets_trouves_max_total->fetch();

	$semaine = time() - (86400*7);
	$mois = time() - (86400*30);

	$chercher_aventure_jour = $bdd->prepare('SELECT id, aventures FROM autorpg_endgames WHERE temps > :temps ORDER BY aventures DESC');
	$chercher_aventure_jour->execute(array('temps' => $semaine));
	$nb_aventure_jour = $chercher_aventure_jour->fetch();

	$chercher_quetes_jour = $bdd->prepare('SELECT id, quetes FROM autorpg_endgames WHERE temps > :temps ORDER BY quetes DESC');
	$chercher_quetes_jour->execute(array('temps' => $semaine));
	$nb_quetes_jour = $chercher_quetes_jour->fetch();

	$chercher_objets_achetes_jour = $bdd->prepare('SELECT id, objetsachetes FROM autorpg_endgames WHERE temps > :temps ORDER BY objetsachetes DESC');
	$chercher_objets_achetes_jour->execute(array('temps' => $semaine));
	$nb_objets_achetes_jour = $chercher_objets_achetes_jour->fetch();

	$chercher_objets_trouves_jour = $bdd->prepare('SELECT id, objetstrouves FROM autorpg_endgames WHERE temps > :temps ORDER BY objetstrouves DESC');
	$chercher_objets_trouves_jour->execute(array('temps' => $semaine));
	$nb_objets_trouves_jour = $chercher_objets_trouves_jour->fetch();


	$chercher_aventure_mois = $bdd->prepare('SELECT id, aventures FROM autorpg_endgames WHERE temps > :temps ORDER BY aventures DESC');
	$chercher_aventure_mois->execute(array('temps' => $semaine));
	$nb_aventure_mois = $chercher_aventure_mois->fetch();

	$chercher_quetes_mois = $bdd->prepare('SELECT id, quetes FROM autorpg_endgames WHERE temps > :temps ORDER BY quetes DESC');
	$chercher_quetes_mois->execute(array('temps' => $semaine));
	$nb_quetes_mois = $chercher_quetes_mois->fetch();

	$chercher_objets_achetes_mois = $bdd->prepare('SELECT id, objetsachetes FROM autorpg_endgames WHERE temps > :temps ORDER BY objetsachetes DESC');
	$chercher_objets_achetes_mois->execute(array('temps' => $semaine));
	$nb_objets_achetes_mois = $chercher_objets_achetes_mois->fetch();

	$chercher_objets_trouves_mois = $bdd->prepare('SELECT id, objetstrouves FROM autorpg_endgames WHERE temps > :temps ORDER BY objetstrouves DESC');
	$chercher_objets_trouves_mois->execute(array('temps' => $semaine));
	$nb_objets_trouves_mois = $chercher_objets_trouves_mois->fetch();

	$temps_debut_historique = "1441058400";
	$jours = (time() - $temps_debut_historique)/86400;
	$nb_parties_moyen = round($nb_parties/$jours , 2);



	?>
		<p>Au total, il y a <?php echo $nb_parties; ?> parties enregistrées dans la base de données (depuis le 21/09/15), qui comptabilisent <?php echo $nb_aventures; ?> tours d'aventures !<br />
		Ce sont également <?php echo $nb_objets_total; ?> objets reçus en jeu (<?php echo $nb_objets_trouves; ?> objets trouvés et <?php echo $nb_objets_achetes; ?> objets achetés) !<br />
		Il ne faut pas oublier les quêtes terminées, qui sont comptabilisées au nombre astonomique de <?php echo $nb_quetes; ?>.<br />
		<br />
		<br />
		La moyenne par partie s'élève donc environ à environ :<br />
		<?php echo $nb_parties_moyen; ?> parties par jour, comptabilisant en moyenne <?php echo $nb_aventures_moyen; ?> tours de jeu, <?php echo $nb_objets_trouves_moyen; ?> objets trouvés (et <?php echo $nb_objets_achetes_moyen; ?> objets achetés), et <?php echo $nb_quetes_moyen; ?> quêtes terminées ! 
		</p><br /><br />

		<p><b>Classements :</b></p>

		<p class="decalagegauche">Totaux :<br />
			&bull; Aventure ayant duré le plus longtemps : <a href="?history=<?php echo $nb_aventure_max_total['id']; ?>">#id <?php echo $nb_aventure_max_total['id']; ?></a>, avec <?php echo $nb_aventure_max_total['aventures']; ?> tours joués.<br />
			&bull; Nombre d'objets trouvés le plus grand : <a href="?history=<?php echo $nb_objets_trouves_max_total['id']; ?>">#id <?php echo $nb_objets_trouves_max_total['id']; ?></a>, avec <?php echo $nb_objets_trouves_max_total['objetstrouves']; ?> objets trouvés.<br />
			&bull; Nombre d'objets achetés le plus grand : <a href="?history=<?php echo $nb_objets_achetes_max_total['id']; ?>">#id <?php echo $nb_objets_achetes_max_total['id']; ?></a>, avec <?php echo $nb_objets_achetes_max_total['objetsachetes']; ?> objets achetés.<br />
			&bull; Nombre maximum de quêtes terminées : <a href="?history=<?php echo $nb_quetes_max_total['id']; ?>">#id <?php echo $nb_quetes_max_total['id']; ?></a>, avec <?php echo $nb_quetes_max_total['quetes']; ?> quêtes complétées.
		</p>

		<p class="decalagegauche">Les 7 derniers jours :<br />
			&bull; Aventure ayant duré le plus longtemps : <a href="?history=<?php echo $nb_aventure_jour['id']; ?>">#id <?php echo $nb_aventure_jour['id']; ?></a>, avec <?php echo $nb_aventure_jour['aventures']; ?> tours joués.<br />
			&bull; Nombre d'objets trouvés le plus grand : <a href="?history=<?php echo $nb_objets_trouves_jour['id']; ?>">#id <?php echo $nb_objets_trouves_jour['id']; ?></a>, avec <?php echo $nb_objets_trouves_jour['objetstrouves']; ?> objets trouvés.<br />
			&bull; Nombre d'objets achetés le plus grand : <a href="?history=<?php echo $nb_objets_achetes_jour['id']; ?>">#id <?php echo $nb_objets_achetes_jour['id']; ?></a>, avec <?php echo $nb_objets_achetes_jour['objetsachetes']; ?> objets achetés.<br />
			&bull; Nombre maximum de quêtes terminées : <a href="?history=<?php echo $nb_quetes_jour['id']; ?>">#id <?php echo $nb_quetes_jour['id']; ?></a>, avec <?php echo $nb_quetes_jour['quetes']; ?> quêtes complétées.
		</p>

		<p class="decalagegauche">Les 30 derniers jours :<br />
			&bull; Aventure ayant duré le plus longtemps : <a href="?history=<?php echo $nb_aventure_mois['id']; ?>">#id <?php echo $nb_aventure_mois['id']; ?></a>, avec <?php echo $nb_aventure_mois['aventures']; ?> tours joués.<br />
			&bull; Nombre d'objets trouvés le plus grand : <a href="?history=<?php echo $nb_objets_trouves_mois['id']; ?>">#id <?php echo $nb_objets_trouves_mois['id']; ?></a>, avec <?php echo $nb_objets_trouves_mois['objetstrouves']; ?> objets trouvés.<br />
			&bull; Nombre d'objets achetés le plus grand : <a href="?history=<?php echo $nb_objets_achetes_mois['id']; ?>">#id <?php echo $nb_objets_achetes_mois['id']; ?></a>, avec <?php echo $nb_objets_achetes_mois['objetsachetes']; ?> objets achetés.<br />
			&bull; Nombre maximum de quêtes terminées : <a href="?history=<?php echo $nb_quetes_mois['id']; ?>">#id <?php echo $nb_quetes_mois['id']; ?></a>, avec <?php echo $nb_quetes_mois['quetes']; ?> quêtes complétées.
		</p>
		<p class="center"><a href="http://l3m.in/p/projets/tests/autorpg/">Retour à l'accueil</a></p>
	<?php

}

?>