Vous essayez de fuir le monstre en jetant un caillou dans la direction opposée d'où vous vous trouvez. C'est raté, il n'est pas si bête et vous fonce dessus.
Vous allez pour vous enfuir mais le monstre vous attrape par votre cape. Vous avez l'air bête et devez le combattre.
Le hasard fait que vous éternuez alors que vous alliez vous retourner pour vous enfuir. Le monstre en profite pour s'approcher assez de vous pour que le combat ne soit plus une option.
Vous y faites le coup du "Oh regarde !" en montrant un arbre. Le monstre ne se retourne même pas. Bien tenté, mais c'est loupé. Battez-vous.
Alors que vous tentez de fuir, vous vous rendez compte que vous avez les jambes engourdies par l'effroi. Vous vous ramassez lamentablement. Le temps de vous relevez tant bien que mal et le monstre est sur vous.
Vous aviez tout prévu. Un pas sur le côté et vous tombez dans la rivière qui glougloute tranquillement. Sauf qu'elle était de l'autre côté, la rivière. Vous vous retrouvez face au monstre.
Le monstre vous fonce dessus avant que vous n'ayez eu le temps d'esquisser un geste de fuite. Il est sacrément rapide !
Vous vous prenez une branche alors que vous n'avez même pas fait trois pas pour vous enfuir. Le monstre vous rattrape alors que vous êtes encore sonné.
Alors que vous voulez fuir, vous glissez et tombez. Vous n'êtes qu'un boulet. Et le monstre est sur vous.
L'idée de fuir était tentante, mais le monstre insulte votre mère. Maintenant c'est une affaire d'honneur, vous devez lui foutre une raclée monumentale. 