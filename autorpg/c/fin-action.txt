Rien ne se passe. Vous arrivez finalement devant un croisement et trois choix s'offrent à vous. Vous décidez d'aller tout droit.
Rien ne se passe. Vous arrivez finalement devant un croisement et trois choix s'offrent à vous. Vous décidez d'aller à gauche.
Rien ne se passe. Vous arrivez finalement devant un croisement et trois choix s'offrent à vous. Vous décidez d'aller à droite.
Rien ne se passe. Vous arrivez finalement devant un croisement et trois choix s'offrent à vous. Vous décidez d'aller en arrière.
Vous commencez à vous ennuyer, vous reprenez donc votre route.
C'est l'heure de manger. Vous cherchez ce que vous pouvez trouver de comestible dans la forêt pendant quelques minutes, puis vous vous installez sur une souche pourrie pour dévorer une pomme de pin.
Vous pensez à une arme que vous avez laissé sur votre lit. Ca vous serait bien utile.
Vous pensez à un bouclier, que vous avez laissé sous votre lit. Ca vous serait bien utile.
Vous pensez à une trompette, que vous avez laissé sous votre lit. Ca ne vous serait pas bien utile actuellement.
Vous chantonnez quelques instants une chanson. Rien ne se passe. Vous reprenez votre marche.
Vous levez les yeux au ciel et voyez un nuage en forme de zizi.
Vous marchez un peu et trébuchez sur un caillou. Ca ne vous apporte rien, mis à part d'avoir mal au pied.
Vous entendez un cri au loin. Vous décidez de presser le pas vers ce qui vous semble être la direction opposée.
Vous buvez une gorgée d'eau à l'outre qui pend à côté du corps sans vie dans la cage en face de vous.
Un oiseau chante dans les frondaisons. Vous écoutez ses trilles mélodieuses quelques instants, puis vous en avez marre et y jetez une pierre avant de poursuivre votre route.
Vous croisez Gloufila (celui qui fera plus tard un essuie-tout magique). Il est occupé à lire un livre et ne vous remarque même pas. Vous poursuivez votre chemin.
Une chauve-souris fonce dans votre (très belle) chevelure. Vous étouffez un cri d'angoisse puis la chassez avec des petits mouvements des mains. Sale bête !
Vous marchez quelques temps en rêvassant et vous vous prenez un arbre. Vous vous remettez en marche mais vous ne rêvassez plus.
Il ne se passe rien, tout est tranquille. Trop tranquille. Ça cache quelque chose. Vous vous mettez à courir droit devant vous.
Il ne se passe rien, tout est tranquille. Trop tranquille. Ça cache quelque chose. Vous vous mettez à courir en zig-zag.
Vous avancez tranquillement entre les frondaisons. 