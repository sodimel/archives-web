<?php
session_start();

include('../../config.php');
include('fonctions.php');

if(isset($_GET['deco'])) // deconnexion
	deconnexion();

if(isset($_GET['login']) && isset($_GET['pseudo']) && isset($_GET['passe'])) // connexion du membre
	tentative_connexion($_GET['pseudo'], $_GET['passe']);

if(isset($_GET['caisse']) && droit_ouvrir_caisse())
	ouvrir_caisse();


verifier_connexion(); // màj des variables de session si la personne est connectée

?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>Argent - Devenez riches !</title>
    <link rel="stylesheet" href="design.css" />
    <meta name="viewport" content="width=device-width" />
    <link href="favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <script src="../../analytics.js" type="text/javascript"></script>
    <link href='http://fonts.googleapis.com/css?family=Nunito|Exo+2' rel='stylesheet' type='text/css'>
</head>
<body>

<header>
	<h1>Argent</h1>
	<?php afficher_menu(); ?>
</header>



<footer>
	<p>
		Argent est un projet du <a href="http://l3m.in">l3m website</a>.<br />
		<i>Attention ! La monnaie dont il est question sur ce site n'a aucune valeur !<i>
	</p>
</footer>
</body>