<?php
if ($_COOKIE["Connexion"] == "yep") // CONNECTE
{
if ($_GET['p'] == "logout") {
$connect = "";
setcookie("Connexion", "yep", (time() - 3600));
header('Location: admin.php');
}
if ($_GET['p'] == "nomserv") {
$i = fopen("nomserve.ur", "r+");
ftruncate($i,0);
fseek($i, 0);
fputs($i, $_POST['nom']);
fclose($i);
}
if ($_GET['p'] == "news") {
$i = fopen("new.s", "r+");
ftruncate($i,0);
fseek($i, 0);
fputs($i, $_POST['news']);
fclose($i);
$dateH = date(H)+1;
if ($dateH >23) { // Pour éviter les heures du type "26:30".
switch ($dateH) {
    case "24":
$dateH = 0;
        break;
    case "25":
$dateH = 1;
        break;
    case "26":
$dateH = 2;
        break;}}
$date = 'le '.date(d).'/'.date(m).'/'.date(Y).' à '.$dateH.':'.date(i);
$i = fopen("da.te", "r+");
ftruncate($i,0);
fseek($i, 0);
fputs($i, $date);
fclose($i);
}
if ($_GET['p'] == "description") {
$i = fopen("descriptio.n", "r+");
ftruncate($i,0);
fseek($i, 0);
fputs($i, $_POST['description']);
fclose($i);
}
if ($_GET['p'] == "bandeau") {
$i = fopen("band.eau", "r+");
ftruncate($i,0);
fseek($i, 0);
fputs($i, $_POST['bandeau']);
fclose($i);
}
if ($_GET['p'] == "favicon") {
$i = fopen("favic.on", "r+");
ftruncate($i,0);
fseek($i, 0);
fputs($i, $_POST['favicon']);
fclose($i);
}
if ($_GET['p'] == "mots") {
$i = fopen("mots.clefs", "r+");
ftruncate($i,0);
fseek($i, 0);
fputs($i, $_POST['mots']);
fclose($i);
}
if ($_GET['p'] == "description2") {
$i = fopen("descripti.on", "r+");
ftruncate($i,0);
fseek($i, 0);
fputs($i, $_POST['description2']);
fclose($i);
}
if ($_GET['p'] == "titre") {
$i = fopen("tit.re", "r+");
ftruncate($i,0);
fseek($i, 0);
fputs($i, $_POST['titre']);
fclose($i);
}
?>
<!DOCTYPE html>
<html>
   <head>
        <title>Panneau admin</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="design.css" />
    </head>
<body>
<div id="bandeau">Statut = <span class="gold">connecte</span> (<a href="?p=logout">se deconnecter</a>) - <a href="http://l3m.in/p/projets/serveur/">acces page serveur</a></div>
<div id="page">
<div id="banner"> </div>
<div class="nomserveur">Panneau admin.</div>

<p><span class="gold">Voici la liste des actions disponibles :</span></p>
<form method="post" action="?p=nomserv">
<p>Changer le nom du serveur : <input class="input" type="text" name="nom" maxlength="42" required /><br />
<input type="submit" value="Changer" /></p>
</form>
<form method="post" action="?p=news">
<p>Changer les news : <br /> <textarea name="news">La date et l'heure se mettent à jour automatiquement, mais l'heure peut cafouiller entre 23:00 et 01:00. Il est donc 
déconseillé de mettre le message à jour vers ces heures.</textarea><br />
<input type="submit" value="Changer" /></p>
</form>
<form method="post" action="?p=description">
<p>Changer la description du serveur : <br /> <textarea name="description"></textarea><br />
<input type="submit" value="Changer" /></p>
</form>
<form method="post" action="?p=bandeau">
<p>Changer le contenu du bandeau : <br /> <textarea name="bandeau"></textarea><br />
<input type="submit" value="Changer" /></p>
</form>
<form method="post" action="?p=favicon">
<p>Changer l'url de la favicon : <input class="input" type="text" name="favicon" maxlength="42" required /><br />
<input type="submit" value="Changer" /></p>
</form>
<form method="post" action="?p=mots">
<p>Changer les mots-clefs : <br /> <textarea name="mots">Les machins pour la recherche via mots-clefs sous google, faut pas changer ça souvent.
Ils sont séparés par des virgules, exemple : "mots, clefs, machin, bidule"</textarea><br />
<input type="submit" value="Changer" /></p>
</form>
<form method="post" action="?p=description2">
<p>Changer a description pour les moteurs de recherche : <br />
<textarea name="description2">/!\ C'est la description pour les moteurs de recherche, faut pas la changer souvent non plus.</textarea><br />
<input type="submit" value="Changer" /></p>
</form>
<form method="post" action="?p=titre">
<p>Changer le titre de la page (qui apparait dans l'onglet) : <input class="input" type="text" name="titre" maxlength="42" required /><br />
<input type="submit" value="Changer" /></p>
</form>
<p><a href="?p=logout">Se déconnecter</a>.<br /><br />
<b>Diverses infos de</b> <i>mise en page</i> :<br />
Mettre en gras : &lt;b&gt;texte&lt;/b&gt;<br />
Mettre en italique : &lt;i&gt;texte&lt;/i&gt;<br />
Mettre en "contour doré" : &lt;span class="gold"&gt;texte&lt;span&gt;</p>

</div>
</body>
</html>
<?php } // NON CONNECTE
else {
?>
<!DOCTYPE html>
<html>
   <head>
        <title>Panneau admin</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="design.css" />
    </head>
<body>
<div id="bandeau">Statut = Non connecte (<a class="gold" href="?p=connect">connexion</a>).</div>
<div id="page">
<div id="banner"> </div>
<div class="nomserveur">Panneau admin.</div>
<?php if ($_GET['p'] == "connect") { ?>

<form method="post" action="connect.php">
<p>Mot de passe : <input class="input" type="password" name="password" maxlength="20" required /><br />
<input type="submit" value="Connexion" /> &bull; <input type="reset" value="Réinitialiser" /></p>
</form>

<?php } elseif ($_GET['p'] == "error") { ?>
<p>Vous vous êtes trompé quelque part, puisque le pseudo ou le mdp ne correspond pas. Vous pouvez réessayer.</p>
<?php } else { ?>
<p class="news">Vous devez vous <span class="gold">connecter</span> pour accéder au panneau d'administration.</p>
<?php } ?>
</div>
</body>
</html>
<?php } ?>