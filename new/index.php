<?php include('s/header.php'); ?>
<?php include('s/nav.php'); ?>
<div id="page">
<?php
if (isset($_GET['about'])) // Page d'infos.
{
?>
	<section>
	<h3>A propos du site.</h3>
	<p>Ce site est le fruit de quelques années d'écriture web. Je me suis mis fin 2011 à apprendre le xHTML et le CSS2 via les excellents cours du site du zéro, 
	puis je suis passé à l'html5 et au css3.<br />
	Aujourd'hui le site a beaucoup évolué (cliquez <a href="?changelog" title="Lien vers le changelog.">ici</a> pour vous rendre sur la page de l'historique des versions), et il me 
	sert avant tout d'index de mes différents projets à travers le web (la section membre me permet de proposer des projets non achevés à des testeurs volontaires).</p>
	</section>
	<section>
	<h3>A propos de moi.</h3>
	<p>Je me nomme Corentin Bettiol. J'ai actuellement 18 ans (je suis né en 1995), et je suis en terminale Scientifique au lycée général et technologique de Bollène (un coin paumé entre 
	Lyon et Marseille). J'aime bien internet (de par son concept de base : transmettre des informations en passant par différents appareils connectés), et plus particulièrement le 
	web, qui est un véritable monde à part, avec ses caractéristiques techniques et ses utilisateurs, qui le modèlent au fil du temps.</p>
	</section>
	
	<section>
	<h3>Les crédits.</h3>
	<p>&bull; Le site a été entièrement codé par Corentin Bettiol.<br />
	&bull; Polices d'écritures importées depuis <a href="http://www.google.com/webfonts">Google Webfonts</a>.<br /></p>
	</section>
	
	<section>
	<h3>Me contacter.</h3>
	<p>Vous pouvez remplir ce formulaire si vous avez une remarque à propos du site, ou bien si vous voulez qu'on discute d'un projet éventuel.</p>
	<form method="post" action="?mail">
	<label for="pseudo">Prénom ou bien pseudonyme :</label> <input type="text" name="pseudo" id="pseudo" maxlength="42" required /><br />
	<label for="email">Adresse email <i>(pour vous répondre)</i> :</label> <input type="email" name="email" id="email" maxlength="42" required />
	<br />
	<label for="sujet">Sujet du message :</label> <input class="input inputdroite" type="text" name="sujet" id="sujet" maxlength="255" /><br />
	<label for="message">Message :</label> <textarea name="Message" id="message" row="10" cols="40" required>Entrez votre message ici, que ce soit une question, une suggestion ou une remarque.</textarea>
	<br />
	<input type="submit" value="Envoyer le message" /> &bull; <input type="reset" value="Réinitialiser" class="input" />
	</form>
	</section>
<?php
}
elseif (isset($_GET['changelog'])) // Page du changelog.
{
?>
	<section>
	<h3>Changelog du site.</h3>
	<p>&bull; 22/04/14 | Petite màj du design, la page "à propos" est à peu près remplie.<br />
	&bull; 21/04/14 | Grosse MàJ du design, création rapide de la navigation entre les pages en php.<br />
	&bull; 21/03/14 | Création de cette nouvelle version. Création du menu.
	</p>
	</section>
	
	<section>
	<h3>Le futur du site.</h3>
	<p>&bull; Plein de projets.<br />
	&bull; Un chat.<br />
	&bull; Un forum.<br />
	&bull; Section membre.<br />
	&bull; Section projets.<br />
	&bull; Section blog.
	</p>
	</section>
<?php
}
elseif (isset($_GET['blog'])) // Page du blog.
{
?>
	<section>
	<h3>Le blog.</h3>
	</section>
	
	<section>
	<h3>Titre du message.</h3>
	<div class="infos_blog">posté le 21/04/14, 0 commentaire.</div>
	<p>
	Voici un message servant d'exemple.<br />
	<i>Italique</i>, <b>gras</b>, <a href="#" title="#">lien</a>.<br />
	<a href="http://imgup.motion-twin.com/twinoid/f/2/908119f1_1923594.jpg" title="image"><img src="http://imgup.motion-twin.com/twinoid/f/2/908119f1_1923594.jpg" class="img_article" /></a><br />
	<a href="http://bigbrowser.blog.lemonde.fr/files/2013/05/video-game-controller-evolution-large1.jpg" title="image"><img src="http://bigbrowser.blog.lemonde.fr/files/2013/05/video-game-controller-evolution-large1.jpg" class="img_article" /></a><br />
	
	</p>
	</section>
<?php
}
elseif (isset($_GET['projets'])) // Page des projets.
{
?>
	<section>
	<h3>Mes projets.</h3>
	</section>
<?php
}
elseif (isset($_GET['login'])) // Page de connexion.
{
?>
	<section>
	<h3>Connexion.</h3>
	</section>
<?php
}
else {
?>
	<section>
	<h3>Mon site perso.</h3>
	<p>Avec des vrais morceaux de css3 et d'html5 dedans.<br />
	Bienvenue sur mon bout d'internet.</p>
	</section>
<?php
}
?>
</div>
</body>
</html>