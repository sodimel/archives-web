ExileMap

Exploration : Troph�es (points sur l3m.in) pour le nombre de cases parcourues.
	Consomme de l'�nergie (genre 1 point par d�placement, avec une r�g�n�ration � 1 point par heure (cumulable jusqu'� 10 points)).
	Ev�nements al�atoires (ou pas, genre chasse au tr�sor script�e) peuvent relancer l'attrait au jeu pour quelques personnes.
Pvp : Chercher les joueurs, les d�fier ou les attaquer (d�bloque des troph�es).
	D�fi gratuit mais d�pendant de la bonne volont� de l'adversaire potentiel qui peut refuser le combat. Attaque ch�re (genre 5 points).
	Chercher un joueur = action � 1 point (indique uniquement une direction).
	Syst�me de chat : on ne sait pas qui parle, et la port�e du message et sa dur�e sont limit�es.
	Possibilit� d'aider les autres joueurs (donc de se forger une r�putation sur le jeu).
S'enrichir : Exploiter des zones sp�cifiques (qui changent avec le temps) pour gagner le plus de "monnaie du jeu" (d�bloque des troph�es + moyen de convertir la monnaie en pi�ces sur l3m.in).
	Gagner des points sur le jeu, augmenter l�g�rement l'�nergie qu'on peut gagner au maximum (genre boutique avec 1000 points -> + 1 point d'�nergie � vie).
	Exploiter une zone ne nous met pas � l'abris d'une attaque, attention � ne pas miner dans le centre, donc.
	Possibilit� de louer la zone pour un temps d�fini (genre boutique avec 1000 points -> une semaine de location) : nous met � l'abris d'une attaque (mais pas de la destruction de pixel-art si on en a mis un).
Cr�ation : Possibilit� de modifier la "topologie" de la zone, en gros une grille de 10px*10px par zone que l'utilisateur peut modifier s'il contr�le la zone en uploadant une image(d�bloque des troph�es + vue a�rienne accessible depuis la map, donc visibilit� pour le contenu du pixel-art de la zone).
	Possibilit� de "prot�ger" le pixel-art (on renforce les mat�riaux sur terre, donc difficult� pour les autres joueurs de d�truire le pixel-art).
Destruction : Si la personne n'est que de passage sur une zone, elle peut d�truire entre 0 (80% de chance par d�faut) et 2 (respectivement 15% pour 1 et 5% pour 2) pixels de l'image.
	Consomme de l'�nergie (beaucoup, genre 5: doit dissuader l'utilisateur lambda de d�truire un pixel juste pour le plaisir).
	Si la personne contr�le la zone, elle peut remplacer l'image (possible une fois par jour et payant, avec un taux de r�ussite variable (mais en moyenne �gal � 50%))(d�bloque des troph�es).



TODO :
Syst�me de troph�es (et donc de points sur l3m.in).
Gestion de la map (cr�ation de plein d'entr�es dans une bdd ? Cr�ation de X entr�es sp�cifiques qui ont des bonus, pour un nombre de zones d�termin�es (genre 4000*4000) ?).
Gestion du profil (ressources, syst�me d'arm�e pour attaquer/d�fendre ?, points d'�nergie qui reviennent chaque heure (pr�voir deux champs heure dans la bdd)).
Gestion des images (upload de petites images ayant pour nom leurs coordonn�es =� permet de faire une boucle qui cherche si une zone � afficher a une image).
  --� Destruction pixel par pixel : via gd (attention � la ram du serveur, mais bon pour des pixels sur des images de 10*10px �a devrait aller je pense).
      $couleur = imagecolorallocate($image, r, g, b);
      ImageSetPixel ($image, $x, $y, $couleur);
Boutique (ce serait cool de cr�er une base pour tous les jeux � venir et de la personnaliser pour chaque jeu). Faudrait que je r�fl�chisse sur les prix aussi. Ceux donn�s dans ce fichier ne le sont qu'� titre d'exemple.

Donc en gros : pas de but r�el dans ce jeu mais une multitude de petits buts � atteindre, tant sous la forme d'achievements globaux � la plateforme l3m.in que sous la forme de, euh, d'une sensation de pouvoir et de ma�trise du jeu (genre les images, les ressources, les attaques...).
S'inspire un peu du principe de kube (images de zones, exploration, r�colte des ressources).


Points positifs :
Pas vraiment de graphismes. Ca peut �tre pli� en une apr�s-midi je pense, si je sais bien quoi faire (genre des fl�ches pour se d�placer, des icones pour les ressources et pour les �v�nements (al�atoires ou non).
Possibilit� de rajouter UN TOUT PETIT c�t� "payant", du genre pouvoir acheter plein de points (mais pas trop histoire de pas tout d�s�quilibrer le jeu quand m�me, genre gagner quelque heures ou jours de minage/d'exploration). Ainsi je pourrais payer l'h�bergement du serveur (et si ya des b�n�fices �a ira dans le loyer de mon appart + des bonus si vraiment �a fait un buzz de folie et que plein de gens payent.

Points n�gatifs :
Faut VRAIMENT PAS tomber dans le c�t� obscur du jeu en ligne, le c�t� pay2win. Donc si mon�tisation de contenu il y a, il faut vraiment que �a soit soft et que �a d�s�quilibre pas trop l'univers du jeu.
Ca risque d'�tre chiant � �quilibrer tout �a.
Les �v�nements al�atoires doivent pas �tre trop nombreux et en m�me temps si yen a trop peu le joueur peut se d�sint�resser du jeu.
Le syst�me social est un peu limit� (le syst�me de chat est un peu tout ce qu'il y a). Faut que je pense � l'impossibilit� de r�v�ler son pseudo et ses coordonn�es (un gros regex bien bourrin avec le pseudo et l'utilisation de num�ros �a devrait le faire. Qu'il remplace les coordonn�es par "Au nord-est" pour les indications les plus pr�cises).
Le nombre de joueurs. Si y'en a trop et qu'ils jouent vraiment souvent au bout d'un moment le serveur risque de pas aimer. Donc faut que je mon�tise et que je passe � l'offre au dessus ou bien que je limite le nombre de joueurs.
Eviter de laisser trop de failles dans le code pasque c'est un gros projet �a :c

sodi - 26/11/14