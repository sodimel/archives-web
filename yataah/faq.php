<?php
if ($_GET['p'] == "contact"){
$pseudo=$_POST['Pseudonyme'];
$email=$_POST['Email'];
$sujet="[QUESTION YATAAH] -" . $_POST['Sujet'];
$message=$_POST['Message'];
$vers = "addr";
$subject = "[Yataah - question]";
$headers = "From " . $votre_nom;
$headers = 'Content-type: text/html; charset=iso-8859-1';
$content .="Pseudonyme : <b>" . $pseudo ."</b><br>";
$content .="Email : <b>" . $email ."</b><br>";
$content .= $message;
$content .="<br></body></html>";
mail($vers,$sujet,$content,$headers);
header("Location:http://l3m.in/p/yataah/faq.php?p=maildone");
}
 include("header.php"); ?>

<?php
if ($_GET['p'] == "tutoriel") /*  SECTION TUTORIEL PROLOGUE  */
{ ?>
<p><b>Les bases du serveur<sup>Partie 1 - Les maps</sup></b></p>

<p>Le serveur comporte plusieurs cartes :<br />
&bull; La <b>map de construction semi-rp</b>. Vous n'avez pas le droit de grief un b�timent (ou m�me la map en g�n�ral, puisque celle-ci n'est pas r�initialis�e). 
Vous choisissez votre coin, b�tissez ce que vous voulez et demandez � un admin de s�curiser la zone gr�ce � un cuboid. Le principe du semi-RP est de vous donner la
possibilit� de construire ce que vous souhaitez du moment que vous y donnez un sens (un cadre et une fonction (genre ville, forteresse...)). Vous pouvez y construire 
des usines � usage strictement priv�, tout en �vitant les tours � mobs et les spawner am�nag�s.<br />
&bull; La <b>carte de collecte des ressources</b> est r�initialis�e chaque mois afin d'�viter qu'elle prenne trop de place et que les ressources soient facilement 
accessibles � tous les membres (m�me aux nouveaux). Aussi ne vous amusez pas � construire quelque chose de pharamineux dessus, vous perdriez tout ! Le PvP est 
activ� sur cette map de niveau de difficult� normal, et le grief y est autoris�. Vous pouvez y construire des usines autant que vous voudrez.<br />
&bull; La <b>carte des terrains</b> est une carte superflat, o� chaque membre qui en fait la demande peut se faire prot�ger un terrain gratuitement et sur lequel il 
peut construire ce qui lui chante. G�n�ralement du stuff de base (servant aux autres projets) y est stock�, et des magasins sont ouverts. C'est l'endroit aux 
trouvailles par excellence, le PvP y est d�sactiv�, les monstres ne spawnent pas et la map est en peaceful, mais vous avez le droit d'y construire autant d'usines 
que vous le voulez sur vos terrains.
</p>
<br />

<p><b>Les bases du serveur<sup>Partie 2 - Les cuboid</sup></b></p>

<p>&bull; Le cuboid est illimit� en taille ou en nombre pour le joueur sur la map de construction semi rp du moment qu'il est justifi� (�vitez quand-m�me de faire 5 
cuboid de 15.000 blocs de long).<br />
&bull; Il n'est en revanche pas possible d'en faire sur la map de collecte de ressources, puisque cela ne sert � rien.<br/>
&bull; La carte des terrains dispose d'une limitation � deux cuboid par joueur, d'une longueur de 25 blocs, et d'une largeur... �gale 
(soit 25*25 blocs de terrain) par cuboid. Les cuboid peuvent �tre li�s (suppression de l'entre-cuboid) si les terrains sont c�te-�-c�te.</p>
<br />

<p><b>Les bases du serveur<sup>Partie 3 - Les plugins</sup></b></p>

<p>Le serveur comporte plusieurs plugins qui ont pour vocation de le faire tendre vers le RP. Actuellement c'est un serveur avec une map semi-RP, sympa quoi.
<br /><br />
&bull; <b>AuthMe reloaded</b> est un plugin qui vous permet de s�curiser votre pseudo (le serveur �tant ouvert aux versions crack, il est imp�ratif afin de 
sauvegarder vos inventaires). Vous pouvez, lors de votre premi�re connexion, prot�ger votre pseudo (commande <b>/register motdepasse motdepasse</b>... Oui, deux 
fois le mot de passe). Ensuite il vous faudra entrer la commande <b>/login  motdepasse</b> � chacune de vos connexions.<br /><br />
&bull; <b>ChairsReloaded</b> vous permet de vous asseoir (accroupissez-vous puis faites un clic droit) sur la plupart des escaliers. Un petit plugin tr���s RP.
<br /><br />
&bull; <b>Decapitation</b> est un plugin RP au possible. En effet, lorsque vous tuez un autre membre du serveur vous gagnez un item "t�te" � l'effigie du skin de 
la t�te de la personne tu�e !<br /><br />
&bull; <b>Essentials</b> (en plus de configurer une grande partie du serveur) dispose d'une section "homes". Ce sont des points de t�l�portation que le joueur 
d�finit en entrant la commande <b>/sethome nomduhome</b>. Les coordonn�es du joueur sur la map sont alors sauvegard�es, et il pourra � loisir s'y rendre en entrant 
la commande <b>/home nomduhome</b>. C'est vachement pratique, et un joueur a le droit � 10 homes.<br /><br />
&bull; <b>iConomyChestShop</b> est le plugin de cr�ation des boutiques. Pour en cr�er une vous allez d�bourser 5 mRod (la monnaie du serveur), et devez poser un 
panneau au dessus d'un coffre.
<br /><b>1�re ligne :</b> Votre pseudo.
<br /><b>2�me ligne :</b> La quantit� vendue (ou achet�e) d'un coup.
<br /><b>3eme ligne :</b> Le prix de vente : Le prix d'achat (donnera un truc du genre 15:12).
<br /><b>4eme ligne :</b> L'ID de l'item, ou bien le nom en anglais (cliquez <a href="http://minecraft-ids.grahamedgecombe.com/" target="_blank">ici</a> 
pour avoir la liste des blocs et leur id).<br />
</p>
<div class="center"><img src="img/tutoshop.png" class="image" /></div>
<br />

<p><b>Les bases du serveur<sup>Partie 4 - Visiter le serveur</sup></b></p>
<p><b>Les visiteurs</b>, vous pouvez vous connecter sur le serveur pour... visiter (sans besoin de connexion ou d'inscription pr�alable, sauf la protection de votre 
pseudo).</p>
<p><span class="decale">�� <a href="faq.php">Retourner sur l'aide</a>.</span></p>
<?php
}
elseif ($_GET['p'] == "faq") /*  SECTION FAQ  */
{ ?>

<p class="center"><b>F<sub>oire</sub> A<sub>ux</sub> Q<sub>uestions</sub></b>, r�ponse � l'essentiel et au reste !</p>
<p>Coming soon.</p>
<p><span class="decale">�� <a href="faq.php">Retourner sur l'aide</a>.</span></p>
<?php
}
elseif ($_GET['p'] == "formulaire") /*  SECTION FORMULAIRE  */
{ ?>

<p class="center"><b>Formulaire</b>, envoyez votre question.</p>
<br />
<div class="center">
<form method="POST" action="faq.php?p=contact">
<input class="input" type="text" name="Pseudonyme" id="pseudo" placeholder="Pseudonyme" maxlength="25" required /> &bull;
<input class="input" type="email" name="Email" id="email" placeholder="eMail" maxlength="75" />
<br >
<input class="input" type="text" name="Sujet" id="sujet" placeholder="Titre du message." maxlength="255" style="width: 30%;" />
<br />
<textarea name="Message" id="details" placeholder="Posez votre question dans ce champ." style="width: 35%;" required class="input"></textarea>
<br />
<input class="input2" type="submit" value="Envoyer le message" /> &bull; <input class="input2" type="reset" value="R�initialiser" />
</form>
</div>

<p><span class="decale">�� <a class="lien" href="faq.php">Retourner sur l'aide</a>.</span></p>
<?php
}
elseif ($_GET['p'] == "maildone"){ ?>
<p><b>Votre question a bien �t� envoy�e !</b><br />Une r�ponse devrait bient�t �tre upload�e sur le site, si la question est pertinente.<br /><br />
<span class="decale">�� <a href="faq.php?p=faq">Retourner sur l'aide</a>.</span></p>
<?php
}
elseif ($_GET['p'] == "plugins"){ // LISTE DES PLUGINS?>
<p>
<b>AuthMe :</b> Ce plugin permet d'attribuer � chaque pseudonyme un mot de passe. Si l'utilisateur se connecte sous un pseudonyme autre que le sien 
(avec la version crack�e), eh bien il se verra demander un mot de passe. Si celui-ci est faux l'utilisateur est kick du serveur. Tr�s pratique pour prot�ger les pseudos.
<br /><br /><b>Lockette :</b> Permet de verrouiller les portes/les fours/les coffres/des blocs � l'aide de panneaux. Tr�s utile.
<br /><br /><b>MagicCarpet :</b> Plugin r�serv� aux visiteurs du serveur : ils peuvent visiter la map ais�ment avec le tapis magique sans pour autant pouvoir se cheater des 
items (comme ce serait le cas en gamemode :P). Ainsi la visite est simplifi�e et plus agr�able.
<br /><br /><b>Essentials :</b> Plugin qui configure les grades/les warps/le spawn, ainsi que d'autres trucs relatifs au jeu. Juste le truc vital qui permet au serveur de tenir debout.
<br /><br /><b>WorldEdit</b> et <b>WorldGuard :</b> Plugins ayant servi � l'am�nagement du serveur avant l'arriv�e des membres ainsi qu'aux protections de 
terrain pour les villes du serveur. En gros c'est ce qui permet aux villes de rester safe et fun.
<br /><br /><b>AntiCreeper :</b> Permet d'emp�cher les creepers de tout faire sauter.
<br /><br /><b>iConomyChestShop :</b> Permet la cr�ation de boutiques, le tout joliment.
<br /><br /><b>ChimneySMP :</b> Permet la cr�ation de chemin�es en multi. Un petit plugin tr�s RP.
<br /><br /><b>Decapitation :</b> Vous voulez des troph�es dans la lign�e des t�tes de mobs de la 1.4 ? En voil�. Lorsque vous tuez un joueur, vous gagnez un item t�te avec son skin.
<br /><br /><b>ChairsReloaded :</b> Permet de s'assoir sur les escaliers. Le plugin RolePlay par excellence sur un serveur qui se veut semi RP \o/
<br /><br /><b>No Cheat Plus :</b> Plugin anti-cheat tr�s pratique.
</p>
<?php }
else /*  MESSAGE D'ACCUEIL  */
{?>
<p>
<b>Les bases du serveur :</b> Ce lien vous m�ne vers une page de tutoriel rapide pour vous communiquer les bases du serveur, ainsi que son fonctionnement interne.
<br /><span class="decale">�� <a class="lien" href="?p=tutoriel">Lien vers le tutoriel</a>.</span>
<br /><span class="decale">�� <a class="lien" href="?p=plugins">Liste des Plugins</a>.</span>
<br /><br />
<b>Foire Aux Questions :</b> La foire aux questions est l'endroit qui liste vos interrogations concernant des points pr�cis du serveur. Un formulaire est � votre disposition afin 
de vous permettre de poser une question qui vous semble importante.
<br /><span class="decale">�� <a class="lien" href="?p=faq">Lien vers la FAQ</a>.</span>
<br /><span class="decale">�� <a class="lien" href="?p=formulaire">Lien vers le formulaire</a>.</span>
</p><br />

<p>
<b>Cr�dits : </b>
 Le serveur est h�berg� par PulseHeberg et tourne sous CraftBukkit. Le site web a �t� r�alis� par sodimel en quelques heures. 
La bonne humeur est apport�e par les membres quotidiennement, et une conversation skype est disponible lorsque vous �tes membre.
</p><br />

<p>
<b>Historique :</b>
</p>
<ul>
<li>Cr�� courant janvier 2012, h�berg� sur l'ordinateur de sodimel et tournant sous minecraft_server.jar.</li>
<li>Passage sous CraftBukkit vers juin 2012.</li>
<li>Ajout de plugins (essentials, lockette...).</li>
<li>Serveur d�di� lou� le 27 septembre 2012.</li>
<li>Grande mise-�-jour des maps le 12/02/13.</li>
<li>M�J (encore) pour la 1.6.2 de minecraft vers d�but aout 2013.</li>
</ul>
<p>Derni�re m�j de la page : le 20/08/13 � 23:55.</p>
<?php } include("footer.php"); ?>