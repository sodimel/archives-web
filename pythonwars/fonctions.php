<?php

function fonction_query(){
	include('../../../../fonctions/connexionbdd.php');
	if(isset($_GET['connect']) && isset($_GET['pseudo']) && $_GET['passe']){
		$pseudo = htmlspecialchars($_GET['pseudo']);
		$cherchermembre = $bdd->prepare('SELECT * FROM pythonwars where pseudo_l3m = :pseudo and mdp = :passe') or die(print_r($bdd->errorInfo()));
		$cherchermembre->execute(array('pseudo' => $pseudo, 'passe' => $_GET['passe']));
		if($infos = $cherchermembre->fetch()){
			$temps = time()+(5*60);
			$modifcamp = $bdd->prepare('UPDATE pythonwars SET ip_connexion = :ip_connexion, temps_connect = :temps_connect where pseudo_l3m = :pseudo') or die(print_r($bdd->errorInfo()));
    		$modifcamp->execute(array('ip_connexion' => $_SERVER["REMOTE_ADDR"], 'temps_connect' => $temps, 'pseudo' => $pseudo));
			echo "1";
		}
		else{
			echo "0";
		}
	}

	elseif(isset($_GET['verif']) && isset($_GET['pseudo'])){
		$pseudo = htmlspecialchars($_GET['pseudo']);
		$cherchermembre = $bdd->prepare('SELECT * FROM l3m_membres where pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$cherchermembre->execute(array('pseudo' => $pseudo));
		if($infos = $cherchermembre->fetch()){
			$cherchermembre = $bdd->prepare('SELECT * FROM pythonwars where pseudo_l3m = :pseudo') or die(print_r($bdd->errorInfo()));
			$cherchermembre->execute(array('pseudo' => $pseudo));
			if($info = $cherchermembre->fetch()){
				echo "1";
			}
			else{
				echo "2";
			}
		}
		else{
			echo "0";
		}
	}

	elseif(isset($_GET['creer']) && isset($_GET['pseudo'])){
		$pseudo = htmlspecialchars($_GET['pseudo']);
		$cherchermembrel3m = $bdd->prepare('SELECT id FROM l3m_membres where pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$cherchermembrel3m->execute(array('pseudo' => $pseudo));
		if($info = $cherchermembrel3m->fetch()){
			$cherchermembre = $bdd->prepare('SELECT * FROM pythonwars where pseudo_l3m = :pseudo') or die(print_r($bdd->errorInfo()));
			$cherchermembre->execute(array('pseudo' => $pseudo));
			if($infos = !$cherchermembre->fetch()){
				$mdp = rand(11111,99999);
				$inscriptionbdd = $bdd->prepare('INSERT INTO pythonwars(id_l3m, pseudo_l3m, mdp, ip_connexion) VALUES(:id, :pseudo, :mdp, :ip)');
			    $inscriptionbdd->execute(array('id' => $info['id'], 'pseudo' => $pseudo, 'mdp' => $mdp, $_SERVER["REMOTE_ADDR"]));
				echo "0";
			}
		}
	}

	elseif(isset($_GET['reset']) && isset($_GET['pseudo']) && isset($_GET['passe'])){
		$pseudo = htmlspecialchars($_GET['pseudo']);
		$mdp = htmlspecialchars($_GET['passe']);

		$cherchermembre = $bdd->prepare('SELECT * FROM l3m_membres where pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$cherchermembre->execute(array('pseudo' => $pseudo));
		if($infos = $cherchermembre->fetch()){
			if(sha1($infos['mdp']) == $mdp){
				$mdp = rand(11111,99999);
				$modifcamp = $bdd->prepare('UPDATE pythonwars SET mdp = :mdp where pseudo_l3m = :pseudo') or die(print_r($bdd->errorInfo()));
    			$modifcamp->execute(array('mdp' => $mdp, 'pseudo' => $pseudo));
    			if(!isset($_GET['noredirect'])){
    				header('Location:http://l3m.in/member.php?jeux');
    			}
    			else{
    				header('Location:http://l3m.in/p/projets/tests/pythonwars/');
    			}
			}
		}
	}
}

function mdpPythonwars(){
	include('../../../../fonctions/connexionbdd.php');
	$cherchermembre = $bdd->prepare('SELECT * FROM pythonwars where pseudo_l3m = :pseudo') or die(print_r($bdd->errorInfo()));
	$cherchermembre->execute(array('pseudo' => $_COOKIE['l3m_pseudo']));
	if($info = $cherchermembre->fetch()){
		echo "<br /><i>Vous êtes connecté sur le l3m website, ". $_COOKIE['l3m_pseudo'] .".<br />Mot de passe pour PythonWars : ". $info['mdp'] ." - <a class=\"petit\" href=\"http://l3m.in/p/projets/tests/pythonwars/?query&reset&pseudo=". $_COOKIE['l3m_pseudo'] ."&passe=". sha1($_COOKIE['l3m_mdp']) ."&noredirect\">reset le mdp ?</a></i>";
	}
}



?>