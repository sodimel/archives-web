#!/usr/bin/python

# -*- coding: utf-8 -*-
from getpass import getpass
import webbrowser 
import urllib.request
import time

def check_connection(reference):
    try:
        urllib.request.urlopen(reference, timeout=1)
        return True
    except urllib.request.URLError:
        return False

def affichageDebut():
	print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
	print(".-.    . .           .  .\n|-'. .-|-|-. .-..-.  |/\|.-. .-..-\n'  '-| '-' '-`-'' '  '  '`-`-'  -'\n   `-'\n   >>> Un projet du l3m website - http://l3m.in\n\n\n\n\n\n")

	a = input("Etes-vous déjà inscrit sur le l3m website ? ")
	b = 0
	while a != "non" and a != "nn" and a != "no" and a != "n" and a != "oui" and a != "ui" and a != "y" and a != "yes":
		a = input("Merci de répondre uniquement par oui ou par non.\nEtes-vous déjà inscrit sur le l3m website ? ")
	while a=="non" or a=="nn" or a=="n" or a=="no":
		if a=="non" or a=="nn" or a=="n" or a=="no":
			print("Ouverture du navigateur sur la page d'inscription dans 2 secondes...")
			time.sleep(1)
			print("1..")
			time.sleep(1)
			webbrowser.open('http://l3m.in/?inscription')
			print("ouverture")
			time.sleep(2)
		b=b+1
		if(b>5):
			a = input("Vous en faites trop, inscrivez-vous et arrêtez ce spam pls.\nVous êtes inscrit ? ")
		else:
			a = input("Et maintenant ? Vous êtes inscrit ? ")

	if a=="oui" or a=="ui" or a=="y" or a=="yes":
		reponse = "0"
		while reponse == "0":
			print("\n\n------ -- --------- -- --- -------\nModule de connexion au l3m website\n------ -- --------- -- --- -------\n\n")
			pseudo = input("Entrez votre pseudo : ")
			print("Appel de la page...")
			adresse = 'http://l3m.in/p/projets/tests/pythonwars/?query&verif&pseudo='+ pseudo
			question = urllib.request.urlopen(adresse)
			reponse = str(question.read().decode('utf-8'))
			print("Réponse de la page...")
			if reponse == "1":
				passe = getpass("\nCompte trouvé !\nEntrez votre mot de passe PythonWars (il ne s'affichera pas).\nAttention ! Ce n'est pas votre mot de passe pour le l3m website mais un nombre à 5 chiffres.\nIl est visible sur la liste des jeux si vous êtes connecté (http://l3m.in/member.php?jeux)\nMot de passe : ")

				print("\nConnexion au l3m website...")

				adresse = 'http://l3m.in/p/projets/tests/pythonwars/?query&connect&pseudo='+ pseudo +'&passe='+ passe

				print("Appel de la page...")

				question = urllib.request.urlopen(adresse)
				reponse = str(question.read().decode('utf-8'))

				if reponse == "0":
					print("Réponse de la page...\nCette combinaison pseudo/mdp est incorrecte.")
				elif reponse =="1":
					print("Réponse de la page...\nConnexion réussie !\nAffichage de l'environnement de jeu...")

			elif reponse == "2":
				print("Ce compte existe sur le l3m website, mais ne dispose pas de mot de passe dédié à PythonWars.\nCréation en cours...")
				adresse = 'http://l3m.in/p/projets/tests/pythonwars/?query&creer&pseudo='+ pseudo
				question = urllib.request.urlopen(adresse)
				reponse = str(question.read().decode('utf-8'))
				if reponse == "0":
					print("Création terminée.\nConnectez-vous (le programme redémarre).\nVotre navigateur va ouvrir la page qui contient votre mot de passe PythonWars.\nSi ce n'est pas le cas, rendez-vous sur http://l3m.in/member.php?jeux")
					time.sleep(1)
					webbrowser.open('http://l3m.in/member.php?jeux')
					affichageDebut()
				else:
					print("Il y a eu un erreur, le programme redémarre. Checkez le l3m website pour voir si tout va bien.")
					affichageDebut()

			elif reponse == "0":
				print("Ce compte n'existe pas sur le l3m website.")

def jeu():
	print("\n\n\n\n\n\nComing soon.")