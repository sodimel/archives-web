#!/usr/bin/python

# -*- coding: utf-8 -*-

from lib.fonctions import *

print("Tentative de connexion...")
if check_connection("http://l3m.in/") == False:
	print("Vous n'avez pas accès au l3m website. Vérifiez votre connexion (ou bien vérifiez si le site existe toujours : http://l3m.in).")
else:
	print("Connexion établie...\nAccès au jeu...")
	affichageDebut()
	jeu()