<?php
include('../../../../config.php');
include('fonctions.php');

if(isset($_GET['query'])){
    fonction_query();
}
else {
?>
<!DOCTYPE html>
<html>
   <head>
        <title>PythonWars</title>
        <meta charset="utf-8">
        <meta name="description" content="Tests avec python et un serveur web." />
        <link rel="stylesheet" href="design.css" />
        <link href='https://fonts.googleapis.com/css?family=Slabo+27px|PT+Sans' rel='stylesheet' type='text/css'>
        <link rel="icon" href="favicon.ico" />
        <script src="../../../../analytics.js" type="text/javascript"></script>
        <meta name="viewport" content="width=device-width" />
    </head>
<body>
<div id="page">
	<h1>PythonWars</h1>
	<p><a href="PythonWars.zip">Téléchargez la source ici</a> <span class="petit">(md5 : <?php echo md5_file('PythonWars.zip'); ?>)</span>
	<br /><?php mdpPythonWars(); ?>

    <p>Vous pourrez vous connecter sur le l3m website via le programme (en ligne de commande), puis vous verrez les autres joueurs connectés (à 5 minutes près). Vous pourrez les défier, leur proposer des échanges...<br />
    Si vous gagnez un combat vous gagnerez un point d'xp, et aurez une petite chance de gagner un item rare.</p>

    <p>Quelques screenshots :</p>

    <p class="center"><img src="screen1.png" alt="screenshot" /><br />
    <img src="screen2.png" alt="screenshot" /><br />
    <a href="http://l3m.in/member.php?jeux" target="_blank"><img src="screen3.png" alt="screenshot" /><br /><i>cliquez pour vous rendre sur la page</i></a><br /></p>
</div>
</body>
</html>

<?php } ?>