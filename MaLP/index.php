<!DOCTYPE html>
<html>
   <head>
        <title>Ma liste personnelle !</title>
        <meta charset="utf-8">
        <meta name="description" content="Gérez votre liste de trucs cools via cette page utilitaire." />
        <meta name="Keywords" content="liste, noel, mlp, malp, sodimel, gérer, cadeaux, liens, description" />
        <link rel="stylesheet" href="design.css" />
        <link href='http://fonts.googleapis.com/css?family=The+Girl+Next+Door|Architects+Daughter' rel='stylesheet' type='text/css'>
        <link href="favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    </head>
<body>
<div id="page">
<div id="menu"><a href="index.php"><span class="menu">Accueil</span></a> &bull; <a href="?p=create"><span class="menu">Créer ma liste !</span></a> &bull; <a href="?p=modify"><span class="menu">Modifier ma liste.</span></a></div>
<h1>MaLP</h1>
<h4>Ma Liste Personnelle des liens menant vers des sites proposant des choses que je juge assez intéressantes pour que je puisse les partager avec mes amis.</h4>
<p>Ce site est en cours de construction. Repassez un peu plus tard :)</p>
</div>
</body>
</html>