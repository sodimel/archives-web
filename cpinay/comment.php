<?php if (isset($_GET["envoi"]) && ($_POST["envoi"] == $_POST["envoi2"])) {

        $prenom = htmlspecialchars($_POST['prenom']);
        $nom = htmlspecialchars($_POST['nom']);
        $email = htmlspecialchars($_POST['email']);
        $message = nl2br(htmlspecialchars($_POST['Message']));
        try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
        catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
        $envoyercommentaire = $bdd->prepare('INSERT INTO cpinay_comments(nom, prenom, email, commentaire, datecomment) VALUES(:nom, :prenom, :email, :commentaire, :datecomment)');
        $envoyercommentaire->execute(array('nom' => $nom,'prenom' => $prenom,'email' => $email,'commentaire' => $message, 'datecomment' => date("d/m/Y")));
        $envoyercommentaire->closeCursor();

        header("Location:http://l3m.in/p/projets/cpinay/comment.php?done"); }

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>XX - envoi de mail</title>
    <link rel="stylesheet" href="design.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Source+Sans+Pro|Poiret+One' rel='stylesheet'>
    <meta name="viewport" content="width=device-width" />
    <link href="favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <script type="text/javascript">
        var RecaptchaOptions = {
        theme : 'custom',
        custom_theme_widget: 'recaptcha_widget'
        };
    </script>
</head>
<body>

    <nav>
        <ul>
            <li><a href="index.php" style="width: 300px;">Retour à l'accueil</a></li>
            <li><a href="livre.php" style="width: 300px;">Retour au livre d'or</a></li>
        </ul>
    </nav>

    <div class="titre">
        <h1>XX</h1>
        <h2>Vérification d'envoi de commentaire</h2>
    </div>

<?php if (isset($_GET["done"])) { ?>

    <p>
        Votre commentaire a bien été envoyé, merci beaucoup pour ce retour :)
    </p>

<?php } else { ?>

    <p>
        Avant d'envoyer votre commentaire, merci de repondre à la question pour prouver que vous n'êtes pas un robot :<br />
        <i>Vous pouvez aussi en profiter pour vous relire.</i>
    </p>

    <form method="post" action="?envoi">
        <div class="centre">
            <label for="prenom">Prénom :</label> 
                <input type="text" name="prenom" id="prenom" maxlength="42" value="<?php echo $_POST['prenom']; ?>" required /> <br />
            <label for="nom">Nom :</label> 
                <input type="text" name="nom" id="nom" maxlength="42" value="<?php echo $_POST['nom']; ?>" /> <br />
            <label for="email">Adresse email  :</label> 
                <input type="email" name="email" id="email" maxlength="42" required value="<?php echo $_POST['email']; ?>" /> <br />
            <span class="textarea">
                <textarea name="Message" id="details" required ><?php echo $_POST['Message']; ?></textarea>
            </span>

<?php
    $rand = rand(0,8);
    switch ($rand) {
        case 0:
            $question = "Combien font 2 fois 7 ?";
            $reponse = "14";
            break;
        case 1:
            $question = "Quelle est la quatrième lettre de l'alphabet ?";
            $reponse = "d";
            break;
        case 2:
            $question = "Vous trouvez-vous sur un site ayant un rapport avec le cheval ? (répondre par oui ou par non)";
            $reponse = "oui";
            break;
        case 3:
            $question = "Quelle est la dernière lettre de l'alphabet ?";
            $reponse = "z";
            break;
        case 4:
            $question = "Etes-vous un robot ? (répondre par oui ou par non)";
            $reponse = "non";
            break;
        case 5:
            $question = "Combien font 8 moins 6 ?";
            $reponse = "2";
            break;
        case 6:
            $question = "Entrez le texte suivant : \"azerty \" suivi du résultat de ce calcul : 2+1.";
            $reponse = "azerty 3";
            break;
        case 7:
            $question = "Combien font 6 plus 4 ?";
            $reponse = "10";
            break;
        case 8:
            $question = "Répondez \"oui\" à cette phrase.";
            $reponse = "oui";
            break;
        default:
            $question = "";
            $reponse = "";
            break;
    }
?>

            <input type="text" name="envoi" id="envoi" maxlength="42" value="<?php echo $reponse; ?>" type="hidden" style="display: none;" /><!-- c'est sale mais ça a tendance à fonctionner :c --> <br />
            <input type="text" name="question" maxlength="666" value="<?php echo $rand; ?>" type="hidden" style="display: none;" />
            <label for="envoi2"><?php echo $question; ?></label> 
                <input type="text" name="envoi2" id="envoi2" maxlength="42" required /> <br />
            <br />

        <div class="bouton">
            <input type="submit" value="Envoyer le commentaire." />
        </div>
        </div>
    </form>

    <?php } ?>
</body>
</html>