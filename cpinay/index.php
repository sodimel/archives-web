<?php 
session_start();
include('lib.php');
include('gestion.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>XX</title>
    <meta name="description" content="Site de XX, comportementaliste équin." />
    <link rel="stylesheet" href="design.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Source+Sans+Pro|Poiret+One' rel='stylesheet'>
    <meta name="viewport" content="width=device-width" />
    <link href="favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
</head>
<body>
<?php if(verifconnect()) { ?>
	<nav>
		<ul>
			<li><a href="admin.php" style="width: 300px;">Panneau d'administration</a></li>
		</ul>
	</nav>
<?php } ?>

	<nav>
		<ul>
			<li><a href="index.php" class="active">Accueil</a></li><!--
			--><li><a href="infos.php">Informations</a></li><!--
			--><li><a href="contact.php">Contact</a></li>
		</ul>
	</nav>

	<div class="titre">
		<h1>XX</h1>
		<h2>Comportementaliste équin</h2>
	</div>

	<?php 
		 afficherpage(1); // affiche le contenu relatif à la page 1 (l'accueil)
	?>

	<?php $finphp = tempschargement(); ?>
	<p class="small"><i>La page a été générée en <?php echo round($finphp-$debutphp, 3); ?> secondes.</i></p>

</body>
</html>