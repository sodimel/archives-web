<?php 
session_start();
include('lib.php');
include('gestion.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>XX - Contact</title>
    <meta name="description" content="Site de XX, comportementaliste équin." />
    <link rel="stylesheet" href="design.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Source+Sans+Pro|Poiret+One' rel='stylesheet'>
    <meta name="viewport" content="width=device-width" />
    <link href="favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
</head>
<body>
<?php if(verifconnect()) { ?>
	<nav>
		<ul>
			<li><a href="admin.php" style="width: 300px;">Panneau d'administration</a></li>
		</ul>
	</nav>
<?php } ?>

	<nav>
		<ul>
			<li><a href="index.php">Accueil</a></li><!--
			--><li><a href="infos.php">Informations</a></li><!--
			--><li><a href="contact.php" class="active">Contact</a></li>
		</ul>
	</nav>
		<nav>
		<ul>
			<li><a href="livre.php" style="width: 300px; color: #FBFE95; text-shadow: 0 0 3px #F8FE47;">Visiter le livre d'or</a></li>
		</ul>
	</nav><br /><br />

	<div class="titre">
		<h1>XX</h1>
		<h2>Comportementaliste équin</h2>
	</div>

		<p>
			Vous voici sur la page de contact, vous pouvez remplir ce court formulaire pour m'envoyer un mail.<br />
			Vous pouvez également visiter le livre d'or du site, en cliquant sur le lien en dessous du menu. Le livre d'or est ouvert, vous pouvez y poster un commentaire.<br />
			<span class="small">Les champs marqués d'un * sont obligatoires.</span></p>
		
	<form method="post" action="mail.php">
		<div class="centre">
			<label for="prenom">*Prénom :</label> 
				<input type="text" name="prenom" id="prenom" maxlength="50" required /> <br />
			<label for="nom">*Nom :</label> 
				<input type="text" name="nom" id="nom" maxlength="50" required /> <br />
			<label for="email">*Adresse email :</label> 
				<input type="email" name="email" id="email" maxlength="255" required /> <br />
			<label for="tel">Numéro de Téléphone :</label>
				<input type="tel" name="tel" id="tel" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" /><br />
			<label for="sujet">*Sujet :</label> 
				<input type="text" name="sujet" id="sujet" maxlength="255" required /> <br /> 
				<span class="textarea">
					<textarea name="Message" id="details" required>Entrez votre *Message ici.</textarea><br />
				</span>

			<div class="bouton">			
				<input class="bouton_envoi" type="submit" value="Envoyer le message" />
				<input class="bouton_envoi" type="reset" value="Réinitialiser" />
			</div>
		</div>
	</form>

	<?php 
		$debutsql = tempschargement();
		 afficherpage(3); // affiche le contenu relatif à la page 3 (les infos sur la page de contact)
		$finsql = tempschargement();
	?>

	<p class="small">
		Attention : mesure anti-robot: les points ont été remplacés par des images de points (c'est simple mais ça marche), pensez à bien les remplacer si vous copiez/collez l'adresse email !
	</p>

	<?php $finphp = tempschargement(); ?>
	<p class="small"><i>La page a été générée en <?php echo round($finphp-$debutphp, 3); ?> secondes.</i></p>

</body>
</html>