<?php
session_start();
if(isset($_GET['deco'])){
	session_destroy(); header('Location:http://l3m.in/p/projets/cpinay/admin.php');
}
include('lib.php');
include('gestion.php');

if(isset($_GET['pseudo']) && isset($_GET['passe'])) { connect(); }
if(isset($_GET['editpost'])){ editpost($_GET['editpost']); }
if(isset($_GET['supppost'])){ supppost($_POST['id']); }
if(isset($_GET['newpost'])){ newpost($_POST['page'], $_POST['message']); }

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>XX</title>
    <meta name="description" content="Site de XX, comportementaliste équin." />
    <link rel="stylesheet" href="design.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Source+Sans+Pro|Poiret+One' rel='stylesheet'>
    <meta name="viewport" content="width=device-width" />
    <link href="favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
</head>
<body>

<div class="titre">
	<h1>XX</h1>
	<h2>Panneau d'administration</h2>
</div>

	<nav>
		<ul>
			<li><a href="http://l3m.in/p/projets/cpinay/" style="width: 300px;">Retour à l'accueil</a></li>
		</ul>
	</nav>

<?php if(verifconnect()) { ?>

	<p>
		Tu es connecté(e), <?php echo $_SESSION['pseudo'] ?>. <a href="?deco" class="small">Se déconnecter</a><br />
		<span style="font-size: 0.8em; opacity: 0.8;"><b>Comment mettre une image ?</b><br />
		<span class="decalagegauche"></span>Il te suffit de copier/coller ce modèle et ensuite de le personnaliser :<br />
		<span class="decalagegauche"></span>&lt;img src="adresse de l'image" class="image gauche" alt="description de l'image pour le cas où elle ne peut pas s'afficher" /&gt;<br />
		<span class="decalagegauche"></span>Tu peux mettre "droite" à la place de "gauche" dans le "class" pour aligner l'image à droite et avoir le texte flottant à sa gauche.</span></p>
		<p><span style="color: #A9DBED; text-shadow: 0 0 2px #45B7E1;">Que faire ?</span><br />
		<?php if(isset($_GET['majpage1'])){ echo ">>"; } else { echo "&bull;"; } ?>
		 <a href="?majpage1">Interagir avec le texte de l'accueil</a>.<br />
		<?php if(isset($_GET['majpage2'])){ echo ">>"; } else { echo "&bull;"; } ?>
		 <a href="?majpage2">Interagir avec le texte de la page d'informations</a>.<br />
		<?php if(isset($_GET['majpage3'])){ echo ">>"; } else { echo "&bull;"; } ?>
		 <a href="?majpage3">Interagir avec les moyens de contact</a>.<br />
	</p>

<?php if(isset($_GET['majpage1'])) {
?><p>
	<span style="color: #A9DBED; text-shadow: 0 0 2px #45B7E1;">Page d'accueil :</span><br />
	<?php if(isset($_GET['view'])){ echo ">>"; } else { echo "&bull;"; } ?>
	 <a href="?majpage1&view">Voir ce qu'il y a déjà</a><br />
	<?php if(isset($_GET['modif'])){ echo ">>"; } else { echo "&bull;"; } ?>
	 <a href="?majpage1&modif">Modifier les textes</a><br />
	<?php if(isset($_GET['suppr'])){ echo ">>"; } else { echo "&bull;"; } ?>
	 <a href="?majpage1&suppr">Supprimer un texte</a><br />
	<?php if(isset($_GET['new'])){ echo ">>"; } else { echo "&bull;"; } ?>
	 <a href="?majpage1&new">Ajouter un texte</a>
</p><?php
modifpage(1);
} ?>

<?php if(isset($_GET['majpage2'])) {
?><p>
	<span style="color: #A9DBED; text-shadow: 0 0 2px #45B7E1;">Page des infos :</span><br />
	<?php if(isset($_GET['view'])){ echo ">>"; } else { echo "&bull;"; } ?>
	 <a href="?majpage2&view">Voir ce qu'il y a déjà</a><br />
	<?php if(isset($_GET['modif'])){ echo ">>"; } else { echo "&bull;"; } ?>
	 <a href="?majpage2&modif">Modifier les textes</a><br />
	<?php if(isset($_GET['suppr'])){ echo ">>"; } else { echo "&bull;"; } ?>
	 <a href="?majpage2&suppr">Supprimer un texte</a><br />
	<?php if(isset($_GET['new'])){ echo ">>"; } else { echo "&bull;"; } ?>
	 <a href="?majpage2&new">Ajouter un texte</a>
</p><?php
modifpage(2);
} ?>

<?php if(isset($_GET['majpage3'])) {
?><p>
	<span style="color: #A9DBED; text-shadow: 0 0 2px #45B7E1;">Page de contact</span><br />
	<?php if(isset($_GET['view'])){ echo ">>"; } else { echo "&bull;"; } ?>
	 <a href="?majpage3&view">Voir ce qu'il y a déjà</a><br />
	<?php if(isset($_GET['modif'])){ echo ">>"; } else { echo "&bull;"; } ?>
	 <a href="?majpage3&modif">Modifier les textes</a><br />
	<?php if(isset($_GET['suppr'])){ echo ">>"; } else { echo "&bull;"; } ?>
	 <a href="?majpage3&suppr">Supprimer un texte</a><br />
	<?php if(isset($_GET['new'])){ echo ">>"; } else { echo "&bull;"; } ?>
	 <a href="?majpage3&new">Ajouter un texte</a>
</p><?php
modifpage(3);
} ?>

<?php if(isset($_GET['done'])) {
?>

<p>
L'action a été effectuée.
</p>

<?php } } else { ?>

	<p>
		Tu n'es pas connecté(e). <a href="http://l3m.in/?connect&redirect=cpinay">Se connecter</a>.
	</p>

<?php }

$finphp = tempschargement(); ?>
<p class="small"><i>La page a été générée en <?php echo round($finphp-$debutphp, 3); ?> secondes.</i></p>

</body>
</html>