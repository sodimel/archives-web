<?php 
session_start();
include('lib.php');
include('gestion.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>XX - Informations</title>
    <meta name="description" content="Site de XX, comportementaliste équin." />
    <link rel="stylesheet" href="design.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Source+Sans+Pro|Poiret+One' rel='stylesheet'>
    <meta name="viewport" content="width=device-width" />
    <link href="favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
</head>
<body>
<?php if(verifconnect()) { ?>
	<nav>
		<ul>
			<li><a href="admin.php" style="width: 300px;">Panneau d'administration</a></li>
		</ul>
	</nav>
<?php } ?>

	<nav>
		<ul>
			<li><a href="index.php">Accueil</a></li><!--
			--><li><a href="infos.php" class="active">Informations</a></li><!--
			--><li><a href="contact.php">Contact</a></li>
		</ul>
	</nav>

	<div class="titre">
		<h1>XX</h1>
		<h2>Comportementaliste équin</h2>
	</div>

	<?php 
		 afficherpage(2); // affiche le contenu relatif à la page 2 (les infos)
	?>

	<p class="small">
		<span class="big">Mentions légales</span><br /><br />
		<b>Siège social</b><br />
		<span class="decalagegauche"></span>
			addr<br />
		<span class="decalagegauche"></span>
			cp ville<br />
		<b>Immatriculation</b><br />
		<span class="decalagegauche"></span>
			Numéro SIREN : X
		<br /><br />
		Le site web a été créé par <a href="http://l3m.in/" target="_blank">Corentin Bettiol</a>.<br />
		L'image du fond d'écran provient de <a href="http://fr.forwallpaper.com/wallpaper/horses-field-grass-green-hill-134452.html" target="_blank">forwallpaper</a>.
	</p>

	<?php $finphp = tempschargement(); ?>
	<p class="small"><i>La page a été générée en <?php echo round($finphp-$debutphp, 3); ?> secondes.</i></p>

</body>
</html>