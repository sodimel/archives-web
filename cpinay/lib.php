<?php
// Fonction qui permet de calculer le temps moyen d'éxecution d'une page sur le site.
function tempschargement()
{
	list($usec, $sec) = explode(" ",microtime());
    return ((float)$usec + (float)$sec);
}
$debutphp = tempschargement(); // on prend le timestamp (précis) du moment où on commence à faire les opérations côté serveur, comme ça on a le temps de génération de la page (avec le code en bas de page).


function afficherpage($page)
{
	try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
	catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

	$affichercontenu = $bdd->prepare('SELECT contenu FROM cpinay_paragraphes WHERE page = ? ORDER BY id');
	$affichercontenu->execute(array($page));
	while ($paragraphe = $affichercontenu->fetch()) { ?>
		<p>
			<?php echo $paragraphe['contenu']; ?>
		</p>
	<?php }
	$affichercontenu->closeCursor();
}

function affichercommentaires()
{
	try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
	catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

	$affichercomments = $bdd->query('SELECT * FROM cpinay_comments ORDER BY id DESC');
	while ($commentaire = $affichercomments->fetch()) { ?>
<p class="commentaireparagraphe">
	<span class="commentaireentete">Message de</span> <span style="color: #A9DBED; text-shadow: 0 0 2px #45B7E1;"><?php echo $commentaire['prenom'] ." ". $commentaire['nom']; ?></span><span class="commentaireentete">, posté le <?php echo $commentaire['datecomment']; ?> :
	<?php if(verifconnect()) { ?><br /><span class="small decalagegauche">Email : <a href="mailto:<?php echo $commentaire['email']; ?>"><?php echo $commentaire['email']; ?></a></span><?php } ?></span><br />
	<span class="decalagegauche commentairecomment"><?php echo $commentaire['commentaire']; ?></span>
</p>
<?php }
	$affichercomments->closeCursor();
}

?>