<?php
	function connect(){ // fonction pour si on arrive depuis l3m.in
		try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
		catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

		$connexion = $bdd->prepare('SELECT * FROM l3m_membres WHERE pseudo = :pseudo');
		$connexion->execute(array('pseudo' => htmlspecialchars($_GET['pseudo']))); // on cherche le pseudo
		if ($donnees = $connexion->fetch()) {
			if(htmlspecialchars($_GET['pseudo']) == "sodimel" || htmlspecialchars($_GET['pseudo'] == "ChaPi4")){ // on vérifie que c'est bien moi ou chachatte qui se connecte
				if(sha1($donnees['mdp']) == $_GET['passe']){ // s'il y est, alors on vérifie le passe
					$_SESSION['pseudo'] = $_GET['pseudo']; // si c'est bon on crée la session
					$_SESSION['passe'] = $_GET['passe'];
					header('Location:http://l3m.in/p/projets/cpinay/admin.php'); // et on redirige
				}
			}
		}
	}

	function verifconnect(){ // vérification de connexion (à chaque chargement de la page)
		if(isset($_SESSION['pseudo']) && isset($_SESSION['passe'])) {
			try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
			catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

			$verifconnect = $bdd->prepare('SELECT * FROM l3m_membres WHERE pseudo = :pseudo'); // on cherche le pseudo
			$verifconnect->execute(array('pseudo' => $_SESSION['pseudo']));
			if ($donnees = $verifconnect->fetch()) {
				if($_SESSION['pseudo'] == "sodimel" || $_SESSION['pseudo'] == ""){ // on vérifie que c'est bien moi ou xxx qui est "connnecté(e)"
					if(sha1($donnees['mdp']) == $_SESSION['passe']){ // s'il y est, alors on vérifie le passe
						return true; // si le if(verifconnect()) renvoie vrai, on est connecté puisque les valeurs concordent (comme l'avion)
						exit();
					}
				}
			}
		}
	}

	function modifpage($number){ // la grosse fonction
		if(isset($_GET['view'])) visualiser($number);
		if(isset($_GET['modif'])) editer($number);
		if(isset($_GET['suppr'])) supprimer($number);
		if(isset($_GET['new'])) nouveau($number);
	}

	function visualiser($number){ // permet de voir ce qu'il y a déjà de posté sur la page $number
		?>
		<p>
			Voici le contenu présent sur la page <span style="color: #A9DBED; text-shadow: 0 0 2px #45B7E1;"><?php echo $number; ?></span> (1 = accueil, 2 = page d'infos, 3 = page de contacts) :
		</p>
		<?php
		try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
		catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

		$trouverposts = $bdd->prepare('SELECT * FROM cpinay_paragraphes WHERE page = :page');
		$trouverposts->execute(array('page' => $number));
		while ($post = $trouverposts->fetch()) {
		
		// $post['contenu'] = preg_replace('#\<img(.+)\/>#i', '[image: $1]', $post['contenu']);
		?>
			<p><?php echo $post['contenu']; ?><br /><br />
			<span class="small" style="color: #C6C7C7; text-shadow: 0 0 1px #5F6060;">Id <?php echo $post['id'] ?> - Dernière modification : <?php echo $post['datemaj']; ?></span></p>
		<?php }
		$trouverposts->closecursor();
	}

	function editer($number){ // formulaire d'édition de contenu
		try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
		catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

		$trouverposts = $bdd->prepare('SELECT * FROM cpinay_paragraphes WHERE page = :page');
		$trouverposts->execute(array('page' => $number));
		while ($post = $trouverposts->fetch()) {
			$post['contenu'] = str_replace('<br />', '', $post['contenu']); 
			?>
			<form method="post" action="?editpost=<?php echo $post['id']; ?>">
				<div class="centre">
					Formulaire de modification du paragraphe <?php echo $post['id']; ?> :<br />
						<textarea name="contenu" required><?php echo $post['contenu']; ?></textarea><br />
						<input name="id" type="hidden" style="display: none;" value="<?php echo $post['id']; ?>" />
					<div class="bouton"><input class="bouton_envoi" type="submit" value="Modifier" /></div>
				</div>
			</form>
	<?php
		}
	}

	function editpost($id){
		try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
		catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

		$modifpseudo = $bdd->prepare('UPDATE cpinay_paragraphes SET contenu = :contenu, datemaj = :datemaj WHERE id = :id');
		$modifpseudo->execute(array('contenu' => nl2br($_POST['contenu']), 'datemaj' => date("d/m/Y"), 'id' => $_POST['id']));

		header('Location:http://l3m.in/p/projets/cpinay/admin.php?done'); // et on redirige
	}

	function supprimer($number){
	?>
		<form method="post" action="?supppost">
			<div class="centre">
				Entre l'id du paragraphe à supprimer : <input type="text" name="id" />
				<div class="bouton"><input class="bouton_envoi" type="submit" value="Supprimer le paragraphe (attention, c'est irreversible :c)" /></div>
			</div>
		</form>
	<?php
	}

	function supppost($id){
		try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
		catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
		$supp = $bdd->prepare('DELETE FROM cpinay_paragraphes WHERE id = :id');
		$supp->execute(array('id' => $id));
		$supp->closecursor();

		header('Location:http://l3m.in/p/projets/cpinay/admin.php?done'); // et on redirige
	}

	function nouveau($number){
	?>
		<form method="post" action="?newpost">
			<div class="centre">
				<input name="page" type="hidden" style="display: none;" value="<?php echo $number; ?>" />
				<span class="textarea">
					<textarea name="message" required>Entre le contenu du nouveau paragraphe ici.</textarea><br />
				</span>
				<div class="bouton"><input class="bouton_envoi" type="submit" value="Envoyer le nouveau paragraphe" /></div>
			</div>
		</form>
	<?php 
	}

	function newpost($page, $message){
		try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
		catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

		$newcomment = $bdd->prepare('INSERT INTO cpinay_paragraphes(page, contenu, datemaj) VALUES(:page, :contenu, :datemaj)');
		$newcomment->execute(array('page' => $page, 'contenu' => nl2br($message), 'datemaj' => date("d/m/Y")));
		$newcomment->closecursor();

		header('Location:http://l3m.in/p/projets/cpinay/admin.php?done'); // et on redirige
	}

?>