<?php
include('lib.php');
session_start();
include('gestion.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>XX - Livre d'or</title>
    <meta name="description" content="Site de XX, comportementaliste équin." />
    <link rel="stylesheet" href="design.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Source+Sans+Pro|Poiret+One' rel='stylesheet'>
    <meta name="viewport" content="width=device-width" />
    <link href="favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
</head>
<body>

    <nav>
        <ul>
            <li><a href="index.php" style="width: 300px;">Retour à l'accueil</a></li>
            <li><a href="#commentaireenvoyer" style="width: 300px;">Envoyer un commentaire</a></li>
        </ul>
    </nav>

	<div class="titre">
		<h1>XX</h1>
		<h2>Comportementaliste équin</h2>
	</div>

	<p>Bienvenue sur le livre d'or.<br />
	<i>Ici sont affichés tous les commentaires postés par des visiteurs du site depuis sa mise en ligne.</i></p>

<?php
affichercommentaires();
?>

	<form method="post" action="comment.php" id="commentaireenvoyer">
		<div class="centre">
			Formulaire d'envoi de commentaire :<br />
			<span class="small">Les champs marqués d'un * sont obligatoires.</span><br /><br />
			<label for="prenom2">*Prénom :</label> 
				<input type="text" name="prenom" id="prenom2" maxlength="50" required /> <br />
			<label for="nom2">Nom :</label> 
				<input type="text" name="nom" id="nom2" maxlength="50" /> <br />
			<label for="email2">*Adresse email :<br />
			<i class="small">(n'est pas affichée, n'est pas partagée à des sites tiers)</i></label> 
				<input type="email" name="email" id="email2" maxlength="255" required /> <br /> 
			<span class="textarea">
				<textarea name="Message" id="details" required>Entrez votre Message ici.</textarea><br />
			</span>

			<div class="bouton">			
				<input class="bouton_envoi" type="submit" value="Envoyer le message" />
				<input class="bouton_envoi" type="reset" value="Réinitialiser" />
			</div>
		</div>
	</form>

	<?php $finphp = tempschargement(); ?>
	<p class="small"><i>La page a été générée en <?php echo round($finphp-$debutphp, 3); ?> secondes.</i></p>

</body>
</html>