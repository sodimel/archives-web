Anciens projets archivés
====

Ces projets ne sont plus disponibles sur internet, mais les codes sources sont sauvegardés ici.

Rendez-vous sur [l3m.in](https://l3m.in/projects) pour avoir une liste détaillée de mes projets (anciens & récents).
Mon ancien hébergeur ne m'autorisait qu'une base de données ; une grande partie des tables utilisées par ces projets se situe dans le fichier `l3m_in_structure.sql`.

![logo morse](http://l3m.in/p/up/files/1553185331.png)