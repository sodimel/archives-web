<?php header('Content-type: text/xml');
echo "<?xml version=\"1.0\"?>\n
<rss version=\"2.0\">\n";
?>
	<channel>
		<title>rssBlog - l3m</title>
		<link>http://l3m.in/blog.php</link>
		<description>Fil rss du blog sur l3m.in/blog.php</description>
		<language>fr</language>

		<copyright>Corentin Bettiol</copyright>
		<webMaster> (Corentin Bettiol)</webMaster>

		<image>
			<title>icon l3m</title>
			<url>http://l3m.in/img/logo_petit.png</url>
			<width>75</width>
			<height>75</height>
		</image>

<?php
include('fonctions/connexionbdd.php');

function description($message){
$taille = strlen($message);
	$message = str_replace("<br />", "BBRRBR", $message);
		if($taille > 195){
			$position = strpos($message, " ", 195);
			if($position == 0){
				$position = 195;
			}
		$liresuite = true;
		}
		else{
			$position = $taille;
		}
		$message = substr($message, 0, $position);
		$message .= " ...";
		
		$message = str_replace("BBRRBR", "\n", $message);
		echo mise_en_page($message);
}

function mise_en_page($texte){

	$texte = preg_replace('#MMM(.*?)MMM#s', '$0', $texte); // modération
	$texte = str_replace('MMM', '', $texte); // modération aussi

	$texte = preg_replace('#LLLhttp://[a-z0-9._=\*/?&-]+LLL#', '$0', $texte); // lien
	$texte = str_replace('LLL', '', $texte); // lien aussi

	$texte = preg_replace('#BBB(.*?)BBB#s', '$0', $texte); // gras
	$texte = str_replace('BBB', '', $texte); // gras aussi

	$texte = preg_replace('#III(.*?)III#s', '$0', $texte); // italique
	$texte = str_replace('III', '', $texte); // italique aussi

	$texte = preg_replace('#IMAGE(.*?)IMAGE#s', '$0', $texte); // italique
	$texte = str_replace('IMAGE', '', $texte); // italique aussi

	return $texte;
}

$chercher_nb_articles = $bdd->query('SELECT COUNT(id) FROM l3m_blog');
$nb_articles = ($chercher_nb_articles->fetchColumn()) - 5;

$chercherarticle = $bdd->prepare('SELECT * FROM l3m_blog ORDER BY id DESC LIMIT 0,5') or die(print_r($bdd->errorInfo()));
$chercherarticle->execute();
while($article = $chercherarticle->fetch()){
	?>

		<item>
			<title><?php echo $article['titre']; ?></title>
			<description><?php echo description($article['message']); ?></description>
			<link>http://l3m.in/blog.php?article=<?php echo $article['id']; ?></link>
			<pubDate><?php echo date(DATE_RSS, $article['date']); ?></pubDate>
		</item>

<?php } ?>
	</channel>
</rss>