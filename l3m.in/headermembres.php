<?php
if ($_GET['p'] == "cookies") {
    setcookie('cookie', 'cookie', time() + 365*24*3600, null, null, false, true);
    header("Location:http://l3m.in/");
}
?>

<!DOCTYPE html>
<html>
   <head>
        <title>l3m website - Section membres</title>
        <meta charset="utf-8">
        <meta name="description" content="Site perso de Corentin Bettiol." />
        <meta name="Keywords" content="l3m, website, sodimel, lemidos, morse, bleu" />
        <link rel="stylesheet" href="design.css" />
        <meta name="viewport" content="width=device-width" />
        <link href="img/fav.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
        <link href='http://fonts.googleapis.com/css?family=Gloria+Hallelujah|Karla' rel='stylesheet' type='text/css'>
        <script src="/analytics.js" type="text/javascript"></script>
    </head>
<body>
<div id="top"></div>

<?php if(empty($_COOKIE['cookie'])) { ?>
<section style="font-size: x-small;">
<h2><span>L'utilisation des cookies sur ce site.</span></h2>
<p>Sur le l3m website, des cookies peuvent être enregistrés à votre insu (notamment via google analytics, qui vous espionne et vide vos comptes bancaires, et qui ne sert pas du tout seulement à analyser le traffic de 
ce site web). Les cookies relatifs à ce site contiennent évidemment des données hyper importantes et confidentielles, et ils sont enregistrés sur votre ordi afin de mieux vous faire cibler par des publicités 
qui font que je peux payer l'hébergement.<br />
<i>Vous voulez lire la doc concernant les cookies ? C'est <a target="_blank" href="http://orteil.dashnet.org/cookieclicker/">par ici que ça se passe</a>.</i><br />
<a href="?p=cookies">J'ai compris tout ce baratin, les cookies ne se mangent pas et ne sont pas méchants, merci de ne plus afficher ce message</a> <i>(la situation est ironique puisque le fait de cliquer sur ce 
lien va enregistrer un cookie sur votre ordinateur qui dit au serveur de ne pas afficher ce bandeau d'information)</i>.</p>
</section>
<hr>
<?php } ?>

<div class="remonter"><a href="#top">Remonter</a></div>
<?php if(!isset($_COOKIE["l3m_pseudo"]) && !isset($_COOKIE["l3m_mdp"])) { ?>
<div class="connexion"><a href="http://l3m.in/?connect">Connexion/Inscription</a></div>
<?php } ?>
	<nav>
        <img src="img/logo_petit.png" alt="Logo_morse" class="logo" />
        <h1><a href="http://l3m.in/">l3m website</a></h1>
        <a href="http://l3m.in/member.php">Accueil</a>
        <a href="?profil">Mon profil</a>
        <a href="?forum">Le forum</a>
        <a href="?jeux">Les jeux</a>
        <a href="?membres">Les membres</a>
	</nav>