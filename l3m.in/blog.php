<?php
include("config.php");
include("fonctions/fonctions_blog.php");

	if(isset($_GET['post_comment'])){
		include("fonctions/captcha.php");
		if(captcha_verif() == true){
			blog_commentaire_bdd($_GET['post_comment'], $_POST['pseudo'], $_POST['lien'], $_POST['email'], $_POST['message']);
			header('Location: http://l3m.in/blog.php?done='. $_GET['post_comment'] .'');
			exit();	
		}
	}

include("header.php");
?>

<section id="blog">

<?php

	if(isset($_GET['article']) && intval($_GET['article'] == $_GET['article'])){
		echo "<h2><span>Article du blog</span></h2>";
		echo navigation_articles_blog($_GET['article']);

		blog_afficher_article($_GET['article']);
	}

	elseif(isset($_GET['page']) && intval($_GET['page'] == $_GET['page'])){
		echo "<h2><span>Blog</span></h2>";

		blog_afficher_page($_GET['page']);
	}

	elseif(isset($_GET['done'])){
		if($_GET['done'] == "mauvaiscalcul"){
			echo "<h2><span>Blog</span></h2>
			<p>Votre commentaire n'a pas pu être posté. En effet, soit vous êtes un robot stupide, soit vous êtes un humain pas très malin (admirez la rime).<br />
			<a href=\"https://www.youtube.com/watch?v=iZLAthIqUf8\">Regarder une super vidéo</a> ou bien <a href=\"blog.php\">retourner au blog</a>.</p>";
		}
		else{
			echo "<h2><span>Blog</span></h2>
			<p>Votre commentaire a bien été posté.<br />
			<a href=\"blog.php?article=". $_GET['done'] ."\">Retourner à l'article</a>.</p>";
		}
	}

	else{
		echo "<h2><span>Blog</span></h2>";

		blog_afficher_page(0);
	}

?>

</section>

<?php include("footer.php"); ?>

</body>
</html>