<?php function tentative_connexion($pseudo, $passe){
	$pseudo = htmlspecialchars($pseudo);
	$passe = htmlspecialchars($passe);
	include('../../fonctions/connexionbdd.php');

	$cherchermembre = $bdd->prepare('SELECT * FROM l3m_membres where pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
	$cherchermembre->execute(array('pseudo' => $pseudo));
	if($entrees = $cherchermembre->fetch()){ // si le membre est inscrit sur le l3m website
		if($passe == sha1($entrees['mdp'])){ // si le membre a le bon mdp

			$cherchermembreperso = $bdd->prepare('SELECT * FROM personnalisation_membres where id_l3m = :id') or die(print_r($bdd->errorInfo()));
			$cherchermembreperso->execute(array('id' => $entrees['id']));
			if($entree = $cherchermembreperso->fetch()){ // si le membre est inscrit sur perso

				$_SESSION['nom_page-perso'] = $entree['nom_page'];
				$_SESSION['pseudo-perso'] = $entrees['pseudo'];
				$_SESSION['points-perso'] = $entree['points'];
				$_SESSION['pointstotal-perso'] = $entree['points'];
				$_SESSION['connecte'] = true;
				$_SESSION['id-l3m'] = $entree['id_l3m'];
				header("Location:http://l3m.in/games/personnalisation/?account");

			}
		}
	}
}

?>