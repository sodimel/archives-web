<!DOCTYPE html>
<html>
   <head>
        <title>SpreadTheGame</title>
        <meta charset="utf-8">
        <meta name="description" content="SpreadTheGame est un projet de gain de niveau via l'écriture de codes dans la vie réelle." />
        <link rel="stylesheet" href="design.css" />
        <link rel="icon" href="favicon.ico" />
        <meta name="viewport" content="width=device-width" />
    </head>
<body>
	<h1>SpreadTheGame, un jeu irl.</h1>

	<p>SpreadTheGame est un petit projet créé par <a target="_blank" href="http://l3m.in/">Corentin Bettiol</a>.<br />
	Une fois votre compte créé, vous devrez collecter (et diffuser) des codes qui vous seront donnés. A vous de trouver les meilleurs endroits où recopier votre code (dans les limites de la légalité bien sûr) !<br />
	Attention toutefois ! Au milieu des codes qui vous feront progresser tous les deux (la personne qui a écrit le code et la personne qui l'a entré sur le site), il existe pléthore de codes ayant des effets moins... glorieux. Qui vont 
	du simple retrait de points d'XP au blocage de tous vos codes pendant quelques jours (tout au plus).</p>

	<p class="small">SpreadTheGame est un jeu lié au réseau de jeux du <a target="_blank" href="http://l3m.in">l3m website</a>.</p>

	<p>J'en ai assez lu, je veux me <a href="http://l3m.in/?connect&redirect=stg">connecter via mon compte l3m.in</a> (ou bien m'<a taget="blank" href="http://l3m.in/?inscription">inscrire</a> puis revenir sur cette page) !</p>
	
</body>
</html>