<?php

include('../../config.php');

function deconnexion(){
	session_destroy();
	header("Location:http://l3m.in/member.php?disconnect&redirect=perso");
}

function publicite($id){
	include('../../fonctions/connexionbdd.php');

	$cherchermembreperso = $bdd->prepare('SELECT * FROM personnalisation_membres where id_l3m = :id') or die(print_r($bdd->errorInfo()));
	$cherchermembreperso->execute(array('id' => $id));
	if($entree = $cherchermembreperso->fetch()){
		if($entree['pub'] == 1){
			$pub = $bdd->prepare('UPDATE personnalisation_membres SET pub = 0 where id_l3m = :id') or die(print_r($bdd->errorInfo()));
			$pub->execute(array('id' => $id));
			$pub->closeCursor();
			header("Location:http://l3m.in/games/personnalisation/?account");
		}
		elseif($entree['pub'] == 0){
			$pub = $bdd->prepare('UPDATE personnalisation_membres SET pub = 1 where id_l3m = :id') or die(print_r($bdd->errorInfo()));
			$pub->execute(array('id' => $id));
			$pub->closeCursor();
			header("Location:http://l3m.in/games/personnalisation/?account");
		}
	}
}

function redirection($visit){
	include('../../fonctions/connexionbdd.php');

	$visit = htmlspecialchars($visit);

	$cherchermembreperso = $bdd->prepare('SELECT * FROM personnalisation_membres where nom_page = :nom') or die(print_r($bdd->errorInfo()));
	$cherchermembreperso->execute(array('nom' => $visit));
	if($entree = $cherchermembreperso->fetch()){
		if($entree['pub'] == 1){
			$chercherip = $bdd->prepare('SELECT * FROM personnalisation_ip where ip = :ip and id_page = :id_page') or die(print_r($bdd->errorInfo()));
			$chercherip->execute(array('ip' => $_SERVER["REMOTE_ADDR"], 'id_page' => $entree['id']));
			if($ip = $chercherip->fetch()){}
			else{
				if(rand(0,100) > 85){
					header('Location:http://adf.ly/2799396/http://l3m.in/games/personnalisation/?visit='. $visit .'');
				}
			}
		}
	}
}

function visite($nom){
	include('../../fonctions/connexionbdd.php');

	$nom = htmlspecialchars($nom);
	$chercherpage = $bdd->prepare('SELECT * FROM personnalisation_membres where nom_page = :nom') or die(print_r($bdd->errorInfo()));
	$chercherpage->execute(array('nom' => $nom));
	if($entree = $chercherpage->fetch()){
		$chercherip = $bdd->prepare('SELECT * FROM personnalisation_ip where ip = :ip and id_page = :id_page') or die(print_r($bdd->errorInfo()));
		$chercherip->execute(array('ip' => $_SERVER["REMOTE_ADDR"], 'id_page' => $entree['id']));
		if($ip = $chercherip->fetch()){
			return $ip['temps'];
		}
		else{
			$points = $entree['points'] + 1;
			$pointstotal = $entree['points_total'] + 1;
			$gagner_point = $bdd->prepare('UPDATE personnalisation_membres SET points = :points, points_total = :pointstotal where id = :id') or die(print_r($bdd->errorInfo()));
			$gagner_point->execute(array('points' => $points, 'pointstotal' => $pointstotal, 'id' => $entree['id']));
			$gagner_point->closeCursor();

			$temps = strtotime('tomorrow midnight');
			$inscription_ip = $bdd->prepare('INSERT INTO personnalisation_ip(ip, id_page, temps) VALUES(:ip, :id_page, :temps)');
            $inscription_ip->execute(array('ip' =>$_SERVER["REMOTE_ADDR"], 'id_page' => $entree['id'], 'temps' => $temps));

			return 'done';
		}
	}
	return 'existepas';
}

function ecrire_visite($nom, $texte){
	$nom = htmlspecialchars($nom);

	include('../../fonctions/connexionbdd.php');

	$chercherpage = $bdd->prepare('SELECT * FROM personnalisation_membres where nom_page = :nom') or die(print_r($bdd->errorInfo()));
	$chercherpage->execute(array('nom' => $nom));
	$entree = $chercherpage->fetch();
	$pt = $entree['points_total'];
	?>

	<h1>Personnalisation, le jeu.</h1>
	
	<?php 
	
	if($texte != 'existepas'){
	?>
		<p class="bouton">La page <span class="bouton"><?php echo $nom; ?></span> possède déjà <?php echo $pt; ?> point<?php if($pt > 1){ echo "s"; } ?> !</p>
	<?php
	}


	if(is_numeric($texte)){
	?>

		<p>Vous n'avez pas fait gagner de point à <?php echo $nom; ?> car vous en avez déjà fait gagner il y a moins de 24 heures.<br />
		<span class="petit">Rappel : Vous pouvez faire gagner des points pour une page une fois par jour seulement.<br />
		<i class="opaGANGNAMSTYLEcite">Vous pourrez faire gagner un point le <?php echo date('d/m à H:i', $texte); ?>.</i></span></p>
	
	<?php
	}
	if($texte == 'existepas'){
	?>

		<p>Cette page n'a pas été trouvée.<br />
		<span class="petit">Inscrivez la votre en <a href="http://l3m.in/games/personnalisation/">cliquant ici</a> !</span></p>
	
	<?php
	}
	if($texte == 'done'){
	?>

	<p>Vous venez d'ailleurs d'y faire gagner un point !</p>

	<p class="petit">
	<?php if(isset($_SESSION['connecte'])){ ?>
		Vous possédez déjà votre propre page ?! C'est extra, merci <?php echo $_SESSION['pseudo-perso']; ?> !
	<?php } else { ?>
		Inscrivez vous et personnalisez votre page en <a href="http://l3m.in/games/personnalisation/">cliquant ici</a> !
	<?php } ?>
	</p>

	<?php
	}
	?>
	<p class="bouton"><a href="http://l3m.in/games/personnalisation/">Retour à l'accueil</a> &bull; <a href="http://l3m.in/games/personnalisation/?rank">Classement</a></p>
	<?php
}

function verifier_connexion(){
	if(isset($_SESSION['connecte'])){
		include('../../fonctions/connexionbdd.php');

		$cherchermembreperso = $bdd->prepare('SELECT * FROM personnalisation_membres where nom_page = :nom') or die(print_r($bdd->errorInfo()));
		$cherchermembreperso->execute(array('nom' => $_SESSION['nom_page-perso']));
		if($entree = $cherchermembreperso->fetch()){
			if($entree['points'] != $_SESSION['points']){
				$_SESSION['points'] = $entree['points'];
			}
			if($entree['points_total'] != $_SESSION['pointstotal-persos']){
				$_SESSION['pointstotal-persos'] = $entree['points_total'];
			}
		return true;
		}
	}
	else{
		if(isset($_COOKIE['l3m_pseudo']) && isset($_COOKIE['l3m_mdp']) && !isset($_SESSION['inscription'])){
			include('../../fonctions/connexionbdd.php');

			$cherchermembrel3m = $bdd->prepare('SELECT * FROM l3m_membres where pseudo = :pseudo and mdp = :mdp') or die(print_r($bdd->errorInfo()));
			$cherchermembrel3m->execute(array('pseudo' => $_COOKIE['l3m_pseudo'], 'mdp' => $_COOKIE['l3m_mdp']));
			if($trouver = $cherchermembrel3m->fetch()){
				$cherchermembreperso = $bdd->prepare('SELECT * FROM personnalisation_membres where id_l3m = :id') or die(print_r($bdd->errorInfo()));
				$cherchermembreperso->execute(array('id' => $trouver['id']));
				if($entree = $cherchermembreperso->fetch()){
					$_SESSION['nom_page-perso'] = $entree['nom_page'];
					$_SESSION['pseudo-perso'] = $trouver['pseudo'];
					$_SESSION['points-perso'] = $entree['points'];
					$_SESSION['pointstotal-perso'] = $entree['points'];
					$_SESSION['connecte'] = true;
					$_SESSION['id-l3m'] = $entree['id_l3m'];
					header("Location:http://l3m.in/games/personnalisation/?account");
					return true;
				}
			}
		}
		return false;
	}
}

function verifier_ip(){
	include('../../fonctions/connexionbdd.php');

    $desinscrire = $bdd->prepare('DELETE FROM personnalisation_ip WHERE temps < :temps') or die(print_r($bdd->errorInfo()));
    $desinscrire->execute(array('temps' => time()));
}

function ecrire_profil($id){
	include('../../fonctions/connexionbdd.php');

	$cherchermembreperso = $bdd->prepare('SELECT * FROM personnalisation_membres where id_l3m = :id') or die(print_r($bdd->errorInfo()));
	$cherchermembreperso->execute(array('id' => $_SESSION['id-l3m']));
	$entree = $cherchermembreperso->fetch();
?>
	<h1>Personnalisation, le jeu.<sup>Paramètres</sup></h1>

	<p>Vous êtes connecté, <?php echo $_SESSION['pseudo-perso']; ?>. <span class="petit"><a href="?deco">Se déconnecter</a> &bull; <a href="http://l3m.in/games/personnalisation/">Retour à l'accueil</a></span></p>

	<p class="bouton">Vous avez <span class="bouton"><?php echo $entree['points']; ?></span> points de libres, pour <span class="bouton"><?php echo $entree['points_total']; ?></span> points récoltés au total.</p>

	<p>Voici votre lien à faire partager au plus de monde possible :</p>
	
	<p class="centrer bouton"><a href="http://l3m.in/games/personnalisation/?visit=<?php echo $entree['nom_page']; ?>">http://l3m.in/games/personnalisation/?visit=<?php echo $entree['nom_page']; ?></a></p>
<?php
	if($entree['pub'] == 1){
		?>
		<p>Vous autorisez la redirection via une pub adf.ly lors du visionnage de votre page.<br />
		<span class="petit">Chaque personne qui visite un lien qui autorise les pubs a 1,5 chance sur 10 d'être redirigé vers adf.ly (avec une limite d'une fois par jour).</span>
		</p>
		<p class="centrer bouton"><a href="?pub">Ne plus autoriser la redirection via une pub pour votre page</a></p>
		<?php
	}
	else {
		?>
		<p>Vous n'autorisez pas la redirection via une pub adf.ly lors du visionnage de votre page.<br />
		<span class="petit">Chaque personne qui visite un lien qui autorise les pubs a 1,5 chance sur 10 d'être redirigé vers adf.ly (avec une limite d'une fois par jour).</span>
		</p>
		<p class="centrer bouton"><a href="?pub">Autoriser la redirection via une pub pour votre page</a></p>
		<?php
	}
?>
	<p></p>
<?php
}

function ecrire_classement(){
	?>
		<h1>Personnalisation, le classement.</h1>
		<p><a href="http://l3m.in/games/personnalisation/">Retour à l'accueil</a></p>
	<?php
	include('../../fonctions/connexionbdd.php');

	$i = 0;
	$cherchermembresclassement = $bdd->prepare('SELECT * FROM personnalisation_membres ORDER BY points_total DESC') or die(print_r($bdd->errorInfo()));
	$cherchermembresclassement->execute();
	while($entree = $cherchermembresclassement->fetch()){
		$i++;
		$pt = $entree['points_total'];

		$chercherpseudol3m = $bdd->prepare('SELECT pseudo FROM l3m_membres WHERE id = :id') or die(print_r($bdd->errorInfo()));
		$chercherpseudol3m->execute(array('id' => $entree['id_l3m']));
		$pseudo = $chercherpseudol3m->fetch();
		?>
			<p class="bouton"><span class="bouton">#<?php echo $i; ?></span> &bull; Page <a href="http://l3m.in/games/personnalisation/?visit=<?php echo $entree['nom_page']; ?>"><?php echo $entree['nom_page']; ?></a> de 
			<a href="http://l3m.in/member.php?membres=<?php echo $entree['id_l3m']; ?>"><?php echo $pseudo['pseudo']; ?></a> - <?php echo $pt; ?> point<?php if($pt > 1){ echo "s"; } ?> récolté<?php if($pt > 1){ echo "s"; } ?>.</p>
		<?php
	}

}

function ecrire_presentation(){
?>
	<h1>Personnalisation, le jeu.</h1>
	<p>Ce jeu vous permet de créer votre page de manière hyper simple et rapide (liez votre compte l3m website (via le bouton de connexion) et choisissez un nom pour votre page).<br />
	Une fois que vous avez votre page, tout ce que vous aurez à faire est de partager un maximum votre lien à vos amis de manière à récolter des "points".<br />
	Ces points vous permettront de personnaliser votre page. Vous pourrez par exemple en dépenser pour changer la couleur de fond, pour ajouter des animations, un petit texte, des images...<br />
	Il est à noter que le lien que vous partagerez pour gagner des points pourra rediriger vers une pub (redirection vers adf.ly) puis vers votre page (une chance et demie sur dix).<br />
	<span class="petit">Si vous ne souhaitez pas de ça, vous pouvez désactiver cette fonction dans vos paramètres de compte.</span></p>
	<?php if(verifier_connexion()) { ?>
	<p>Vous êtes connecté, <?php echo $_SESSION['pseudo-perso']; ?>. <span class="petit"><a href="?account">Paramètres</a></span>.</p>
	<p><a href="?rank">Classement</a></p>
	<?php } else { ?>
	<p class="centrer bouton"><a href="http://l3m.in/?connect&redirect=personnalisation">Se connecter</a></p>
<?php }
}

function ecrire_footer(){
?>
	<div id="footer">
		<p>Personnalisation est un projet de sodimel (<a href="http://l3m.in/">l3m website</a>, <a href="http://twinoid.com/ev/48311221">changelog</a> sur Twinoid).</p>
	</div>
<?php
}

function tentative_connexion($pseudo, $passe){
	$pseudo = htmlspecialchars($pseudo);
	$passe = htmlspecialchars($passe);
	include('../../fonctions/connexionbdd.php');

	$cherchermembre = $bdd->prepare('SELECT * FROM l3m_membres where pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
	$cherchermembre->execute(array('pseudo' => $pseudo));
	if($entrees = $cherchermembre->fetch()){ // si le membre est inscrit sur le l3m website
		if($passe == sha1($entrees['mdp'])){ // si le membre a le bon mdp

			$cherchermembreperso = $bdd->prepare('SELECT * FROM personnalisation_membres where id_l3m = :id') or die(print_r($bdd->errorInfo()));
			$cherchermembreperso->execute(array('id' => $entrees['id']));
			if($entree = $cherchermembreperso->fetch()){ // si le membre est inscrit sur perso

				$_SESSION['nom_page-perso'] = $entree['nom_page'];
				$_SESSION['pseudo-perso'] = $entrees['pseudo'];
				$_SESSION['points-perso'] = $entree['points'];
				$_SESSION['pointstotal-perso'] = $entree['points'];
				$_SESSION['connecte'] = true;
				$_SESSION['id-l3m'] = $entree['id_l3m'];
				header("Location:http://l3m.in/games/personnalisation/?account");

			}
			else{ // si le membre n'est pas inscrit sur le jeu

				$inscription_perso = $bdd->prepare('INSERT INTO personnalisation_membres(id_l3m, nom_page) VALUES(:id_l3m, :nom_page)');
				$inscription_perso->execute(array(
					'id_l3m' => $entrees['id'],
					'nom_page' => ""));
				$inscription_perso->closeCursor();

				$_SESSION['inscription'] = true;
				$_SESSION['pseudo-perso'] = $entrees['pseudo'];
				$_SESSION['id-l3m'] = $entrees['id'];
				header("Location:http://l3m.in/games/personnalisation/?register");
			}
		}
	}
	else{ // si le membre n'est pas trouvé sur le l3m website
		header("Location:http://l3m.in/games/personnalisation/?compte");
	}
}

function ecrire_inscription(){
	if($_SESSION['inscription']){
?>
	<h1>Personnalisation, le jeu.<sup>Création du nom de la page</sup></h1>
	<form method="post" action="?register" class="bouton">
	<p>Votre inscription est presque terminée, il ne reste plus qu'à vous choisir un nom de page, <?php echo $_SESSION['pseudo-perso']; ?>.</p>
		<input type="text" name="nompage" maxlength="42" required /> <input type="submit" value="Choisir son nom de page" />
	</form>
<?php
	}
}

function creer_page($nom){
	if($nom != ""){
		include('../../fonctions/connexionbdd.php');

		$nom = htmlspecialchars($nom);
		$creer_nompage = $bdd->prepare('UPDATE personnalisation_membres SET nom_page = :nom_page where id_l3m = :id') or die(print_r($bdd->errorInfo()));
		$creer_nompage->execute(array('nom_page' => $nom, 'id' => $_SESSION['id-l3m']));
		$creer_nompage->closeCursor();

		$_SESSION['points-perso'] = 0;
		$_SESSION['pointstotal-perso'] = 0;
		$_SESSION['nom_page-perso'] = $nom;
		$_SESSION['connecte'] = true;
		unset($_SESSION['inscription']);

		header("Location:http://l3m.in/games/personnalisation/?account");
	}
}

?>