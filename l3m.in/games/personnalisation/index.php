<?php
session_start();

include('../../config.php');
include('fonctions.php');

if(isset($_GET['deco'])){ // deconnexion
	deconnexion();
}

if(isset($_GET['pub']) && isset($_SESSION['id-l3m'])){ // changer l'autorisation de visionnage de pub
	publicite($_SESSION['id-l3m']);
}

if(isset($_GET['login']) && isset($_GET['pseudo']) && isset($_GET['passe'])){ // connexion du membre (ou inscription)
	tentative_connexion($_GET['pseudo'], $_GET['passe']);
}

if(isset($_GET['register']) && isset($_POST['nompage']) && isset($_SESSION['inscription'])){ // création de la page du membre
	creer_page($_POST['nompage']);
}

elseif(isset($_GET['visit'])){ // visite (redirection de pub et/ou statut de la visite (pour affichage plus bas))
	redirection($_GET['visit']);
	$texte = visite($_GET['visit']);
}

verifier_connexion(); // màj des variables de session si la personne est connectée
verifier_ip(); // destruction des ips qui ont plus de 24 heures (autorisation des visites)


?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>Personnalisation, le jeu !</title>
    <link rel="stylesheet" href="design.css" />
    <meta name="viewport" content="width=device-width" />
    <link href="favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <script src="../../analytics.js" type="text/javascript"></script>
    <link href='http://fonts.googleapis.com/css?family=Nunito|Exo+2' rel='stylesheet' type='text/css'>
</head>
<body>

<?php

if(isset($_GET['register'])){
	ecrire_inscription();
}

elseif(isset($_GET['visit'])){
	ecrire_visite($_GET['visit'], $texte);
}

elseif(isset($_GET['account']) && verifier_connexion()){
	ecrire_profil($_SESSION['id-l3m']);
}

elseif(isset($_GET['rank'])){
	ecrire_classement();
}

else{
	ecrire_presentation();
}

ecrire_footer();
?>

</body>
</html>