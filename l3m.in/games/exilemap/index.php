<?php include('header.php'); ?>

	<p>Bienvenue sur ExileMap, un jeu du réseau l3m.<br />
    <i>ExileMap, c'est des dizaines de joueurs (pour l'instant :D) qui explorent une grande grille de zones (qui se modifient selon le temps qui passe). C'est des interactions possibles, plusieurs buts (s'enrichir, 
    explorer, pvp, destruction, création, ect.), pas trop chronophage et une totale liberté de faire ce qu'on veut.</i><br />
    <?php if(isset($_COOKIE["l3m_pseudo"])) { ?>
	Vous êtes connecté, <?php echo $_COOKIE["l3m_pseudo"]; ?> (<a href="?disconnect">se déconnecter</a>).
    <?php if(isset($_GET["premierefois"])) { ?><br />C'est la première fois que vous vous connectez sur le jeu, <?php echo $_COOKIE["l3m_pseudo"]; ?>. Vous allez donc suivre un petit tutoriel afin de vous apprendre les 
    bases de ExileMap. :)<br />
    <a href="?tuto1">C'est parti</a> !
    <?php } } else { ?>
    Vous pouvez vous connecter via votre compte l3m, en cliquant <a href="http://l3m.in/?connect&redirect=exilemap">ici</a>.
    <?php } ?></p>

    <p>Exemple du dessin vitefaitmalfait d'un prototype éventuel du projet que je pourrais peut-être réaliser :<br />
    <img src="exilemap.png" alt="exemple du jeu" /></p>
    

    <p class="small">Un jeu en php utilisant des bases de données par <a href="http://l3m.in/">Corentin Bettiol</a>. Une partie du concept est issu du jeu <a href="http://kube.muxxu.com/">kube</a>.</p>
</body>
</html>