<?php

include('../../config.php');

function deconnexion(){
	session_destroy();
	header("Location:http://l3m.in/member.php?disconnect&redirect=perso");
}

function verifier_connexion(){
	if(isset($_SESSION['argent-connecte'])){
		include('../../fonctions/connexionbdd.php');

		$cherchermembre = $bdd->prepare('SELECT * FROM l3m_membres where pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$cherchermembre->execute(array('pseudo' => $_SESSION['argent-pseudo']));
		if($entree = $cherchermembre->fetch()){
			if($entree['argent'] != $_SESSION['argent-argent']){
				$_SESSION['l3m-argent'] = $entree['argent'];
			}
		return true;
		}
	}
	else{
		if(isset($_COOKIE['l3m_pseudo']) && isset($_COOKIE['l3m_mdp']) && !isset($_SESSION['inscription'])){
			include('../../fonctions/connexionbdd.php');

			$cherchermembrel3m = $bdd->prepare('SELECT * FROM l3m_membres where pseudo = :pseudo and mdp = :mdp') or die(print_r($bdd->errorInfo()));
			$cherchermembrel3m->execute(array('pseudo' => $_COOKIE['l3m_pseudo'], 'mdp' => $_COOKIE['l3m_mdp']));
			if($trouver = $cherchermembrel3m->fetch()){
				$_SESSION['pseudo-argent'] = $trouver['pseudo'];
				$_SESSION['argent-argent'] = $entree['argent'];
				$_SESSION['argent-connecte'] = true;
				$_SESSION['id-argent'] = $entree['id'];
				header("Location:http://l3m.in/games/argent/");
				return true;
			}
		}
		return false;
	}
}

function tentative_connexion($pseudo, $passe){
	$pseudo = htmlspecialchars($pseudo);
	$passe = htmlspecialchars($passe);
	include('../../fonctions/connexionbdd.php');

	$cherchermembre = $bdd->prepare('SELECT * FROM l3m_membres where pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
	$cherchermembre->execute(array('pseudo' => $pseudo));
	if($entrees = $cherchermembre->fetch()){ // si le membre est inscrit sur le l3m website
		if($passe == sha1($entrees['mdp'])){ // si le membre a le bon mdp
			$_SESSION['pseudo-argent'] = $trouver['pseudo'];
			$_SESSION['argent-argent'] = $entree['argent'];
			$_SESSION['argent-connecte'] = true;
			$_SESSION['id-argent'] = $entree['id'];
			header("Location:http://l3m.in/games/argent/");
		}
	}
	else{ // si le membre n'est pas trouvé sur le l3m website
		header("Location:http://l3m.in/games/argent/?compte");
	}
}

function droit_ouvrir_caisse(){
	include('../../fonctions/connexionbdd.php');
	$verif = true;
	$cherchercaisse = $bdd->prepare('SELECT * FROM argent_objets where id_l3m = :id_l3m') or die(print_r($bdd->errorInfo()));
	$cherchercaisse->execute(array('id_l3m' => $_SESSION['id-argent']));
	while($entrees = $cherchercaisse->fetch()){
		if($entrees['temps'] > strtotime('yesterday midnight'))
			$verif = false;
	}
	return $verif;
}

function ouvrir_caisse(){
	include('../../fonctions/connexionbdd.php');
	$nom = "épée rouillée";
	$valeur = 1;
	$temps = time();
}


function afficher_menu(){
	?>
	<nav>
	<?php if(!isset($_SESSION['argent-connecte'])){ ?>
		<a href="http://l3m.in/?connect&redirect=argent">Connexion</a>
	<? } ?>
		<a href="?infos">Aide</a>
		<a href="?deco">Déconnexion</a>
	<?php if(isset($_SESSION['argent-connecte']) && droit_ouvrir_caisse()){ ?>
		<a href="?caisse">Ouvrir la caisse journalière</a>
	<?php } ?>

	</nav>
	<?php
}

?>