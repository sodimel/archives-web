<?php

include("fonctions/fonctions_member.php");

if(isset($_GET['blog']) && getGrade() > 2){
	$blog = verif_connexion_sodimel();
	if($_GET['blog'] == "new" && $blog == true && isset($_POST['titre'])){
		poster_message_blog($_POST['titre'], $_POST['categorie'], $_POST['message']);
	}
	if($_GET['blog'] == "edit" && $blog == true && isset($_POST['titre']) && isset($_GET['id'])){
		edit_message_blog($_GET['id'], $_POST['titre'], $_POST['categorie'], $_POST['message']);
	}
}

if (isset($_GET["register"])){
	include("fonctions/captcha.php");
	if(captcha_verif() == true){
		inscrire_membre($_POST["pseudo"], $_POST["mdp"], $_POST["email"], date("d/m/Y"));
	}
}

if (isset($_GET["connect"])) {
	connecter_membre($_POST["pseudo"], $_POST["mdp"]);
}

if (isset($_GET["configdone"])) {
	configuration_profil_membre($_POST["email"], $_POST["avatar"], $_POST["site"], $_POST["sexe"], $_POST["bio"]);
}

if(isset($_GET['recovery'])){
	include("fonctions/captcha.php");
	if(captcha_verif() == true){
		reinitialiser_mail($_POST['email']);
	}
}

if(isset($_GET['changemdp'])){
	changer_mdp($_POST['oldmdp'], $_POST['newmdp']);
}

if (isset($_GET["disconnect"]) && isset($_COOKIE["l3m_pseudo"]) && isset($_COOKIE["l3m_mdp"])){
	deconnexion_membre();
}

if (isset($_GET["desinscription"]) && isset($_COOKIE["l3m_pseudo"]) && isset($_COOKIE["l3m_mdp"])) {
	desinscription_membre();
}


include("headermembres.php");


if(isset($_GET['error'])){
	ecrire_erreur();
}

elseif(isset($_GET['piece'])){
	ecrire_piece();
}

elseif(isset($_GET['good'])) {
	ecrire_messages($_GET['good']);
}

elseif(isset($_GET['profil'])){
	ecrire_profil();
}

elseif(isset($_GET['config']) && isset($_COOKIE['l3m_pseudo']) && isset($_COOKIE['l3m_mdp'])){
	ecrire_page_configuration_profil();
}

elseif(isset($_GET['jeux'])){
	ecrire_page_jeux();
}

elseif(isset($_GET['membres'])){
	ecrire_liste_membres();
}

elseif(isset($_GET['forum'])){
	?>
		<section>
			<h2><span>Forum</span></h2>
			<p>Coming soon.</p>
		</section>
	<?php
}

elseif(isset($_GET['blog']) && $blog == true && getGrade() > 2){
	?>
		<section>
			<h2><span>Gestion du blog !</span></h2>
			<?php echo gestion_blog();
			switch($_GET['blog']){ 
				case "new":
					contenu_nouveau_message_blog();
				break;
				case "edit":
					contenu_edit_message_blog($_GET['id']);
				break;
				case "delete":
					contenu_delete_message_blog();
				break;
			}
			?>
		</section>
	<?php
}

else{
	ecrire_page_defaut();
}


include("footer.php");
?>

</body>
</html>