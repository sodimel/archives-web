<?php

function ecrire_avertissement_cookie(){
	if(empty($_COOKIE['cookie'])){
		?>
		<section style="font-size: x-small;">
			<h2><span>L'utilisation des cookies sur ce site.</span></h2>
			<p>
				Sur le l3m website, des cookies peuvent être enregistrés à votre insu (notamment via google analytics, qui vous espionne et vide vos comptes bancaires, et qui ne sert pas du tout seulement à analyser le traffic de 
				ce site web). Les cookies relatifs à ce site contiennent évidemment des données hyper importantes et confidentielles, et ils sont enregistrés sur votre ordi afin de mieux vous faire cibler par des publicités 
				qui font que je peux payer l'hébergement.<br />
				<i>Vous voulez lire la doc concernant les cookies ? C'est <a target="_blank" href="http://orteil.dashnet.org/cookieclicker/">par ici que ça se passe</a>.</i><br />
				<a href="?p=cookies">J'ai compris tout ce baratin, les cookies ne se mangent pas et ne sont pas méchants, merci de ne plus afficher ce message</a> <i>(la situation est ironique puisque le fait de cliquer sur ce 
				lien va enregistrer un cookie sur votre ordinateur qui dit au serveur de ne pas afficher ce bandeau d'information)</i>.
			</p>
		</section>
		<hr>
		<?php
	}
}

function ecrire_connexion_inscription(){
	if(!isset($_COOKIE["l3m_pseudo"]) && !isset($_COOKIE["l3m_mdp"])){
		?>
		<div class="connexion"><a href="http://l3m.in/?connect">Connexion/Inscription</a></div>
		<?php
	}
}

function ecrire_menu(){

	if($_SERVER['PHP_SELF'] == "/index.php"){

		if(isset($_GET['connect']) or isset($_GET['inscription']) or isset($_GET['don']) or isset($_GET['verif_email'])){
			?>
				<h1><a href="http://l3m.in/">l3m website</a></h1>
			<?php
		}
		else{
			?>
				<h1>l3m website</h1>
		        <a href="#about">A propos</a>
		        <a href="#contact">Contact</a>
		        <a href="#projets">Projets</a>
		        <a href="blog.php">Blog</a>
	        	<a href="http://l3m.in/member.php">Section membres</a>
        	<?php
        }
    }
    else{
    	?>
	    	<h1><a href="http://l3m.in/">l3m website</a></h1>
	        <a href="http://l3m.in/#about">A propos</a>
	        <a href="http://l3m.in/#contact">Contact</a>
	        <a href="http://l3m.in/#projets">Projets</a>
	        <a href="blog.php">Blog</a>
	        <a href="http://l3m.in/member.php">Section membres</a>
		<?php
	}
}


?>