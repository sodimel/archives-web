<?php

/* AFFICHAGE DE PAGE */

function blog_afficher_page($page){
	include("fonctions/connexionbdd.php");
	include("fonctions/fonctions_mise_en_page.php");

	if($page != 0) { // si on est pas sur l'accueil
		$nombre_bdd_page = ($page-1)*5;
	}
	else{ // si on est sur l'accueil
		$nombre_bdd_page = 0;
		$page = 1;
	}

	if (is_int($nombre_bdd_page) == true && $nombre_bdd_page >= 0){ // si le nombre est bon, on peut afficher les articles du blog
		$liste_articles_blog = $bdd->query('SELECT * FROM `l3m_blog` order by `id` desc limit '. $nombre_bdd_page .', 5') or die(print_r($bdd->errorInfo()));
		while ($article = $liste_articles_blog->fetch())
		{
			$id = $article['id'];
			echo blog_ecrire_article($article['id'], $article['titre'], $article['date'], $article['message'], $article['category'], "liste"); // écrit l'article
		}

		echo blog_page($page, $id);

		$liste_articles_blog->closeCursor();
	}
}

function blog_page($page, $id){
	$page_afficher = "<p class=\"centre\">";
	$pagesuivante = $page+1;
	$pageprecedente = $page-1;

	if($page > 1) {
		$page_afficher .= "<a href=\"?page=". $pageprecedente ."\">Page précédente</a>";
	}
	else{
		$page_afficher .= "<i>Page précédente</i>";
	}

	$page_afficher .= " &bull; Page #". $page ." &bull; ";

	if($id > 1) {
		$page_afficher .= "<a href=\"?page=". $pagesuivante ."\">Page suivante</a>";
	}
	else{
		$page_afficher .= "<i>Page suivante</i>";
	}

	return $page_afficher;
}



/* AFFICHAGE D'UN ARTICLE AVEC LES COMMENTAIRES ET LE FORMULAIRE DE SOUMISSION */

function blog_afficher_article($id){
	include("fonctions/connexionbdd.php");
	include("fonctions/fonctions_mise_en_page.php");

	if($id != 0){

		$chercher_article_blog = $bdd->query('SELECT * FROM `l3m_blog` WHERE `id` = '.$id.'') or die(print_r($bdd->errorInfo())); // Si l'id existe, ...
		if ($article = $chercher_article_blog->fetch())
		{
			$cat = blog_switch_categories($article['category']);
			echo blog_ecrire_article($article['id'], $article['titre'], $article['date'], $article['message'], $article['category'], "article"); // écrit l'article


			$liste_commentaires_blog = $bdd->query('SELECT * FROM `l3m_comments` WHERE `idmessage` = '. $id .' order by `id`') or die(print_r($bdd->errorInfo())); // Si l'id existe, ...
			while ($commentaire = $liste_commentaires_blog->fetch()) {
				echo blog_ecrire_commentaire($commentaire['id'], $commentaire['pseudo'], $commentaire['lien'], $commentaire['commentaire'], $commentaire['date']);
			}

			echo blog_poster_commentaire($id);
		}
	}
}

function blog_switch_categories($cat){
	switch ($cat) {
    case "1":
		return "général";
        break;
    case "2":
		return "codage";
        break;
    case "3":
		return "écriture web";
        break;
    case "4":
		return "3615 MYLIFE";
        break;}
}

/* LES FONCTIONS D'ECRITURE DE CONTENU */


function blog_ecrire_article($id, $titre, $date, $message, $categorie, $liste){
	$date = date('d/m/Y \à H:i:s', $date);

	if($liste == "liste"){
	$taille = strlen($message);
	$message = str_replace("<br />", "BBRRBR", $message);
		if($taille > 495){
			$position = strpos($message, " ", 495);
			if($position == 0){
				$position = 499;
			}
		$liresuite = true;
		}
		else{
			$position = $taille;
		}
		$message = substr($message, 0, $position);
		
		$message = str_replace("BBRRBR", "<br />", $message);
		$message = mise_en_page(1, $message);
	}
	else{
		$message = mise_en_page(1, $message);
	}

	if(getGrade() > 2)
		$edit = '<i><a href="member.php?blog=edit&id='. $id .'" title="éditer le message '. $id .'" class="petit">éditer</a></i>';
	else
		$edit = "";

if($liresuite){

	return "<p class=\"decalagegauche articleblog\">
				<a href=\"?article=". $id ."\" title=\"Lien vers l\'article\"><span class=\"titre\">". $titre ."</span></a> ". $edit ."<br />
				<span class=\"petit\">Message #". $id ." posté le ". $date ." dans ". blog_switch_categories($categorie) .", ". blog_ecrire_nombre_commentaires($id) .".</span><br />
				". $message ." ...<a href=\"?article=". $id ."\" title=\"Lien vers l\'article\">Lire la suite de l'article</a>.
			</p>
			<hr>";
}
else{
	return "<p class=\"decalagegauche articleblog\">
				<a href=\"?article=". $id ."\" title=\"Lien vers l\'article\"><span class=\"titre\">". $titre ."</span></a> ". $edit ."<br />
				<span class=\"petit\">Message #". $id ." posté le ". $date ." dans ". blog_switch_categories($categorie) .", ". blog_ecrire_nombre_commentaires($id) .".</span><br />
				". $message ."
			</p>
			<hr>";
}
}

function blog_ecrire_nombre_commentaires($id){
	include("fonctions/connexionbdd.php");

	$nombre_commentaires_blog = $bdd->query('SELECT COUNT(*) FROM `l3m_comments` WHERE `idmessage` = '. $id .'') or die(print_r($bdd->errorInfo()));
	$nombre_commentaires = $nombre_commentaires_blog->fetchColumn();
	if($nombre_commentaires == 0){
		return "aucun commentaire";
	}
	elseif($nombre_commentaires == 1){
		return $nombre_commentaires ." commentaire";
	}
	else{
		return $nombre_commentaires ." commentaires";
	}
}

function blog_ecrire_commentaire($id, $pseudo, $lien, $message, $date){

	if(getGrade() > 2){
		$message = mise_en_page(1, $message);
	}

	else{
		$message = mise_en_page(0, $message);
	}
	
	$commentaire = "<p class=\"commentaireblog\">Commentaire <i>#". $id ."</i> de ";

	if ($lien == ""){
		$commentaire .= htmlspecialchars($pseudo);
	}

	else{
		$commentaire .= "<a href=\"". htmlspecialchars($lien) ."\" title=\"site web du posteur\">". htmlspecialchars($pseudo) ."</a>";
	}

	$commentaire .= ", <i>posté le ". $date .".</i><br />";

	$commentaire .=  $message ."</p><hr />";
	
	return $commentaire;

}

function blog_poster_commentaire($id){
	$formulaire = "<p><b>Poster un commentaire</b><br />". texte_mise_en_page() ."<form method=\"post\" action=\"blog.php?post_comment=". $id ."\">
	<label for=\"pseudo\">Pseudo :</label> <input id=\"pseudo\" ";
	if(isset($_COOKIE['pseudo_commentaire'])){
		$formulaire .= "value=\"". $_COOKIE['pseudo_commentaire'] ."\" ";
	}
	$formulaire .= "type=\"text\" name=\"pseudo\" maxlength=\"42\" required /><br />
	<label for=\"lien\">Lien :</label> <input id=\"lien\" ";
	if(isset($_COOKIE['lien_commentaire'])){
		$formulaire .= "value=\"". $_COOKIE['lien_commentaire'] ."\" ";
	}
	$formulaire .= "type=\"text\" name=\"lien\" maxlength=\"255\" /><br />";

	$formulaire .= "Résolvez le Captcha suivant :
				<div class=\"g-recaptcha\" data-sitekey=\"6LeTe_kSAAAAAIMWgRDaHLMfsSahL7gfZLOviBbK\"></div>";

	$formulaire .= "<input id=\"calcul\" type=\"hidden\" name=\"calcul2\" value=\"". $z ."\" maxlength=\"2\" />";

	$formulaire .= "
	<label for=\"email\">Email :</label> <input id=\"email\" ";
	if(isset($_COOKIE['email_commentaire'])){
		$formulaire .= "value=\"". $_COOKIE['email_commentaire'] ."\" ";
	}
	$formulaire .= "type=\"text\" name=\"email\" maxlength=\"256\" /><br />
	<i>L'email me servira à vous contacter en privé, il n'est pas affiché sur le site, ni partagé à d'autres sites internet.</i><br /><br />

	<textarea name=\"message\" required>Entrez votre commentaire ici.</textarea><br />
	<input value=\"Envoyer le commentaire\" type=\"submit\"> <input value=\"Réinitialiser\" type=\"reset\">
	</form>";

	return $formulaire;
}



/* ENVOI COMMENTAIRE BDD */

function blog_commentaire_bdd($id, $pseudo, $lien, $email, $message){

	include("fonctions/connexionbdd.php");
	$pseudo = htmlspecialchars($pseudo);
	$lien = htmlspecialchars($lien);
	$email = htmlspecialchars($email);
	$message = nl2br(htmlspecialchars($message));
	$date = date('d/m/Y', time()) ." à ". date('H:i:s', time());
	setcookie("pseudo_commentaire",$pseudo,time()+31556926);
	setcookie("lien_commentaire",$lien,time()+31556926);
	setcookie("email_commentaire",$email,time()+31556926);

	$envoyer_commentaire_bdd = $bdd->prepare('INSERT INTO l3m_comments(idmessage, pseudo, email, lien, commentaire, date, ip) VALUES(:idmessage, :pseudo, :email, :lien, :commentaire, :date, :ip)');
	$envoyer_commentaire_bdd->execute(array(
		'idmessage' => $id,
		'pseudo' => $pseudo,
		'email' => $email,
		'lien' => $lien,
		'commentaire' => $message,
		'date' => $date,
		'ip' => $_SERVER["REMOTE_ADDR"]));
	$envoyer_commentaire_bdd->closeCursor();
}

/* NAVIGATION ARTICLES BLOG */

function navigation_articles_blog($page){
	include("fonctions/connexionbdd.php");

	$pagemoins = $page - 1;
	$pageplus = $page + 1;

	$nombre_articles_blog = $bdd->query('SELECT COUNT(*) FROM `l3m_blog` ') or die(print_r($bdd->errorInfo()));
	$nombre_articles = $nombre_articles_blog->fetchColumn();

	$code = "<p>";
	if($page == 1){
		$code .= "<i>Article précédent</i>";
	}
	else{
		$code .= "<a href=\"?article=". $pagemoins ."\">Article précédent</a>";
	}

	$code .= " &bull; <a href=\"blog.php\">Liste des articles</a>";

	if($page >= $nombre_articles){
		$code .= " &bull; <i>Article suivant</i>";
	}
	else{
		$code .= " &bull; <a href=\"?article=". $pageplus ."\">Article suivant</a>";
	}

	$code .= "</p>";

	return $code;
}

function getGrade(){
	if(isset($_COOKIE['l3m_pseudo']) && isset($_COOKIE['l3m_mdp'])){

		include("fonctions/connexionbdd.php");
		$connexion = $bdd->prepare('SELECT grade FROM l3m_membres where pseudo = :pseudo and mdp = :mdp') or die(print_r($bdd->errorInfo()));
		$connexion->execute(array('pseudo' => htmlspecialchars($_COOKIE['l3m_pseudo']), 'mdp' => htmlspecialchars($_COOKIE['l3m_mdp'])));
		if ($entrees = $connexion->fetch()){
			return $entrees['grade'];
		}
	}
	return 0;
}

?>