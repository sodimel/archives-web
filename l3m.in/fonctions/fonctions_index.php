<?php

/* PAGES */


function ecrire_page_defaut(){
	?>
		<section id="about">
			<h2><span>A propos de moi</span></h2>
			<p>Bienvenue sur mon site personnel.<br />
			Je suis Corentin Bettiol, étudiant en master informatique à Grenoble. Je crée des sites internet depuis plus de cinq ans, et cette page peut faire office de portfolio.<br />
			</p>

			<h2><span>A propos du site</span></h2>
			<p>Ce site web est le fruit de plusieurs années de déambulations sur le web.<br />
			Il me sert de point de centralisation de plusieurs de mes projets, et me permet de faire quelques tests pas forcément référencés.<br />
			<i>Firefox est hautement recommandé pour visiter ce site (et tous les sites sur internet en général). En effet, il y a de réelles différences de design si vous utilisez un autre navigateur (et elles ne sont pas intentionnelles).</i></p>
		</section>
		<hr>

		<section id="contact">
			<h2><span>Contact</span></h2>
			<p>Voici un formulaire vous permettant de me contacter. Vous pouvez me parler d'un projet, vous pouvez me faire une (des) remarque(s) constructive(s) sur ce que j'ai déjà fait, vous pouvez me proposer d'aller boire un café... Vous pouvez faire tant de choses en écrivant seulement quelques mots ! <i>Surprenez-moi <img src="img/smiley1.png" alt=":)"></i></p>

		<form method="post" action="index.php?verif_email">
				<label for="pseudo">Prénom/Pseudo :</label> <input type="text" name="pseudo" id="pseudo" maxlength="42" required /> <br />
				<label for="email">Adresse email  :</label> <input type="email" name="email" id="email" maxlength="42" required /> <br />
				<label for="sujet">Sujet :</label> <input type="text" name="sujet" id="sujet" maxlength="255" /> <br />
				<label for="Message">Message :</label> <textarea name="message" id="details" required >Entrez votre message ici, que ce soit une question, une suggestion ou une remarque.</textarea><br />
				<input type="submit" value="Envoyer le message" /> <input type="reset" value="Réinitialiser" />
		</form>
		</section>
		<hr>

		<section id="projets">
			<h2><span>Projets</span></h2>
			<p>Voici la liste de mes projets. Pas forcément récents, pas forcément hébergés. Il s'agit principalement d'une catégorie vous permettant de vous faire une idée de ce que j'ai déjà fait.</p>
			<?php ecrire_liste_projets(); ?>
		</section>
	<?php
}

function ecrire_liste_projets(){
	include("fonctions/connexionbdd.php");

	$trouver_projet = $bdd->query('SELECT * FROM `l3m_liste_projets` order by `id`') or die(print_r($bdd->errorInfo()));
	while ($entrees = $trouver_projet->fetch())
		{
			$entrees['description'] = str_replace("\n", "<br />", $entrees['description']);
			?>
				<p class="decalagegauche">
					<a href="<?php echo $entrees['lien']; ?>" title="lien vers le projet n°<?php echo $entrees['id']; ?>, ou vers une description du projet, voire à un aperçu"><span class="titre"><?php echo $entrees['nom']; ?></span></a><br />
					<span class="petit">Projet #<?php echo $entrees['id']; ?></span><br />
					<?php echo $entrees['description']; ?>
				</p>
				<hr>
			<?php
		}
}

function ecrire_page_connexion(){
	?>
		<section>
			<h2 style="display: inline-block;"><span>Connexion</span></h2>
			<h5 style="display: inline-block; margin-left: 5px;"><a href="?inscription">S'inscrire</a></h5>
			<p>Voici le formulaire de connexion au l3m website. Une fois connecté vous aurez accès aux différents tests de "jeux" en ligne que j'expérimente. Les pages sont grosso modo remplies de 
			fautes de frappe et le code est sale et mal rédigé, mais le tout semble à première vue fonctionnel.</p>

		<form method="post" action="member.php?connect<?php if(isset($_GET["redirect"])) { echo "&redirect=". $_GET["redirect"]; } ?>">
		<b>Connexion : </b><br />
				<label for="pseudo">Pseudonyme :</label> <input type="text" name="pseudo" id="pseudo" maxlength="42" required /> <br />
				<label for="mdp">Mot de passe :</label> <input type="password" name="mdp" id="mdp" maxlength="255" required /> <br /><br />
				<input type="submit" value="Se connecter" /> <input type="reset" value="Réinitialiser" />
		</form>
		<p class="petit"><a href="?recovery">Mot de passe oublié ?</a></p>
		</section>
	<?php
}

function ecrire_page_inscription(){
	?>
		<section>
			<h2 style="display: inline-block;"><span>Inscription</span></h2>
			<h5 style="display: inline-block; margin-left: 5px;"><a href="?connect">Se connecter</a></h5>
			<p>Voici le formulaire d'inscription au l3m website. Une fois inscrit vous aurez accès aux différents tests de "jeux" en ligne que j'expérimente. Les pages sont grosso modo remplies de 
			fautes de frappe et le code est sale et mal rédigé, mais le tout semble à première vue fonctionnel.</p>

		<form method="post" action="member.php?register">
		<b>Inscription : </b><br />
				<label for="pseudo">Pseudo :</label> <input type="text" name="pseudo" id="pseudo" maxlength="42" required /> <br />
				<label for="email">Adresse email  :</label> <input type="email" name="email" id="email" maxlength="200" required /> <br />
				<label for="mdp">Mot de passe :</label> <input type="password" name="mdp" id="mdp" maxlength="255" required /> <br /><br />
				Résolvez le Captcha suivant :
				<div class="g-recaptcha" data-sitekey="6LeTe_kSAAAAAIMWgRDaHLMfsSahL7gfZLOviBbK"></div>
				<input type="submit" value="S'inscrire" /> <input type="reset" value="Réinitialiser" />
		</form>
		</section>
	<?php
}


function ecrire_page_recovery(){
	?>
		<section>
			<h2><span>Oubli de mot de passe</span></h2>
			<p>Voici le formulaire de réinitialisation de mot de passe.<br />
			Entrez votre adresse email pour qu'un mail contenant votre nouveau mot de passe vous soit envoyé.</p>

		<form method="post" action="member.php?recovery">
				<label for="email">Adresse email  :</label> <input type="email" name="email" id="email" maxlength="200" required /> <br />
				<div class="g-recaptcha" data-sitekey="6LeTe_kSAAAAAIMWgRDaHLMfsSahL7gfZLOviBbK"></div>
				<input type="submit" value="Réinitialiser votre mot de passe" />
		</form>
		</section>
	<?php
}


function ecrire_page_dons(){
	?>
		<section>
			<h2><span>Page de dons</span></h2>
			<p>Vous avez plusieurs moyens de me faire un don :<br />
			<img alt="" border="0" src="https://www.paypalobjects.com/fr_FR/i/scr/pixel.gif" width="1" height="1"></form>
			<br /><br />
			<a href="bitcoin:"><img src="http://l3m.in/img/bitcoin.png"alt="bitcoin" /></a> <br />
			<span style="margin-left: 40px; font-size: 0.8em;">Adresse Bitcoin : </span><br /><br />
			<a href="dogecoin:"><img src="http://l3m.in/img/dogecoin.png" alt="dogecoin" /></a> <br />
			<span style="margin-left: 40px; font-size: 0.8em;">Adresse Dogecoin : </span><br /><br />
			Vous n'avez pas de dogecoin ni de bitcoin et votre compte paypal est vide ? Vous pouvez m'aider à gagner quelques microcentimes en cliquant sur le bouton ci-dessous (qui vous ramènera à 
			l'accueil du site après vous avoir fait patienter quelques secondes).<br />
			<a href="http://adf.ly/StTrD"><img src="http://l3m.in/img/adfly.png" alt="adfly" /></a><br />
			<i>Si vous voulez revenir à l'accueil sans passer par la pub, <a href="http://l3m.in/">cliquez ici</a> ou bien sur "l3m website" en haut de la page.</i>
			</p>
		</section>
	<?php
}

function ecrire_reponse_don($don){
	if($don == "bad"){
		?>
			<section>
			<p>Vous avez annulé votre paiement, mais ce n'est pas grave, vous avez d'autres manières de me faire un don (voir plus bas <img src="img/smiley1.png" alt=":)" />).</p>
			</section>
		<?php
		ecrire_page_dons();
	}
	elseif($don == "good"){
		?>
			<section>
			<p>Merci du fond du coeur <3</p>
			</section>
		<?php
		ecrire_page_dons();
	}
}


/* EMAIL */


function ecrire_verif_email($pseudo, $email, $sujet, $message){
	?>
		<section>
			<h2><span>Vérification</span></h2>
			<p>Avant d'envoyer le mail, merci de remplir ce captcha pour prouver que vous n'êtes pas un robot :<br />
			<i>Vous pouvez aussi en profiter pour vous relire.</i></p>
			<form method="post" action="index.php?envoi_email">
				<label for="pseudo">Prénom/Pseudo :</label> <input type="text" name="pseudo" id="pseudo" maxlength="42" value="<?php echo $pseudo; ?>" required /> <br />
				<label for="email">Adresse email  :</label> <input type="email" name="email" id="email" maxlength="42" required value="<?php echo $email; ?>" /> <br />
				<label for="sujet">Sujet :</label> <input type="text" name="sujet" id="sujet" maxlength="255" value="<?php echo $sujet; ?>" /> <br />
				<label for="Message">Message :</label><textarea name="message" id="details" required ><?php echo $message; ?></textarea><br />
				<div class="g-recaptcha" data-sitekey=""></div>
				<input type="submit" value="Envoyer le mail." /></p>
			</form>
		</section>
	<?php
}

function envoyer_email($pseudo, $email, $sujet, $message){
	include("fonctions/captcha.php");
		if(captcha_verif() == true){
			$adresse = "";
			$sujet="[l3m contact] - " . $sujet;
			$message = nl2br(htmlspecialchars($message));

			$content = "<body><html>";
			$content .="Pseudonyme : <b>" . $pseudo ."</b><br />";
			$content .="Email : <b>" . $email ."</b><br />";
			$content .= "Message : <br />". $message ."<br />";
			$content .="</body></html>";
			$header = "Content-type: text/html; charset=utf-8\n";
			mail($adresse,$sujet,$content,$header);
		}
}

function ecrire_page_email(){
	?>
		<section>
			<h2><span>Merci pour cet email !</span></h2>
			<p>
				Je regarde mes mails au moins trois fois par semaine, vous devriez recevoir une réponse bientôt <img src="img/smiley1.png" alt=":)" />
			</p>
		</section>
	<?php
}

?>