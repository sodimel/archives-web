<?php

function captcha_verif(){
	$captcha;
	if(isset($_POST['g-recaptcha-response'])){
		$captcha=$_POST['g-recaptcha-response'];
	}
	if(!$captcha){
		echo '<section><p>Captcha incorrect.</p><br /><p><a href="javascript:history.back()">Précédent</a></p></section>';
		return false;
	}
	$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
	if($response.success==false)
	{
		echo '<section><p>Captcha incorrect.</p><br /><p><a href="javascript:history.back()">Précédent</a></p></section>';
		return false;
	}
	else
	{
		return true;
	}
}

?>