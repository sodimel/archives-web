<?php

function mise_en_page($niveau, $texte){

	if($niveau == 1){ // si message de l'admin ou message du blog = fonctions spéciales

		$texte = preg_replace('#MMM(.*?)MMM#s', '<span class="moderation_message">$0</span>', $texte); // modération
		$texte = str_replace('MMM', '', $texte); // modération aussi

	}

	$texte = preg_replace('#LLLhttps?://[a-zA-Z0-9._=\*/?&-:@%\#]+LLL#', '<a href="$0" target="_blank">$0</a>', $texte); // lien
	$texte = str_replace('LLL', '', $texte); // lien aussi

	$texte = preg_replace('#:\)#', '<img src="http://l3m.in/img/smiley1.png" class="smiley" alt=":)" />', $texte); // smiley :)

	$texte = preg_replace('#:\(#', '<img src="http://l3m.in/img/smiley2.png" class="smiley" alt=":(" />', $texte); // smiley :(

	$texte = preg_replace('#:D#', '<img src="http://l3m.in/img/smiley3.png" class="smiley" alt=":D" />', $texte); // smiley :D

	$texte = preg_replace('#:P#', '<img src="http://l3m.in/img/smiley4.png" class="smiley" alt=":P" />', $texte); // smiley :P

	$texte = preg_replace('#:o#', '<img src="http://l3m.in/img/smiley5.png" class="smiley" alt=":o" />', $texte); // smiley :o

	$texte = preg_replace('#BBB(.*?)BBB#s', '<b>$0</b>', $texte); // gras
	$texte = str_replace('BBB', '', $texte); // gras aussi

	$texte = preg_replace('#III(.*?)III#s', '<i>$0</i>', $texte); // italique
	$texte = str_replace('III', '', $texte); // italique aussi

	$texte = preg_replace('#IMAGE(.*?)IMAGE#s', '<img src="$0" alt="$0" class="image_message" />', $texte); // italique
	$texte = str_replace('IMAGE', '', $texte); // italique aussi

	return $texte;
}

function texte_mise_en_page(){
	?>
		<span class="petit">Aide de mise en page : IIItexteIII = texte en italique, BBBtexteBBB = texte en gras<br />
		IMAGEadresseIMAGE = image, LLLhttp://adresse.comLLL = lien, smiley <img src="http://l3m.in/img/smiley1.png" alt=":)" /> = :), 
		smiley <img src="http://l3m.in/img/smiley2.png" alt=":(" /> = :(, smiley <img src="http://l3m.in/img/smiley3.png" alt=":D" /> = :D,
		 smiley <img src="http://l3m.in/img/smiley4.png" alt=":P" /> = :P. </span><br />
	<?php
}

?>