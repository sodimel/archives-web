<?php
include("fonctions/fonctions_mise_en_page.php");

// INSCRIPTION / DESINSCRIPTION

function inscrire_membre($pseudo, $mdp, $email, $date){
	if($_POST["pseudo"] == ""){
	  	header("Location:http://l3m.in/member.php?error");
	  	exit();
	}
	
	if(!preg_match("#^[A-Za-z0-9._-]+@[A-Za-z0-9._-]{2,}\.[A-Za-z]{2,4}$#", $_POST['email'])){
		header("Location:http://l3m.in/member.php?error");
	  	exit();
	}

	include("fonctions/connexionbdd.php");

	$inscription = $bdd->prepare('SELECT * FROM l3m_membres where pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
	$inscription->execute(array('pseudo' => $_POST["pseudo"]));

	if ($entrees = $inscription->fetch()){
		header("Location:http://l3m.in/member.php?error");
		exit();
	}
	$inscription->closeCursor();


	$pseudo = htmlspecialchars($pseudo);
	$mdp = crypt($mdp, '');
	$email = htmlspecialchars($email);
	$avatar = "http://l3m.in/img/avatar.gif";
	$genre = 0;
	$bio = "Pas de bio pour l'instant.";


	$inscription_confirm = $bdd->prepare('INSERT INTO l3m_membres(pseudo, mdp, email, avatar, ip_connexion, date_inscription, genre, bio) VALUES(:pseudo, :mdp, :email, :avatar, :ip_connexion, :date_inscription, :genre, :bio)');
	$inscription_confirm->execute(array(
		'pseudo' => $pseudo,
		'mdp' => $mdp,
		'email' => $email,
		'avatar' => $avatar,
		'ip_connexion' => $_SERVER["REMOTE_ADDR"],
		'date_inscription' => $date,
		'genre' => $genre,
		'bio' => $bio));
	$inscription_confirm->closeCursor();
	header("Location:http://l3m.in/member.php?good=1");
}



function desinscription_membre(){
	if (crypt($_POST["mdp"], '') != $_COOKIE["l3m_mdp"]){
		header("Location:http://l3m.in/member.php?error");
		exit();
	}
	setcookie('pseudo', '', -1);
	setcookie('mdp', '', -1);
	$desinscription = $bdd->prepare('DELETE * from l3m_membres where pseudo = :pseudo and mdp = :mdp') or die(print_r($bdd->errorInfo()));
	$desinscription->execute(array('pseudo' => $_COOKIE["l3m_pseudo"],'mdp' => crypt($_POST["mdp"], '')));
	$desinscription->closeCursor();

	header("Location:http://l3m.in/member.php?good=4");
}


// CONNEXION / DECONNEXION

function connecter_membre($pseudo, $mdp){
	$pseudo = htmlspecialchars($pseudo);
	$mdp = crypt($mdp, '');

		include("fonctions/connexionbdd.php");
		$connexion = $bdd->prepare('SELECT * FROM l3m_membres where pseudo = :pseudo and mdp = :mdp') or die(print_r($bdd->errorInfo()));
		$connexion->execute(array('pseudo' => $pseudo, 'mdp' => $mdp));
			if ($entrees = $connexion->fetch()){

				$changerip = $bdd->prepare('UPDATE l3m_membres SET ip_connexion = :ip where pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
				$changerip->execute(array('ip' => $_SERVER["REMOTE_ADDR"], 'pseudo' => $pseudo));

				setcookie('l3m_pseudo', $pseudo, time() + 365*24*3600, null, null, false, true);
				setcookie('l3m_mdp', $mdp, time() + 365*24*3600, null, null, false, true);
				$_SESSION['l3m_pseudo'] = $pseudo;
				$_SESSION['l3m_mdp'] = $mdp;
				if(isset($_GET['redirect'])){
					redirection_connexion($_GET['redirect']);
				}
				else{
					redirection_connexion("base");
				}
			}
			else{
				header("Location:http://l3m.in/member.php?error");
				exit();
			}
		$connexion->closeCursor();
}


function redirection_connexion($redirect){
	switch($redirect){ 
    	case "exilemap":
   			header("Location:http://l3m.in/games/exilemap/?connect&pseudo=". $_POST['pseudo'] ."&passe=". sha1(crypt($_POST["mdp"], '')));
   			break;
   		case "cpinay":
   			header("Location:http://l3m.in/p/projets/cpinay/admin.php?pseudo=". $_POST['pseudo'] ."&passe=". sha1(crypt($_POST["mdp"], '')));
   			break;
   		case "personnalisation":
   			header("Location:http://l3m.in/games/personnalisation/?login&pseudo=". $_POST['pseudo'] ."&passe=". sha1(crypt($_POST["mdp"], '')));
   			break;
   		case "stg":
   			header("Location:http://l3m.in/games/stg/?login&pseudo=". $_POST['pseudo'] ."&passe=". sha1(crypt($_POST["mdp"], '')));
   			break;
   		case "argent":
   			header("Location:http://l3m.in/games/argent/?login&pseudo=". $_POST['pseudo'] ."&passe=". sha1(crypt($_POST["mdp"], '')));
   			break;
   		case "base":
   			header("Location:http://l3m.in/member.php?good=2");
   			break;
   		default:
    		header("Location:http://l3m.in/member.php?good=2");
    		break;
   	}
}


function deconnexion_membre(){
	setcookie('l3m_pseudo', '', -1);
	setcookie('l3m_mdp', '', -1);
	if(isset($_GET['redirect'])){
		redirection_deconnexion($_GET['redirect']);
	}
	else{
		redirection_deconnexion("base");
	}
}

function redirection_deconnexion($redirect){
	switch($redirect)
	{ 
    	case 'exilemap':
   			header("Location:http://l3m.in/games/exilemap/");
   		break;

   		case 'perso':
   			header("Location:http://l3m.in/games/personnalisation/");
   		break;

   		case "base":
   			header("Location:http://l3m.in/member.php?good=4");
    	break;

    	default:
    		header("Location:http://l3m.in/member.php?good=4");
    	break;
   	}
}

function reinitialiser_mail($email){
	include("fonctions/connexionbdd.php");
	$email = htmlspecialchars($email);

	if(!isset($_COOKIE['l3m_pseudo'])){
		$trouvermembre = $bdd->prepare('SELECT * FROM l3m_membres where email = :email') or die(print_r($bdd->errorInfo()));
		$trouvermembre->execute(array('email' => $email));
		if($entrees = $trouvermembre->fetch()){
			$mdp = randomot(20);
			$mdpcrypt = crypt($mdp, '');

			$changermdp = $bdd->prepare('UPDATE l3m_membres SET mdp = :mdp where email = :email') or die(print_r($bdd->errorInfo()));
			$changermdp->execute(array('mdp' => $mdpcrypt, 'email' => $email));

	    	$message = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	    	<tr><td style=\"text-align: center;\"><img src=\"http://i.imgur.com/wsOkMf0.png\" alt=\"icone l3m website\" /></td></tr></table><br />
	    	Quelqu'un a changé le mot de passe du compte ". $entrees['pseudo'] ." sur le <a href=\"http://l3m.in/\">l3m website</a> en utilisant la fonction d'oubli de mot de passe.<br />
	    	Voici le nouveau mot de passe : <b>". $mdp ."</b>";

	        $sujet = utf8_decode("Réinitialisation du mdp sur le l3m website");
	        $adresse = $email;
	        $header = "From: automatic.l3m <automatic@l3m.in>\n";
	        $header .= "Content-type: text/html; charset=iso-8859-1\n";
	        $content = "<body><html>".  utf8_decode($message) ."</body></html>";
	        mail($adresse, $sujet, $content, $header);

	        header("Location:http://l3m.in/member.php?good=6");
	    }
	}
}

function randomot($nombre){
	$mot = "";
	$chaine = "abcdefghijklmnpqrstuvwxy0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	srand((double)microtime()*1000000);

	for($i=0; $i<$nombre; $i++) {
		$mot .= $chaine[rand()%strlen($chaine)];
	}
	return $mot;
}

function changer_mdp($old, $new){
	include("fonctions/connexionbdd.php");
	$old = crypt($old, '');
	$new = crypt($new, '');

	$trouvermembre = $bdd->prepare('SELECT * FROM l3m_membres where pseudo = :pseudo and mdp = :mdp') or die(print_r($bdd->errorInfo()));
	$trouvermembre->execute(array('pseudo' => $_COOKIE['l3m_pseudo'], 'mdp' => $old));
	if($entrees = $trouvermembre->fetch()){
		$changermdp = $bdd->prepare('UPDATE l3m_membres SET mdp = :mdp where pseudo = :pseudo') or die(print_r($bdd->errorInfo()));
		$changermdp->execute(array('mdp' => $new, 'pseudo' => $_COOKIE['l3m_pseudo']));

		setcookie('l3m_pseudo', $_COOKIE['pseudo'], time() + 365*24*3600, null, null, false, true);
		setcookie('l3m_mdp', $new, time() + 365*24*3600, null, null, false, true);

		header("Location:http://l3m.in/member.php?good=7");
	}
}


// AFFICHER PROFIL / CONFIGURER PROFIL

function ecrire_profil(){
	if(!isset($_COOKIE["l3m_pseudo"])){
		?>
			<section>
				<h2><span>Profil.</span></h2>
				<p>
					Votre profil aurait été affiché ici, si vous aviez été connecté.<br />
					<i>Cette page, comme un nombre assez important des pages de ce site, n'est affichée que pour les membres connectés.</i><br />
					Donc <a href="index.php?connect">connectez-vous</a>, ou bien <a href="index.php?inscription">inscrivez-vous</a> :)
				</p>
			</section>
		<?php
	}
	else{
		?>
			<section>
				<p>
					Voici votre profil, <?php echo $_COOKIE["l3m_pseudo"]; ?>.<br />
					Vous pouvez le modifier en accédant à vos <a href="?config" title="lien paramêtres">paramètres</a>.<br />
					<i>Vous pouvez vous <a href="?disconnect" title="déconnexion">déconnecter</a>.</i>
					<?php if($_COOKIE["l3m_pseudo"] == "sodimel") { ?>
						<br /><a href="?blog">Gérer le blog</a>.
					<?php } ?>
				</p>
		<?php
			include("fonctions/connexionbdd.php");
			$infos_profil = $bdd->prepare('SELECT id FROM l3m_membres where pseudo = :pseudo and mdp = :mdp') or die(print_r($bdd->errorInfo()));
			$infos_profil->execute(array('pseudo' => $_COOKIE["l3m_pseudo"], 'mdp' => $_COOKIE["l3m_mdp"]));
			$entrees = $infos_profil->fetch();
			$infos_profil->closeCursor();

			afficher_profil($entrees['id'], "profil");

			echo "</section>";
	}
}

function ecrire_page_configuration_profil(){
	?>
		<section>
			<h2><span>Configuration du compte.</span></h2>
			<p>
				Vous pouvez modifier certaines des options de votre compte ici.
			</p>
	<?php
	if(isset($_COOKIE["l3m_pseudo"]) && isset($_COOKIE['l3m_mdp'])){
		include("fonctions/connexionbdd.php");
		$config_profil = $bdd->prepare('SELECT * FROM l3m_membres where pseudo = :pseudo and mdp = :mdp') or die(print_r($bdd->errorInfo()));
		$config_profil->execute(array('pseudo' => $_COOKIE["l3m_pseudo"], 'mdp' => $_COOKIE["l3m_mdp"]));
		$entrees = $config_profil->fetch();
		$config_profil->closeCursor();
		?>
			<form method="post" action="?configdone">
				<label for="email">Adresse email :</label> <input value="<?php echo $entrees["email"]; ?>" type="text" name="email" id="email" maxlength="42" required /> <br />
				<label for="avatar">Adresse de votre avatar  :</label> <input value="<?php echo $entrees["avatar"]; ?>" type="text" name="avatar" id="avatar" maxlength="255" required /> <br /><br />
				<p>
					<i>Attention, votre lien doit être de la forme <b>http://monsite.com/adresse.extension</b>, sinon il ne sera pas reconnu.</i>
				</p>
				<label for="site">Adresse de votre site (facultatif)  :</label> <input value="<?php echo $entrees["site"]; ?>" type="url" name="site" id="site" maxlength="255" /> <br /><br />
				<label for="genre">Genre  :</label> <input name="sexe" value="1" type="radio" <?php if($entrees["genre"] == 1){ echo "checked"; } ?>>Femme <input name="sexe" value="2" type="radio" <?php if($entrees["genre"] == 2){ echo "checked"; } ?>>Homme 
				<input name="sexe" value="0" type="radio" <?php if($entrees["genre"] == 0){ echo "checked"; } ?>>Autre <br /><br />
				Modifiez votre bio (1999 caractères autorisés, 495 affichés sur la liste des membres) :<br />
				<p>
					<?php texte_mise_en_page(); ?>
				</p>
				<textarea name="bio" maxlength="1999" class="textarea_grand"><?php echo $entrees["bio"]; ?></textarea><br /><br />
				<p class="petit">
					Actuellement, voici à quoi ressemble votre bio :
				</p>
				<p>
					<span class="preview_profil"><?php echo mise_en_page(0,nl2br(htmlspecialchars($entrees['bio'])));; ?></span>
				</p>
				<input type="submit" value="Modifier vos informations" /> <input type="reset" value="Réinitialiser" />
			</form>
			<br /><br />

			<p>
				Changement de mot de passe :
			</p>
			<form method="post" action="?changemdp">
				<label for="mdp">Ancien mot de passe :</label> <input type="password" name="oldmdp" id="mdp" maxlength="255" required /> <br /><br />
				<label for="mdp">Nouveau mot de passe :</label> <input type="password" name="newmdp" id="mdp" maxlength="255" required /> <br />
				<input type="submit" value="Changer de mot de passe" />
			</form>

			<br /><br />

			<p>
				<i>En cliquant sur ce bouton, vous pouvez supprimer votre compte. Attention, toutes les données seront perdues irrémédiablement, il n'y a pas de possibilité de récupération.</i>
			</p>
			<form method="post" action="?desinscription">
				<label for="mdp">Mot de passe :</label> <input type="password" name="mdp" id="mdp" maxlength="255" required /> <br />
				<input type="submit" value="Se désinscrire" />
			</form>
		<?php
	}
	?>
		</section>
	<?php
}

function configuration_profil_membre($email, $avatar, $site, $sexe, $bio){

	if(!preg_match("%^((https?://)|(www\.))([a-z0-9-].?)+(:[0-9]+)?(/.*)?$%i", $avatar)){
		$avatar = "http://l3m.in/img/avatar.gif";
	}
	if(!preg_match("%^((https?://)|(www\.))([a-z0-9-].?)+(:[0-9]+)?(/.*)?$%i", $site)){
		$site = "";
	}

  	$email = htmlspecialchars($email);
  	$avatar = htmlspecialchars($avatar);
  	$site = htmlspecialchars($site);
  	$sexe = htmlspecialchars($sexe);
  	$bio = mb_strimwidth($bio, 0, 1999);
  	$pseudo = $_COOKIE['l3m_pseudo'];
  	$mdp = $_COOKIE['l3m_mdp'];

  	include("fonctions/connexionbdd.php");

	$config_done = $bdd->prepare('UPDATE l3m_membres SET email = :email, avatar = :avatar, site = :site, genre = :genre, bio = :bio where pseudo = :pseudo and mdp = :mdp') or die(print_r($bdd->errorInfo()));
	$config_done->execute(array('email' => $email,
							  'avatar' => $avatar,
							  'site' => $site,
							  'genre' => $sexe,
							  'bio' => $bio,
							  'pseudo' => $pseudo,
							  'mdp' => $mdp));
	$config_done->closeCursor();
	
	header("Location:http://l3m.in/member.php?good=3");
}


// MESSAGES VALIDATION / ERREUR

function ecrire_erreur(){
	?>
	<section>
		<h2><span>Erreur.</span></h2>
		<p>
			Il y a eu une erreur. Cela peut venir de plusieurs choses : Le captcha entré est incorrect (pour le cas où vous avez entré un captcha), le pseudo est déjà pris 
			(dans le cas où vous voudriez vous inscrire), c'est la fin du monde et un missile vient de tomber sur la salle des serveurs du l3m website, un rongeur maléfique 
			vient de ronger le fil reliant le serveur au web [...].<br />
			Enfin bref, dans tous les cas y'a eu une erreur quelque part.
		</p>
	</section>
	<?php
}

function ecrire_messages($message){
	?>
		<section>
		<h2><span>Ayé, fini.</span></h2>
	<?php
	switch ($message){ 
    	case 1:
   			?>
				<p>Bien joué, t'es inscrit(e) sur le site. Maintenant faut te connecter et tu pourras configurer ton profil.</p>
				</section>
			<?php
   			break;

   		case 2:
   			?>
				<p>Bien joué, t'es connecté(e) sur le site, <?php echo $_COOKIE["l3m_pseudo"]; ?>. T'as plus qu'à l'explorer un peu maintenant. Il devrait y avoir des trucs cool plus tard.</p>
				</section>
			<?php
   			break;

   		case 3:
   			?>
				<p>Bien joué, t'as mis à jour tes infos.</p>
				</section>
			<?php
   			break;

   		case 4:
   			?>
				<p>Bien joué, t'es déconnecté(e) du site. A bientôt :)</p>
				</section>
			<?php
   			break;

   		case 5:
   			?>
				<p>Bien joué, t'es désinscrit(e). Go te réinscrire maintenant, petit galopin.</p>
				</section>
			<?php
   			break;

   		case 6:
   			?>
				<p>Bien joué, tu viens de réinitialiser ton mot de passe. Consulte vite ta boite mail.</p>
				</section>
			<?php
   			break;

   		case 7:
   			?>
				<p>Bien joué, ton mot de passe a bien été changé.</p>
				</section>
			<?php
   			break;

   		case 42:
   			?>
				<p>Bien joué, t'as trouvé <a href="http://l3m.in/member.php?piece=good">une pièce</a> secrète.</p>
				</section>
			<?php
   			break;

    	default:
    		?>
    			<p>Message d'origine inconnue, merci de contacter sodimel.</p>
    			</section>
    		<?php
    		break;
   	}
}


// VRAC (gagner pieces cachées)

function ecrire_piece(){
	?>
		<section>
			<h2><span>Gain de pièce ?</span></h2>
			<?php
			if($_GET['piece'] == 404){
				include("fonctions/connexionbdd.php");
				$verifier_piece404 = $bdd->prepare('SELECT piece_404, pieces FROM l3m_membres where pseudo = :pseudo and mdp = :mdp') or die(print_r($bdd->errorInfo()));
				$verifier_piece404->execute(array('pseudo' => $_COOKIE["l3m_pseudo"],'mdp' => $_COOKIE["l3m_mdp"]));
				$entree = $verifier_piece404->fetch();

				if ($entree['piece_404'] == 0){
					$piece404_2 = $bdd->prepare('UPDATE l3m_membres SET piece_404 = 1, pieces = :pieces, piecestotales = :piecestotales where pseudo = :pseudo and mdp = :mdp') or die(print_r($bdd->errorInfo()));
					$piece404_2->execute(array('pieces' => $entree["pieces"]+1, 'piecestotales' => $entree["piecestotales"]+1, 'pseudo' => $_COOKIE["l3m_pseudo"], 'mdp' => $_COOKIE["l3m_mdp"]));
					$piece404_2->closeCursor();
					?>
						<p>
							Bien joué, t'as cliqué sur le message de la page 404. T'as gagné une pièce.
							</p>
						</section>
					<?php
				}
				else{
					?>
						<p>
							Désolé, t'as déjà gagné ta pièce. Ouste <img src="http://l3m.in/img/smiley4.png" alt=":P" />
						</p>
						</section>
					<?php
				}
			}
			elseif($_GET['piece'] == 'good'){
				include("fonctions/connexionbdd.php");
				$verifier_piecegood = $bdd->prepare('SELECT piece_good, pieces FROM l3m_membres where pseudo = :pseudo and mdp = :mdp') or die(print_r($bdd->errorInfo()));
				$verifier_piecegood->execute(array('pseudo' => $_COOKIE["l3m_pseudo"],'mdp' => $_COOKIE["l3m_mdp"]));
				$entree = $verifier_piecegood->fetch();

				if ($entree['piece_good'] == 0){
					$piecegood_2 = $bdd->prepare('UPDATE l3m_membres SET piece_404 = 1, pieces = :pieces, piecestotales = :piecestotales where pseudo = :pseudo and mdp = :mdp') or die(print_r($bdd->errorInfo()));
					$piecegood_2->execute(array('pieces' => $entree["pieces"]+1, 'piecestotales' => $entree["piecestotales"]+1, 'pseudo' => $_COOKIE["l3m_pseudo"], 'mdp' => $_COOKIE["l3m_mdp"]));
					$piecegood_2->closeCursor();
					?>
						<p>
							Bien joué, t'as cliqué sur le message de la page good. T'as gagné une pièce.
							</p>
						</section>
					<?php
				}
				else{
					?>
						<p>
							Désolé, t'as déjà gagné ta pièce. Ouste <img src="http://l3m.in/img/smiley4.png" alt=":P" />
						</p>
						</section>
					<?php
				}
			}
			else{
				?>
				<p>
					Désolé, ce code de pièce n'est associé à aucun gain de pièce.
					</p>
				</section>
				<?php
			}
}


// LISTE DES MEMBRES

function ecrire_liste_membres(){

	 if(!isset($_COOKIE['l3m_pseudo']) && !isset($_COOKIE['l3m_mdp'])){
		?>
	 		<section>
				<h2><span>Liste des membres.</span></h2>
				<p>
					La liste des membres aurait été située ici si vous aviez été connecté.<br />
					<i>Cette page, comme un nombre assez important des pages de ce site, n'est affichée que pour les membres connectés.</i>
				</p>
			</section>
		<?php
	}

 	if(is_numeric($_GET['membres'])){
 		?>
			<section>
				<?php afficher_profil($_GET['membres'], "profil"); ?>
			</section>

 		<?php
 	}
 	else{
 		?>
			<section>
				<h2><span>Liste des membres.</span></h2>
				<p>
					Voici la liste des membres, <?php echo $_COOKIE['l3m_pseudo']; ?>.<br />
					<i>Vous pourrez modifier l'ordre d'affichage... bientôt.</i>
				</p>

		<?php 
 			include("fonctions/connexionbdd.php");
			$listemembres = $bdd->query('SELECT id FROM l3m_membres order by id DESC') or die(print_r($bdd->errorInfo()));
			while($entrees = $listemembres->fetch()){
				afficher_profil($entrees['id'], "liste");
			}
			$listemembres->closeCursor();
		?>
			</section>
		<?php
	}
}


// DEFAUT

function ecrire_page_defaut(){
	?>
		<section>
			<h2><span>Section membres.</span></h2>
			<p>
				Vous voici sur la page de la section membres.
				<?php
				if(isset($_COOKIE["l3m_pseudo"])){
					?>
					<br /><i>Vous êtes actuellement connecté, <?php echo $_COOKIE["l3m_pseudo"]; ?>.</i>
					<?php
				}
				else{
					?>
					Si ce n'est pas déjà fait, vous pouvez vous <a href="http://l3m.in/?incription">inscrire</a>.<br />
					Cette section comprend plusieurs petites choses basiques, notamment [todo].
					<?php
				}
				?>
			</p>
		</section>
	<?php
}

// GLOBAL

function afficher_profil($id, $page){
	$id = htmlspecialchars($id);

	include("fonctions/connexionbdd.php");

	$afficher_profil = $bdd->query('SELECT * FROM l3m_membres WHERE id = '. $id .'') or die(print_r($bdd->errorInfo()));
	if($entrees = $afficher_profil->fetch()){
		if($page == "liste"){
			$taille = strlen($entrees['bio']);
			if($taille > 495){
				$position = strpos($entrees['bio'], " ", 495);
				if($position == 0){
					$position = 499;
				}
			$liresuite = true;
			}
			else{
				$position = $taille;
			}
			$bio = mise_en_page(0,nl2br(htmlspecialchars(substr($entrees['bio'], 0, $position))));
		}
		else{
			$bio = mise_en_page(0,nl2br(htmlspecialchars($entrees['bio'])));
		}
		if($page == "profil"){
			?>
			<h2><span>Profil de <?php echo $entrees['pseudo']; ?>.</span></h2>
			<?php
		}
		?>
		<div class="profil" id="profil<?php echo $entrees["id"]; ?>">
			<div clas="testt">
			<div class="avatar_profil" style="background-image: url('<?php echo $entrees["avatar"]; ?>');"> </div>
			<span class="pseudo_profil">
			<a href="?membres=<?php echo $entrees['id']; ?>" title="profil de <?php echo $entrees["pseudo"]; ?>"><?php echo $entrees["pseudo"]; ?></a>
			</span><br />
			<div class="infos_profil_1">
				       <img src="http://l3m.in/img/<?php echo $entrees["genre"]; ?>.png" alt="Genre" class="image_profil" /> 
				<?php if($entrees["site"] != "") { ?>
				&bull; <a href="<?php echo $entrees["site"]; ?>">Site internet</a> 
				<?php } ?>
				&bull; Inscrit le <?php echo $entrees["date_inscription"]; ?> 
				&bull; <?php echo $entrees["pieces"]; ?> pièce<?php
				if($entrees['pieces'] > 1){ echo "s"; } ?> 
				(dont <?php echo $entrees["pieces_upppj"]; ?> via <a href="upppj.php">upppj</a>).
				<?php if($page == "profil") { ecrire_jeux($id); } ?>
			</div> <br />
			</div>
			<div class="infos_profil_2">
				<b>Bio de <?php echo $entrees["pseudo"]; ?> :</b><br />
				<p>
					<?php echo $bio;
					if($liresuite){ echo "... <a href=\"?membres=". $entrees['id'] ."\" title=\"profil de ".  $entrees["pseudo"] ."\">
					Lire la suite de la description</a>."; } ?>
				</p>
			</div>
		</div>
		<?php
		if($page == "profil"){
			?>
			<p>
				<a href="?membres#profil<?php echo $entrees["id"]; ?>">Retour à la liste des membres</a>.
			</p>
			<?php
		}
	}
}

function ecrire_jeux($id){
	include("fonctions/connexionbdd.php");

	$cherchermembreperso = $bdd->prepare('SELECT * FROM personnalisation_membres where id_l3m = :id') or die(print_r($bdd->errorInfo()));
	$cherchermembreperso->execute(array('id' => $id));
	if($entree = $cherchermembreperso->fetch()){
	?>
		<br /><span class="petit"><b>Jeux liés : </b> <a href="http://l3m.in/games/personnalisation/?visit=<?php echo $entree['nom_page']; ?>">Personnalisation</a></span>
	<?php
	}
}

function ecrire_page_jeux(){
?>
	<section>
		<h2><span>Jeux intégrés au réseau des membres l3m</span></h2>
		<p><b>&bull; <a href="http://l3m.in/games/personnalisation/">Personnalisation</a></b> : Dans la lignée des jeux demandant des clics pour permettre à l'utilisateur de jouer, sauf que là c'est pour personnaliser une page.</p>

		<p><b>&bull; <a href="http://l3m.in/p/projets/tests/pythonwars/">PythonWars</a></b> : Petit projet en développement alliant Python et php/MySQL. Pas encore disponible au public.<?php mdpPythonwars(); ?></p>
	</section>

	<section>
		<h2><span>Jeux en vrac</span></h2>
		<p>
			<b>&bull; <a href="http://l3m.in/p/projets/tests/gems/">Gems</a></b> : Le projet gems est un petit projet. Il suffit de cliquer sur les boutons pour être assigné à un camp de manière aléatoire et pour y faire gagner des "gems" (
			toujours de manière aléatoire). Un historique est présent, et une fonction de partage sur <a href="http://twinoid.com">twinoid.com</a> est également présente.<br /><br />

			<b>&bull; <a href="http://l3m.in/p/projets/tests/deplacement/">Deplacement</a></b> : Un projet en cours de réalisation (débuté en fin juillet 2015). Une aide concernant les informations relatives au projet (dans son état d'
			avancement actuel) est présente (<a href="http://l3m.in/p/projets/tests/deplacement/?aide">ici</a>), donc pas d'explications ici ! Jouez au projet, découvrez-le, faites-vous une place dans le classement !<br /><br />

			<b>&bull; <a href="http://l3m.in/p/projets/tests/autorpg/">AutoRPG</a></b> : Un projet en cours de création (débuté fin aout 2015). Vous évoluez dans un monde aléatoire, dans la peau d'un aventurier. La particularité du 
			projet ? Vous ne pouvez pas interférer avec l'aventurier. Vous vous contentez de lire les différents textes générés et de cliquer sur un bouton. Quelques personnes ont défini ce projet comme étant addictif. Je ne sais pas 
			pourquoi.
		</p>
	</section>
<?php
}

function mdpPythonwars(){
	include("fonctions/connexionbdd.php");
	$cherchermembre = $bdd->prepare('SELECT * FROM pythonwars where pseudo_l3m = :pseudo') or die(print_r($bdd->errorInfo()));
	$cherchermembre->execute(array('pseudo' => $_COOKIE['l3m_pseudo']));
	if($info = $cherchermembre->fetch()){
		echo "<br /><i>Mot de passe pour PythonWars : ". $info['mdp'] ." - <a href=\"http://l3m.in/p/projets/tests/pythonwars/?query&reset&pseudo=". $_COOKIE['l3m_pseudo'] ."&passe=". sha1($_COOKIE['l3m_mdp']) ."\">reset le mdp ?</a></i>";
	}
}

function verif_connexion_sodimel(){
	if(isset($_COOKIE['l3m_pseudo']) && isset($_COOKIE['l3m_mdp'])){
		$pseudo = htmlspecialchars($_COOKIE['l3m_pseudo']);
		$mdp = $_COOKIE['l3m_mdp'];

		include("fonctions/connexionbdd.php");
		$connexion = $bdd->prepare('SELECT * FROM l3m_membres where pseudo = :pseudo and mdp = :mdp') or die(print_r($bdd->errorInfo()));
		$connexion->execute(array('pseudo' => $pseudo, 'mdp' => $mdp));
		if ($entrees = $connexion->fetch()){
			return true;
		}
		else{
			return false;
		}
	}
	else{
		return false;
	}
}

function gestion_blog(){
	?>
		<p>
		&bull; <a href="?blog=new">Nouvel article</a>.<br />
		&bull; <a href="?blog=edit&id=1">Editer un message</a>.<br />
		&bull; <a href="?blog=delete">Supprimer un message</a>.
		</p>
	<?php
}

function contenu_nouveau_message_blog(){
	?>
		<form method="post" action="?blog=new">
				<label for="titre">Titre :</label> <input name="titre" id="titre" maxlength="42" required type="text"> <br />
				<label for="categorie">Catégorie  :</label> <input name="categorie" id="categorie" maxlength="1" required type="number"> <br />
				<span class="petit">1 : Général &bull; 2 : Codage &bull; 3 : Ecriture Web &bull; 4 : 3615MYLIFE</span><br />
				<label for="message">Message :</label> <textarea name="message" id="message">Message</textarea><br />
				<input value="Publier" type="submit"> <input value="Réinitialiser" type="reset">
		</form>
	<?php
}

function contenu_edit_message_blog($id){
	include("fonctions/connexionbdd.php");
	$editblog = $bdd->prepare('SELECT * FROM l3m_blog where id = :id') or die(print_r($bdd->errorInfo()));
	$editblog->execute(array('id' => $id));
	if ($entrees = $editblog->fetch()){
	?>
		<form method="post" action="?blog=edit&id=<?php echo $id; ?>">
				<label for="titre">Titre :</label> <input name="titre" id="titre" maxlength="42" required type="text" value="<?php echo $entrees['titre']; ?>"> <br />
				<label for="categorie">Catégorie  :</label> <input name="categorie" id="categorie" maxlength="1" required type="number" value="<?php echo $entrees['category']; ?>"> <br />
				<span class="petit">1 : Général &bull; 2 : Codage &bull; 3 : Ecriture Web &bull; 4 : 3615MYLIFE</span><br />
				<label for="message">Message :</label> <textarea name="message" id="message"><?php echo preg_replace("/<br \/>/", "", $entrees['message']); ?></textarea><br />
				<input value="Publier" type="submit"> <input value="Réinitialiser" type="reset">
		</form>
	<?php
	}
	else{
		?>
		<p>
			L'id spécifié ne correspond à aucun message du blog.
		</p>
		<?php
	}
}

function contenu_delete_message_blog(){
	?>
		Coming soon.
	<?php
}

function poster_message_blog($titre, $categorie, $message){
	include("fonctions/connexionbdd.php");
	$post_message = $bdd->prepare('INSERT INTO l3m_blog(date, titre, message, category) VALUES(:date, :titre, :message, :category)');
	$post_message->execute(array('date' => time(), "titre" => $titre, "message" => nl2br($message), "category" => $categorie));

	header("Location:http://l3m.in/member.php?blog");
}

function edit_message_blog($id, $titre, $categorie, $message){
	$id = htmlspecialchars($id);
	$titre = htmlspecialchars($titre);
	$category = htmlspecialchars($category);
	$message = htmlspecialchars($message);
	include("fonctions/connexionbdd.php");
	$editblog = $bdd->prepare('UPDATE l3m_blog SET titre = :titre, category = :category, message = :message where id = :id') or die(print_r($bdd->errorInfo()));
	$editblog->execute(array('titre' => $titre, 'category' => $categorie, 'message' => nl2br($message), 'id' => $id));	
}

function getGrade(){
	if(isset($_COOKIE['l3m_pseudo']) && isset($_COOKIE['l3m_mdp'])){
		$pseudo = htmlspecialchars($_COOKIE['l3m_pseudo']);
		$mdp = $_COOKIE['l3m_mdp'];

		include("fonctions/connexionbdd.php");
		$connexion = $bdd->prepare('SELECT grade FROM l3m_membres where pseudo = :pseudo and mdp = :mdp') or die(print_r($bdd->errorInfo()));
		$connexion->execute(array('pseudo' => $pseudo, 'mdp' => $mdp));
		if ($entrees = $connexion->fetch()){
			return $entrees['grade'];
		}
		else{
			return 0;
		}
	}
	else{
		return 0;
	}
}

?>