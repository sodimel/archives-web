<?php

	include("fonctions/fonctions_index.php");

	if(isset($_GET['envoi_email'])){ // fonction d'envoi de mail
		envoyer_email($_POST['pseudo'], $_POST['email'], $_POST['sujet'], $_POST['message']);
		header("Location:http://l3m.in/?maildone");
	}

	include("header.php");

	if(isset($_GET['verif_email'])){ // fonction de verif de mail
		ecrire_verif_email($_POST['pseudo'], $_POST['email'], $_POST['sujet'], $_POST['message']);
	}

	if(isset($_GET['maildone'])){ // si on est sur la page de remerciement de mail
		ecrire_page_email();
	}

	if(isset($_GET['don']) && $_GET['don'] != ""){ // si c'est sur la page de don et qu'il y a une interaction (genre on a fait un don ou pas)
		ecrire_reponse_don($_GET['don']);
	}

	elseif(isset($_GET['don']) && $_GET['don'] == ""){ // sinon
		ecrire_page_dons();
	}

	elseif(isset($_GET['recovery'])){ // si on est sur la page de connexion
		ecrire_page_recovery();
	}

	elseif(isset($_GET['connect'])){ // si on est sur la page de connexion
		ecrire_page_connexion();
	}

	elseif(isset($_GET['inscription'])){ // si on est sur la page d'inscription
		ecrire_page_inscription();
	}

	else{ // sinon on affiche le contenu de base :)
		ecrire_page_defaut();
	}

	include("footer.php");

?>

</body>
</html>