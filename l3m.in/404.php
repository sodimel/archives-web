<?php
if(isset($_GET['background-css-404'])){

header("Content-type: text/css; charset: UTF-8");
?>
/* http://www.alsacreations.com/tuto/lire/1620-une-video-arriere-plan-sur-toute-la-page.html */
#video-fond {
  overflow: hidden;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  opacity: 0.6;
  z-index: -1;
}
#video-fond > video {
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
}
@media (min-aspect-ratio: 16/9) {
  #video-fond > video {
	height: 300%;
	top: -100%;
  }
}
@media (max-aspect-ratio: 16/9) {
  #video-fond > video {
	width: 300%;
	left: -100%;
  }
}
@supports (object-fit: cover) {
  #video-fond > video {
	width: 100%;
	height: 100%;
	top: 0;
	left: 0;
	object-fit: cover;
  }
}



body{
	z-index: 8;
}

section, nav>a, .footer{
	background-color: rgba(227,227,227,0.8);
	box-shadow: 0 0 10px 2px #E3E3E3;
}

.footer{
	max-width: 400px;
	margin: auto;
}

i{
	color: #6C6C6C;
	opacity: 1;
}

<?php
}

else{
	include("header.php"); ?>

<div id="video-fond"><video autoplay loop><source type="video/mp4" src="img/CloudsLoop.mp4" media="(orientation: landscape)"></video></div>


			<?php if(isset($_GET['403'])) { ?>
				<section>
					<h2><span>403 - page non trouvée.</span></h2>
					<p>J'ai fait un petit nettoyage récemment, laissez-moi le temps de tout ranger :)<br />
					<i>Le code d'erreur 403 est généralement retourné lorsque le navigateur n'a pas le droit d'accéder à l'adresse indiquée.</i></p>
				</section>
			<?php } if(isset($_GET['404'])) { ?>
				<section>
					<h2><span>404 - page non trouvée.</span></h2>
					<p>J'ai fait un petit nettoyage récemment, laissez-moi le temps de tout ranger :)<br />
					<i>Le code d'erreur 404 est généralement retourné lorsque la page à laquelle le navigateur tente d'accéder n'est pas disponible.</i></p>
				</section>
			<?php } if(isset($_GET['500'])) { ?>
				<section>
					<h2><span>500 - Erreur interne.</span></h2>
					<p>J'ai fait un petit nettoyage récemment, laissez-moi le temps de tout ranger :)<br />
					<i>Le code d'erreur 500 est généralement retourné lorsque la page à laquelle le navigateur tente d'accéder est victime de son succès, ou bien que le serveur l'hébergeant a des soucis de connexion.</i></p>
				</section>
			<?php } if(isset($_GET['418'])) { ?>
				<section>
					<h2><span>418 - I am a teapot.</span></h2>
					<p><i>Le code d'erreur 418 est généralement retourné lorsque la page à laquelle le navigateur tente d'accéder est <a href="https://tools.ietf.org/html/rfc2324">une théière</a>.</i></p>
				</section>
			<?php } if(isset($_GET['dogecoin'])) { ?>
				<section>
					<h2><span>Dogecoin to the moon !</span></h2>
					<p>Hey ! Welcome here ! Please read <a href="http://www.nuew.net/doge2moon">this page</a> for more informations (thanks nuew, you've explained the dogecoinonthemoon.com's project) :)</p>
				</section>
				<section>
					<p style="font-size: 1.2em;">You can also <a href="http://l3m.in/p/doge/">check my Faucet</a> (in french) :)</p>
				</section>
			<?php } ?>






		<section>
			<p><i><?php
		$i = rand(1,18);
		switch ($i) {
		case 1:
			echo "\"Quoté sur DansTonChat. <span style=\"font-size: xx-small\">#15687 &bull; #16035</span>\"";
			break;
		case 2:
			echo "\"Je ne sais pas jongler.\"";
			break;
		case 3:
			echo "\"Je n'ai jamais appris le javascript.\"";
			break;
		case 4:
			echo "\"J'aime le piment d'espelette.\"";
			break;
		case 5:
			echo "\"La réponse à la vie, à l'univers et au reste est <b>42</b>.\"";
			break;
		case 6:
			echo "\"Le dessin c'est cool.\"";
			break;
		case 7:
			echo "\"HTML5 et CSS3, un beau mélange.\"";
			break;
		case 8:
			if(isset($_COOKIE["pseudo"])) {
			echo "\"Its dangerous to go alone, take <a href=\"member.php?piece=404\">this</a>.\"";
			}
			else {
			echo "\"Its dangerous to go alone.\"";
			}
			break;
		case 9:
			echo "\"La procrastination ça claque.\"";
			break;
		case 10:
			echo "\"Plus tard je voudrais faire ornithorynque.\"";
			break;
		case 11:
			echo "\"Mon bonnet mickey et moi nous allons bien.\"";
			break;
		case 12:
			echo "\"Les transitions c'est magique.\"";
			break;
		case 13:
			echo "\"Red Dwarf est une super série.\"";
			break;
		case 14:
			echo "\"Tu connais AutoRPG ?\"";
			break;
		case 15:
			echo "\"J'ai l'impression d'avoir ma plaque à côté de la vie.\"";
			break;
		case 16:
			echo "\"Si ton t-shirt n'est pas rentré dans ton pantalon, alors c'est ton pantalon qui est rentré dans ton t-shirt.\"";
			break;
		case 17:
			echo "\"La vie manque cruellement de raccourcis clavier.\" - <a href=\"http://www.bouletcorp.com/2007/03/04/control-z-prononcer-pomme-z-si-vous-etes-sur-mac/\">Boulet</a>, 2007.\"";
			break;
		case 18:
			echo "\"See you space cowboy...\"";
			break;

			//http://www.bouletcorp.com/2007/03/04/control-z-prononcer-pomme-z-si-vous-etes-sur-mac/
		}
			?></i></p>
		</section>

		<?php include('footer.php'); ?>

	</body>
	</html>
<?php } ?>