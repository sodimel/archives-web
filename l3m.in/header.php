<?php
include('config.php');

if ($_GET['p'] == "cookies") { // si la personne a compris les cookies :)
    setcookie('cookie', 'cookie', time() + 365*24*3600, null, null, false, true);
    header("Location:http://l3m.in/");
}

include('fonctions/fonctions_header.php');

?>

<!DOCTYPE html>
<html>
   <head>
        <meta charset="utf-8">
        <title>l3m website</title>
        <meta name="description" content="Site perso de Corentin Bettiol." />
        <link rel="stylesheet" href="design.css" />
        <?php if($_SERVER['PHP_SELF'] == "/404.php"){ echo "<link rel=\"stylesheet\" href=\"404.php?background-css-404\" />\n"; } ?>
        <meta name="viewport" content="width=device-width" />
        <link href="img/fav.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
        <link href='http://fonts.googleapis.com/css?family=Gloria+Hallelujah|Karla' rel='stylesheet' type='text/css'>
        <script src="/analytics.js" type="text/javascript"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <meta name="wot-verification" content="78d7ac5fbd1662683ec0"/> 
    </head>
<body <?php if($_SERVER['PHP_SELF'] == "/404.php"){ echo "id=\"bg404\" "; } ?>>
<div id="top"></div>

<?php ecrire_avertissement_cookie(); ?>

<div class="remonter"><a href="#top">Remonter</a></div>

<?php ecrire_connexion_inscription(); ?>

	<nav>
        <img src="img/logo_petit.png" alt="Logo_morse" class="logo" />
        <?php ecrire_menu(); ?>
	</nav>