<?php include("header.php") ?>
<div id="ecr">
<h1>RPG - 
<?php
if ($_GET['page'] == "articles")
echo"Boutique";
elseif ($_GET['page'] == "ikea")
echo"Boutique";
elseif ($_GET['page'] == "soin")
echo"Le Joyeux Hopital";
elseif ($_GET['page'] == "taverne")
echo"Qu�tes";
elseif ($_GET['achat'])
echo"Finalisation d'achat";
else
{ ?>Le village<?php } ?>
</h1>

<div id="infos">
<span class="stats"><?php echo $_COOKIE['or']; ?><img src="gold.png" /></span> 
<span class="stats"><?php echo $_COOKIE['atq']; ?><img src="sword.png" /></span> 
<span class="stats"><?php echo $_COOKIE['def']; ?><img src="shield.png" /></span> 
<span class="stats"><?php echo $_COOKIE['life']; ?><img src="life.png" /></span>
<span class="stats"><?php echo $_COOKIE['pseudo_RPG']; ?></span>
</div>

<?php
if ($_GET['page'] == "soin") /*  L'hopital  */
{
$paye = ceil($_COOKIE['or']/5);
?>
<p>Apr�s avoir demand� votre chemin � un gosse qui vous a gratifi� d'un charmant "On aime pas les �trangers par ici, tu sais ?", vous trouvez finalement un b�timent � l'air insalubre 
et � la charpente courb�e. Un pannonceau tomb� depuis belle lurette vous indique que vous vous trouvez devant "Le Joyeux Hopital". A l'entr�e, une sorte de Y�ti de sexe f�minin 
vous fait observer qu'elle est l'hotesse d'accueil et qu'elle est ici pour vous renseigner.<br />
Vous lui faites remarquer que vous voulez �tre soign�. Apr�s la fin de votre tirade elle ramasse une blouse crasseuse, l'enfile et vous fait asseoir sur une table pleine de sang.<br />
Apr�s cela elle empoigne une �norme seringue remplie de liquide vert et d�clare : "Vous payez d'avance.".<br />
<b>Que faire ?</b></p>

<div id="menu">
<?php
if ($_COOKIE['life'] < 10 && $_COOKIE['or'] > 0)
{ ?>
<a href="LAPIQURE.php" class="menu">PAYER <?php echo $paye; ?> PIECES D'OR</a>
<a href="vilage.php" class="menu">RETOURNER A L'ENTREE DU VILLAGE</a>
<?php }
else { ?>
<a href="vilage.php" class="menu">VOUS AVEZ TOUTES VOS VIES, SORTIR D'ICI</a>
<?php } ?>
</div>

<?php }
elseif ($_GET['page'] == "ikea") /*  Magasin  */
{?>
<p>Apr�s avoir fl�n� un peu du c�t� de la taverne vous vous rendez compte qu'une petite boutique miteuse la jouxte. Apr�s un regard � travers les carreaux troubles ne vous 
apportant rien mis � part un regard d�sol� d'un rat qui passe par l�, vous vous d�cidez et entrez dans le taudis inf�me.<br />
Une sonnerie aigre retentit et une sorte de momie apparut � cot� de vous. Sous votre regard m�dus� elle s'en fut se placer derri�re le comptoir et se mit � vous sourire d'un air 
un peu b�te.<br />
Vous commencez alors � fl�ner entre les rayons poussi�reux et d�couvrez quelques tr�sors qui pourraient vous aider dans vos aventures.</p>
<div id="menu"><a href="?page=articles" class="menu">AFFICHER LES ARTICLES A VENDRE</a></div>
<div id="menu"><a href="vilage.php" class="menu">POSER LES ARTICLES ET SORTIR DISCRETEMENT</a></div>
<?php }
elseif ($_GET['page'] == "articles") /*  Articles du magasin  */
{
?>
<p>Vous faites rapidement un tri parmis toutes les bricoles qui trainent, et s�lectionnez ce qui vous semble le plus important.</p>
<div id="menu">
<a href="achat.php?page=mag1" class="menu">PAIRE DE COUTELAS </a>3 PIECES D'OR (+1 ATTAQUE)
</div>
<div id="menu">
<a href="achat.php?page=mag2" class="menu">PROTECTION POUR OMOPLATE DROITE </a>5 PIECES D'OR (+1 DEFENSE)
</div>
<div id="menu">
<a href="achat.php?page=mag3" class="menu">EPEE VORPALE AVEC POIGNEE CUIR ET GARDE AMOVIBLE </a>42 PIECES D'OR (+10 ATTAQUE)
</div>
<div id="menu">
<a href="achat.php?page=mag4" class="menu">FIGURINE DE CHAT TROP CUTE </a>1500 PIECES D'OR (+666 ATTAQUE)
</div>
<div id="menu">
<a href="vilage.php" class="menu">REPOSER LES ARTICLES ET SORTIR D'ICI</a>
</div>
<?php }
elseif ($_GET['page'] == "LAPIQURE") /*  Les soins de l'hopital  */
{
?>
<p>Le soleil vient tout juste de se lever. Le grizzli en blouse vous explique qu'elle s'est tromp�e de s�rum, mais que vous �tes tout de m�me gu�ri. Elle vous assure m�me que plus 
jamais votre nez ne vous d�mangera. Vous ne voulez m�me pas savoir ce qui �tait contenu dans la seringue.<br />
Vous retournez pr�s de la place du village, l'esprit encore un peu embrum�.</p>
<?php }
elseif ($_GET['page'] == "hakuna_matata") /*  D�but de l'aventure  */
{?>

<p>Apr�s avoir err� pendant plusieurs heures dans une for�t vous aper�evez de la fum�e non loin. Vous vous approchez et vous retrouvez devant une palissade.<br />
Apr�s quelques pas h�sitants, vous voici dans le village. Vous entendez au loin les cris d'un cochon que l'on �gorge.<br />
Sympa comme ambiance.<br />
Les villageois vous regardent d'un oeil m�fiant, et vous voyez des enfants � l'air teigneux qui s'amusent � brutaliser un pauvre sac d'os qui ressemble 
vaguement � un chien. Mieux vaut faire rapidement ce que vous avez � faire puis filer d'ici au plus vite, les ravages de la consanguinit� semblent avoir fait leurs oeuvres.</p>

<?php }
elseif ($_GET['page'] == "taverne") /*  les qu�tes  */
{ ?>
<p>Apr�s avoir enjamb� les diverses bouteilles r�pandues sur le sol vous vous essuyez les pieds sur un manteau d'ivrogne. Le manteau gesticule, et vous vous d�p�chez d'entrer 
dans la taverne vers laquelle vous vous dirigiez initialement. L'ambiance � l'int�rieur est plut�t inqui�tante, mais vous trouvez finalement ce que vous cherchiez : Un type �trange 
rev�tu d'un manteau sombre vous d�viseage. Vous aper�evez des petites fiches qui tra�nent sur la table � laquelle il est accoud�, et vous en emparez d'une au hasard.<br />
L'homme n'a pas bronch�, et vous lisez une liste de qu�tes � accomplir.</p>

<div id="menu"><a href="?page=defis-marrants-et-intuitifs" class="menu">AFFICHER LA LISTE DES QUETES</a></div>
<div id="menu"><a href="vilage.php" class="menu">POSER LA FEUILLE ET RESSORTIR DE LA TAVERNE</a></div>
<?php }
elseif ($_GET['page'] == "defis-marrants-et-intuitifs") /*  Liste des qu�tes  */
{ ?>
<br /s>
<div id="menu"><a href="?page=quete1" class="menu">RAMASSER UNE PATTE DE LOUP </a> - RECOMPENSE : 5 PIECES D'OR</div>
<div id="menu"><a href="vilage.php" class="menu">REPOSER LA FEUILLE ET ALLER PRENDRE L'AIR</a></div>
<?php }
elseif ($_GET['page'] == "inventaire") /*  L'inventaire  */
{ ?>
<p>Vous vous asseyez dans un coin et faites le point sur ce que vous poss�dez.</p>
COMING SOON
<div id="menu"><a href="vilage.php" class="menu">RAMASSER VOS AFFAIRES ET RETOURNER VOUS PROMENER</a></div>
<?php }
elseif ($_GET['page'] == "sauvegarde") /*  La Sauvegarde  */
{ ?>

<p>COMING SOON<br />
Vous pouvez toutefois <a href="http://l3m.in/p/projets/rpg/">revenir � l'accueil du site</a>.</p>


<div id="menu"><a href="vilage.php" class="menu">RANGER VOTRE PLUME ET VOTRE LIVRE DANS VOTRE SAC</a></div>
<?php }
else {
?>
<p>Vous avez devant vous l'enti�ret� du village, soit environ une petite dizaine de maisons serr�es entre des remparts �pais et en sale �tat. L'avenue principale n'est qu'un passage 
boueux et plusieurs ivrognes assoupis sont blottis dans des recoins sombres et � l'aspect peu engageant.</p>
<?php } ?>

<?php
if ($_GET['page'] == "articles")
echo"";
elseif ($_GET['page'] == "ikea")
echo"";
elseif ($_GET['page'] == "soin")
echo"";
elseif ($_GET['page'] == "taverne")
echo"";
elseif ($_GET['page'] == "defis-marrants-et-intuitifs")
echo"";
elseif ($_GET['page'] == "inventaire")
echo"";
elseif ($_GET['page'] == "sauvegarde")
echo"";
else
{ ?>
<?php include('menu3.php'); ?>
<?php } ?>
</div>
</body>
</html>