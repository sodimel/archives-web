<!DOCTYPE html>
<html>
   <head>
        <title>fonction apparitiontexte_l3m</title>
        <meta charset="utf-8">
    </head>
<style>
span{
animation: apparition 15s infinite;
}

@keyframes apparition
{
0%   {
color: white;
}
30%  {
color: grey;
}
50%  {
color: black;
}
70%  {
color: grey;
}
100%{
color: white;
}
}
</style>
<body>
<p><span>Ce texte...<br /></span>
<span> apparait...</span>
<span> progressivement...</span>
<span> (et disparait aussi)</span>.</p>
</html>