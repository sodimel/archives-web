<?php
if(isset($_GET["tourner"])) {
	setcookie('tourne', 'oui', time() + 365*24*3600, null, null, false, true);
	header("Location:http://l3m.in/p/projets/pixel/");
}
elseif(isset($_GET["pastourner"])) {
	setcookie('tourne', 'oui', time() - 1, null, null, false, true);
	header("Location:http://l3m.in/p/projets/pixel/");
}


?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Symétrie de pixels.</title>
<link href='http://fonts.googleapis.com/css?family=Indie+Flower|Ubuntu' rel='stylesheet' type='text/css'>
<style type="text/css">
	body{
		width: 95%;
		max-width: 750px;
		background-color: #E5EAED;
		margin: 15px auto 15px auto;
		color: #0F2732;
		font-family: 'Indie Flower', cursive;
	}
	h1{
		background-color: #B0D9ED;
		color: #3F7590;
		text-shadow: 1px 1px 0 #10445E;
		width: 50%;
		min-width: 280px;
		border-radius: 5px;
		padding: 5px;
		margin: auto;
		text-align: center;
	}
	p{
		background-color: #ABC1CB;
		box-shadow: 0 0 3px 2px #ABC1CB;
		border-radius: 3px;
		padding: 7px;
		font-size: 1.2em;
	}
	.img, .separation{
		margin: 12px;
		width: 120px;
		height: 120px;
		background-color: #FEFEFE;
		padding: 5px;
		vertical-align: middle;
		border-radius: 3px;
	}
	.img{
		height: auto;
	}

	.img_tourne{
		<?php if(isset($_COOKIE['tourne'])) { echo "animation: logoquitourne 4s linear infinite; -webkit-animation: logoquitourne 4s infinite;"; } ?>
	}
		@keyframes logoquitourne
		{
			100% {transform: rotate(360deg);}
		}

	@webkit-keyframes logoquitourne
		{
			100% {transform: rotate(360deg);}
		}

	.separation{
		display: inline-block;
	}
	i{
		opacity: 0.5;
		font-family: 'Ubuntu', sans-serif;
	}
	.div{
		display: inline-block;
		width: 15px;
		height: 15px;
	}
</style>
</head>
<body>
<h1>Symétrie de pixels.</h1>
<p>Je me suis mis à faire des pixel-art il y a quelques temps (en début d'année 2014) lorsque j'avais des soucis de connexion et que j'avais la flemme de lancer un jeu ou bien de quitter simplement l'ordinateur. 
Aujourd'hui j'en fais toujours. Ils sont tout petits, du coup je fous aussi une version zoomée de chaque image.</p>
<p>Voici donc les images dont on parle tant depuis quelques lignes :<br />
<?php $nombre =  1; while (file_exists($nombre .'.png')) { ?>
<img class="img" src="<?php echo $nombre; ?>.png" alt="Image numéro <?php echo $nombre; ?>" />
<span class="separation"><img class="img_tourne" src="<?php echo $nombre; ?>.png" alt="Image numéro <?php echo $nombre; ?>" /><br /><i>Image <?php echo $nombre; ?></i></span><span class="div"> </span>
<? $nombre++; } ?>
</p>
<p>Vous pouvez faire tourner les images en cliquant <a href="?tourner">ici</a> <?php if(isset($_COOKIE['tourne'])) { ?>(<a href="?pastourner">arrêter de faire tourner les images</a>)<?php } ?>.</p>
<p>Ces pixels sont créés par <a href="http://l3m.in/">Corentin Bettiol</a>.</p>
</body>
</html>