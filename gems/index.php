<?php
    require '../../../../config.php'; // pour l'heure
    include 'lib.php';
    $version = 'version';
    $version = file_get_contents($version);

    verif();
    $testip = test_ip($_SERVER["REMOTE_ADDR"]);
    if($testip == true && !isset($_COOKIE['id_joueur']))
    {
        header("Location:http://l3m.in/p/projets/tests/gems/");
    }

    if(isset($_GET['definircookies']) && !isset($_COOKIE['id_joueur']) && !isset($_COOKIE['etape'])){
        definir_cookies($_GET['definircookies']);
        header("Location:http://l3m.in/p/projets/tests/gems/index.php?definirbdd");
    }

    if(isset($_GET['definirbdd']) && isset($_COOKIE['id_joueur']) && $_COOKIE['etape'] == 0){
        definir_bdd();
        header("Location:http://l3m.in/p/projets/tests/gems/");
    }

    if(isset($_GET['definircamp']) && $_COOKIE['etape'] == 1) {
        definir_camp();
        header("Location:http://l3m.in/p/projets/tests/gems/");
    }

    if(isset($_GET['definirgemmes']) && $_COOKIE['etape'] == 2 && isset($_POST['pseudo'])) {
        definir_gemmes($_POST['pseudo']);
        header("Location:http://l3m.in/p/projets/tests/gems/");
    }

    if(isset($_GET['historique']) && $_GET['historique'] == ""){
    	header("Location:http://l3m.in/p/projets/tests/gems/?historique=0");
    }

?>

<!DOCTYPE html>
<html>
   <head>
        <meta charset="utf-8">
        <title>Gems</title>
        <link rel="stylesheet" href="design.css" />
        <link href='http://fonts.googleapis.com/css?family=Indie+Flower|Abel' rel='stylesheet' type='text/css'>
        <script src="../../../../analytics.js" type="text/javascript"></script>
        <link rel="icon" href="favicon.ico" />
        <meta name="viewport" content="width=device-width" />
    </head>
<body>

<div id="page">
    <h1><img src="gems_inclinee.png" alt="[logo]" /> Gems<sup>alpha</sup></h1>

	<?php if(isset($_GET['historique']) && is_numeric($_GET['historique']) == true && $_GET['historique'] >= 0)
    {
    	echo "<p><b>Historique du registre.</b></p>";
    	ecrire_historique($_GET['historique']);
	}
    elseif(isset($_GET['log']) && $_GET['log'] != "")
    {
    ?>
        <p><b>Fonction de log</b><br />
        <?php ecrirelog($_GET['log']); ?></p>
        <p><i>Après avoir consulté les archives dans cette vieille bibliothèque, vous pouvez <a href="http://l3m.in/p/projets/tests/gems/">retourner chercher des gemmes</a> 
        sur les landes désertées du royaume.</i></p>
    <?php
    }
    else
    {

            if(!isset($_COOKIE['id_joueur']) && !isset($_COOKIE['etape']) && $testip == false)
            {
        ?>
            <p><a href="http://l3m.in/p/projets/tests/gems/index.php?definircookies=0">Commencez l'aventure Gems journalière </a> !<br />
            <i class="petit">Vous pouvez également choisir de <a href="http://l3m.in/p/projets/tests/gems/index.php?definircookies=1">cliquer ici</a> 
            pour visionner une petite pub lors de l'attribution de votre camp, ce qui permettra au webmaster de gagner quelques centimes pour payer sont hébergement web.</i><br />
            <i>Attention, vous devez accepter les cookies pour pouvoir prendre part au jeu ! Votre ip est également relevée de manière journalière, elle n'est pas utilisée à d'autres fins que de vous 
            permettre de récupérer automatiquement vos données si vous supprimez accidentellement vos cookies.</i></p>
        <?php
            } else {
                if($_COOKIE['etape'] == 1 && $testip == true)
                {
        ?>
            <p>Vous allez maintenant pouvoir découvrir à quel camp vous appartenez !<br />
            Pour ce faire, <a href="<?php if($_COOKIE['lienpub'] == 1) echo "http://adf.ly/1I4jnm"; else echo "http://l3m.in/p/projets/tests/gems/index.php?definircamp" ; ?>">cliquez ici</a>.</p>
        <?php
                }
                else {
                    if($_COOKIE['etape'] == 2 && $testip == true)
                    {
        ?>
            <form method="post" action="http://l3m.in/p/projets/tests/gems/index.php?definirgemmes">
                <p>Pour finir, indiquez votre pseudo pour le registre (laissez vide pour n'avoir qu'un id), cette action définira également combien de gemmes vous avez trouvé aujourd'hui pour 
                votre équipe (de manière aléatoire) :)<br />
                    <input type="text" name="pseudo" maxlength="20" <?php if(isset($_COOKIE['pseudo_joueur'])) echo "value=\"". $_COOKIE['pseudo_joueur'] ."\""; ?> /><br />      
                    <input class="button" type="submit" value="Ecrire son pseudo dans le registre" />.
                </p>
            </form>
        <?php
                    }
                    else
                    {
                        $lientwino = lientwino();
        ?>
            <p>Vous avez terminé vos actions quotidiennes, revenez demain pour rejouer !<br />
            
            <span class="petit">Vous pouvez dès à présent consulter les scores globaux via le registre, les statistiques journalières via cette page, et aussi partager le permalien de votre 
            action.</span></p>

            <p><?php 
            if(isset($_COOKIE['pseudo_joueur'])) { echo $_COOKIE['pseudo_joueur'] .", v" ; } else { echo "V"; } 
            ?>ous faites partie du camp <span class="camp"><?php if($_COOKIE['camp'] == 1) echo "des Blauns"; if($_COOKIE['camp'] == 2) echo "de l'armée de Tompand"; ?></span>, et vous avez gagné 
            <span class="gemmes"><?php echo $_COOKIE['gemmes']; ?> gemmes</span> pour eux.</p>

            <span class="separation"> </span>

            <p><a href="?log=<?php echo $_COOKIE['lien']; ?>">Voici le lien</a> de votre action (écrite dans le registre jusqu'à la fin des temps).<br />
            <i class="petit">Vous pouvez également partager votre score journalier sur twinoid <a href="<?php echo $lientwino; ?>">en cliquant sur ce lien</a> (cette action nécessite que vous soyez 
            connecté sur twinoid).</i><br />
            New : Vous pouvez dès à présent consulter l'historique des actions effectuées en <a href="http://l3m.in/p/projets/tests/gems/?historique">lisant la totalité du registre</a>.</p>

            <span class="separation"> </span>
            <p><b>Statistiques globales de la journée :</b><br />
            Sur <?php echo stats('all'); ?> participant<?php if(stats('all') > 1) echo "s"; ?> aujourd'hui, il y a <span class="camp"><?php echo stats('camp1'); ?> Blauns</span> 
            (qui ont récolté <span class="gemmes"><?php echo stats('gems1'); ?> gemmes</span>) et <span class="camp"><?php echo stats('camp2'); ?> membres de l'armée de Tompand</span> (qui ont récolté 
            <span class="gemmes"><?php echo stats('gems2'); ?> gemmes</span>), pour un total de <span class="gemmes"><?php echo stats('allgems'); ?> gemmes</span> récoltées.
        </p>
        <?php
                    }
                }
            }
    }

    $finphp = tempschargement();
    ?>
    <p class="petit centrer">Gems est un projet de <a href="http://l3m.in/">sodimel</a> (<a href="http://twinoid.com/user/12661">profil twinoid</a>). Version <?php echo $version; ?>. 
    Génération en <?php echo round($finphp-$debutphp, 3); ?>s.<br />
    <a href="http://twinoid.com/ev/46414372">Mises à jour</a> &bull; <a href="http://twinoid.com/ev/46414365">Commentaires</a></p>


</div>

</body>
</html>