<?php
    require '../../../../config.php'; // pour l'heure
    include 'lib.php';
    include_once('Gems.class.php');
    $version = 'version';
    $version = file_get_contents($version);

    if(!isset($_COOKIE['id_joueur']) && !isset($_COOKIE['mktime_minuit']) && !isset($_COOKIE['temps_entree']) && !isset($_COOKIE['camp']) && !isset($_COOKIE['gemmes']) && test_ip($_SERVER["REMOTE_ADDR"]) == false){
        $joueur = new Gems(0);
        $joueur->creer_cookies();
        $joueur->envoyer_bdd($_SERVER['REMOTE_ADDR']);
    }
?>

<!DOCTYPE html>
<html>
   <head>
        <meta charset="utf-8">
        <title>Gems</title>
        <link rel="stylesheet" href="design.css" />
        <link href='http://fonts.googleapis.com/css?family=Indie+Flower|Abel' rel='stylesheet' type='text/css'>
        <link rel="icon" href="favicon.ico" />
        <meta name="viewport" content="width=device-width" />
    </head>
<body>

<div id="page">
    <h1><img src="gems_inclinee.png" alt="[logo]" /> Gems<sup>alpha</sup></h1>

    <?php $joueur = new Gems ?>

    <p class="petit centrer">Gems est un projet de <a href="http://l3m.in/">sodimel</a> (<a href="http://twinoid.com/user/12661">profil twinoid</a>). Version <?php echo $version; ?>.<br />
    <a href="http://twinoid.com/ev/46414372">Mises à jour</a> &bull; <a href="http://twinoid.com/ev/46414365">Commentaires</a></p>
</div>

</body>
</html>