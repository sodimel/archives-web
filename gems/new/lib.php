<?php

function test_ip($ip){
    try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
    catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

    $chercherip = $bdd->prepare('SELECT * FROM gems_tests WHERE ip = :ip') or die(print_r($bdd->errorInfo()));
    $chercherip->execute(array('ip' => $ip));
    if ($trouver = $chercherip->fetch()) // si on trouve l'ip
    {
        if(!isset($_COOKIE['id_joueur'])){ // et si le joueur n'a pas de cookies
            $minuit = mktime(23,59,59); // timestamp de 23:59:59 du jour courant

            setcookie("mktime_minuit",$trouver['mktime_minuit'],$minuit); // on les lui refait
            setcookie("temps_entree",$trouver['temps_entree'],$minuit);
            setcookie("id_joueur",$trouver['id_joueur'],$minuit);
            setcookie("camp",$trouver['camp'],$minuit);
            setcookie("gemmes",$trouver['gemmes'],$minuit);
            setcookie("etape",2,$minuit);

            $lien = $trouver['id_joueur'] .".". $trouver['temps_entree'];
            setcookie("lien",$lien,$minuit);
        }
        return true; // et on renvoie vrai
    }
    else return false; // sinon bah on renvoie faux et la condition est validée (puisque c'est faux)
}

?>