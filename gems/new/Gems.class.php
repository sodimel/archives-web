<?php

class Gems{
	private $id_joueur;
	private $camp;
	private $gemmes;
	private $mktime_minuit;
	private $temps_entree;
	private $lien;
	private $lienpub;
	private $etape;


	public function __construct($ip){
		if($ip == 0)
		{
			$this->id_joueur = mt_rand(10000000,99999999) . substr(time(), -3);

			$this->camp = mt_rand(0,1);
			$this->gemmes = randomgemmes(mt_rand(1,101));

			$thit->mktime_minuit = mktime(23,59,59);
			$this->temps_entree = time();

			$this->lien = $id_joueur .".". $temps_entree;
			$this->lienpub = 0;

			$this->etape = 0;
		}
		else
		{
			recuperer_donnees($ip);
		}
	}

	public function creer_cookies(){
		setcookie("mktime_minuit",$this->mktime_minuit,$this->mktime_minuit);
		setcookie("temps_entree", $this->temps_entree, $this->mktime_minuit);
		setcookie("id_joueur",    $this->id_joueur,    $this->mktime_minuit);
		setcookie("camp",         $this->camp,         $this->mktime_minuit);
		setcookie("gemmes",       $this->gemmes,       $this->mktime_minuit);
   		setcookie("lien",         $this->lien,         $this->mktime_minuit);
	}

	public function envoyer_bdd($ip){
		try { $bdd = new PDO('mysql:host=l3m.in.mysql;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
    	catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

    	$inscriptionbdd = $bdd->prepare('INSERT INTO gems_tests(id_joueur, camp, gemmes, mktime_minuit, temps_entree, ip) VALUES(:id_joueur, :camp, :gemmes, :mktime_minuit, :temps_entree, :ip)');
	    $inscriptionbdd->execute(array('id_joueur' => $this->id_joueur,
		    					 	   'camp' => $this->camp,
		    						   'gemmes' => $this->gemmes,
		   							   'mktime_minuit' => $this->mktime_minuit,
		 							   'temps_entree' => $this->temps_entree,
		 							   'ip' => $ip));
	    $inscriptionbdd->closeCursor();

	    $inscriptionlog = $bdd->prepare('INSERT INTO gems_tests_logs(id_joueur, time_joueur, camp, gemmes) VALUES(:id_joueur, :time_joueur, :camp, :gemmes)');
	    $inscriptionlog->execute(array('id_joueur' => $this->id_joueur,
	    							   'time_joueur' => $this->temps_entree,
		    					 	   'camp' => $this->camp,
		    						   'gemmes' => $this->gemmes));
	    $inscriptionlog->closeCursor();

	    $this->etape = 1;
	}

	public function recuperer_donnees($ip){
		
	}

	public function randomgemmes($i){
		if($i>=1 && $i<=10){
			return 1;
		}
		if($i>=11 && $i<=20){
			return 2;
		}
		if($i>=21 && $i<=30){
			return 3;
		}
		if($i>=31 && $i<=40){
			return 4;
		}
		if($i>=41 && $i<=50){
			return 5;
		}
		if($i>=51 && $i<=60){
			return 6;
		}
		if($i>=61 && $i<=70){
			return 7;
		}
		if($i>=71 && $i<=80){
			return 8;
		}
		if($i>=81 && $i<=90){
			return 9;
		}
		if($i>=91 && $i<=100){
			return 10;
		}
		if($i==101){
			return 15;
		}
	}


}

?>