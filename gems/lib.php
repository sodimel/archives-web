<?php

function tempschargement()
{
	list($usec, $sec) = explode(" ",microtime());
    return ((float)$usec + (float)$sec);
}
$debutphp = tempschargement();

function verif(){
	$temps_minuit_hier = mktime(0,0,0);
	try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
    catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
    $champ = $bdd->query('SELECT temps_entree FROM gems_tests WHERE id = 1');
	if ($verifjour = $champ->fetch()) {
		$heurebdd = $verifjour['temps_entree'];
	}
	if($heurebdd<=$temps_minuit_hier){
		$delete = $bdd->prepare('TRUNCATE TABLE `gems_tests`');
		$delete->execute();
	}
}

function definir_cookies($lienpub){

	$id_joueur = mt_rand(10000000,99999999) . substr(time(), -3); // aléatoire : 8 chiffres + 3 derniers nombres du timestamp
	$minuit = mktime(23,59,59); // timestamp de 23:59:59 du jour courant
	$camp = mt_rand(1,2); // le camp c'est de l'aléatoire
	$gemmes = randomgemmes(mt_rand(1,101)); // les gemmes aussi c'est de l'aléatoire :)
	
	// $secondes_minuit = $minuit - time(); // secondes restant jusqu'à minuit

	if($lienpub == 1) // si les gens sont gentils ou bien s'ils ne le sont pas :°
	{
		setcookie("lienpub",1,$minuit); // gentils <3
	}
	elseif($lienpub == 0)
	{
		setcookie("lienpub",0,$minuit); // pressés/pas de pub :c
	}
	setcookie("mktime_minuit",$minuit,$minuit);
	setcookie("temps_entree",time(),$minuit);
	setcookie("id_joueur",$id_joueur,$minuit);
	setcookie("camp",$camp,$minuit);
	setcookie("gemmes",$gemmes,$minuit);
	setcookie("etape",0,$minuit);

}

function definir_bdd(){

	// fonction du jeu

	try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
	catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
	$inscriptionbdd = $bdd->prepare('INSERT INTO gems_tests(id_joueur, camp, gemmes, mktime_minuit, temps_entree, ip) VALUES(:id_joueur, :camp, :gemmes, :mktime_minuit, :temps_entree, :ip)');
    $inscriptionbdd->execute(array('id_joueur' => $_COOKIE['id_joueur'],
	    					 	   'camp' => $_COOKIE['camp'],
	    						   'gemmes' => $_COOKIE['gemmes'],
	   							   'mktime_minuit' => $_COOKIE['mktime_minuit'],
	 							   'temps_entree' => $_COOKIE['temps_entree'],
	 							   'ip' => $_SERVER['REMOTE_ADDR']));
    $inscriptionbdd->closeCursor();

    // fonction de log dans la bdd de logs

    $inscriptionlog = $bdd->prepare('INSERT INTO gems_tests_logs(id_joueur, time_joueur, camp, gemmes) VALUES(:id_joueur, :time_joueur, :camp, :gemmes)');
    $inscriptionlog->execute(array('id_joueur' => $_COOKIE['id_joueur'],
    							   'time_joueur' => $_COOKIE['temps_entree'],
	    					 	   'camp' => $_COOKIE['camp'],
	    						   'gemmes' => $_COOKIE['gemmes']));
    $inscriptionlog->closeCursor();

    // sauvegarde (sur le temps court) du lien vers l'action vers la bdd de log

    $lien = $_COOKIE['id_joueur'] .".". $_COOKIE['temps_entree'];
    setcookie("lien",$lien,mktime(23,59,59));

    setcookie("etape",1,$minuit);

}

function definir_camp(){

	try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
    catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
	$modifcamp = $bdd->prepare('UPDATE gems_tests SET camp = :camp where id_joueur = :id_joueur AND temps_entree = :temps_entree') or die(print_r($bdd->errorInfo()));
    $modifcamp->execute(array('camp' => $_COOKIE['camp'],
    						  'id_joueur' => $_COOKIE['id_joueur'],
    						  'temps_entree' => $_COOKIE['temps_entree']));
    $modifcamp->closeCursor();
    setcookie("etape",2,$minuit);

}

function definir_gemmes($pseudo){

	if(strlen($pseudo) > 20){ // on évite que des malins foutent des pseudos de + de 20 caractères
		$pseudo = substr($pseudo, 0, 20);
	}
	$pseudo = str_replace("'", ".", htmlspecialchars($pseudo));
	if(mb_strtolower($pseudo) == "sodimel"){
		$pseudo = "lemidos";
	}
	if(mb_strtolower($pseudo) == "sodimel42"){
		$pseudo = "sodimel";
	}

	try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
    catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
	$modifgemmes = $bdd->prepare('UPDATE gems_tests SET gemmes = :gemmes where id_joueur = :id_joueur AND temps_entree = :temps_entree') or die(print_r($bdd->errorInfo()));
    $modifgemmes->execute(array('gemmes' => $_COOKIE['gemmes'],
    						    'id_joueur' => $_COOKIE['id_joueur'],
    						    'temps_entree' => $_COOKIE['temps_entree']));
    $modifgemmes->closeCursor();

	$ajouterpseudo = $bdd->prepare('UPDATE gems_tests_logs SET pseudo_joueur = :pseudo_joueur where id_joueur = :id_joueur AND time_joueur = :time_joueur') or die(print_r($bdd->errorInfo()));
    $ajouterpseudo->execute(array('pseudo_joueur' => $pseudo,
    						    'id_joueur' => $_COOKIE['id_joueur'],
    						    'time_joueur' => $_COOKIE['temps_entree']));
    $ajouterpseudo->closeCursor();
    setcookie("pseudo_joueur",$pseudo,time()+31556926); // le cookie de pseudo enregistré pendant un an

    setcookie("etape",3,mktime(23,59,59));

}

function randomgemmes($i){ // aléatoire du nombre de gemmes gagnées par la personne.

	if($i>=1 && $i<=10){
		return 1;
	}
	if($i>=11 && $i<=20){
		return 2;
	}
	if($i>=21 && $i<=30){
		return 3;
	}
	if($i>=31 && $i<=40){
		return 4;
	}
	if($i>=41 && $i<=50){
		return 5;
	}
	if($i>=51 && $i<=60){
		return 6;
	}
	if($i>=61 && $i<=70){
		return 7;
	}
	if($i>=71 && $i<=80){
		return 8;
	}
	if($i>=81 && $i<=90){
		return 9;
	}
	if($i>=91 && $i<=100){
		return 10;
	}
	if($i==101){
		return 15;
	}

}

function stats($entree){ // les stats en bas de page :P

	try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
    catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

    if($entree == "all")
    {
    	$chercher_nombre_participants = $bdd->query('SELECT COUNT(id) FROM gems_tests');
	    $nombre_participants = $chercher_nombre_participants->fetchColumn();
	    return $nombre_participants;
    }

    if($entree == "camp1")
    {
		$chercher_nombre_participants_1 = $bdd->query('SELECT COUNT(id) FROM gems_tests WHERE camp = 1');
	    $nombre_participants_1 = $chercher_nombre_participants_1->fetchColumn();
	    return $nombre_participants_1;
    }

    if($entree == "camp2")
    {
		$chercher_nombre_participants_2 = $bdd->query('SELECT COUNT(id) FROM gems_tests WHERE camp = 2');
    	$nombre_participants_2 = $chercher_nombre_participants_2->fetchColumn();
    	return $nombre_participants_2;
    }

    if($entree == "gems1")
    {
		$chercher_gemmes_1 = $bdd->query('SELECT SUM(gemmes) as gemmes1 FROM gems_tests WHERE camp = 1');
		$trouver_gemmes_1 = $chercher_gemmes_1->fetch();
		if ($trouver_gemmes_1['gemmes1'] >= 1) return $trouver_gemmes_1['gemmes1'];
		else return 0;
    }

    if($entree == "gems2")
    {
		$chercher_gemmes_2 = $bdd->query('SELECT SUM(gemmes) as gemmes2 FROM gems_tests WHERE camp = 2');
		$trouver_gemmes_2 = $chercher_gemmes_2->fetch();
		if ($trouver_gemmes_2['gemmes2'] >= 1) return $trouver_gemmes_2['gemmes2'];
		else return 0;
    }

    if($entree == "allgems")
    {
		$chercher_gemmes = $bdd->query('SELECT SUM(gemmes) as gemmes3 FROM gems_tests');
		$trouver_gemmes = $chercher_gemmes->fetch();
		if ($trouver_gemmes['gemmes3'] >= 1) return $trouver_gemmes['gemmes3'];
		else return 0;
    }
}

function test_ip($adresse_ip){ // on cherche à savoir si le type a son ip de présente dans la bdd

	try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
    catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

    $chercherip = $bdd->prepare('SELECT * FROM gems_tests WHERE ip = :ip') or die(print_r($bdd->errorInfo()));
    $chercherip->execute(array('ip' => $adresse_ip));
    if ($trouver = $chercherip->fetch()) // si on trouve l'ip
    {
    	if(!isset($_COOKIE['id_joueur'])){ // et si le joueur n'a pas de cookies
	    	$minuit = mktime(23,59,59); // timestamp de 23:59:59 du jour courant

	    	setcookie("mktime_minuit",$trouver['mktime_minuit'],$minuit); // on les lui refait
			setcookie("temps_entree",$trouver['temps_entree'],$minuit);
			setcookie("id_joueur",$trouver['id_joueur'],$minuit);
			setcookie("camp",$trouver['camp'],$minuit);
			setcookie("gemmes",$trouver['gemmes'],$minuit);
			setcookie("etape",3,$minuit);

			$lien = $trouver['id_joueur'] .".". $trouver['temps_entree'];
    		setcookie("lien",$lien,$minuit);
		}
    	return true; // et on renvoie vrai
    }
    else return false; // sinon bah on renvoie faux et la condition est validée (puisque c'est faux)

}

function ecrirelog($log){

	$log2 = str_replace("'", "BITELOLENCORERATE", htmlspecialchars($log));
	if($log != $log2)
	{
		echo "<i>Votre tentative pour modifier le registre a échoué.</i><br />";
	}
	$donnees = explode (".", $log2);

	try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
    catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

    $chercherip = $bdd->prepare('SELECT * FROM gems_tests_logs WHERE id_joueur = :id_joueur AND time_joueur = :time_joueur') or die(print_r($bdd->errorInfo()));
    $chercherip->execute(array('id_joueur' => $donnees[0],
    						   'time_joueur' => $donnees[1]));
    if ($trouver = $chercherip->fetch()) // si on trouve l'id
    {
    	if($trouver['pseudo_joueur'] != "")
    	{
    		echo $trouver['pseudo_joueur'] ." (id ". $donnees[0] .")";
    	}
    	else
    	{
    		echo "L'id ". $donnees[0];
    	}
    	echo " a ramassé <span class=\"gemmes\">". $trouver['gemmes'] ." gemmes</span> pour le camp ";
    	if($trouver['camp'] == 1)
    	{
    		echo "des <span class=\"camp\">Blauns</span>, le ". date('d/m/Y', $trouver['time_joueur']) ." aux alentours de ". date('H', $trouver['time_joueur']);
    		if(date('H', time()) == 1) echo " heure";
			elseif(date('H', time()) == 0) echo " (minuit)";
			else echo " heures";
			 
			if(date('H', time()) < 12 && date('H', time()) > 0) echo " du matin.";
			else echo ".";
    	}
    	else
    	{
    		echo "de <span class=\"camp\">l'armée de Tompand</span>, le ". date('d/m/Y', $trouver['time_joueur']) ." aux alentours de ". date('H', $trouver['time_joueur']);
    		if(date('H', time()) == 1) echo " heure";
			elseif(date('H', time()) == 0) echo " (minuit)";
			else echo " heures";
			 
			if(date('H', time()) < 12 && date('H', time()) > 0) echo " du matin.";
			else echo ".";
    	}
    }
    else
    {
    	echo "L'événement sélectionné est illisible, peut-être vous êtes vous trompé de page en feuilletant le registre.";
	}

}

function lientwino(){
	$lien = "http://twinoid.com/fr/tid/nexus?t=";
	$contenulien = "Je viens de trouver ". $_COOKIE['gemmes'] ." gemmes pour ";
	if($_COOKIE['camp'] == 1)
	{
		$contenulien = $contenulien . "le camp des Blauns sur [link=http://l3m.in/p/projets/tests/gems/?log=". $_COOKIE['lien'] ."]le projet Gems[/link]";
	}
	else
	{
		$contenulien = $contenulien . "l'armée de Tompand sur [link=http://l3m.in/p/projets/tests/gems/?log=". $_COOKIE['lien'] ."]le projet Gems[/link]";
	}
	if($_COOKIE['gemmes'] < 4)
	{
		$contenulien = $contenulien . " :calim:";
	}
	elseif($_COOKIE['gemmes'] > 6)
	{
		$contenulien = $contenulien . " :youpi:";
	}
	else
	{
		$contenulien = $contenulien . " :sq_surprised:";
	}
	$contenulien = $contenulien . "
[aparte]N'hesitez pas à [link=http://l3m.in/p/projets/tests/gems/]jouer[/link] vous aussi :)[/aparte]";
	$lien = $lien . urlencode($contenulien);
	return $lien;
}

function ecrire_historique($page){

	$temps_actuel = time() - ($page*86400);
	if($temps_actuel < strtotime('30-05-2015')){
		$page = 1;
	}

	$tempsverif = time() - (($page+1)*86400);
	$pageplus = $page + 1;
	$pagemoins = $page -1;

	if($page > 0){
		echo "<p style=\"text-align: center;\"><a href=\"http://l3m.in/p/projets/tests/gems/?historique=". $pagemoins ."\">Page suivante</a> &bull; ";
	}
	else{
		echo "<p style=\"text-align: center;\"><span style=\"color: #515151;\">Page suivante</span> &bull; ";
	}
	if($tempsverif < strtotime('30-05-2015')){
		echo "<span style=\"color: #515151;\">Page précédente</span></p>";
	}
	else{
		echo "<a href=\"http://l3m.in/p/projets/tests/gems/?historique=". $pageplus ."\">Page précédente</a></p>";
	}
	echo "<span class=\"separation\"> </span>
	<p>Voici les entrées dans le registre pour le ". date('d/m/Y', $temps_actuel) ." :</p>
	<p>";


	try { $bdd = new PDO('mysql:host=;dbname=', '', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); }
    catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
    	$chercher_nombre_participants_total = $bdd->query('SELECT COUNT(id) FROM gems_tests_logs');
	    $nombre_participants_total = $chercher_nombre_participants_total->fetchColumn();

	    $temps_minuit = mktime(23,59,59);
	    $temps_minuit = $temps_minuit - ($page*86400);
	    $temps_initial = strtotime('30-05-2015');
	    $temps_hier = $temps_minuit - 86400;
	    $j = 0;
	    while($temps_actuel > $temps_hier){

	    	$chercher_participants_jour = $bdd->prepare('SELECT * FROM gems_tests_logs WHERE time_joueur < :temps_actuel AND time_joueur > :time_joueur_hier');
	    	$chercher_participants_jour->execute(array('temps_actuel' => $temps_minuit,
    						   						   'time_joueur_hier' => $temps_hier));
	    	while($donnees = $chercher_participants_jour->fetch())
	    	{
	    		$i = 1;
	    		$j++;
	    		echo "<span class=\"historique\">&bull; ";
	    		if($donnees['pseudo_joueur'] != ""){
	    			echo $donnees['pseudo_joueur'] ." (<a href=\"http://l3m.in/p/projets/tests/gems/?log=". $donnees['id_joueur'] .".". $donnees['time_joueur'] 
	    			."\">id #". $donnees['id_joueur'] ."</a>)";
	    		}
	    		else{
	    			echo "L'id <a href=\"http://l3m.in/p/projets/tests/gems/?log=". $donnees['id_joueur'] .".". $donnees['time_joueur'] 
	    			."\">#". $donnees['id_joueur'] ."</a>";
	    		}
	    		echo " a récolté <span class=\"gemmes\">". $donnees['gemmes'] ." gemmes</span> le "
					. date('d/m/Y',$donnees['time_joueur']) ." vers ". date('H',$donnees['time_joueur']) ." heures pour <span class=\"camp\">";
	    		if($donnees['camp'] == 1){
	    			echo "le camp des Blauns</span>.<br />";
	    		}
	    		else{
	    			echo "l'armée de Tompand</span>.<br />";
	    		}
	    	}
	    	if($i == 0){
	    		echo "<span class=\"historique\">&bull; Il n'y a aucune entrée pour le ". date('d/m/Y', $temps_actuel) .".</span>";
	    	}
	    	$i = 0;
	    	echo "<p class=\"petit\">Il y a ". $j ." entrées dans le registre pour le ". date('d/m/Y', $temps_actuel) .", et 
	    	". $nombre_participants_total ." entrées au total.</p>";
	    	$temps_actuel -= 86400;
	    }

	echo "</p>
	<span class=\"separation\"></span><p>";
	echo "Après avoir usé vos yeux de longues minutes à déchiffrer le registre, vous pouvez <a href=\"http://l3m.in/p/projets/tests/gems/\">retourner à l'accueil</a>.</p>";

}

?>